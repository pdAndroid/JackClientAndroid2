package com.party.jackclientandroid.event;

/**
 * Created by Administrator on 2018/10/3.
 */

public class RefundEvent {
    private boolean result;

    public RefundEvent(boolean result) {
        this.result = result;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }
}
