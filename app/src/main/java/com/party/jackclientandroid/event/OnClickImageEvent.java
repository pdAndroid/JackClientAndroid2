package com.party.jackclientandroid.event;

import com.party.jackclientandroid.bean.ImageBean;

import java.util.ArrayList;

/**
 * Created by 派对 on 2018/12/6.
 */

public interface OnClickImageEvent {

    void onClick(boolean isAdded, int itemPosition, int position, String url, ArrayList<ImageBean> urlList);
}
