package com.party.jackclientandroid.event;

/**
 * Created by Administrator on 2018/9/13.
 */

public class MessageEvent {

    private int onceCount;
    private double originalPrice;
    private long couponId;

    public long getCouponId() {
        return couponId;
    }

    public void setCouponId(long couponId) {
        this.couponId = couponId;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    @Override
    public String toString() {
        return "MessageEvent{" +
                "onceCount=" + onceCount +
                ", originalPrice=" + originalPrice +
                '}';
    }
}
