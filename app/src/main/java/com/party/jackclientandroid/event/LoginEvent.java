package com.party.jackclientandroid.event;

/**
 * Created by Administrator on 2018/10/2.
 */

public class LoginEvent {
    private boolean loginRtn;

    public LoginEvent(boolean loginRtn) {
        this.loginRtn = loginRtn;
    }

    public boolean isLoginRtn() {
        return loginRtn;
    }

    public void setLoginRtn(boolean loginRtn) {
        this.loginRtn = loginRtn;
    }
}
