package com.party.jackclientandroid.event;

/**
 * 支付宝和微信的支付回调
 */
public class AuthResultEvent {

    String code;

    int EventType;

    public int getEventType() {
        return EventType;
    }

    public void setEventType(int eventType) {
        EventType = eventType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public AuthResultEvent(String code) {
        this.code = code;
    }
}
