package com.party.jackclientandroid.event;

/**
 * Created by Administrator on 2018/10/19.
 */

public class NeighbourSearchEvent {
    private String searchKey;

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }
}
