package com.party.jackclientandroid.event;

/**
 * Created by Administrator on 2018/10/1.
 */

public class ShopSearchEvent {
    private String searchKey;
    private int shopTypeId;//附近模块中店铺类型id

    public String getSearchKey() {
        return searchKey;
    }

    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public int getShopTypeId() {
        return shopTypeId;
    }

    public void setShopTypeId(int shopTypeId) {
        this.shopTypeId = shopTypeId;
    }
}
