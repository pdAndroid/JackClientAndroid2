package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/8/27.
 */

public class MyAttention {

    /**
     * averageCons : 50
     * avgScore : 7
     * enviroScore : 7
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * id : 1530523930811
     * serviceScore : 7
     * shopName : 寇芊芊的包子店
     * shopPhone : 124578824965
     * symbolBuild : 春熙路
     * tasteScore : 7
     */

    private double averageCons;
    private int avgScore;
    private int enviroScore;
    private String homeImg;
    private String id;
    private int serviceScore;
    private String shopName;
    private String shopPhone;
    private String symbolBuild;
    private int tasteScore;
    private long create;
    private boolean isAttention = true;

    public boolean isAttention() {
        return isAttention;
    }

    public void setAttention(boolean attention) {
        isAttention = attention;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public double getAverageCons() {
        return averageCons;
    }

    public void setAverageCons(double averageCons) {
        this.averageCons = averageCons;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }
}
