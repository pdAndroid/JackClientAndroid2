package com.party.jackclientandroid.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/8/9.
 */

public class GroupBuyCouponDetail implements Serializable{

    /**
     * appointment : 1
     * availableDate : 8,9
     * buyPersonLimit : 10
     * buyPrice : 300
     * dateEnd : 2018-07-04
     * dateStart : 2018-07-04
     * dinersNumberName : 单人餐
     * groupBuyImg : https://pdkj.oss-cn-beijing.aliyuncs.com/user/1/group_buy/1531275614261.png
     * id : 0
     * onceCount : 1
     * originalPrice : 500
     * ruleContent : 请咨询商家
     * shopAddress : 四川省成都市温江区海川路
     * shopId : 1530523930811
     * shopName : 寇芊芊的包子店
     * shopPhone : 124578824965
     * timeEnd : 11:22:21
     * timeStart : 11:22:17
     * title : 二十五元团购卷
     */

    private int appointment;
    private String availableDate;
    private int buyPersonLimit;
    private String buyPrice;
    private String ruleSplicing;
    private String dinersNumberName;
    private String groupBuyImg;
    private long id;
    private int onceCount;
    private String originalPrice;
    private String ruleContent;
    private String shopAddress;
    private long shopId;
    private String shopName;
    private String shopPhone;
    private long timeEnd;
    private long timeStart;
    private long dateEnd;
    private long dateStart;
    private String title;

    public String getRuleSplicing() {
        return ruleSplicing;
    }

    public void setRuleSplicing(String ruleSplicing) {
        this.ruleSplicing = ruleSplicing;
    }

    public int getAppointment() {
        return appointment;
    }

    public void setAppointment(int appointment) {
        this.appointment = appointment;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getDinersNumberName() {
        return dinersNumberName;
    }

    public void setDinersNumberName(String dinersNumberName) {
        this.dinersNumberName = dinersNumberName;
    }

    public String getGroupBuyImg() {
        return groupBuyImg;
    }

    public void setGroupBuyImg(String groupBuyImg) {
        this.groupBuyImg = groupBuyImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "GroupBuyCouponDetail{" +
                "appointment=" + appointment +
                ", availableDate='" + availableDate + '\'' +
                ", buyPersonLimit=" + buyPersonLimit +
                ", buyPrice=" + buyPrice +
                ", dateEnd='" + dateEnd + '\'' +
                ", dateStart='" + dateStart + '\'' +
                ", dinersNumberName='" + dinersNumberName + '\'' +
                ", groupBuyImg='" + groupBuyImg + '\'' +
                ", id=" + id +
                ", onceCount=" + onceCount +
                ", originalPrice=" + originalPrice +
                ", ruleContent='" + ruleContent + '\'' +
                ", shopAddress='" + shopAddress + '\'' +
                ", shopId=" + shopId +
                ", shopName='" + shopName + '\'' +
                ", shopPhone='" + shopPhone + '\'' +
                ", timeEnd='" + timeEnd + '\'' +
                ", timeStart='" + timeStart + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
