package com.party.jackclientandroid.bean;

/**
 * 下拉菜单筛选实体<br>
 */
public class PopupFilterBean {
    private String name;
    private String groupName;
    private transient boolean isChecked;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
