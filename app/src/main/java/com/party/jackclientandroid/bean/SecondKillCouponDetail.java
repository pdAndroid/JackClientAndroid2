package com.party.jackclientandroid.bean;

import com.party.jackclientandroid.okhttp.HttpLogger;

/**
 * Created by Administrator on 2018/9/27.
 */

public class SecondKillCouponDetail {

    /**
     * appointment : 1
     * availableDate : 1,2,3,4,5,6,7
     * buyPersonLimit : 1
     * buyPrice : 82
     * dateEnd : 1537286400000
     * dateStart : 1537286400000
     * endSaleTime : 1536224919000
     * goodsRange : 2
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * id : 1
     * onceCount : 1
     * originalPrice : 90
     * ruleContent : test
     * shopAddress : 四川省成都市温江区海川路
     * shopId : 1530523930811
     * shopName : 寇芊芊的包子店
     * shopPhone : 124578824965
     * startSaleTime : 1538062562000
     * stockCount : 10
     * symbolBuild : 融信广场
     * timeEnd : 32400000
     * timeStart : -28800000
     */

    private int appointment;
    private String availableDate;
    private int buyPersonLimit;
    private String buyPrice;
    private long dateEnd;
    private long dateStart;
    private long endSaleTime;
    private String goodsRange;
    private String homeImg;
    private long id;
    private int onceCount;
    private String originalPrice;
    private String ruleContent;
    private String shopAddress;
    private long shopId;
    private String shopName;
    private String shopPhone;
    private long startSaleTime;
    private int stockCount;
    private String symbolBuild;
    private long timeEnd;
    private long timeStart;
    private String ruleSplicing;


    public String getRuleSplicing() {
        return ruleSplicing;
    }

    public void setRuleSplicing(String ruleSplicing) {
        this.ruleSplicing = ruleSplicing;
    }

    public String getAvailableDate() {
        return availableDate;
    }


    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getEndSaleTime() {
        return endSaleTime;
    }

    public void setEndSaleTime(long endSaleTime) {
        this.endSaleTime = endSaleTime;
    }

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public long getStartSaleTime() {
        return startSaleTime;
    }

    public void setStartSaleTime(long startSaleTime) {
        this.startSaleTime = startSaleTime;
    }

    public int getStockCount() {
        return stockCount;
    }

    public void setStockCount(int stockCount) {
        this.stockCount = stockCount;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }
}
