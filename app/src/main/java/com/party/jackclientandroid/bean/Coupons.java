package com.party.jackclientandroid.bean;

/**
 * Created by 派对 on 2018/12/3.
 */

public interface Coupons {
    long getId();

    void setId(long id);

    String getTitle();

    double getBuyPrice();

    void setBuyPrice(double buyPrice);
}
