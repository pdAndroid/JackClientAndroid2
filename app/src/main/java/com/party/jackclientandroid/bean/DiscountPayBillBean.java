package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.party.jackclientandroid.okhttp.HttpLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 2018/9/6.
 */

/**
 */
public class DiscountPayBillBean implements DisplayableItem, Parcelable {

    @Expose
    @SerializedName("id")
    private long shopId;
    private String discount;
    private String shopName;
    private int isUseDiscount;
    private String shopRebateMoney;
    private String xiapinUseDiscount;
    private int isUseXiapinCoin;
    private String totalComsumeNumber;
    private String maxUseXiapinMoney;
    private ArrayList<CouponBean> coupons;

    public DiscountPayBillBean(long id, String shopRebateMoney, String discount, String totalComsumeNumber) {
        this.shopId = id;
        this.shopRebateMoney = shopRebateMoney;
        this.discount = discount;
        this.totalComsumeNumber = totalComsumeNumber;
    }


    protected DiscountPayBillBean(Parcel in) {
        shopId = in.readLong();
        discount = in.readString();
        shopName = in.readString();
        isUseDiscount = in.readInt();
        shopRebateMoney = in.readString();
        coupons = in.createTypedArrayList(CouponBean.CREATOR);
    }

    public static final Creator<DiscountPayBillBean> CREATOR = new Creator<DiscountPayBillBean>() {
        @Override
        public DiscountPayBillBean createFromParcel(Parcel in) {
            return new DiscountPayBillBean(in);
        }

        @Override
        public DiscountPayBillBean[] newArray(int size) {
            return new DiscountPayBillBean[size];
        }
    };

    public String getXiapinUseDiscount() {
        return xiapinUseDiscount;
    }

    public void setXiapinUseDiscount(String xiapinUseDiscount) {
        this.xiapinUseDiscount = xiapinUseDiscount;
    }

    public String getTotalComsumeNumber() {
        return totalComsumeNumber;
    }

    public void setTotalComsumeNumber(String totalComsumeNumber) {
        this.totalComsumeNumber = totalComsumeNumber;
    }

    public String getMaxUseXiapinMoney() {
        return maxUseXiapinMoney;
    }

    public void setMaxUseXiapinMoney(String maxUseXiapinMoney) {
        this.maxUseXiapinMoney = maxUseXiapinMoney;
    }

    public int getIsUseXiapinCoin() {
        return isUseXiapinCoin;
    }

    public void setIsUseXiapinCoin(int isUseXiapinCoin) {
        this.isUseXiapinCoin = isUseXiapinCoin;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getIsUseDiscount() {
        return isUseDiscount;
    }

    public void setIsUseDiscount(int isUseDiscount) {
        this.isUseDiscount = isUseDiscount;
    }

    public String getShopRebateMoney() {
        return shopRebateMoney;
    }

    public void setShopRebateMoney(String shopRebateMoney) {
        this.shopRebateMoney = shopRebateMoney;
    }

    public ArrayList<CouponBean> getCoupons() {
        return coupons;
    }

    public void setCoupons(ArrayList<CouponBean> coupons) {
        this.coupons = coupons;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(shopId);
        dest.writeString(discount);
        dest.writeString(shopName);
        dest.writeInt(isUseDiscount);
        dest.writeString(shopRebateMoney);
        dest.writeTypedList(coupons);
    }

}
