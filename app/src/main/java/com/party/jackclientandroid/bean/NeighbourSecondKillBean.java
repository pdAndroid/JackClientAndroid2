package com.party.jackclientandroid.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/10/19.
 */

public class NeighbourSecondKillBean {

    /**
     * beans : [{"buyPersonLimit":0,"buyPrice":9.9,"endSaleTime":0,"groupBuyImg":null,"id":28,"itemSalesSum":1,"onceCount":0,"originalPrice":50,"startSaleTime":0,"stockCount":0,"title":"50.0元秒杀券","total":0},{"buyPersonLimit":1,"buyPrice":9.9,"endSaleTime":1539792000000,"groupBuyImg":null,"id":29,"itemSalesSum":0,"onceCount":1,"originalPrice":50,"startSaleTime":0,"stockCount":10,"title":"50.0元秒杀券","total":0},{"buyPersonLimit":1,"buyPrice":9.9,"endSaleTime":1540051200000,"groupBuyImg":null,"id":30,"itemSalesSum":0,"onceCount":1,"originalPrice":50,"startSaleTime":0,"stockCount":10,"title":"50.0元秒杀券","total":0}]
     * distance : 0
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/home_img/1534994219552_1539745075385.png
     * id : 1534994219553
     * latitude : 30.685578
     * longitude : 103.862884
     * shopName : 华莱士
     * symbolBuild : 珠江广场
     */

    private double distance;
    private String homeImg;
    private long id;
    private double latitude;
    private double longitude;
    private String shopName;
    private String symbolBuild;
    private List<BeansBean> beans;

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public List<BeansBean> getBeans() {
        return beans;
    }

    public void setBeans(List<BeansBean> beans) {
        this.beans = beans;
    }

    public static class BeansBean {
        /**
         * buyPersonLimit : 0
         * buyPrice : 9.9
         * endSaleTime : 0
         * groupBuyImg : null
         * id : 28
         * itemSalesSum : 1
         * onceCount : 0
         * originalPrice : 50
         * startSaleTime : 0
         * stockCount : 0
         * title : 50.0元秒杀券
         * total : 0
         */

        private int buyPersonLimit;
        private double buyPrice;
        private long endSaleTime;
        private Object groupBuyImg;
        private long id;
        private int itemSalesSum;
        private int onceCount;
        private double originalPrice;
        private long startSaleTime;
        private int stockCount;
        private String title;
        private int total;

        public int getBuyPersonLimit() {
            return buyPersonLimit;
        }

        public void setBuyPersonLimit(int buyPersonLimit) {
            this.buyPersonLimit = buyPersonLimit;
        }

        public double getBuyPrice() {
            return buyPrice;
        }

        public void setBuyPrice(double buyPrice) {
            this.buyPrice = buyPrice;
        }

        public long getEndSaleTime() {
            return endSaleTime;
        }

        public void setEndSaleTime(long endSaleTime) {
            this.endSaleTime = endSaleTime;
        }

        public Object getGroupBuyImg() {
            return groupBuyImg;
        }

        public void setGroupBuyImg(Object groupBuyImg) {
            this.groupBuyImg = groupBuyImg;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public int getItemSalesSum() {
            return itemSalesSum;
        }

        public void setItemSalesSum(int itemSalesSum) {
            this.itemSalesSum = itemSalesSum;
        }

        public int getOnceCount() {
            return onceCount;
        }

        public void setOnceCount(int onceCount) {
            this.onceCount = onceCount;
        }

        public double getOriginalPrice() {
            return originalPrice;
        }

        public void setOriginalPrice(double originalPrice) {
            this.originalPrice = originalPrice;
        }

        public long getStartSaleTime() {
            return startSaleTime;
        }

        public void setStartSaleTime(long startSaleTime) {
            this.startSaleTime = startSaleTime;
        }

        public int getStockCount() {
            return stockCount;
        }

        public void setStockCount(int stockCount) {
            this.stockCount = stockCount;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }
}
