package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.party.jackclientandroid.okhttp.HttpLogger;

import java.util.Date;

/**
 * Created by 派对 on 2018/11/8.
 */

public class CouponBean implements Parcelable {

    private String availableDate;
    private String buyPrice;
    private int count;
    private String goodsRange;
    private Long id;
    private String itemName;
    private int onceCount;
    private String originalPrice;
    private String shopName;


    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(availableDate);
        dest.writeString(buyPrice);
        dest.writeInt(count);
        dest.writeString(goodsRange);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(itemName);
        dest.writeInt(onceCount);
        dest.writeString(originalPrice);
        dest.writeString(shopName);
    }

    protected CouponBean(Parcel in) {
        availableDate = in.readString();
        buyPrice = in.readString();
        count = in.readInt();
        goodsRange = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        itemName = in.readString();
        onceCount = in.readInt();
        originalPrice = in.readString();
        shopName = in.readString();
    }

    public static final Creator<CouponBean> CREATOR = new Creator<CouponBean>() {
        @Override
        public CouponBean createFromParcel(Parcel in) {
            return new CouponBean(in);
        }

        @Override
        public CouponBean[] newArray(int size) {
            return new CouponBean[size];
        }
    };
}
