package com.party.jackclientandroid.bean;

import com.party.jackclientandroid.okhttp.HttpLogger;

/**
 * Created by Administrator on 2018/9/4.
 */

public class UserOrderExtraDetail {

    /**
     * id : 308
     * itemExpire : 1542556800000
     * itemId : 14
     * itemName : 50.0元代金券
     * onceCount : 1
     * orderCode : 1538821290979
     * orderState : 2
     * orderStateName : 待使用
     * originalPrice : 50
     * price : 9.9
     * showCode : true
     * type : 6
     */

    private String shopPhone;
    private String userPhone;
    private String shopName;
    private String shopId;
    private long create;
    private String finalPrice;

    private long id;
    private long itemExpire;
    private long itemId;
    private String itemName;
    private int onceCount;
    private String orderCode;
    private int orderState;
    private String orderStateName;
    private double originalPrice;
    private double price;
    private boolean isShowCode;
    private int type;
    private boolean isSelected;

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public String getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(String finalPrice) {
        this.finalPrice = finalPrice;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getItemExpire() {
        return itemExpire;
    }

    public void setItemExpire(long itemExpire) {
        this.itemExpire = itemExpire;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public String getOrderStateName() {
        return orderStateName;
    }

    public void setOrderStateName(String orderStateName) {
        this.orderStateName = orderStateName;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isShowCode() {
        return isShowCode;
    }

    public void setShowCode(boolean showCode) {
        isShowCode = showCode;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
