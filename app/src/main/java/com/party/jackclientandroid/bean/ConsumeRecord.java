package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/7/26.
 */

public class ConsumeRecord {

    /**
     * create : 1537285444000
     * id : 1535177765176
     * remarks : 众享红包
     * tradeRecordStateName : 红包收入
     * tradeRecordType : 0
     * value : 6417.22
     */

    private long create;
    private long id;
    private String remarks;
    private String tradeRecordStateName;
    private int tradeRecordType;
    private double value;
    private transient String createTime;
    private String xiapinCoin;

    public String getXiapinCoin() {
        return xiapinCoin;
    }

    public void setXiapinCoin(String xiapinCoin) {
        this.xiapinCoin = xiapinCoin;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTradeRecordStateName() {
        return tradeRecordStateName;
    }

    public void setTradeRecordStateName(String tradeRecordStateName) {
        this.tradeRecordStateName = tradeRecordStateName;
    }

    public int getTradeRecordType() {
        return tradeRecordType;
    }

    public void setTradeRecordType(int tradeRecordType) {
        this.tradeRecordType = tradeRecordType;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
