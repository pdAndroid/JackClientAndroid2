package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Administrator on 2018/7/23.
 */

public class DeliciousFood {

    /**
     * averageCons : 50
     * avgScore : 7
     * coupons : [{"buyPrice":80,"couponState":1,"id":1,"onceCount":1,"originalPrice":100},{"buyPrice":80,"couponState":2,"id":12,"onceCount":1,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":1531737109972,"onceCount":2,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":1531737498459,"onceCount":2,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":1531813253548,"onceCount":1,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":1531813423094,"onceCount":3,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":1533193210804,"onceCount":2,"originalPrice":90},{"buyPrice":103,"couponState":1,"id":1533193248915,"onceCount":3,"originalPrice":111},{"buyPrice":8888,"couponState":1,"id":1534750734617,"onceCount":1,"originalPrice":9998},{"buyPrice":79,"couponState":1,"id":1534750801765,"onceCount":1,"originalPrice":114},{"buyPrice":80,"couponState":1,"id":15317374984591,"onceCount":2,"originalPrice":100},{"buyPrice":80,"couponState":1,"id":153173749845913,"onceCount":2,"originalPrice":100}]
     * enviroScore : 7
     * groupBuys : [{"buyPrice":300,"id":0,"name":"单人餐","title":"二十五元团购卷"},{"buyPrice":300,"id":1,"name":"双人餐","title":"现金卷"},{"buyPrice":300,"id":2,"name":"三人餐","title":"折扣券"},{"buyPrice":300,"id":3,"name":"4-5人餐","title":"通用卷"},{"buyPrice":300,"id":15312781295145,"name":"单人餐","title":"历史卷"}]
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * id : 1530523930811
     * serviceScore : 7
     * shopName : 寇芊芊的包子店
     * symbolBuild : 融信广场
     * tasteScore : 7
     */
    @Expose
    @SerializedName("averageCons")
    private double averageCons;
    @Expose
    @SerializedName("avgScore")
    private int avgScore;
    @Expose
    @SerializedName("enviroScore")
    private int enviroScore;
    @Expose
    @SerializedName("homeImg")
    private String homeImg;
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("serviceScore")
    private int serviceScore;
    @Expose
    @SerializedName("shopName")
    private String shopName;
    @Expose
    @SerializedName("symbolBuild")
    private String symbolBuild;
    @Expose
    @SerializedName("tasteScore")
    private int tasteScore;
    @Expose
    @SerializedName("coupons")
    private List<CouponsBean> coupons;
    @Expose
    @SerializedName("groupBuys")
    private List<GroupBuysBean> groupBuys;

    public double getAverageCons() {
        return averageCons;
    }

    public void setAverageCons(double averageCons) {
        this.averageCons = averageCons;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }

    public List<CouponsBean> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponsBean> coupons) {
        this.coupons = coupons;
    }

    public List<GroupBuysBean> getGroupBuys() {
        return groupBuys;
    }

    public void setGroupBuys(List<GroupBuysBean> groupBuys) {
        this.groupBuys = groupBuys;
    }
}
