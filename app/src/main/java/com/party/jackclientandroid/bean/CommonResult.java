package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/9/3.
 */

public class CommonResult {
    private long id;
    private String money;
    private String waitMoney;
    private double sum;
    private int tradeRecordType;
    private String phone;
    private int roleId;
    private int count;
    private Boolean result;
    private double dividendSum;
    private String roleText;
    private String qr_code;
    private String return_code;
    private String QRCode;
    private String showText;
    private String rebateMoney;
    private String xiapinCoin;
    private String parentPhone;
    private long unsealingTime;
    private String remarks;

    public long getUnsealingTime() {
        return unsealingTime;
    }

    public void setUnsealingTime(long unsealingTime) {
        this.unsealingTime = unsealingTime;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getParentPhone() {
        return parentPhone;
    }

    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    public String getXiapinCoin() {
        return xiapinCoin;
    }

    public void setXiapinCoin(String xiapinCoin) {
        this.xiapinCoin = xiapinCoin;
    }

    public String getRebateMoney() {
        return rebateMoney;
    }

    public void setRebateMoney(String rebateMoney) {
        this.rebateMoney = rebateMoney;
    }

    public String getRoleText() {
        return roleText;
    }

    public void setRoleText(String roleText) {
        this.roleText = roleText;
    }

    public String getWaitMoney() {
        return waitMoney;
    }

    public void setWaitMoney(String waitMoney) {
        this.waitMoney = waitMoney;
    }

    @Expose
    @SerializedName("invitationRewardsSum")
    private double invitationRewardsSum;

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getQr_code() {
        return qr_code;
    }

    public void setQr_code(String qr_code) {
        this.qr_code = qr_code;
    }

    public double getDividendSum() {
        return dividendSum;
    }

    public void setDividendSum(double dividendSum) {
        this.dividendSum = dividendSum;
    }

    public double getInvitationRewardsSum() {
        return invitationRewardsSum;
    }

    public void setInvitationRewardsSum(double invitationRewardsSum) {
        this.invitationRewardsSum = invitationRewardsSum;
    }

    public double getMembershipRewardsSum() {
        return membershipRewardsSum;
    }

    public void setMembershipRewardsSum(double membershipRewardsSum) {
        this.membershipRewardsSum = membershipRewardsSum;
    }

    @Expose
    @SerializedName("membershipRewardsSum")

    private double membershipRewardsSum;

    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    @Expose
    @SerializedName("isCancel")

    private boolean isCancel;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public int getTradeRecordType() {
        return tradeRecordType;
    }

    public void setTradeRecordType(int tradeRecordType) {
        this.tradeRecordType = tradeRecordType;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getShowText() {
        return showText;
    }

    public void setShowText(String showText) {
        this.showText = showText;
    }
}
