package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.party.jackclientandroid.okhttp.HttpLogger;

public class User {

    private String token;
    private String icon;
    private String nickname;
    private String phone;
    private int roleId;
    private String name;
    /**
     * 1 是有通过认证 0 没有通过认证
     */
    private int isAuthentication;
    /**
     * 1 是绑定 0 没有通过认证
     */
    private int isBindingWx;
    /**
     * 1 是绑定 0 没有通过认证
     */
    private int isBindingAli;

    /**
     * 1 是有密码 0 没有密码
     */
    private int isPassword;

    public int getIsBindingAli() {
        return isBindingAli;
    }

    public void setIsBindingAli(int isBindingAli) {
        this.isBindingAli = isBindingAli;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public int getIsAuthentication() {
        return isAuthentication;
    }


    public int getIsBindingWx() {
        return isBindingWx;
    }

    public void setIsBindingWx(int isBindingWx) {
        this.isBindingWx = isBindingWx;
    }


    /**
     * 是否需要支付宝认证
     *
     * @return
     */
    public boolean isBandingAli() {
        switch (isBindingAli) {
            case 1:// 绑定
                return false;
            default: //如果没有绑定支付宝  查看是否身份证已认证
                return true;
        }
    }

    /**
     * 是否实名认证（两个都认证）
     *
     * @return
     */
    public boolean hasNeedAuth() {
        if (isIDCardAuth()) {
            if (isBandingAli()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 是否需要去实名认证
     *
     * @return
     */
    public boolean isIDCardAuth() {
        switch (isAuthentication) {
            case 1:
                return false;
            case 2:
                return false;
            default://0 是未认证
                return true;
        }
    }


    public String getAuthText() {
        switch (isAuthentication) {
            case -1:
                return "不通过";
            case 1:
                return "已认证";
            case 2:
                return "审核中";
            default://0 是未认证
                return "去认证";
        }
    }

    public void setIsAuthentication(int isAuthentication) {
        this.isAuthentication = isAuthentication;
    }

    public int getIsPassword() {
        return isPassword;
    }

    public void setIsPassword(int isPassword) {
        this.isPassword = isPassword;
    }
}