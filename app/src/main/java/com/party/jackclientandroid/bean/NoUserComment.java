package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/10/17.
 */

public class NoUserComment implements DisplayableItem{

    String title;

    public NoUserComment(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
