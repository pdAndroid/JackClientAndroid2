package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/10/18.
 */

public class StringBean {

    private String value;

    public StringBean(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
