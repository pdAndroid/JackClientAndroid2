package com.party.jackclientandroid.bean;

import android.view.View;

import com.zhy.adapter.recyclerview.CommonAdapter;

import java.util.List;

/**
 * Created by Administrator on 2018/10/18.
 */

public class FilterPopupViewObj {
    private View filterView;
    private CommonAdapter commonAdapter;
    private List<SearchMenuBean> searchMenuBeanList;

    public View getFilterView() {
        return filterView;
    }

    public void setFilterView(View filterView) {
        this.filterView = filterView;
    }

    public CommonAdapter getCommonAdapter() {
        return commonAdapter;
    }

    public void setCommonAdapter(CommonAdapter commonAdapter) {
        this.commonAdapter = commonAdapter;
    }

    public List<SearchMenuBean> getSearchMenuBeanList() {
        return searchMenuBeanList;
    }

    public void setSearchMenuBeanList(List<SearchMenuBean> searchMenuBeanList) {
        this.searchMenuBeanList = searchMenuBeanList;
    }
}
