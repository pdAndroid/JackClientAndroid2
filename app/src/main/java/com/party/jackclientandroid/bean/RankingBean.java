package com.party.jackclientandroid.bean;

import java.io.Serializable;

/**
 * Created by 派对 on 2018/11/23.
 */

public class RankingBean implements Serializable {

    private String icon;
    private String nickname;
    private String invitationRewardsSum;
    private String dividendSum;
    private String membershipRewardsSum;

    private int layout;
    private int fontColor;
    private int headImage;
    private int numImage;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getInvitationRewardsSum() {
        return invitationRewardsSum;
    }

    public void setInvitationRewardsSum(String invitationRewardsSum) {
        this.invitationRewardsSum = invitationRewardsSum;
    }

    public String getDividendSum() {
        return dividendSum;
    }

    public void setDividendSum(String dividendSum) {
        this.dividendSum = dividendSum;
    }

    public String getMembershipRewardsSum() {
        return membershipRewardsSum;
    }

    public void setMembershipRewardsSum(String membershipRewardsSum) {
        this.membershipRewardsSum = membershipRewardsSum;
    }
}
