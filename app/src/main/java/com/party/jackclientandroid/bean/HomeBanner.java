package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/8/11.
 */

public class HomeBanner {

    /**
     * id : 1530240815136
     * imgUrl : https://pdkj.oss-cn-beijing.aliyuncs.com/banner/banner-one.jpg
     * type : null
     * value : 1
     */
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("imgUrl")
    private String imgUrl;
    @Expose
    @SerializedName("type")
    private Object type;
    @Expose
    @SerializedName("value")
    private String value;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
