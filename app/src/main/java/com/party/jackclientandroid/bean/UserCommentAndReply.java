package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/8/19.
 */

public class UserCommentAndReply implements DisplayableItem {

    /**
     * commentImg : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1532070852279.png
     * content : 很好吃
     * create : 1530935216000
     * enviroScore : 1
     * icon : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1534152623862.png
     * id : 1
     * nickName : 大吉大利
     * orderId : 1530342500804
     * parentId : 0
     * serviceScore : 1
     * shopId : 1530523930811
     * tasteScore : 1
     * userId : 1
     */

    private String commentImg;
    private String content;
    private long create;
    private int enviroScore;
    private String icon;
    private long id;
    private String nickName;
    private long orderId;
    private int parentId;
    private int serviceScore;
    private long shopId;
    private int tasteScore;
    private long userId;

    public String getCommentImg() {
        return commentImg;
    }

    public void setCommentImg(String commentImg) {
        this.commentImg = commentImg;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
