package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponsBean {
    /**
     * buyPrice : 80
     * couponState : 1
     * id : 1
     * onceCount : 1
     * originalPrice : 100
     */
    @Expose
    @SerializedName("buyPrice")
    private int buyPrice;
    @Expose
    @SerializedName("couponState")
    private int couponState;
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("onceCount")
    private int onceCount;
    @Expose
    @SerializedName("originalPrice")
    private int originalPrice;

    public int getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(int buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getCouponState() {
        return couponState;
    }

    public void setCouponState(int couponState) {
        this.couponState = couponState;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public int getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(int originalPrice) {
        this.originalPrice = originalPrice;
    }
}
