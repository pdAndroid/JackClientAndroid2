package com.party.jackclientandroid.bean;

import java.util.List;

public class PlayerInfoBean {

    private String coverURL;
    private String homeImg;
    private String describe;
    private String shopId;
    private String shopName;
    private String videoId;
    private String videoTime;
    private String videoURL;
    private VideoDetail videoDetail;

    public VideoDetail getVideoDetail() {
        if (videoDetail == null) videoDetail = new VideoDetail();
        return videoDetail;
    }

    public void setVideoDetail(VideoDetail videoDetail) {
        videoDetail.setShopId(shopId);
        this.videoDetail = videoDetail;
    }

    public String getCoverURL() {
        return coverURL;
    }

    public void setCoverURL(String coverURL) {
        this.coverURL = coverURL;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }


    public String getVideoTime() {
        return videoTime;
    }

    public void setVideoTime(String videoTime) {
        this.videoTime = videoTime;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

}
