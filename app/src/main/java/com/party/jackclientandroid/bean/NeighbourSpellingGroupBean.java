package com.party.jackclientandroid.bean;

import com.party.jackclientandroid.okhttp.HttpLogger;

import java.util.List;

/**
 * Created by Administrator on 2018/10/18.
 * 邻里惠中的查询实体
 */

public class NeighbourSpellingGroupBean {

    /**
     * beans : [{"buyPersonLimit":1,"buyPrice":100,"endSaleTime":1540018800000,"groupBuyImg":"https://pdkj.oss-cn-beijing.aliyuncs.com/shop/group_buy/1534994219553_1539841023478.png","id":9,"itemSalesSum":0,"onceCount":1,"originalPrice":125,"startSaleTime":0,"stockCount":100,"title":"舒服的套餐","total":0},{"buyPersonLimit":1,"buyPrice":100,"endSaleTime":1540018800000,"groupBuyImg":"https://pdkj.oss-cn-beijing.aliyuncs.com/shop/group_buy/1534994219553_1539841234630.png","id":10,"itemSalesSum":0,"onceCount":1,"originalPrice":125,"startSaleTime":0,"stockCount":100,"title":"舒服的套餐","total":0}]
     * distance : 0
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/home_img/1534994219552_1539745075385.png
     * id : 1534994219553
     * latitude : 30.685578
     * longitude : 103.862884
     * shopName : 华莱士
     */

    private double distance;
    private String homeImg;
    private long id;
    private double latitude;
    private double longitude;
    private String shopName;
    private List<SpellingCoupon> secondKillBeans;
    private String symbolBuild;

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public List<SpellingCoupon> getSecondKillBeans() {
        return secondKillBeans;
    }

    public void setSecondKillBeans(List<SpellingCoupon> secondKillBeans) {
        this.secondKillBeans = secondKillBeans;
    }

}
