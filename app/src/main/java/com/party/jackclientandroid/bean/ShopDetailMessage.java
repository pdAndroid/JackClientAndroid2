package com.party.jackclientandroid.bean;

public class ShopDetailMessage  {

    /**
     * areaId : 1
     * averageCons : 2
     * bussClose : 00:00:00
     * bussOpen : 00:00:00
     * discount : 9.5
     * enviroScore : 5
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * id : 1534152366915
     * introduce : 666
     * latitude : 66
     * longitude : 66
     * serviceScore : 4
     * shopAddress : 融信儒风酒店
     * shopName : 寇芊芊的玉米店
     * shopPhone : 15328074182
     * symbolBuild : 融信广场
     * tasteScore : 7
     */

    private long areaId;
    private int averageCons;
    private String bussClose;
    private String bussOpen;
    private double discount;
    private int enviroScore;
    private String homeImg;
    private long id;
    private String introduce;
    private int latitude;
    private int longitude;
    private int serviceScore;
    private String shopAddress;
    private String shopName;
    private String shopPhone;
    private String symbolBuild;
    private int tasteScore;

    public long getAreaId() {
        return areaId;
    }

    public void setAreaId(long areaId) {
        this.areaId = areaId;
    }

    public int getAverageCons() {
        return averageCons;
    }

    public void setAverageCons(int averageCons) {
        this.averageCons = averageCons;
    }

    public String getBussClose() {
        return bussClose;
    }

    public void setBussClose(String bussClose) {
        this.bussClose = bussClose;
    }

    public String getBussOpen() {
        return bussOpen;
    }

    public void setBussOpen(String bussOpen) {
        this.bussOpen = bussOpen;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }
}
