package com.party.jackclientandroid.bean;

import java.io.Serializable;

/**
 * Created by 派对 on 2018/11/23.
 */

public class BindingInfo implements Serializable {

    private String bindingKey;

    // 绑定支付宝使用
    private String verKey;

    private String nickname;

    private String avatar;

    public String getVerKey() {
        return verKey;
    }

    public void setVerKey(String verKey) {
        this.verKey = verKey;
    }

    public String getBindingKey() {
        return bindingKey;
    }

    public void setBindingKey(String bindingKey) {
        this.bindingKey = bindingKey;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
