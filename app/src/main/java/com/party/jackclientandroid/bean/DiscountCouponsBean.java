package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/10/2.
 */

public class DiscountCouponsBean implements Parcelable {

    private int onceCount;//商家限制使用数量
    private double originalPrice;
    private long couponId;
    private double discount;
    private int num;
    private String couponTitle;
    private long shopId;

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public long getCouponId() {
        return couponId;
    }

    public void setCouponId(long couponId) {
        this.couponId = couponId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.onceCount);
        dest.writeDouble(this.originalPrice);
        dest.writeLong(this.couponId);
        dest.writeDouble(this.discount);
        dest.writeInt(this.num);
        dest.writeString(this.couponTitle);
    }

    public DiscountCouponsBean() {
    }

    protected DiscountCouponsBean(Parcel in) {
        this.onceCount = in.readInt();
        this.originalPrice = in.readDouble();
        this.couponId = in.readLong();
        this.discount = in.readDouble();
        this.num = in.readInt();
        this.couponTitle = in.readString();
    }

    public static final Parcelable.Creator<DiscountCouponsBean> CREATOR = new Parcelable.Creator<DiscountCouponsBean>() {
        @Override
        public DiscountCouponsBean createFromParcel(Parcel source) {
            return new DiscountCouponsBean(source);
        }

        @Override
        public DiscountCouponsBean[] newArray(int size) {
            return new DiscountCouponsBean[size];
        }
    };
}
