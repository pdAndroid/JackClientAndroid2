package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/8/19.
 */

public class RecommendVegetable {

    /**
     * id : 1
     * imgUrl : https://pdkj.oss-cn-beijing.aliyuncs.com/goods/e40b2e1de365e2f4.jpg
     * title : 土豆
     */
    @Expose
    @SerializedName("id")
    private long id;
    @Expose
    @SerializedName("imgUrl")
    private String imgUrl;
    @Expose
    @SerializedName("title")
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
