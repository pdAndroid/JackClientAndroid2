package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/8/24.
 */

public class PayType {

    /**
     * pay_name :
     * id :
     */

    private String pay_name;
    private String id;

    public String getPay_name() {
        return pay_name;
    }

    public void setPay_name(String pay_name) {
        this.pay_name = pay_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
