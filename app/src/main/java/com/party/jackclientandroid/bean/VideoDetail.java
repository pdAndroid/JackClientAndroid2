package com.party.jackclientandroid.bean;


import android.view.View;

public class VideoDetail {

    private String shopId;
    private int concernNum;//该视频已关注数量
    private int commentNum;//评论数量
    private int niceNum;// 点赞数量
    private int userIsNice;//本用户是否关注该视频
    private int userIsConcern;//用户是否关注该店铺
    private int haveRedBag;//是否有红包
    private long expireTime;
    private Object redBagView;//红包标识图。


    public Object getRedBagView() {
        return redBagView;
    }

    public void setRedBagView(Object redBagView) {
        this.redBagView = redBagView;
    }

    public void setHaveRedBag(int haveRedBag) {
        this.haveRedBag = haveRedBag;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public int getNiceNum() {
        return niceNum;
    }

    public void setNiceNum(int niceNum) {
        this.niceNum = niceNum;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public int getConcernNum() {
        return concernNum;
    }

    public void setConcernNum(int concernNum) {
        this.concernNum = concernNum;
    }


    public int getUserIsNice() {
        return userIsNice;
    }

    public void setUserIsNice(int userIsNice) {
        this.userIsNice = userIsNice;
    }

    public int getCommentNum() {
        return commentNum;
    }

    public void setCommentNum(int commentNum) {
        this.commentNum = commentNum;
    }

    public int getUserIsConcern() {
        return userIsConcern;
    }

    public void setUserIsConcern(int userIsConcern) {
        this.userIsConcern = userIsConcern;
    }

    public int getHaveRedBag() {
        return haveRedBag;
    }


}
