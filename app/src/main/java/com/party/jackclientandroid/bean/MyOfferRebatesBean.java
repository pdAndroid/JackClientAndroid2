package com.party.jackclientandroid.bean;

/**
 * Created by 南宫灬绝痕 on 2018/12/18.
 */

public class MyOfferRebatesBean {

    /**
     * shopId :
     * id : 1
     * money :
     * shopName :
     * homeImg :
     * longitude :
     * latitude :
     * shopAddress :
     */

    private String shopId;
    private int id;
    private String money;
    private String shopName;
    private String homeImg;
    private String longitude;
    private String latitude;
    private String shopAddress;

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }
}
