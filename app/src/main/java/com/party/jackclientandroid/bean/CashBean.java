package com.party.jackclientandroid.bean;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 派对 on 2018/11/8.
 */

public class CashBean {

    private String minRate;
    private String rate;
    private String wxAppId;
    private String aliAppId;
    private List<Account> accounts;


    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }

    public String getAliAppId() {
        return aliAppId;
    }

    public void setAliAppId(String aliAppId) {
        this.aliAppId = aliAppId;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public String getMinRate() {
        return minRate;
    }

    public void setMinRate(String minRate) {
        this.minRate = minRate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void addAccount(Account data) {
        if (accounts == null) accounts = new ArrayList<>();
        accounts.add(data);
    }

    public class Account {
        private String userName;
        private String avatar;
        private int cardType; //1: 微信 ，2 支付宝，

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public int getCardType() {
            return cardType;
        }

        public void setCardType(int cardType) {
            this.cardType = cardType;
        }
    }

    public Account getWxAccount() {
        if (accounts == null) return null;
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).cardType == 1) {
                return accounts.get(i);
            }
        }
        return null;
    }

    public Account getAliAccount() {
        if (accounts == null) return null;
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).cardType == 2) {
                return accounts.get(i);
            }
        }
        return null;
    }
}
