package com.party.jackclientandroid.bean;

/**
 * Created by 南宫灬绝痕 on 2018/12/13.
 */

public class HallMeal implements DisplayableItem {
    String title;

    public HallMeal(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
