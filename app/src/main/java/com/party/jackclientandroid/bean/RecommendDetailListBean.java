package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/9/12.
 */

public class RecommendDetailListBean {

    /**
     * create : 1534754003000
     * remarks : adsd
     * tradeRecordStateName : 推荐奖励
     * value : 2
     */

    private long create;
    private String remarks;
    private String tradeRecordStateName;
    private double value;

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTradeRecordStateName() {
        return tradeRecordStateName;
    }

    public void setTradeRecordStateName(String tradeRecordStateName) {
        this.tradeRecordStateName = tradeRecordStateName;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
