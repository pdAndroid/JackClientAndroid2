package com.party.jackclientandroid.bean;

import com.facebook.stetho.json.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/9/28.
 */

public class ShopDetail_Header implements DisplayableItem {
    private int areaId;
    private double averageCons;
    private int avgScore;
    private int bussClose;
    private int bussOpen;
    private double discount;
    private int enviroScore;
    private String homeImg;
    private long id;
    private String introduce;
    private double latitude;
    private double longitude;
    private int serviceScore;
    private String shopAddress;
    private String shopName;
    private String userConcern;
    private String shopPhone;
    private String symbolBuild;
    private int tasteScore;
    private int isAttention;

    public int getIsAttention() {
        return isAttention;
    }

    public void setIsAttention(int isAttention) {
        this.isAttention = isAttention;
    }

    public String getUserConcern() {
        return userConcern;
    }

    public void setUserConcern(String userConcern) {
        this.userConcern = userConcern;
    }


    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public double getAverageCons() {
        return averageCons;
    }

    public void setAverageCons(double averageCons) {
        this.averageCons = averageCons;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

    public int getBussClose() {
        return bussClose;
    }

    public void setBussClose(int bussClose) {
        this.bussClose = bussClose;
    }

    public int getBussOpen() {
        return bussOpen;
    }

    public void setBussOpen(int bussOpen) {
        this.bussOpen = bussOpen;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }
}
