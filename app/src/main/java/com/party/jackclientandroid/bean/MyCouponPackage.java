package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.party.jackclientandroid.okhttp.HttpLogger;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/10/2.
 */

public class MyCouponPackage implements Parcelable {

    private String userOrderId;
    private long dateStart;
    private long dateEnd;
    private long timeStart;
    private long timeEnd;
    private double discount;
    private String imgUrl;
    private long itemId;
    private String itemName;
    private double price;
    private long shopId;
    private String shopName;
    private int type;
    private int count;
    private String orderCode;
    private int onceCount;
    private double originalPrice;


    public String getUserOrderId() {
        return userOrderId;
    }

    public void setUserOrderId(String userOrderId) {
        this.userOrderId = userOrderId;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }


    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public MyCouponPackage() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(userOrderId);
        dest.writeLong(dateStart);
        dest.writeLong(dateEnd);
        dest.writeLong(timeStart);
        dest.writeLong(timeEnd);
        dest.writeDouble(discount);
        dest.writeString(imgUrl);
        dest.writeLong(itemId);
        dest.writeString(itemName);
        dest.writeDouble(price);
        dest.writeLong(shopId);
        dest.writeString(shopName);
        dest.writeInt(type);
        dest.writeInt(count);
        dest.writeString(orderCode);
        dest.writeInt(onceCount);
        dest.writeDouble(originalPrice);
    }


    protected MyCouponPackage(Parcel in) {
        userOrderId = in.readString();
        dateStart = in.readLong();
        dateEnd = in.readLong();
        timeStart = in.readLong();
        timeEnd = in.readLong();
        discount = in.readDouble();
        imgUrl = in.readString();
        itemId = in.readLong();
        itemName = in.readString();
        price = in.readDouble();
        shopId = in.readLong();
        shopName = in.readString();
        type = in.readInt();
        count = in.readInt();
        orderCode = in.readString();
        onceCount = in.readInt();
        originalPrice = in.readDouble();
    }

    public static final Creator<MyCouponPackage> CREATOR = new Creator<MyCouponPackage>() {
        @Override
        public MyCouponPackage createFromParcel(Parcel in) {
            return new MyCouponPackage(in);
        }

        @Override
        public MyCouponPackage[] newArray(int size) {
            return new MyCouponPackage[size];
        }
    };
}
