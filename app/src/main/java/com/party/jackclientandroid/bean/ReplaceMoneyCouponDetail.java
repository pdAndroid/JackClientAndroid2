package com.party.jackclientandroid.bean;

import java.io.Serializable;

public class ReplaceMoneyCouponDetail extends GoodDetail implements Serializable {

    private int appointment;
    private String availableDate;
    private String goodsRange;
    private double latitude;
    private double longitude;
    private String ruleContent;
    private String shopAddress;
    private String shopName;
    private String shopPhone;
    private String ruleSplicing;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRuleSplicing() {
        return ruleSplicing;
    }

    public void setRuleSplicing(String ruleSplicing) {
        this.ruleSplicing = ruleSplicing;
    }

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public int getAppointment() {
        return appointment;
    }

    public void setAppointment(int appointment) {
        this.appointment = appointment;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }


    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

}
