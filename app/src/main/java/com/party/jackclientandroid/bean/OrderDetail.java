package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/8/13.
 */

public class OrderDetail implements Parcelable {

    /**
     * create : 1538191603000
     * finalPrice : 100
     * latitude : 66
     * longitude : 66
     * num : 1
     * orderCode : 1538191604991
     * orderState : 1
     * shopAddress : 四川省成都市温江区海川路
     * shopId : 1530523930811
     * shopName : 寇芊芊的包子店
     * shopPhone : 124578824965
     * userPhone : null
     */

    private long create;
    private double finalPrice;
    private double latitude;
    private double longitude;
    private int num;
    private String orderCode;
    private int orderState;
    private String shopAddress;
    private long shopId;
    private String shopName;
    private String shopPhone;
    private String userPhone;
    private double discount;

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.create);
        dest.writeDouble(this.finalPrice);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeInt(this.num);
        dest.writeString(this.orderCode);
        dest.writeInt(this.orderState);
        dest.writeString(this.shopAddress);
        dest.writeLong(this.shopId);
        dest.writeString(this.shopName);
        dest.writeString(this.shopPhone);
        dest.writeString(this.userPhone);
        dest.writeDouble(this.discount);
    }

    public OrderDetail() {
    }

    protected OrderDetail(Parcel in) {
        this.create = in.readLong();
        this.finalPrice = in.readDouble();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.num = in.readInt();
        this.orderCode = in.readString();
        this.orderState = in.readInt();
        this.shopAddress = in.readString();
        this.shopId = in.readLong();
        this.shopName = in.readString();
        this.shopPhone = in.readString();
        this.userPhone = in.readString();
        this.discount = in.readDouble();
    }

    public static final Parcelable.Creator<OrderDetail> CREATOR = new Parcelable.Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel source) {
            return new OrderDetail(source);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };
}
