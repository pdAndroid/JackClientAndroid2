package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/8/25.
 */

public class SearchPoint {

    /**
     * id : 1
     * item : 包子店
     */

    private String id;
    private String item;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
