package com.party.jackclientandroid.bean;

public class GroupBuysBean {
    /**
     * buyPrice : 300
     * id : 0
     * name : 单人餐
     * title : 二十五元团购卷
     */

    private int buyPrice;
    private long id;
    private String name;
    private String title;

    public int getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(int buyPrice) {
        this.buyPrice = buyPrice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}