package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/9/3.
 */

public class GroupBuyCouponIncludeGoods {

    /**
     * categoryId : 1
     * categoryName : 素菜
     * count : 1
     * id : 5
     * isHot : true
     * price : 50
     * title : 土豆
     * unitName : 份
     */

    private long categoryId;
    private String categoryName;
    private int count;
    private long id;
    private boolean isHot;
    private double price;
    private String title;
    private String unitName;

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isIsHot() {
        return isHot;
    }

    public void setIsHot(boolean isHot) {
        this.isHot = isHot;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }
}
