package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/9/27.
 */

/**
 * 拼团卷使用
 */
public class SpellingCoupon extends SpellingGroupCouponBean{

    private int spellingType;// 1 拼团中  2  拼团成功
    private String couponImg;
    private double latitude;
    private double longitude;
    private String spellingStateString;
    private int saleNumber;


    public int getSaleNumber() {
        return saleNumber;
    }

    public void setSaleNumber(int saleNumber) {
        this.saleNumber = saleNumber;
    }

    public String getCouponImg() {
        return couponImg;
    }

    public void setCouponImg(String couponImg) {
        this.couponImg = couponImg;
    }

    public int getSpellingType() {
        return spellingType;
    }

    public void setSpellingType(int spellingType) {
        this.spellingType = spellingType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getSpellingStateString() {
        return spellingStateString;
    }

    public void setSpellingStateString(String spellingStateString) {
        this.spellingStateString = spellingStateString;
    }
}
