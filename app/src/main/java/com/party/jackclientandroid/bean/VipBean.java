package com.party.jackclientandroid.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/8/26.
 */

public class VipBean {

    /**
     * parentPhone : 15328074182
     * responses : [{"currentPrice":0,"id":1,"name":"青铜会员","originalPrice":0},{"currentPrice":0,"id":1,"name":"青铜会员","originalPrice":0},{"currentPrice":0,"id":1,"name":"青铜会员","originalPrice":0}]
     * role :
     */

    private String parentPhone;
    private String role;
    private List<ResponsesBean> responses;

    public String getParentPhone() {
        return parentPhone;
    }

    public void setParentPhone(String parentPhone) {
        this.parentPhone = parentPhone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<ResponsesBean> getResponses() {
        return responses;
    }

    public void setResponses(List<ResponsesBean> responses) {
        this.responses = responses;
    }

    public static class ResponsesBean {
        /**
         * currentPrice : 0
         * id : 1
         * name : 青铜会员
         * originalPrice : 0
         */

        private double currentPrice;
        private int id;
        private String name;
        private double originalPrice;

        public double getCurrentPrice() {
            return currentPrice;
        }

        public void setCurrentPrice(double currentPrice) {
            this.currentPrice = currentPrice;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getOriginalPrice() {
            return originalPrice;
        }

        public void setOriginalPrice(double originalPrice) {
            this.originalPrice = originalPrice;
        }
    }
}
