package com.party.jackclientandroid.bean;


import java.util.List;

public class FilterCondition {

    private int id;
    private int parentId;
    private String title;
    private String key;
    private String value;
    private String type;
    List<FilterCondition> data;

    public List<FilterCondition> getData() {
        return data;
    }

    public void setData(List<FilterCondition> data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
