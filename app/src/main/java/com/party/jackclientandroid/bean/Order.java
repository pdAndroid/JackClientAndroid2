package com.party.jackclientandroid.bean;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Administrator on 2018/8/16.
 */

public class Order implements Parcelable {

    /**
     * create : 1537002710000
     * finalPrice : 300.0
     * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
     * id : 1535249984832
     * num : 1
     * orderCode : 1537002711400
     * orderState : 待付款
     * shopName : 寇芊芊的包子店
     * type : 2
     */
    private long create;
    private double finalPrice;
    private String homeImg;
    private String imgUrl;
    private String itemName;
    private long id;
    private int num;
    private String orderCode;
    private int orderState;
    private String shopName;
    private int type;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public int getOrderState() {
        return orderState;
    }

    public void setOrderState(int orderState) {
        this.orderState = orderState;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(double finalPrice) {
        this.finalPrice = finalPrice;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }


    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.itemName);
        dest.writeString(this.imgUrl);
        dest.writeLong(this.create);
        dest.writeDouble(this.finalPrice);
        dest.writeString(this.homeImg);
        dest.writeLong(this.id);
        dest.writeInt(this.num);
        dest.writeString(this.orderCode);
        dest.writeInt(this.orderState);
        dest.writeString(this.shopName);
        dest.writeInt(this.type);
    }

    public Order() {
    }

    protected Order(Parcel in) {
        this.itemName = in.readString();
        this.imgUrl = in.readString();
        this.create = in.readLong();
        this.finalPrice = in.readDouble();
        this.homeImg = in.readString();
        this.id = in.readLong();
        this.num = in.readInt();
        this.orderCode = in.readString();
        this.orderState = in.readInt();
        this.shopName = in.readString();
        this.type = in.readInt();
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel source) {
            return new Order(source);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };
}
