package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/8/9.
 */

public class GroupBuyCoupon implements DisplayableItem {

    /**
     * availableDate : 8,9
     * buyPersonLimit : 10
     * buyPrice : 300
     * dinersNumberName : 单人餐
     * groupBuyImg : https://pdkj.oss-cn-beijing.aliyuncs.com/user/1/group_buy/1531275614261.png
     * id : 0
     * onceCount : 1
     * originalPrice : 500
     * title : 二十五元团购卷
     */

    private String availableDate;
    private int buyPersonLimit;
    private double buyPrice;
    private String dinersNumberName;
    private String groupBuyImg;
    private long id;
    private int onceCount;
    private double originalPrice;
    private String title;
    private String shopName;

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public String getDinersNumberName() {
        return dinersNumberName;
    }

    public void setDinersNumberName(String dinersNumberName) {
        this.dinersNumberName = dinersNumberName;
    }

    public String getGroupBuyImg() {
        return groupBuyImg;
    }

    public void setGroupBuyImg(String groupBuyImg) {
        this.groupBuyImg = groupBuyImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
