package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/10/13.
 */

public class ShopDetail_NavigationBean implements DisplayableItem{

    public String name;

    public ShopDetail_NavigationBean(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
