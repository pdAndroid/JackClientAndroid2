package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VerCode {

    String type;
    String phone;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
