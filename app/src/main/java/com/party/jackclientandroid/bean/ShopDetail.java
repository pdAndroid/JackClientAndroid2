package com.party.jackclientandroid.bean;

import com.party.jackclientandroid.Constant;

import java.util.List;

public class ShopDetail implements DisplayableItem {

    private int areaId;
    private double averageCons;
    private int avgScore;
    private int bussClose;
    private int bussOpen;
    private double discount;
    private int enviroScore;
    private String homeImg;
    private long id;
    private String introduce;
    private double latitude;
    private double longitude;
    private int serviceScore;
    private String shopAddress;
    private String shopName;
    private String shopPhone;
    private String rebateDiscount;
    private String symbolBuild;
    private String userConcern;
    private int tasteScore;
    private int isAttention;
    private String rebateRemark;
    private String totalComsumeNumber;
    private List<CommentsBean> comments;
    private List<CouponsBean> coupons;
    private List<GroupBuysBean> groupBuys;
    private List<ItemsBean> items;
    private List<SecondKillsBean> secondKills;
    private List<SpellingGroupsBean> spellingGroups;
    private List<ShopDetailRebateBean> rebateDiscounts;
    private String tab;

    public ShopDetail(String rebateRemark, List<ShopDetailRebateBean> rebateDiscounts) {
        this.rebateRemark = rebateRemark;
        this.rebateDiscounts = rebateDiscounts;
    }

    public String getRebateDiscount() {
        if (rebateDiscount != null) {
            if (Float.parseFloat(rebateDiscount) == 0) {
                return null;
            }
        }
        return rebateDiscount;
    }

    public void setRebateDiscount(String rebateDiscount) {
        this.rebateDiscount = rebateDiscount;
    }

    public List<ShopDetailRebateBean> getRebateDiscounts() {
        return rebateDiscounts;
    }

    public void setRebateDiscounts(List<ShopDetailRebateBean> rebateDiscounts) {
        this.rebateDiscounts = rebateDiscounts;
    }

    public String getRebateRemark() {
        return rebateRemark;
    }

    public void setRebateRemark(String rebateRemark) {
        this.rebateRemark = rebateRemark;
    }

    public String getTotalComsumeNumber() {
        return totalComsumeNumber;
    }

    public void setTotalComsumeNumber(String totalComsumeNumber) {
        this.totalComsumeNumber = totalComsumeNumber;
    }

    public int getIsAttention() {
        return isAttention;
    }

    public void setIsAttention(int isAttention) {
        this.isAttention = isAttention;
    }

    public String getUserConcern() {
        return userConcern;
    }

    public void setUserConcern(String userConcern) {
        this.userConcern = userConcern;
    }

    public String getTab() {
        return tab;
    }

    public void setTab(String tab) {
        this.tab = tab;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public double getAverageCons() {
        return averageCons;
    }

    public void setAverageCons(double averageCons) {
        this.averageCons = averageCons;
    }

    public int getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(int avgScore) {
        this.avgScore = avgScore;
    }

    public int getBussClose() {
        return bussClose;
    }

    public void setBussClose(int bussClose) {
        this.bussClose = bussClose;
    }

    public int getBussOpen() {
        return bussOpen;
    }

    public void setBussOpen(int bussOpen) {
        this.bussOpen = bussOpen;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getEnviroScore() {
        return enviroScore;
    }

    public void setEnviroScore(int enviroScore) {
        this.enviroScore = enviroScore;
    }

    public String getHomeImg() {
        return homeImg;
    }

    public void setHomeImg(String homeImg) {
        this.homeImg = homeImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getServiceScore() {
        return serviceScore;
    }

    public void setServiceScore(int serviceScore) {
        this.serviceScore = serviceScore;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public int getTasteScore() {
        return tasteScore;
    }

    public void setTasteScore(int tasteScore) {
        this.tasteScore = tasteScore;
    }

    public List<CommentsBean> getComments() {
        return comments;
    }

    public void setComments(List<CommentsBean> comments) {
        this.comments = comments;
    }

    public List<CouponsBean> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponsBean> coupons) {
        this.coupons = coupons;
    }

    public List<GroupBuysBean> getGroupBuys() {
        return groupBuys;
    }

    public void setGroupBuys(List<GroupBuysBean> groupBuys) {
        this.groupBuys = groupBuys;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public List<SecondKillsBean> getSecondKills() {
        return secondKills;
    }

    public void setSecondKills(List<SecondKillsBean> secondKills) {
        this.secondKills = secondKills;
    }

    public List<SpellingGroupsBean> getSpellingGroups() {
        return spellingGroups;
    }

    public void setSpellingGroups(List<SpellingGroupsBean> spellingGroups) {
        this.spellingGroups = spellingGroups;
    }

    public static class CommentsBean implements DisplayableItem {
        /**
         * commentImg : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1532070852279.png
         * content : 很好吃
         * create : 1530935216000
         * enviroScore : 1
         * icon : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1534152623862.png
         * id : 1
         * nickName : 大吉大利
         * orderId : 1530342500804
         * parentId : 0
         * serviceScore : 1
         * shopId : 1530523930811
         * tasteScore : 1
         * userId : 1
         */

        private String commentImg;
        private String content;
        private long create;
        private int enviroScore;
        private String icon;
        private int id;
        private String nickName;
        private long orderId;
        private int parentId;
        private int serviceScore;
        private long shopId;
        private int tasteScore;
        private int userId;

        public String getCommentImg() {
            return commentImg;
        }

        public void setCommentImg(String commentImg) {
            this.commentImg = commentImg;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public long getCreate() {
            return create;
        }

        public void setCreate(long create) {
            this.create = create;
        }

        public int getEnviroScore() {
            return enviroScore;
        }

        public void setEnviroScore(int enviroScore) {
            this.enviroScore = enviroScore;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public long getOrderId() {
            return orderId;
        }

        public void setOrderId(long orderId) {
            this.orderId = orderId;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public int getServiceScore() {
            return serviceScore;
        }

        public void setServiceScore(int serviceScore) {
            this.serviceScore = serviceScore;
        }

        public long getShopId() {
            return shopId;
        }

        public void setShopId(long shopId) {
            this.shopId = shopId;
        }

        public int getTasteScore() {
            return tasteScore;
        }

        public void setTasteScore(int tasteScore) {
            this.tasteScore = tasteScore;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }
    }

    //代金券
    public static class CouponsBean extends GoodDetail implements DisplayableItem {
        /**
         * availableDate : 1,2,3,4,5,6,7
         * buyPersonLimit : 1
         * buyPrice : 82
         * goodsRange : 全店通用
         * id : 1
         * originalPrice : 90
         */
        private String availableDate;
        private String goodsRange;
        private int saleNumber;


        public CouponsBean() {
            setGoodsType(Constant.GOODS_TYPE_REPLACE_MONEY);
            setGoodsName(getOriginalPrice() + "元代金券");
        }

        public int getSaleNumber() {
            return saleNumber;
        }

        public void setSaleNumber(int saleNumber) {
            this.saleNumber = saleNumber;
        }

        public String getAvailableDate() {
            return availableDate;
        }

        public void setAvailableDate(String availableDate) {
            this.availableDate = availableDate;
        }

        public String getGoodsRange() {
            return goodsRange;
        }

        public void setGoodsRange(String goodsRange) {
            this.goodsRange = goodsRange;
        }

    }

    //秒杀券
    public static class SecondKillsBean extends GoodDetail implements DisplayableItem {
        /**
         * buyPersonLimit : 1
         * buyPrice : 82
         * endSaleTime : 1536224919000
         * goodsRange : 部分商品
         * homeImg : https://pdkj.oss-cn-beijing.aliyuncs.com/home/home_img.jpg
         * id : 1
         * onceCount : 1
         * originalPrice : 90
         * shopId : 1530523930811
         * shopName : 寇芊芊的包子店
         * startSaleTime : 1538062562000
         * stockCount : 10
         * symbolBuild : 融信广场
         * total : 100
         */

        private long endSaleTime;
        private String goodsRange;
        private String shopName;
        private long startSaleTime;
        private int stockCount;
        private String symbolBuild;
        private int total;


        public SecondKillsBean() {
            setGoodsType(Constant.GOODS_TYPE_SECOND_KILL);
            setGoodsName(getOriginalPrice() + "元代金券");
        }

        public long getEndSaleTime() {
            return endSaleTime;
        }

        public void setEndSaleTime(long endSaleTime) {
            this.endSaleTime = endSaleTime;
        }

        public String getGoodsRange() {
            return goodsRange;
        }

        public void setGoodsRange(String goodsRange) {
            this.goodsRange = goodsRange;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public long getStartSaleTime() {
            return startSaleTime;
        }

        public void setStartSaleTime(long startSaleTime) {
            this.startSaleTime = startSaleTime;
        }

        public int getStockCount() {
            return stockCount;
        }

        public void setStockCount(int stockCount) {
            this.stockCount = stockCount;
        }

        public String getSymbolBuild() {
            return symbolBuild;
        }

        public void setSymbolBuild(String symbolBuild) {
            this.symbolBuild = symbolBuild;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }

    //拼团券
    public static class SpellingGroupsBean extends GoodDetail implements DisplayableItem {
        /**
         * buyPersonLimit : 1
         * buyPrice : 1
         * dinersNumberId : 2
         * dinersNumberName : 双人餐
         * endSaleTime : 1537833600000
         * groupBuyImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/group_buy/1534320439906_1537869403991.png
         * id : 1
         * onceCount : 1
         * originalPrice : 555
         * shopName : 寇芊芊的包子店
         * startSaleTime : 1538063355000
         * stockCount : 50
         * symbolBuild : 融信广场
         * title : 12321
         * total : 100
         * unionEndNumber : 1
         * unionStartNumber : 1
         */

        private int dinersNumberId;
        private String dinersNumberName;
        private long endSaleTime;
        private String groupBuyImg;
        private String shopName;
        private long startSaleTime;
        private int stockCount;
        private String symbolBuild;
        private String title;
        private int total;
        private int unionEndNumber;
        private int unionStartNumber;


        public SpellingGroupsBean() {
            setGoodsType(Constant.GOODS_TYPE_SPELLING_GROUP);
            setGoodsName(getOriginalPrice() + "元代金券");
        }

        public int getDinersNumberId() {
            return dinersNumberId;
        }

        public void setDinersNumberId(int dinersNumberId) {
            this.dinersNumberId = dinersNumberId;
        }

        public String getDinersNumberName() {
            return dinersNumberName;
        }

        public void setDinersNumberName(String dinersNumberName) {
            this.dinersNumberName = dinersNumberName;
        }

        public long getEndSaleTime() {
            return endSaleTime;
        }

        public void setEndSaleTime(long endSaleTime) {
            this.endSaleTime = endSaleTime;
        }

        public String getGroupBuyImg() {
            return groupBuyImg;
        }

        public void setGroupBuyImg(String groupBuyImg) {
            this.groupBuyImg = groupBuyImg;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public long getStartSaleTime() {
            return startSaleTime;
        }

        public void setStartSaleTime(long startSaleTime) {
            this.startSaleTime = startSaleTime;
        }

        public int getStockCount() {
            return stockCount;
        }

        public void setStockCount(int stockCount) {
            this.stockCount = stockCount;
        }

        public String getSymbolBuild() {
            return symbolBuild;
        }

        public void setSymbolBuild(String symbolBuild) {
            this.symbolBuild = symbolBuild;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getUnionEndNumber() {
            return unionEndNumber;
        }

        public void setUnionEndNumber(int unionEndNumber) {
            this.unionEndNumber = unionEndNumber;
        }

        public int getUnionStartNumber() {
            return unionStartNumber;
        }

        public void setUnionStartNumber(int unionStartNumber) {
            this.unionStartNumber = unionStartNumber;
        }
    }

    //套餐券
    public static class GroupBuysBean extends GoodDetail implements DisplayableItem {
        /**
         * availableDate : 1,2,3,4,5,6,7
         * buyPersonLimit : 1
         * buyPrice : 11
         * dinersNumberName : 4-5人餐
         * groupBuyImg : https://pdkj.oss-cn-beijing.aliyuncs.com/shop/group_buy/1534320439906_1537513675607.png
         * id : 2
         * onceCount : 1
         * originalPrice : 333
         * title : ASD
         */

        private String availableDate;
        private String dinersNumberName;
        private String groupBuyImg;
        private String title;
        private int stockCount;
        private int total;


        public GroupBuysBean() {
            setGoodsType(Constant.GOODS_TYPE_GROUP_BUY);
            setGoodsName(getOriginalPrice() + "元代金券");
        }

        public int getStockCount() {
            return stockCount;
        }

        public void setStockCount(int stockCount) {
            this.stockCount = stockCount;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public String getAvailableDate() {
            return availableDate;
        }

        public void setAvailableDate(String availableDate) {
            this.availableDate = availableDate;
        }

        public String getDinersNumberName() {
            return dinersNumberName;
        }

        public void setDinersNumberName(String dinersNumberName) {
            this.dinersNumberName = dinersNumberName;
        }

        public String getGroupBuyImg() {
            return groupBuyImg;
        }

        public void setGroupBuyImg(String groupBuyImg) {
            this.groupBuyImg = groupBuyImg;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }


    public static class ItemsBean {
        /**
         * id : 2
         * imgUrl : https://pdkj.oss-cn-beijing.aliyuncs.com/goods/lALPBbCc1gkm9CnNAlTNA6w_940_596.png_620x10000q90g.jpg
         * title : 马铃薯
         */

        private long id;
        private String imgUrl;
        private String title;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }


}
