package com.party.jackclientandroid.bean;

/**
 * Created by 派对 on 2018/11/23.
 */

public class RegisterInfo {

    private String wxAppId;

    private String registerKey;


    public String getRegisterKey() {
        return registerKey;
    }

    public void setRegisterKey(String registerKey) {
        this.registerKey = registerKey;
    }

    public String getWxAppId() {
        return wxAppId;
    }

    public void setWxAppId(String wxAppId) {
        this.wxAppId = wxAppId;
    }
}
