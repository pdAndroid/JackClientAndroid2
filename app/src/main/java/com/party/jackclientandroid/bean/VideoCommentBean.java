package com.party.jackclientandroid.bean;

public class VideoCommentBean {

    /**
     * commentIcon : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1534152623862.png
     * commentImg : https://pdkj.oss-cn-beijing.aliyuncs.com/user/2/food_safety_permit/1532070852279.png
     * commentNickname : 大吉大利
     * content : 很好吃
     * create : 1530935216000
     * id : 1
     * modified : 1536129490000
     * parentId : 0
     * replyIcon : https://pdkj.oss-cn-beijing.aliyuncs.com/images/http%3A//tmp/wx903822fb7d2f3961.o6zAJs05q3Yznes9aVLrDS4TDWhs.xwCaEqjH5Ztfb6d571e0aa133d2cdbb4743ee774b45c.png
     * replyNickname : 狗狗
     * state : 1
     * videoId : 1530523930811
     */

    private String commentIcon;
    private String commentImg;
    private String commentNickname;
    private String content;
    private long create;
    private long id;
    private long modified;
    private long parentId;
    private String replyIcon;
    private String replyNickname;
    private String state;
    private String videoId;

    public String getCommentIcon() {
        return commentIcon;
    }

    public void setCommentIcon(String commentIcon) {
        this.commentIcon = commentIcon;
    }

    public String getCommentImg() {
        return commentImg;
    }

    public void setCommentImg(String commentImg) {
        this.commentImg = commentImg;
    }

    public String getCommentNickname() {
        return commentNickname;
    }

    public void setCommentNickname(String commentNickname) {
        this.commentNickname = commentNickname;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getCreate() {
        return create;
    }

    public void setCreate(long create) {
        this.create = create;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getReplyIcon() {
        return replyIcon;
    }

    public void setReplyIcon(String replyIcon) {
        this.replyIcon = replyIcon;
    }

    public String getReplyNickname() {
        return replyNickname;
    }

    public void setReplyNickname(String replyNickname) {
        this.replyNickname = replyNickname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }
}
