package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2018/8/9.
 */

public class ShopClassification {

    /**
     * id : 4
     * imgUrl : http://pdkj.oss-cn-beijing.aliyuncs.com/home/shop_type/luck_bag.png
     * name : 酒店住宿
     */
    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("imgUrl")
    private String imgUrl;
    @Expose
    @SerializedName("name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
