package com.party.jackclientandroid.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/8/9.
 */

public class CouponDetail implements Serializable {

    /**
     * id : 1
     * original_price : 100
     * buy_price : 100
     * appointment : 免预约，1
     * date_start : 1531238400000
     * date_end : 1531238400000
     * time_start : -27582000
     * time_end : -27582000
     * created : 1531211292000
     * buy_person_limit : 1
     * stock_count : 1
     * once_count : 1
     * unavailable_date : 6,7
     * range_name : 全店通用
     * shop_id : 1530523930811
     * shop_name : 达吉的包子店
     * home_img : https://pdkj.oss-cn-beijing.aliyuncs.com/home/1351845737893.jpg
     * shop_address : 融信小场场
     * shop_phone : 110
     * street : 温江区
     */

    private long id;
    private double original_price;
    private double buy_price;
    private String appointment;
    private long date_start;
    private long date_end;
    private int time_start;
    private int time_end;
    private long created;
    private int buy_person_limit;
    private int stock_count;
    private int once_count;
    private String unavailable_date;
    private String range_name;
    private long shop_id;
    private String shop_name;
    private String home_img;
    private String shop_address;
    private String shop_phone;
    private String street;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getOriginal_price() {
        return original_price;
    }

    public void setOriginal_price(int original_price) {
        this.original_price = original_price;
    }

    public double getBuy_price() {
        return buy_price;
    }

    public void setBuy_price(int buy_price) {
        this.buy_price = buy_price;
    }

    public String getAppointment() {
        return appointment;
    }

    public void setAppointment(String appointment) {
        this.appointment = appointment;
    }

    public long getDate_start() {
        return date_start;
    }

    public void setDate_start(long date_start) {
        this.date_start = date_start;
    }

    public long getDate_end() {
        return date_end;
    }

    public void setDate_end(long date_end) {
        this.date_end = date_end;
    }

    public int getTime_start() {
        return time_start;
    }

    public void setTime_start(int time_start) {
        this.time_start = time_start;
    }

    public int getTime_end() {
        return time_end;
    }

    public void setTime_end(int time_end) {
        this.time_end = time_end;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public int getBuy_person_limit() {
        return buy_person_limit;
    }

    public void setBuy_person_limit(int buy_person_limit) {
        this.buy_person_limit = buy_person_limit;
    }

    public int getStock_count() {
        return stock_count;
    }

    public void setStock_count(int stock_count) {
        this.stock_count = stock_count;
    }

    public int getOnce_count() {
        return once_count;
    }

    public void setOnce_count(int once_count) {
        this.once_count = once_count;
    }

    public String getUnavailable_date() {
        return unavailable_date;
    }

    public void setUnavailable_date(String unavailable_date) {
        this.unavailable_date = unavailable_date;
    }

    public String getRange_name() {
        return range_name;
    }

    public void setRange_name(String range_name) {
        this.range_name = range_name;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getHome_img() {
        return home_img;
    }

    public void setHome_img(String home_img) {
        this.home_img = home_img;
    }

    public String getShop_address() {
        return shop_address;
    }

    public void setShop_address(String shop_address) {
        this.shop_address = shop_address;
    }

    public String getShop_phone() {
        return shop_phone;
    }

    public void setShop_phone(String shop_phone) {
        this.shop_phone = shop_phone;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
