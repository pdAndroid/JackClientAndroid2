package com.party.jackclientandroid.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/8/19.
 */

public class RecommendVegetableListBean implements DisplayableItem{
    private List<ShopDetail.ItemsBean> itemsBeanList;

    public List<ShopDetail.ItemsBean> getItemsBeanList() {
        return itemsBeanList;
    }

    public void setItemsBeanList(List<ShopDetail.ItemsBean> itemsBeanList) {
        this.itemsBeanList = itemsBeanList;
    }
}
