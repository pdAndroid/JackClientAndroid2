package com.party.jackclientandroid.bean;

import android.view.ViewGroup;
import android.widget.RadioGroup;

/**
 * Created by 南宫灬绝痕 on 2018/12/13.
 */

public class RebateHeader implements DisplayableItem {
    String discount;
    private int currentWeek;
    private RadioGroup.OnCheckedChangeListener listener;

    public RebateHeader(String discount) {
        this.discount = discount;
    }

    public RebateHeader() {
        System.out.println();
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }


    public void setRadioOnClickListener(RadioGroup.OnCheckedChangeListener listener) {
        this.listener = listener;
    }

    public RadioGroup.OnCheckedChangeListener getRadioListener() {
        return listener;
    }

    public int getCurrentWeek() {
        return currentWeek;
    }

    public void setCurrentWeek(int currentWeek) {
        this.currentWeek = currentWeek;
    }

}
