package com.party.jackclientandroid.bean;

import java.util.List;

/**
 * Created by Administrator on 2018/9/27.
 */

public class SpellingGroupCouponDetail {


    private int appointment;
    private int buyPersonLimit;
    private String buyPrice;
    private long dateEnd;
    private long dateStart;
    private String dinersNumberName;
    private long endSaleTime;
    private String groupBuyImg;
    private long id;
    private int onceCount;
    private String originalPrice;
    private String ruleContent;
    private String shopAddress;
    private long shopId;
    private String shopName;
    private String shopPhone;
    private long startSaleTime;
    private int stockCount;
    private String symbolBuild;
    private long timeEnd;
    private long timeStart;
    private String title;
    private String ruleSplicing;
    private int unionEndNumber;
    private int unionStartNumber;
    private List<ItemsBean> items;

    public String getRuleSplicing() {
        return ruleSplicing;
    }

    public void setRuleSplicing(String ruleSplicing) {
        this.ruleSplicing = ruleSplicing;
    }

    public int getAppointment() {
        return appointment;
    }

    public void setAppointment(int appointment) {
        this.appointment = appointment;
    }

    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public String getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        this.buyPrice = buyPrice;
    }

    public long getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(long dateEnd) {
        this.dateEnd = dateEnd;
    }

    public long getDateStart() {
        return dateStart;
    }

    public void setDateStart(long dateStart) {
        this.dateStart = dateStart;
    }

    public String getDinersNumberName() {
        return dinersNumberName;
    }

    public void setDinersNumberName(String dinersNumberName) {
        this.dinersNumberName = dinersNumberName;
    }

    public long getEndSaleTime() {
        return endSaleTime;
    }

    public void setEndSaleTime(long endSaleTime) {
        this.endSaleTime = endSaleTime;
    }

    public String getGroupBuyImg() {
        return groupBuyImg;
    }

    public void setGroupBuyImg(String groupBuyImg) {
        this.groupBuyImg = groupBuyImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public String getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(String originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getRuleContent() {
        return ruleContent;
    }

    public void setRuleContent(String ruleContent) {
        this.ruleContent = ruleContent;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public long getShopId() {
        return shopId;
    }

    public void setShopId(long shopId) {
        this.shopId = shopId;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopPhone() {
        return shopPhone;
    }

    public void setShopPhone(String shopPhone) {
        this.shopPhone = shopPhone;
    }

    public long getStartSaleTime() {
        return startSaleTime;
    }

    public void setStartSaleTime(long startSaleTime) {
        this.startSaleTime = startSaleTime;
    }

    public int getStockCount() {
        return stockCount;
    }

    public void setStockCount(int stockCount) {
        this.stockCount = stockCount;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public long getTimeEnd() {
        return timeEnd;
    }

    public void setTimeEnd(long timeEnd) {
        this.timeEnd = timeEnd;
    }

    public long getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(long timeStart) {
        this.timeStart = timeStart;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUnionEndNumber() {
        return unionEndNumber;
    }

    public void setUnionEndNumber(int unionEndNumber) {
        this.unionEndNumber = unionEndNumber;
    }

    public int getUnionStartNumber() {
        return unionStartNumber;
    }

    public void setUnionStartNumber(int unionStartNumber) {
        this.unionStartNumber = unionStartNumber;
    }

    public List<ItemsBean> getItems() {
        return items;
    }

    public void setItems(List<ItemsBean> items) {
        this.items = items;
    }

    public static class ItemsBean {
        /**
         * count : 1
         * id : 2
         * imgUrl : https://pdkj.oss-cn-beijing.aliyuncs.com/goods/lALPBbCc1gkm9CnNAlTNA6w_940_596.png_620x10000q90g.jpg
         * title : 马铃薯
         * unitName : 份
         */

        private int count;
        private long id;
        private String imgUrl;
        private String title;
        private String unitName;
        private double price;

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }
    }
}
