package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/9/27.
 */

public class SpellingGroupCouponBean {

    private int buyPersonLimit;
    private double buyPrice;
    private int dinersNumberId;
    private String dinersNumberName;
    private long endSaleTime;
    private String groupBuyImg;
    private long id;
    private int onceCount;
    private double originalPrice;
    private String shopName;
    private long startSaleTime;
    private int stockCount;
    private String symbolBuild;
    private String title;
    private int total;
    private int unionEndNumber;
    private int unionStartNumber;


    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public int getDinersNumberId() {
        return dinersNumberId;
    }

    public void setDinersNumberId(int dinersNumberId) {
        this.dinersNumberId = dinersNumberId;
    }

    public String getDinersNumberName() {
        return dinersNumberName;
    }

    public void setDinersNumberName(String dinersNumberName) {
        this.dinersNumberName = dinersNumberName;
    }

    public long getEndSaleTime() {
        return endSaleTime;
    }

    public void setEndSaleTime(long endSaleTime) {
        this.endSaleTime = endSaleTime;
    }

    public String getGroupBuyImg() {
        return groupBuyImg;
    }

    public void setGroupBuyImg(String groupBuyImg) {
        this.groupBuyImg = groupBuyImg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public long getStartSaleTime() {
        return startSaleTime;
    }

    public void setStartSaleTime(long startSaleTime) {
        this.startSaleTime = startSaleTime;
    }

    public int getStockCount() {
        return stockCount;
    }

    public void setStockCount(int stockCount) {
        this.stockCount = stockCount;
    }

    public String getSymbolBuild() {
        return symbolBuild;
    }

    public void setSymbolBuild(String symbolBuild) {
        this.symbolBuild = symbolBuild;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getUnionEndNumber() {
        return unionEndNumber;
    }

    public void setUnionEndNumber(int unionEndNumber) {
        this.unionEndNumber = unionEndNumber;
    }

    public int getUnionStartNumber() {
        return unionStartNumber;
    }

    public void setUnionStartNumber(int unionStartNumber) {
        this.unionStartNumber = unionStartNumber;
    }
}
