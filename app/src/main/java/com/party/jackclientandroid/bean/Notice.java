package com.party.jackclientandroid.bean;

/**
 * Created by 南宫灬绝痕 on 2019/1/9.
 */

public class Notice {

    /**
     * content :
     * img :
     * title :
     */

    private String content;
    private String img;
    private String title;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
