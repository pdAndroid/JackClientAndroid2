package com.party.jackclientandroid.bean;

/**
 * Created by 南宫灬绝痕 on 2018/12/19.
 */

public class DetailsDiscountPaymentBean {

    /**
     * value : 1
     * tradeType : 1
     * create : 1
     * tradeName :
     */

    private String value;
    private String tradeType;
    private String create;
    private String tradeName;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public String getTradeName() {
        return tradeName;
    }

    public void setTradeName(String tradeName) {
        this.tradeName = tradeName;
    }
}
