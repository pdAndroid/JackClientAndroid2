package com.party.jackclientandroid.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by 南宫灬绝痕 on 2018/12/20.
 */

public class ShopDetailRebateBean implements DisplayableItem {

    private String startTimeAxis;
    private String endTimeAxis;
    private String discount;
    private String remarks;

    @JSONField(serialize = false)
    private int week;

    @JSONField(serialize = false)
    private ShopDetailRebateBean rightShopDetailRebateBean;

    @JSONField(serialize = false)
    private boolean defaultSet = false;
    String title;

    public boolean isDefaultSet() {
        return defaultSet;
    }

    public void setDefaultSet(boolean defaultSet) {
        this.defaultSet = defaultSet;
    }

    public ShopDetailRebateBean getRightShopDetailRebateBean() {
        return rightShopDetailRebateBean;
    }

    public void setRightShopDetailRebateBean(ShopDetailRebateBean rightShopDetailRebateBean) {
        this.rightShopDetailRebateBean = rightShopDetailRebateBean;
    }

    public ShopDetailRebateBean() {
    }

    public static ShopDetailRebateBean getDefault(String discount) {
        ShopDetailRebateBean bean = new ShopDetailRebateBean();
        bean.setDefaultSet(true);
        bean.setDiscount(discount);
        return bean;

    }

    public ShopDetailRebateBean(String startTimeAxis, String endTimeAxis, String discount, String title) {
        this.startTimeAxis = startTimeAxis;
        this.endTimeAxis = endTimeAxis;
        this.discount = discount;
        this.title = title;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    public int getWeek() {
        return Integer.parseInt(startTimeAxis) / (24 * 60);
    }

    public String getStartTimeAxis() {
        return startTimeAxis;
    }

    public void setStartTimeAxis(String startTimeAxis) {
        this.startTimeAxis = startTimeAxis;
    }

    public String getEndTimeAxis() {
        return endTimeAxis;
    }

    public void setEndTimeAxis(String endTimeAxis) {
        this.endTimeAxis = endTimeAxis;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
