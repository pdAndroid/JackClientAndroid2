package com.party.jackclientandroid.bean;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WxPaymentBean {
    @Expose
    @SerializedName("appId")
    private String appId;
    @Expose
    @SerializedName("partnerId")
    private String partnerId;
    @Expose
    @SerializedName("prepayId")
    private String prepayId;
    @Expose
    @SerializedName("packageValue")
    private String packageValue;
    @Expose
    @SerializedName("nonceStr")
    private String nonceStr;
    @Expose
    @SerializedName("timeStamp")
    private String timeStamp;
    @Expose
    @SerializedName("sign")
    private String sign;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public String getPackageValue() {
        return packageValue;
    }

    public void setPackageValue(String packageValue) {
        this.packageValue = packageValue;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
