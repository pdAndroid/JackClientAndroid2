package com.party.jackclientandroid.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/10/18.
 */

public class SearchMenuBean implements Serializable{

    /**
     * id : 3
     * title : 分类
     * value : 1
     */

    private int id;
    private String title;
    private String value;
    private boolean isSelected;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
