package com.party.jackclientandroid.bean;

public class ReplaceMoneyCoupon implements DisplayableItem {

    /**
     * availableDate : 6,7
     * buyPersonLimit : 1
     * buyPrice : 80
     * id : 1
     * originalPrice : 100
     */

    private String availableDate;
    private int buyPersonLimit;
    private double buyPrice;
    private long id;
    private double originalPrice;
    private int count;
    private int onceCount;
    private String goodsRange;
    private String shopName;

    public String getGoodsRange() {
        return goodsRange;
    }

    public void setGoodsRange(String goodsRange) {
        this.goodsRange = goodsRange;
    }

    public int getOnceCount() {
        return onceCount;
    }

    public void setOnceCount(int onceCount) {
        this.onceCount = onceCount;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public int getBuyPersonLimit() {
        return buyPersonLimit;
    }

    public void setBuyPersonLimit(int buyPersonLimit) {
        this.buyPersonLimit = buyPersonLimit;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }
}
