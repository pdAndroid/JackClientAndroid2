package com.party.jackclientandroid.bean;

/**
 * Created by Administrator on 2018/9/27.
 */
public class BlackCardItem extends SecondKillCouponBean {

    //黑卡专区使用
    private int itemSalesSum;
    private String title;
    private double latitude;
    private double longitude;
    private String couponImg;

    public String getCouponImg() {
        return couponImg;
    }

    public void setCouponImg(String couponImg) {
        this.couponImg = couponImg;
    }

    public int getItemSalesSum() {
        return itemSalesSum;
    }

    public void setItemSalesSum(int itemSalesSum) {
        this.itemSalesSum = itemSalesSum;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
