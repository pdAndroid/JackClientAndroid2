package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.GroupTitleBean;
import com.party.jackclientandroid.ui_home.activity.ReplyListActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

public class ShopDetail_GroupHeadItem implements ItemViewDelegate<DisplayableItem> {

    private Context context;

    public ShopDetail_GroupHeadItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_group_head;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof GroupTitleBean;
    }

    @Override
    public void convert(ViewHolder holder, final DisplayableItem item, int position) {
        GroupTitleBean groupTitleBean = (GroupTitleBean)item;
        holder.setText(R.id.tv_time, groupTitleBean.getGroupName());
        holder.setVisible(R.id.iv_right, groupTitleBean.isShowRightArrow());
        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((GroupTitleBean) item).getGroupName().equals("用户评价")){
                    /*Intent intent = new Intent(context, ReplyListActivity.class);
                    intent.putExtra("shopId",((ShopDetailActivity)context).getShopId());
                    intent.putExtra("orderId",userCommentAndReply.getOrderId());
                    context.startActivity(intent);*/
                }
            }
        });
    }
}
