package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Application;
import android.content.Context;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.HallMeal;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by 南宫灬绝痕 on 2018/12/13.
 */

public class ShopDetail_HallMeal implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private Application application;

    public ShopDetail_HallMeal(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_hallmeal;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof HallMeal;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem displayableItem, int position) {
        final TextView tab_1 = holder.getView(R.id.discount_tv);
    }
}
