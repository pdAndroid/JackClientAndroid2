package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopDetail_NavigationBean;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.utils.DensityUtils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

public class ShopDetail_NavigationItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;

    public ShopDetail_NavigationItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_shop_detail_navigation;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail_NavigationBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final TextView tab_1 = holder.getView(R.id.tab_1);
        final TextView tab_2 = holder.getView(R.id.tab_2);
        final TextView tab_3 = holder.getView(R.id.tab_3);
        holder.setOnClickListener(R.id.tab_1, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tab_1.setTextColor(context.getResources().getColor(R.color.color_f89f03));
                tab_2.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));
                tab_3.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));

                tab_1.setBackground(context.getResources().getDrawable(R.drawable.shape_bottom_line));
                tab_2.setBackground(null);
                tab_3.setBackground(null);
                ((ShopDetailActivity)context).clickTab(1);
                ((ShopDetailActivity)context).smoothMoveToPosition(((ShopDetailActivity)context).getmRecyclerView(),3);
            }
        });
        holder.setOnClickListener(R.id.tab_2, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tab_2.setBackground(context.getResources().getDrawable(R.drawable.shape_bottom_line));
                tab_1.setBackground(null);
                tab_3.setBackground(null);

                tab_2.setTextColor(context.getResources().getColor(R.color.color_f89f03));
                tab_1.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));
                tab_3.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));
                ((ShopDetailActivity)context).clickTab(2);
                ((ShopDetailActivity)context).smoothMoveToPosition(((ShopDetailActivity)context).getmRecyclerView(),((ShopDetailActivity)context).getCouponCount()+4);

            }
        });
        holder.setOnClickListener(R.id.tab_3, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tab_3.setBackground(context.getResources().getDrawable(R.drawable.shape_bottom_line));
                tab_1.setBackground(null);
                tab_2.setBackground(null);

                tab_3.setTextColor(context.getResources().getColor(R.color.color_f89f03));
                tab_1.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));
                tab_2.setTextColor(context.getResources().getColor(R.color.color_8a8a8a));
                ((ShopDetailActivity)context).clickTab(3);
                ((ShopDetailActivity)context).smoothMoveToPosition(((ShopDetailActivity)context).getmRecyclerView(),((ShopDetailActivity)context).getCouponCount()+4+((ShopDetailActivity)context).getCommentCount());
            }
        });
    }
}
