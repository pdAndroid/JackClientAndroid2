package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.RebateHeader;
import com.party.jackclientandroid.utils.Utils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import butterknife.BindView;

/**
 * Created by 南宫灬绝痕 on 2018/12/22.
 * 商家详情-消费返利
 */

public class ShopDetail_ConsumerRebateHeader implements ItemViewDelegate<DisplayableItem> {
    @BindView(R.id.tv_shop_detail_counsumer_rabate_title)
    TextView tvShopDetailCounsumerRabateTitle;
    @BindView(R.id.commit_dis_tv)
    TextView commitDisTv;
    @BindView(R.id.rb_shop_detail_consumer_rebate_one)
    RadioButton rbShopDetailConsumerRebateOne;
    @BindView(R.id.rb_shop_detail_consumer_rebate_two)
    RadioButton rbShopDetailConsumerRebateTwo;
    @BindView(R.id.rb_shop_detail_consumer_rebate_three)
    RadioButton rbShopDetailConsumerRebateThree;
    @BindView(R.id.rb_shop_detail_consumer_rebate_four)
    RadioButton rbShopDetailConsumerRebateFour;
    @BindView(R.id.rb_shop_detail_consumer_rebate_five)
    RadioButton rbShopDetailConsumerRebateFive;
    @BindView(R.id.rb_shop_detail_consumer_rebate_six)
    RadioButton rbShopDetailConsumerRebateSix;
    @BindView(R.id.rb_shop_detail_consumer_rebate_seven)
    RadioButton rbShopDetailConsumerRebateSeven;
    @BindView(R.id.week_radio_group)
    RadioGroup weekRadioGroup;
    private Context context;
    private boolean isInit = true;//只调用一次

    public ShopDetail_ConsumerRebateHeader(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_shop_detail_counsumer_rebate_head;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof RebateHeader;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem displayableItem, int position) {
        RebateHeader rebateHeader = (RebateHeader) displayableItem;
        RadioGroup view = holder.getView(R.id.week_radio_group);
        if (isInit) {//此处代码只执行一次
            if (rebateHeader.getDiscount() != null) {
                holder.setText(R.id.commit_dis_tv, "(其他时段返利" + Utils.changeStr100(rebateHeader.getDiscount()) + "%)");
            }

            RadioButton childAt = (RadioButton) view.getChildAt(rebateHeader.getCurrentWeek());
            childAt.setText("今天");
            childAt.setChecked(true);
            isInit = false;
        }
        view.setOnCheckedChangeListener(rebateHeader.getRadioListener());
    }
}
