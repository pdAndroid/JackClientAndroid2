package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.ui_home.activity.GroupBuyCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * 套餐券Item
 */
public class ShopDetail_GroupBuyItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private MApplication application;

    public ShopDetail_GroupBuyItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_group_buy_coupon;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail.GroupBuysBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final ShopDetail.GroupBuysBean groupBuysBean = (ShopDetail.GroupBuysBean) item;
        holder.setText(R.id.tv_group_buy_coupon_price, "¥" + groupBuysBean.getBuyPrice());
        holder.setText(R.id.tv_group_buy_coupon_name, groupBuysBean.getTitle());
        holder.setText(R.id.tv_replace_money_coupon_sale, "销量：" + (groupBuysBean.getTotal() - groupBuysBean.getStockCount()));
        TextView view = holder.getView(R.id.tv_group_buy_coupon_oringeprice);
        view.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        view.setText("¥" + groupBuysBean.getOriginalPrice());
        holder.setText(R.id.tv_group_buy_coupon_meum, CommonUtil.getAvailableDate(groupBuysBean.getAvailableDate()));
        ((ShopDetailActivity) context).getmApplication().getImageLoaderFactory().loadCommonImgByUrl((Activity) context, groupBuysBean.getGroupBuyImg(), (ImageView) holder.getView(R.id.iv_group_buy_coupon));
        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GroupBuyCouponDetailActivity.class);
                intent.putExtra("couponId", groupBuysBean.getId());
                context.startActivity(intent);
            }
        });
        holder.setOnClickListener(R.id.btn_group_buy_coupon_buy, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", groupBuysBean);
                context.startActivity(intent);
            }
        });
    }
}
