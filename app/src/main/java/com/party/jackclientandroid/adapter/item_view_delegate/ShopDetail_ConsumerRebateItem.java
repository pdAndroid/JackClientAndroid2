package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DiscountPayBillBean;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopDetailRebateBean;
import com.party.jackclientandroid.utils.Utils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by 南宫灬绝痕 on 2018/12/22.
 */

public class ShopDetail_ConsumerRebateItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;

    public ShopDetail_ConsumerRebateItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_shop_detail_counsumer_rebate;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetailRebateBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem displayableItem, int position) {
        ShopDetailRebateBean bean = (ShopDetailRebateBean) displayableItem;

        if (!bean.isDefaultSet()) {//是否是默认设置
            holder.setText(R.id.tv_shop_detail_consumer_rebate_ratio_one, "优惠返利" + Utils.changeStr100(bean.getDiscount()) + "%");
            holder.setText(R.id.tv_shop_detail_consumer_rebate_time_one, changeToTime(bean));
            ShopDetailRebateBean rightBean = bean.getRightShopDetailRebateBean();
            if (rightBean != null) {
                holder.setText(R.id.tv_shop_detail_consumer_rebate_ratio_two, "优惠返利" + Utils.changeStr100(rightBean.getDiscount()) + "%");
                holder.setText(R.id.tv_shop_detail_consumer_rebate_time_two, changeToTime(rightBean));
            }
        } else {
            holder.setText(R.id.tv_shop_detail_consumer_rebate_ratio_one, "优惠返利" + Utils.changeStr100(bean.getDiscount()) + "%");
            holder.setText(R.id.tv_shop_detail_consumer_rebate_time_one, changeToTime(bean));
        }
    }

    public String changeToTime(ShopDetailRebateBean bean) {
        return "时间：" + changeTime(bean.getStartTimeAxis()) + "~" + changeTime(bean.getEndTimeAxis());
    }

    public String changeTime(String stampStr) {
        int stamp = Integer.parseInt(stampStr);
        int time = stamp % (24 * 60);
        int hour = time / 60;
        int min = time % 60;
        return (hour > 9 ? hour : "0" + hour) + ":" + (min > 9 ? min : "0" + min);
    }
}
