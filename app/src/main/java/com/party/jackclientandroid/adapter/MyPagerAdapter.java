package com.party.jackclientandroid.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.VipBean;
import com.party.jackclientandroid.ui_mine.partner_center.VipCenterActivity;

import java.util.List;

public class MyPagerAdapter extends PagerAdapter {

    private List<String> mDataList;
    private BaseActivity mContext;
    private View.OnClickListener mListener;

    public MyPagerAdapter(List<String> dataList, BaseActivity context) {
        mDataList = dataList;
        mContext = context;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View inflate = LayoutInflater.from(container.getContext()).inflate(R.layout.cardviewpager_item, container, false);
        ImageView img = inflate.findViewById(R.id.vip_card_view_status_iv);
        mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, mDataList.get(position), img);
        img.setOnClickListener((View v) -> {
            v.setTag(position);
            mListener.onClick(v);
        });
        container.addView(inflate);
        return inflate;
    }

    public void setOnItemClickListener(View.OnClickListener listener) {
        mListener = listener;
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View) object));
    }
}
