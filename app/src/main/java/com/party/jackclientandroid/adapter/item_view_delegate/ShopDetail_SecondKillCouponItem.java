package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.GoodDetail;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.controller.ConvertController;
import com.party.jackclientandroid.ui_home.activity.SecondKillCouponDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.TimeUtil;
import com.party.jackclientandroid.utils.Utils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import cn.iwgang.countdownview.CountdownView;

/**
 * 秒杀券Item
 */
public class ShopDetail_SecondKillCouponItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private MApplication application;

    public ShopDetail_SecondKillCouponItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.delegate_second_kill_coupon;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail.SecondKillsBean;
    }


    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final ShopDetail.SecondKillsBean secondKillCouponBean = (ShopDetail.SecondKillsBean) item;
        holder.setText(R.id.tv_replace_money_coupon_price, "¥" + secondKillCouponBean.getBuyPrice());
        holder.setText(R.id.tv_replace_money_coupon_name, Utils.clearZero(secondKillCouponBean.getOriginalPrice()) + "代金券");
        holder.setText(R.id.tv_replace_money_coupon_time, secondKillCouponBean.getGoodsRange());
//        holder.setText(R.id.remain_count_tv, "剩余" + secondKillCouponBean.getStockCount() + "张");
        //倒计时
        CountdownView countdownView = holder.getView(R.id.count_down_view);
        LinearLayout remain_time_ll = holder.getView(R.id.remain_time_ll);
        TextView remain_time_tv = holder.getView(R.id.remain_time_tv);
        long currTimeStamp = TimeUtil.getCurrTimeStamp();
        long endTimeStamp = secondKillCouponBean.getEndSaleTime();
        String result = TimeUtil.getDistanceTime(currTimeStamp, endTimeStamp);
        if (result.equals("不到一天")) {
            countdownView.start(endTimeStamp - currTimeStamp);
            remain_time_ll.setVisibility(View.VISIBLE);
            remain_time_tv.setVisibility(View.GONE);
        } else {
            remain_time_tv.setText(result);
            remain_time_tv.setVisibility(View.VISIBLE);
            remain_time_ll.setVisibility(View.GONE);
        }

        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SecondKillCouponDetailActivity.class);
                intent.putExtra("couponId", secondKillCouponBean.getId());
                context.startActivity(intent);
            }
        });
        holder.setOnClickListener(R.id.btn_replace_money_coupon_buy, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", secondKillCouponBean);
                context.startActivity(intent);
            }
        });
    }
}
