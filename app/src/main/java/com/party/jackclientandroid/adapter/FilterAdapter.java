package com.party.jackclientandroid.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.FilterCondition;

import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private Context mContext;
    private List<FilterCondition> mDatas;
    private List<FilterCondition> searchCondition;
    private View.OnClickListener itemListener;

    public FilterAdapter(Context context, List<FilterCondition> filterConditionList, List<FilterCondition> searchCondition) {
        this.mContext = context;
        this.mDatas = filterConditionList;
        this.searchCondition = searchCondition;
        inflater = LayoutInflater.from(context);
    }

    //创建ViewHolder
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_filter_grid_item, parent, false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    //绑定ViewHolder
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //为textview 赋值
        FilterCondition itemFilter = mDatas.get(position);
        holder.tv.setText(mDatas.get(position).getTitle());
        holder.tv.setTag(position);
        if (searchCondition.size() > 0 && searchCondition.get(0).getId() == itemFilter.getId()) {
            holder.tv.setSelected(true);
            holder.tv.setTextColor(Color.WHITE);
        } else {
            holder.tv.setSelected(false);
            holder.tv.setTextColor(Color.BLACK);
        }
        holder.tv.setOnClickListener(itemListener);
    }

    public void setItemListener(View.OnClickListener listener) {
        itemListener = listener;
    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv;

        public MyViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.recycle_tv);
        }
    }

}


