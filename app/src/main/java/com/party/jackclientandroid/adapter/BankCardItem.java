package com.party.jackclientandroid.adapter;

import android.app.Activity;
import android.text.TextUtils;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.BankCard;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

public class BankCardItem implements ItemViewDelegate<BankCard> {
    Activity mActivity;

    public BankCardItem(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_bank_card;
    }

    @Override
    public boolean isForViewType(BankCard item, int position) {
        return !TextUtils.isEmpty(item.getName());
    }

    @Override
    public void convert(ViewHolder holder, BankCard item, int position) {

    }
}
