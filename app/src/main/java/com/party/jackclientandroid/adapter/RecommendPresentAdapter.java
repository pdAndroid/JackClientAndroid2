package com.party.jackclientandroid.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.RankingBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 南宫灬绝痕 on 2019/1/3.
 */

public class RecommendPresentAdapter extends RecyclerView.Adapter<RecommendPresentAdapter.ViewHolder> {
    private Context context;
    private List<RankingBean> data;

    public RecommendPresentAdapter(Context context, List<RankingBean> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = View.inflate(context, R.layout.item_recommend_present, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == 0) {
            Glide.with(context).load(data.get(0).getIcon()).into(holder.ivRecommendCourteousHeadimgBg);
            holder.tvRecommendCourteousName.setText(data.get(0).getNickname() + "");
            holder.tvRecommendCourteousDividendSum.setText(data.get(0).getDividendSum() + "");
            holder.tvRecommendCourteousInvitationRewardsSum.setText(data.get(0).getInvitationRewardsSum() + "");
            holder.tvRecommendCourteousMembershipRewardsSum.setText(data.get(0).getMembershipRewardsSum() + "");
            holder.tvRecommendCourteousName.setTextColor(context.getResources().getColor(R.color.orange_fe));
            holder.ivRecommendCourteousRanking.setVisibility(View.VISIBLE);
            holder.ivRecommendCourteousRanking.setBackgroundResource(R.mipmap.iv_recommend_present_number_one);
        } else if (position == 1) {
            Glide.with(context).load(data.get(1).getIcon()).into(holder.ivRecommendCourteousHeadimgBg);
            holder.tvRecommendCourteousName.setText(data.get(1).getNickname() + "");
            holder.tvRecommendCourteousDividendSum.setText(data.get(1).getDividendSum() + "");
            holder.tvRecommendCourteousInvitationRewardsSum.setText(data.get(1).getInvitationRewardsSum() + "");
            holder.tvRecommendCourteousMembershipRewardsSum.setText(data.get(1).getMembershipRewardsSum() + "");
            holder.tvRecommendCourteousName.setTextColor(context.getResources().getColor(R.color.yellow_ff));
            holder.ivRecommendCourteousRanking.setVisibility(View.VISIBLE);
            holder.ivRecommendCourteousRanking.setBackgroundResource(R.mipmap.iv_recommend_present_number_two);
        } else if (position == 2) {
            Glide.with(context).load(data.get(0).getIcon()).into(holder.ivRecommendCourteousHeadimgBg);
            holder.tvRecommendCourteousName.setText(data.get(2).getNickname() + "");
            holder.tvRecommendCourteousDividendSum.setText(data.get(2).getDividendSum() + "");
            holder.tvRecommendCourteousInvitationRewardsSum.setText(data.get(2).getInvitationRewardsSum() + "");
            holder.tvRecommendCourteousMembershipRewardsSum.setText(data.get(2).getMembershipRewardsSum() + "");
            holder.tvRecommendCourteousName.setTextColor(context.getResources().getColor(R.color.blue_56));
            holder.ivRecommendCourteousRanking.setVisibility(View.VISIBLE);
            holder.ivRecommendCourteousRanking.setBackgroundResource(R.mipmap.iv_recommend_present_number_three);
        } else {
//        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, item.getImgUrl(), (ImageView) viewHolder.getView(R.id.coupon_image_iv));
            Glide.with(context).load(data.get(position).getIcon()).into(holder.ivRecommendCourteousHeadimgBg);
            holder.tvRecommendCourteousName.setText(data.get(position).getNickname() + "");
            holder.tvRecommendCourteousDividendSum.setText(data.get(position).getDividendSum() + "");
            holder.tvRecommendCourteousInvitationRewardsSum.setText(data.get(position).getInvitationRewardsSum() + "");
            holder.tvRecommendCourteousMembershipRewardsSum.setText(data.get(position).getMembershipRewardsSum() + "");
            holder.ivRecommendCourteousRanking.setVisibility(View.GONE);
            holder.tvRecommendCourteousRanking.setVisibility(View.VISIBLE);
            switch (position) {
                case 3:
                    holder.tvRecommendCourteousRanking.setText("第四名");
                    break;
                case 4:
                    holder.tvRecommendCourteousRanking.setText("第五名");
                    break;
                case 5:
                    holder.tvRecommendCourteousRanking.setText("第六名");
                    break;
                case 6:
                    holder.tvRecommendCourteousRanking.setText("第七名");
                    break;
                case 7:
                    holder.tvRecommendCourteousRanking.setText("第八名");
                    break;
                case 8:
                    holder.tvRecommendCourteousRanking.setText("第九名");
                    break;
                case 9:
                    holder.tvRecommendCourteousRanking.setText("第十名");
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_recommend_courteous_headimg_bg)
        ImageView ivRecommendCourteousHeadimgBg;
        @BindView(R.id.iv_recommend_courteous_headimg_front)
        ImageView ivRecommendCourteousHeadimgFront;
        @BindView(R.id.tv_recommend_courteous_name)
        TextView tvRecommendCourteousName;
        @BindView(R.id.tv_recommend_courteous_invitationRewardsSum)
        TextView tvRecommendCourteousInvitationRewardsSum;
        @BindView(R.id.tv_recommend_courteous_membershipRewardsSum)
        TextView tvRecommendCourteousMembershipRewardsSum;
        @BindView(R.id.tv_recommend_courteous_dividendSum)
        TextView tvRecommendCourteousDividendSum;
        @BindView(R.id.iv_recommend_courteous_ranking)
        ImageView ivRecommendCourteousRanking;
        @BindView(R.id.tv_recommend_courteous_ranking)
        TextView tvRecommendCourteousRanking;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
