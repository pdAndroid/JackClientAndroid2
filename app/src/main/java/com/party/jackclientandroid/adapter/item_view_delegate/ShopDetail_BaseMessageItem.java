package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopBaseMessage;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.utils.TimeUtil;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by Administrator on 2018/9/9.
 * 商家详情-商家信息
 */

public class ShopDetail_BaseMessageItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private int time;

    public ShopDetail_BaseMessageItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_shop_base_message;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        ShopDetail shopDetail = (ShopDetail) item;
        holder.setText(R.id.shop_name_tv, shopDetail.getShopName());
        holder.setText(R.id.shop_type_tv, shopDetail.getTab());
        holder.setText(R.id.shop_phone_tv, shopDetail.getShopPhone());
        time = shopDetail.getBussOpen();
        if (shopDetail.getBussOpen() == shopDetail.getBussClose()) {
            holder.setText(R.id.business_time_tv, "24小时营业");
        } else {
            holder.setText(R.id.business_time_tv, TimeUtil.timeAnalyze(shopDetail.getBussOpen()) + " - " + TimeUtil.timeAnalyze(shopDetail.getBussClose()));
        }
    }
}
