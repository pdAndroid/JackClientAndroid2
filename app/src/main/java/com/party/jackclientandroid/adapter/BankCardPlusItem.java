package com.party.jackclientandroid.adapter;

import android.app.Activity;
import android.text.TextUtils;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.BankCard;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

public class BankCardPlusItem implements ItemViewDelegate<BankCard> {
    Activity mActivity;

    public BankCardPlusItem(Activity mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_bank_card_plus;
    }

    @Override
    public boolean isForViewType(BankCard item, int position) {
        return TextUtils.isEmpty(item.getName());
    }

    @Override
    public void convert(ViewHolder holder, BankCard item, int position) {

    }
}
