package com.party.jackclientandroid.adapter;

import android.app.Activity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackclientandroid.photoview.OnPhotoTapListener;
import com.party.jackclientandroid.photoview.PhotoView;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.event.OnPhotoTapEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;


/**
 * 照片详情适配器<br>
 */

public class PictureDetailAdapter extends PagerAdapter {
    private Activity mContext;
    /**
     * 图片地址集合
     */
    private List<ImageBean> mList;
    /**
     * PhotoView集合，有多少个图片就创建多少个PhotoView。
     */
    private List<PhotoView> mPhoto = new ArrayList<>();

    /**
     * 构造方法，初始化适配器
     *
     * @param mContext
     * @param mList
     */
    public PictureDetailAdapter(Activity mContext, List<ImageBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
        initPhoto();
    }

    RequestOptions ro1 = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_img_load_fail);
    RequestOptions ro2 = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .placeholder(R.drawable.ic_image)
            .error(R.drawable.ic_img_load_fail);

    private void initPhoto() {
        mPhoto.clear();
        List<PhotoView> photos = new ArrayList<>();
        PhotoView v;
        //这个LayoutParams可以理解为java代码中的布局参数，相当于XML文件中的属性。
        ViewPager.LayoutParams params = new ViewPager.LayoutParams();
        params.gravity = Gravity.CENTER;
        //设置宽度填充满父布局
        params.width = ViewPager.LayoutParams.WRAP_CONTENT;
        //高度为自身大小
        params.height = ViewPager.LayoutParams.WRAP_CONTENT;
//        一次性创建需要的PhotoView
        for (int i = mPhoto.size(); i < mList.size(); i++) {
            v = new PhotoView(mContext);
            //将布局参数设置进PhotoView
            v.setLayoutParams(params);
//            添加到集合中去
            photos.add(v);
            if (mContext.getApplication() instanceof MApplication) {
                if (mList.get(i).getImgPath().startsWith("http:") || mList.get(i).getImgPath().startsWith("https:")) {
                    Glide.with(mContext)
                            .load(mList.get(i).getImgPath())
                            .apply(ro2)
                            .into(v);
                } else {
                    Glide.with(mContext)
                            .load(mList.get(i).getImgPath())
                            .apply(ro1)
                            .into(v);
                }

//                ((MApplication) mContext.getActivity().getApplication())
//                        .getImageLoaderFactory().loadCommonImgByUrl(mContext, mList.get(i).getImgPath(), v);
            } else {
                //使用Glide加载图片
                Glide.with(mContext).load(mList.get(i).getImgPath()).into(v);
            }
            v.setOnPhotoTapListener(new OnPhotoTapListener() {
                @Override
                public void onPhotoTap(ImageView view, float x, float y) {
                    EventBus.getDefault().post(new OnPhotoTapEvent());
                }
            });
        }
        mPhoto.addAll(photos);
        photos.clear();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public void notifyDataSetChanged() {
        initPhoto();
        super.notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mPhoto.get(position));
        Log.i("TAG", "instantiateItem: " + mPhoto.get(position).getScaleType());
        return mPhoto.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        try {
            container.removeView(mPhoto.get(position));
        } catch (Exception e) {
        }
    }

}