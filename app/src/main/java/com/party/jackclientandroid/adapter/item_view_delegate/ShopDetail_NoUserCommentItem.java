package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.NoUserComment;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.view.MNineGridLayout;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ShopDetail_NoUserCommentItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;

    public ShopDetail_NoUserCommentItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.layout_shop_detail_no_user_comment;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof NoUserComment;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {

    }
}
