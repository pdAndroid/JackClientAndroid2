package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.ShopDetail_Header;
import com.party.jackclientandroid.controller.AMapController;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.ui_mine.choose_location.DisplayPositionActivity;
import com.party.jackclientandroid.utils.PhoneUtil;
import com.party.jackclientandroid.utils.TimeUtil;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/9/9.
 */

public class ShopDetail_ShopDetailItem implements ItemViewDelegate<DisplayableItem> {
    private MApplication application;
    private Context context;
    private AMapController aMapController;

    public ShopDetail_ShopDetailItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
        aMapController = new AMapController((Activity) context);
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_shop_detail;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail_Header;
    }

    @Override
    public void convert(ViewHolder holder, final DisplayableItem item, int position) {
        final ShopDetail_Header shopDetail = (ShopDetail_Header) item;
        application.getImageLoaderFactory().loadCommonImgByUrl((Activity) context, shopDetail.getHomeImg(), (ImageView) holder.getView(R.id.shop_image_iv));
        holder.setText(R.id.shop_name_tv, shopDetail.getShopName());
        holder.setText(R.id.symbol_build_tv, shopDetail.getSymbolBuild());
        holder.setText(R.id.fans_tv, shopDetail.getUserConcern() + "粉丝");
        holder.setText(R.id.service_score_tv, "服务：" + shopDetail.getServiceScore());
        holder.setText(R.id.environment_score_tv, "环境：" + shopDetail.getEnviroScore());
        holder.setText(R.id.address_detail_tv, shopDetail.getShopAddress());
        holder.setText(R.id.taste_score_tv, "口味：" + shopDetail.getTasteScore());
        holder.setOnClickListener(R.id.return_iv, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
        holder.setOnClickListener(R.id.call_phone_iv, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PhoneUtil.callPhone((Activity) context, shopDetail.getShopName(),
                        "电话号码：" + shopDetail.getShopPhone(),
                        shopDetail.getShopPhone());
            }
        });
        TextView shopNameTv = holder.getView(R.id.shop_name_tv);

        aMapController.setHelpView(shopNameTv);
        aMapController.setWholeLayout(((ShopDetailActivity) context).getmRefreshLayout());
        aMapController.setCurrLatitude(shopDetail.getLatitude());
        aMapController.setCurrLongitude(shopDetail.getLongitude());

        holder.setOnClickListener(R.id.navigation_ll, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DisplayPositionActivity.class);
                intent.putExtra("latitude", shopDetail.getLatitude());
                intent.putExtra("longitude", shopDetail.getLongitude());
                intent.putExtra("shop_name", shopDetail.getShopName());
                intent.putExtra("shop_address", shopDetail.getShopAddress());
                context.startActivity(intent);
            }
        });
        holder.setOnClickListener(R.id.shop_image_iv, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ImageBean> urlList = new ArrayList<>();
                ImageBean imageBean = new ImageBean();
                imageBean.setImgPath(shopDetail.getHomeImg());
                urlList.add(imageBean);
                Intent intent = new Intent(context, PictureDetailActivity.class);
                intent.putExtra("currentItem", 0);
                intent.putExtra("dataList", urlList);
                intent.putExtra("deleteFlag", false);
                context.startActivity(intent);
            }
        });
        final ImageView attention_iv = holder.getView(R.id.attention_iv);
        if (shopDetail.getIsAttention() == 1) {
            attention_iv.setImageResource(R.mipmap.attention_state);
        } else {
            attention_iv.setImageResource(R.mipmap.not_attention_state);
        }
        attention_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!((BaseActivity) context).isLogin()) {
                    ((BaseActivity) context).handleToLogin();
                    return;
                }
                if (shopDetail.getIsAttention() == 1) {
                    DialogController.showConfirmDialog((Activity) context, "你确定要取消关注？", new ConfirmDialog.DialogListener() {
                        @Override
                        public void cancel(ConfirmDialog dialog) {
                            dialog.dismiss();
                        }

                        @Override
                        public void ok(ConfirmDialog dialog) {
                            ((ShopDetailActivity) context).cancelAttention(shopDetail, attention_iv);
                            dialog.dismiss();
                        }
                    });
                } else {
                    ((ShopDetailActivity) context).addAttention(shopDetail, attention_iv);
                }
            }
        });
        long averageScore = (shopDetail.getEnviroScore() + shopDetail.getServiceScore() + shopDetail.getTasteScore()) / 3;
        LinearLayout starLl = holder.getView(R.id.star_ll);
        starLl.removeAllViews();
        if (averageScore % 2 == 0) {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                } else {
                    starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                }
            }
        } else {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                } else if (i == averageScore / 2 + 1) {
                    starLl.addView(getStarIv(R.mipmap.star_half_selected, starLl));
                } else {
                    starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                }
            }
        }
    }

    private ImageView getStarIv(int imageId, View view) {
        ImageView starIv = (ImageView) ((Activity) context).getLayoutInflater().inflate(R.layout.layout_star_iv, (ViewGroup) view.getParent(), false);
        application.getImageLoaderFactory().loadCommonImgByUrl((Activity) context, imageId, starIv);
        return starIv;
    }
}
