package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.DiscountPayBillBean;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.party.jackclientandroid.utils.Utils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Administrator on 2018/9/9.
 */

public class ShopDetail_DiscountPayBillItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;

    public ShopDetail_DiscountPayBillItem(Context context) {
        this.context = context;
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_favourable_pay_bill;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof DiscountPayBillBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final DiscountPayBillBean discountPayBillBean = (DiscountPayBillBean) item;
        BigDecimal discount = new BigDecimal(discountPayBillBean.getDiscount());
        //是否首单
        if (discountPayBillBean.getIsUseDiscount() == 1) {
            if (discount.floatValue() >= 1) {
                holder.setText(R.id.discount_tv, "暂无折扣");
            } else {
                holder.setText(R.id.discount_tv, "首单" + Utils.changeStr10(discountPayBillBean.getDiscount()) + "折   " + discountPayBillBean.getShopRebateMoney() + "");
            }
        } else {
            holder.setText(R.id.discount_tv, discountPayBillBean.getShopRebateMoney() + "");
        }
        holder.setText(R.id.tv_favorite_check_amount, "买单量：" + discountPayBillBean.getTotalComsumeNumber());

        holder.setOnClickListener(R.id.pay_bill_btn, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof BaseActivity) {
                    if (!((BaseActivity) context).isLogin()) {
                        ((BaseActivity) context).handleToLogin();
                        return;
                    }
                }
                Intent intent = new Intent(context, DiscountPayBillActivity.class);
                intent.putExtra("shopId", discountPayBillBean.getShopId() + "");
                context.startActivity(intent);
            }
        });
    }

}
