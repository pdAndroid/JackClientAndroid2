package com.party.jackclientandroid.adapter;

import android.app.Activity;

import com.party.jackclientandroid.bean.BankCard;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;

import java.util.List;

public class BankCardAdapter extends MultiItemTypeAdapter {
    public BankCardAdapter(Activity context, List<BankCard> datas) {
        super(context, datas);
        addItemViewDelegate(new BankCardItem(context));
        addItemViewDelegate(new BankCardPlusItem(context));
    }
}
