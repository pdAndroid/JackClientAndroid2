package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.ui_home.activity.SpellingGroupCouponDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.TimeUtil;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import cn.iwgang.countdownview.CountdownView;

/**
 * 拼团券Item
 */
public class ShopDetail_SpellingGroupCouponItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private MApplication application;

    public ShopDetail_SpellingGroupCouponItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.delegate_spelling_group_coupon;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail.SpellingGroupsBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final ShopDetail.SpellingGroupsBean spellingGroupCouponBean = (ShopDetail.SpellingGroupsBean) item;
        ((ShopDetailActivity) context).getmApplication().getImageLoaderFactory().loadCommonImgByUrl((Activity) context, spellingGroupCouponBean.getGroupBuyImg(), (ImageView) holder.getView(R.id.iv_delegate_spelling_group));
        TextView tv_delegate_spelling_group_coupon_oringeprice = holder.getView(R.id.tv_delegate_spelling_group_coupon_oringeprice);
        tv_delegate_spelling_group_coupon_oringeprice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.setText(R.id.tv_delegate_spelling_group_coupon_name, spellingGroupCouponBean.getDinersNumberName());
        holder.setText(R.id.tv_delegate_spelling_group_coupon_menu, "兑现日期：" + TimeUtil.stampToDateString(spellingGroupCouponBean.getEndSaleTime()).toString());
        holder.setText(R.id.tv_delegate_spelling_group_coupon_date, "还剩" + spellingGroupCouponBean.getStockCount() + "张");
        holder.setText(R.id.tv_delegate_spelling_group_coupon_price, "¥" + spellingGroupCouponBean.getBuyPrice());
        holder.setText(R.id.tv_delegate_spelling_group_coupon_oringeprice, "门市价：¥" + spellingGroupCouponBean.getOriginalPrice());
        /*view.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG);*/
        //倒计时
        CountdownView countdownView = holder.getView(R.id.count_down_view);
        LinearLayout remain_time_ll = holder.getView(R.id.remain_time_ll);
        TextView remain_time_tv = holder.getView(R.id.remain_time_tv);
        long currTimeStamp = TimeUtil.getCurrTimeStamp();
        long endTimeStamp = spellingGroupCouponBean.getEndSaleTime();
        String result = TimeUtil.getDistanceTime(currTimeStamp, endTimeStamp);
        if (result.equals("不到一天")) {
            countdownView.start(endTimeStamp - currTimeStamp);
            remain_time_ll.setVisibility(View.VISIBLE);
            remain_time_tv.setVisibility(View.GONE);
        } else {
            remain_time_tv.setText(result);
            remain_time_tv.setVisibility(View.VISIBLE);
            remain_time_ll.setVisibility(View.GONE);
        }

        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long id = spellingGroupCouponBean.getId();
                Intent intent = new Intent(context, SpellingGroupCouponDetailActivity.class);
                intent.putExtra("couponId", spellingGroupCouponBean.getId());
                context.startActivity(intent);
            }
        });
        holder.setOnClickListener(R.id.btn_replace_money_coupon_buy, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", spellingGroupCouponBean);
                context.startActivity(intent);
            }
        });
    }
}
