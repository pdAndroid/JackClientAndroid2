package com.party.jackclientandroid.adapter;

import android.graphics.Paint;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.VipBean;
import com.party.jackclientandroid.ui_mine.partner_center.VipCenterActivity;

import java.util.List;

public class VipPagerAdapter extends PagerAdapter {

    private List<VipBean.ResponsesBean> data;
    private BaseActivity mContext;

    private String level_1 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/bronzeVip.png";
    private String level_2 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/notGold.png";
    private String level_2_ok = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/goldVip.png";
    private String level_3 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/notDiamonde.png";
    private String level_3_ok = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/DiamondsVip.png";

    public VipPagerAdapter(List<VipBean.ResponsesBean> data, BaseActivity mContext) {
        this.data = data;
        this.mContext = mContext;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View inflate = LayoutInflater.from(container.getContext()).inflate(R.layout.cardviewpager_item, container, false);
        ImageView ivbg = inflate.findViewById(R.id.vip_card_view_status_iv);
        switch (position) {
            case 0: //青铜会员
                mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, level_1, ivbg);
                break;
            case 1://黄金会员
                if (((VipCenterActivity) mContext).getUserRoleId() >= data.get(position).getId()) {
                    mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, level_2_ok, ivbg);
                } else {
                    mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, level_2, ivbg);
                }
                break;
            case 2://钻石会员
                if (((VipCenterActivity) mContext).getUserRoleId() >= data.get(position).getId()) {
                    mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, level_3_ok, ivbg);
                } else {
                    mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, level_3, ivbg);
                }
                break;
        }
        container.addView(inflate);
        return inflate;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(((View) object));
    }
}
