package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.ConstUtils;
import com.party.jackclientandroid.view.MNineGridLayout;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ShopDetail_UserCommentItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;

    public ShopDetail_UserCommentItem(Context context) {
        this.context = context;
    }

    @Override

    public int getItemViewLayoutId() {
        return R.layout.item_comment_to_shop;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail.CommentsBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final ShopDetail.CommentsBean userCommentAndReply = (ShopDetail.CommentsBean) item;
        long averageScore = (userCommentAndReply.getEnviroScore() + userCommentAndReply.getServiceScore() + userCommentAndReply.getTasteScore()) / 3;
        LinearLayout starLl = holder.getView(R.id.star_ll);
        starLl.removeAllViews();
        if (averageScore % 2 == 0) {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                } else {
                    starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                }
            }
        } else {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                } else if (i == averageScore / 2 + 1) {
                    starLl.addView(getStarIv(R.mipmap.star_half_selected, starLl));
                } else {
                    starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                }
            }
        }
        ((ShopDetailActivity) context).getmApplication().getImageLoaderFactory().loadCommonImgByUrl((Activity) context, userCommentAndReply.getIcon(), (ImageView) holder.getView(R.id.header_image));
        holder.setText(R.id.nickname_tv, userCommentAndReply.getNickName());
        holder.setText(R.id.comment_content_tv, userCommentAndReply.getContent());
        holder.setText(R.id.comment_time_tv, CommonUtil.formatTime(userCommentAndReply.getCreate()));
        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(context, ReplyListActivity.class);
                intent.putExtra("shopId",((ShopDetailActivity)context).getShopId());
                intent.putExtra("orderId",userCommentAndReply.getOrderId());
                context.startActivity(intent);*/
            }
        });
        MNineGridLayout mNineGridLayout = holder.getView(R.id.grid_layout);
        String[] imagePathArr = userCommentAndReply.getCommentImg().split(",");
        List<ImageBean> imageBeanList = new ArrayList<>();
        for (int i = 0; i < imagePathArr.length; i++) {
            ImageBean imageBean = new ImageBean();
            if (imagePathArr[i].length() == 0) continue;
            imageBean.setImgPath(imagePathArr[i]);
            imageBeanList.add(imageBean);
        }

        mNineGridLayout.setOnClickItem((boolean isAdded, int itemPosition, int position1, String url, ArrayList<ImageBean> urlList) -> {
            ((BaseActivity)context).showPicture(isAdded, itemPosition, position1, url,urlList);
        });

        mNineGridLayout.setItemPosition(position);
        mNineGridLayout.setUrlList(imageBeanList);
        mNineGridLayout.setIsShowAll(true);
        mNineGridLayout.notifyDataSetChanged();
    }

    private ImageView getStarIv(int imageId, View view) {
        ImageView starIv = (ImageView) ((Activity) context).getLayoutInflater().inflate(R.layout.layout_star_iv, (ViewGroup) view.getParent(), false);
        ((ShopDetailActivity) context).getmApplication().getImageLoaderFactory().loadCommonImgByUrl((Activity) context, imageId, starIv);
        return starIv;
    }
}
