package com.party.jackclientandroid.adapter.item_view_delegate;

import com.party.jackclientandroid.bean.DisplayableItem;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * Created by 南宫灬绝痕 on 2019/1/12.
 */

public class ShopDetail_AnnotationItem implements ItemViewDelegate<DisplayableItem> {
    @Override
    public int getItemViewLayoutId() {
        return 0;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return false;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem displayableItem, int position) {

    }
}
