package com.party.jackclientandroid.adapter.item_view_delegate;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.ui_home.activity.ReplaceMoneyCouponDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.Utils;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

/**
 * 代金券Item
 */
public class ShopDetail_ReplaceMoneyCouponItem implements ItemViewDelegate<DisplayableItem> {
    private Context context;
    private MApplication application;

    public ShopDetail_ReplaceMoneyCouponItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.item_replace_money_coupon;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof ShopDetail.CouponsBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final ShopDetail.CouponsBean replaceMoneyCoupon = (ShopDetail.CouponsBean) item;
        holder.setText(R.id.tv_replace_money_coupon_name, Utils.clearZero(replaceMoneyCoupon.getOriginalPrice()) + "元代金券");
        holder.setText(R.id.tv_replace_money_coupon_time, CommonUtil.getAvailableDate(replaceMoneyCoupon.getAvailableDate()) + "|" + CommonUtil.getGoodsRange(replaceMoneyCoupon.getGoodsRange()));
        holder.setText(R.id.tv_replace_money_coupon_price, "¥" + replaceMoneyCoupon.getBuyPrice());
        holder.setText(R.id.tv_replace_money_coupon_sale, Utils.clearZero("总销量：" + replaceMoneyCoupon.getSaleNumber() + "张"));
        holder.setOnClickListener(R.id.whole_layout, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ReplaceMoneyCouponDetailActivity.class);
                intent.putExtra("couponId", replaceMoneyCoupon.getId());
                context.startActivity(intent);
            }
        });
        holder.setOnClickListener(R.id.btn_replace_money_coupon_buy, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", replaceMoneyCoupon);
                context.startActivity(intent);
            }
        });
    }
}
