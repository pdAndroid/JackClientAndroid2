package com.party.jackclientandroid.adapter.item_view_delegate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.DisplayableItem;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.RecommendVegetableListBean;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ItemViewDelegate;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

public class ShopDetail_RecommendGoodsItem implements ItemViewDelegate<DisplayableItem> {

    private Context context;
    private MApplication application;

    public ShopDetail_RecommendGoodsItem(Context context) {
        this.context = context;
        application = ((BaseActivity) context).getmApplication();
    }

    @Override
    public int getItemViewLayoutId() {
        return R.layout.delegete_recommend_vegetable_rv;
    }

    @Override
    public boolean isForViewType(DisplayableItem item, int position) {
        return item instanceof RecommendVegetableListBean;
    }

    @Override
    public void convert(ViewHolder holder, DisplayableItem item, int position) {
        final RecommendVegetableListBean recommendVegetableListBean = (RecommendVegetableListBean) item;
        RecyclerView recyclerView = holder.getView(R.id.recycler_view);
        holder.setText(R.id.recommend_num_tv, "商家招牌（" + recommendVegetableListBean.getItemsBeanList().size() + ")");
        final List<ShopDetail.ItemsBean> itemsBeanList = recommendVegetableListBean.getItemsBeanList();
        //商家推荐
        CommonAdapter commonAdapter = new CommonAdapter<ShopDetail.ItemsBean>(application, R.layout.item_recommend_vegetable, itemsBeanList) {
            @Override
            protected void convert(ViewHolder viewHolder, ShopDetail.ItemsBean itemsBean, final int position) {
                ImageView dinnerIv = (ImageView) viewHolder.getView(R.id.dinner_iv);
                ((ShopDetailActivity) context).getmApplication().getImageLoaderFactory().loadCommonImgByUrl((Activity) context, itemsBean.getImgUrl(), (ImageView) viewHolder.getView(R.id.dinner_iv));
                viewHolder.setText(R.id.recommend_title_tv, itemsBean.getTitle());
                viewHolder.setOnClickListener(R.id.dinner_iv, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<ImageBean> urlList = new ArrayList<>();
                        for (ShopDetail.ItemsBean ib : itemsBeanList) {
                            ImageBean imageBean = new ImageBean();
                            imageBean.setImgPath(ib.getImgUrl());
                            urlList.add(imageBean);
                        }
                        Intent intent = new Intent(context, PictureDetailActivity.class);
                        intent.putExtra("currentItem", position);
                        intent.putExtra("dataList", urlList);
                        intent.putExtra("deleteFlag", false);
                        context.startActivity(intent);
                    }
                });
            }
        };
        recyclerView.setLayoutManager(new LinearLayoutManager(application, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(commonAdapter);
        recyclerView.setNestedScrollingEnabled(false);
    }
}
