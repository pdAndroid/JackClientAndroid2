package com.party.jackclientandroid;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.party.jackclientandroid.utils.DensityUtils;

public abstract class BaseActivityTitle extends BaseActivity {

    private View mTitleView;

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        handleTitle();
    }


    private void handleTitle() {
        mTitleView = View.inflate(mActivity, R.layout.layout_common_title, null);
        int dip50 = DensityUtils.dip2px(mActivity, 50f);
        FrameLayout contentView = findViewById(android.R.id.content);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, dip50);
        View rootView = contentView.getChildAt(0);
        contentView.addView(mTitleView, 0, layoutParams);
        FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) rootView.getLayoutParams();
        layoutParams2.topMargin = dip50;
        rootView.setLayoutParams(layoutParams2);
        View leftIv = mTitleView.findViewById(R.id.iv_back);
        leftIv.setOnClickListener((View v) -> {
            finish();
        });
    }

    public void setMiddleText(String text) {
        TextView textView = mTitleView.findViewById(R.id.tv_title);
        textView.setText(text);
    }

    public void setRightBtn(String rightText, View.OnClickListener listener) {
        TextView viewById = mTitleView.findViewById(R.id.tv_right);
        viewById.setVisibility(View.VISIBLE);
        viewById.setOnClickListener(listener);
        viewById.setText(rightText);
    }

    public void callPhone(String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
