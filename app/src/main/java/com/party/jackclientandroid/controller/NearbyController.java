package com.party.jackclientandroid.controller;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;

public class NearbyController extends BaseController {
    private RequestOptions requestOptionsNoAim;

    public NearbyController(BaseActivity baseActivity) {
        super(baseActivity);
        requestOptionsNoAim = new RequestOptions();
        requestOptionsNoAim
                .placeholder(R.color.color_b2b2b2)
                .error(R.color.color_8a8a8a)
                .fitCenter()
                .skipMemoryCache(false)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    public ImageView getStarIv(int imageId, View view) {
        ImageView imageView = (ImageView) mInflater.inflate(R.layout.layout_star_iv, (ViewGroup) view.getParent(), false);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(baseActivity.get(), imageId, imageView);
        return imageView;
    }

    public View getCouponView(double buyPrice, String desc, int image) {
        View view = mInflater.inflate(R.layout.layout_coupon_item, null, false);
        ImageView imageView = view.findViewById(R.id.image_iv);
        ((TextView) view.findViewById(R.id.buy_price_tv)).setText("¥" + buyPrice);
        ((TextView) view.findViewById(R.id.desc_tv)).setText(desc);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(baseActivity.get(), image, imageView);
        return view;
    }
}