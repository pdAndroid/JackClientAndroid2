package com.party.jackclientandroid.controller;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.Notice;
import com.party.jackclientandroid.bean.RedPacketBean;
import com.party.jackclientandroid.bean.VideoCommentBean;
import com.party.jackclientandroid.ui_mine.my_wallet.BindingAccountZfbActivity;
import com.party.jackclientandroid.view.dialog.ActionSheetDialog;
import com.party.jackclientandroid.view.dialog.AuthDialog;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;
import com.party.jackclientandroid.view.dialog.DeletePictureDialog;
import com.party.jackclientandroid.view.dialog.NoticeDialog;
import com.party.jackclientandroid.view.dialog.MessageDialog;
import com.party.jackclientandroid.view.dialog.MonitoringDialog;
import com.party.jackclientandroid.view.dialog.PurchaseMembershipDialog;
import com.party.jackclientandroid.view.dialog.QRCodeDialog;
import com.party.jackclientandroid.view.dialog.RebatePackageDialog;
import com.party.jackclientandroid.view.dialog.RecommendPresentDialog;
import com.party.jackclientandroid.view.dialog.RedPacketDialog;
import com.party.jackclientandroid.view.dialog.RedPacketDialogCopy;
import com.party.jackclientandroid.view.dialog.VideoCommentDialog;

import java.util.Arrays;
import java.util.List;

public class DialogController {
    /**
     * 显示wifi的对话框
     *
     * @param activity
     */
    public static void showVideoPictureDialog(Activity activity, CharSequence message, DeletePictureDialog.OkListener listener) {
        DeletePictureDialog alertDialog = new DeletePictureDialog.Builder(activity)
                .setCancelable(true)
                .setMessage(message)
                .setOkListener(listener)
                .create();
        alertDialog.show();
        setWidthAndHeight2(activity, alertDialog);
    }

    /**
     * 修改推荐人
     *
     * @param activity
     */
    public static void PurchaseMembershipDialog(Activity activity, PurchaseMembershipDialog.SettingListener listener) {
        PurchaseMembershipDialog alertDialog = new PurchaseMembershipDialog.Builder(activity)
                .setCancelable(true)
                .setListener(listener)
                .create();
        alertDialog.show();
        setWidthAndHeight2(activity, alertDialog);
    }

    public static QRCodeDialog showQRCodeDialog(Activity activity, Bitmap bitmap) {
        QRCodeDialog qrCodeDialog = new QRCodeDialog.Builder(activity)
                .setCancelable(true)
                .setBitmap(bitmap)
                .create();
        qrCodeDialog.show();
        setWidthAndHeight2(activity, qrCodeDialog);
        return qrCodeDialog;
    }

    //获得公告
    public static NoticeDialog showNoticeDialog(BaseActivity activity, Notice notice) {
        NoticeDialog noticeDialog = new NoticeDialog.Builder(activity)
                .setCancelable(true)
                .setNotice(notice)
                .create();
        noticeDialog.show();
        setWidthAndHeight(activity, noticeDialog);
        return noticeDialog;
    }

    //监控
    public static MonitoringDialog showMonitoringDialog(Activity activity, String remarks, long unsealingTime) {
        MonitoringDialog monitoringDialog = new MonitoringDialog.Builder(activity)
                .setCancelable(true)
                .setContent(remarks, unsealingTime)
                .create();
        monitoringDialog.show();
        setWidthAndHeight2(activity, monitoringDialog);
        return monitoringDialog;
    }

    public static void showConfirmDialog(Context context
            , String title
            , String message
            , String okTxt
            , String cancelTxt
            , View.OnClickListener okListener
            , View.OnClickListener cancelListener) {
        new MessageDialog(context).builder().setTitle(title)
                .setMsg(message)
                .setPositiveButton(okTxt, okListener)
                .setNegativeButton(cancelTxt, cancelListener).show();
    }


    public static void showConfirmDialog(Context context, String title, String message, View.OnClickListener okListener, View.OnClickListener cancelListener) {
        showConfirmDialog(context, title, message, "确认", "取消", okListener, cancelListener);
    }

    public static void showConfirmDialog(Context context, String title, String message, String okStr, View.OnClickListener okListener) {
        showConfirmDialog(context, title, message, okStr, "取消", okListener, (View v) -> {
        });
    }


    public static void showConfirmDialog(Context context, String title, String message, View.OnClickListener okListener) {
        MessageDialog messageDialog = new MessageDialog(context).builder().setTitle(title)
                .setMsg(message);

        messageDialog.setPositiveButton("确认", (View v) -> {
            v.setTag(messageDialog);
            okListener.onClick(v);
        });
        messageDialog.setNegativeButton("取消", (View v) -> {
        });
        messageDialog.show();
    }

    public static void showMustConfirmDialog(Context context, String title, String message, View.OnClickListener okListener) {
        new MessageDialog(context).builder().setTitle(title)
                .setMsg(message)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .setPositiveButton("确认", okListener).show();
    }

    public static void showMustConfirmDialog(Context context, String message, View.OnClickListener okListener) {
        showMustConfirmDialog(context, "温馨提示", message, okListener);
    }

    public static void showConfirmDialog(Context context, String message, View.OnClickListener okListener) {
        showConfirmDialog(context, "温馨提示", message, okListener);
    }

    public static void showConfirmDialog(Context context, String title, String message) {
        MessageDialog messageDialog = new MessageDialog(context).builder().setTitle(title)
                .setMsg(message);
        messageDialog.setPositiveButton("确认", (View v) -> {
        });
        messageDialog.show();
    }


    public static ActionSheetDialog showMenuList(String[] datas, final Context context, ActionSheetDialog.OnSheetItemClickListener listener) {
        ActionSheetDialog dialog = showMenuList(Arrays.asList(datas), context, listener, null, null);
        return dialog;
    }

    public static ActionSheetDialog showMenuList(final List<String> data, final Context context, ActionSheetDialog.OnSheetItemClickListener listener) {
        ActionSheetDialog dialog = showMenuList(data, context, listener, null, null);
        return dialog;
    }

    public static ActionSheetDialog showMenuList(final List<String> data,
                                                 final Context context,
                                                 ActionSheetDialog.OnSheetItemClickListener listener,
                                                 String cancelTxt, View.OnClickListener bottomListener) {
        ActionSheetDialog dialog = new ActionSheetDialog(context)
                .builder()
                .setCancelable(true)
                .setCanceledOnTouchOutside(true);
        for (int i = 0; i < data.size(); i++) {
            dialog.addSheetItem(data.get(i), ActionSheetDialog.SheetItemColor.Blue, listener);
        }

        //新增按钮
        if (cancelTxt != null) dialog.setBottomText(cancelTxt);
        if (bottomListener != null) dialog.setBottomListener(bottomListener);
        dialog.show();
        return dialog;
    }


    /**
     * 显示删除图片的对话框
     *
     * @param activity
     */
    public static void showDeletePictureDialog(Activity activity, int position, DeletePictureDialog.OkListener listener) {
        DeletePictureDialog alertDialog = new DeletePictureDialog.Builder(activity)
                .setCancelable(true)
                .setPosition(position)
                .setOkListener(listener)
                .create();
        alertDialog.show();
    }

    /**
     * 展示确认对话框
     *
     * @param activity
     * @param message
     * @param dialogListener
     */
    public static void showConfirmDialog(Activity activity, String message, ConfirmDialog.DialogListener dialogListener) {
        ConfirmDialog alertDialog = new ConfirmDialog.Builder(activity)
                .setCancelable(true)
                .setMessage(message)
                .setDialogListener(dialogListener)
                .create();
        alertDialog.show();
        setWidthAndHeight2(activity, alertDialog);
    }

    public static void showConfirmDialog2(Activity activity, String message, String cancelText, String okText, ConfirmDialog.DialogListener dialogListener) {
        ConfirmDialog alertDialog = new ConfirmDialog.Builder(activity)
                .setCancelable(true)
                .setMessage(message)
                .setCancelBtnText(cancelText)
                .setOkBtnText(okText)
                .setDialogListener(dialogListener)
                .create();
        alertDialog.setCancelTextColor(activity.getResources().getColor(R.color.copy_ios_dialog_font_color));
        alertDialog.setOkTextColor(activity.getResources().getColor(R.color.color_dd4b5e));
        alertDialog.show();
        setWidthAndHeight2(activity, alertDialog);
    }

    /**
     * 显示视频里面的评论列表
     *
     * @param activity
     */
    public static void showVideoCommentList(final Activity activity, List<VideoCommentBean> dataList, final AddVideoCommentListener listener) {
        try {
            VideoCommentDialog sheetDialog = new VideoCommentDialog(activity);
            sheetDialog.showCommentList(activity, dataList, listener);
            sheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 展示推荐列表
     *
     * @param activity
     * @param recommend_state
     */
    public static void showRecommendPresentDialog(Activity activity, int recommend_state) {
        RecommendPresentDialog alertDialog = new RecommendPresentDialog.Builder(activity)
                .setCancelable(true)
                .setRecommendState(recommend_state)
                .create();
        alertDialog.show();
        setWidthAndHeight(activity, alertDialog, 0.8f, 0.8f);
    }

    /**
     * 显示红包的对话框
     *
     * @param activity
     */
    public static RedPacketDialogCopy showRedPacketDialog(Activity activity, RedPacketBean redPacketBean, RedPacketDialogCopy.OkListener listener) {
        RedPacketDialogCopy alertDialog = new RedPacketDialogCopy.Builder(activity)
                .setCancelable(true)
                .setRedPacketBean(redPacketBean)
                .setOkListener(listener)
                .create();
        alertDialog.show();
        return alertDialog;
    }


    /**
     * 显示红包的对话框
     *
     * @param activity
     */
    public static RedPacketDialog showRedPacketDialog1(Activity activity, View.OnClickListener toUserListener) {
        RedPacketDialog alertDialog = new RedPacketDialog.Builder(activity)
                .setToUserListener(toUserListener)
                .create();
        alertDialog.show();
        return alertDialog;
    }

    public static RebatePackageDialog showRebateDialog(Activity activity, String shopName, String rebateMoney, View.OnClickListener listener) {
        RebatePackageDialog alertDialog = new RebatePackageDialog.Builder(activity)
                .setContent(shopName, rebateMoney)
                .setOnClickListener(listener)
                .create();
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnCancelListener((DialogInterface dialog) -> {
            dialog.dismiss();
//            activity.finish();
        });
        return alertDialog;
    }


    public static void setWidthAndHeight(Activity activity, AlertDialog alertDialog) {
        // 将对话框的大小按屏幕大小的百分比设置
        setWidthAndHeight(activity, alertDialog, 0.8f, 1f);
    }


    public static void setWidthAndHeight2(Activity activity, AlertDialog alertDialog) {
        setWidthAndHeight(activity, alertDialog, 0.8f, 0.5f);
    }

    public static void setWidthAndHeight(Activity activity, AlertDialog alertDialog, float width, float height) {
        // 将对话框的大小按屏幕大小的百分比设置
        WindowManager windowManager = activity.getWindowManager();
        DisplayMetrics dm = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(dm);
        WindowManager.LayoutParams lp = alertDialog.getWindow().getAttributes();
        lp.width = (int) (dm.widthPixels * width); //设置宽度
        lp.height = (int) (dm.heightPixels * height);
        alertDialog.getWindow().setAttributes(lp);
    }


    public static void showAuthTip(BaseActivity mActivity, View.OnClickListener listener) {
        new AuthDialog.Builder(mActivity).setAliAuth(listener).create().show();
    }
}
