package com.party.jackclientandroid.controller;

import android.app.Dialog;

public interface AddVideoCommentListener {
    void addComment(String str, Dialog dialog);
}
