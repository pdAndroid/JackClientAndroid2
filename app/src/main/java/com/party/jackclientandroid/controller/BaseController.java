package com.party.jackclientandroid.controller;

import android.content.res.Resources;
import android.view.LayoutInflater;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;

import java.lang.ref.WeakReference;

/**
 * Created by tangxuebing on 2018/6/26.
 */

public class BaseController {
    protected WeakReference<BaseActivity> baseActivity;
    protected MApplication mApplication;
    protected LayoutInflater mInflater;
    protected Resources mResources;

    public BaseController(BaseActivity baseActivity) {
        this.baseActivity = new WeakReference<>(baseActivity);
        this.mApplication = (MApplication) baseActivity.getApplication();
        mInflater = LayoutInflater.from(baseActivity);
        mResources = baseActivity.getResources();
    }
}
