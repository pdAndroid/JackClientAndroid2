package com.party.jackclientandroid.controller;

import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.bean.GoodDetail;
import com.party.jackclientandroid.bean.GroupBuyCouponDetail;
import com.party.jackclientandroid.bean.ReplaceMoneyCouponDetail;
import com.party.jackclientandroid.bean.SecondKillCouponDetail;
import com.party.jackclientandroid.bean.SpellingGroupCouponDetail;

/**
 * Created by Administrator on 2018/10/6.
 */

public class ConvertController {

    public static GoodDetail getGoodsDetailObj(SpellingGroupCouponDetail spellingGroupCouponDetail){
        GoodDetail goodDetail = new GoodDetail();
        goodDetail.setBuyPrice(spellingGroupCouponDetail.getBuyPrice());
        goodDetail.setGoodsType(Constant.GOODS_TYPE_SPELLING_GROUP);
        goodDetail.setId(spellingGroupCouponDetail.getId());
        goodDetail.setOriginalPrice(spellingGroupCouponDetail.getOriginalPrice());
        goodDetail.setBuyPersonLimit(spellingGroupCouponDetail.getBuyPersonLimit());
        goodDetail.setGoodsName(spellingGroupCouponDetail.getDinersNumberName());
        goodDetail.setDateEnd(spellingGroupCouponDetail.getDateEnd());
        goodDetail.setDateStart(spellingGroupCouponDetail.getDateStart());
        goodDetail.setTimeStart(spellingGroupCouponDetail.getTimeStart());
        goodDetail.setTimeEnd(spellingGroupCouponDetail.getTimeEnd());
        goodDetail.setShopId(spellingGroupCouponDetail.getShopId());
        goodDetail.setHomeImg(spellingGroupCouponDetail.getGroupBuyImg());
        goodDetail.setOnceCount(spellingGroupCouponDetail.getOnceCount());
        return  goodDetail;
    }

    public static GoodDetail getGoodsDetailObj(ReplaceMoneyCouponDetail replaceMoneyCouponDetail){
        GoodDetail goodDetail = new GoodDetail();
        if(replaceMoneyCouponDetail!=null){
            goodDetail.setBuyPrice(replaceMoneyCouponDetail.getBuyPrice());
            goodDetail.setGoodsType(Constant.GOODS_TYPE_REPLACE_MONEY);
            goodDetail.setId(replaceMoneyCouponDetail.getId());
            goodDetail.setOriginalPrice(replaceMoneyCouponDetail.getOriginalPrice());
            goodDetail.setBuyPersonLimit(replaceMoneyCouponDetail.getBuyPersonLimit());
            goodDetail.setGoodsName(replaceMoneyCouponDetail.getOriginalPrice()+"元代金券");
            goodDetail.setShopId(replaceMoneyCouponDetail.getShopId());
            goodDetail.setHomeImg(replaceMoneyCouponDetail.getHomeImg());
            goodDetail.setDateEnd(replaceMoneyCouponDetail.getDateEnd());
            goodDetail.setDateStart(replaceMoneyCouponDetail.getDateStart());
            goodDetail.setTimeEnd(replaceMoneyCouponDetail.getTimeEnd());
            goodDetail.setTimeStart(replaceMoneyCouponDetail.getTimeStart());
            goodDetail.setOnceCount(replaceMoneyCouponDetail.getOnceCount());
        }else {
            //showToast("等一等");
        }
        return  goodDetail;
    }

    public static GoodDetail getGoodsDetailObj(GroupBuyCouponDetail groupBuyCouponDetail){
        GoodDetail goodDetail = new GoodDetail();
        goodDetail.setBuyPrice(groupBuyCouponDetail.getBuyPrice());
        goodDetail.setGoodsType(Constant.GOODS_TYPE_GROUP_BUY);
        goodDetail.setId(groupBuyCouponDetail.getId());
        goodDetail.setOriginalPrice(groupBuyCouponDetail.getOriginalPrice());
        goodDetail.setBuyPersonLimit(groupBuyCouponDetail.getBuyPersonLimit());
        goodDetail.setGoodsName(groupBuyCouponDetail.getDinersNumberName());
        goodDetail.setDateEnd(groupBuyCouponDetail.getDateEnd());
        goodDetail.setDateStart(groupBuyCouponDetail.getDateStart());
        goodDetail.setTimeStart(groupBuyCouponDetail.getTimeStart());
        goodDetail.setTimeEnd(groupBuyCouponDetail.getTimeEnd());
        goodDetail.setShopId(groupBuyCouponDetail.getShopId());
        goodDetail.setHomeImg(groupBuyCouponDetail.getGroupBuyImg());
        goodDetail.setOnceCount(groupBuyCouponDetail.getOnceCount());
        return  goodDetail;
    }

    public static GoodDetail getGoodsDetailObj(SecondKillCouponDetail secondKillCouponDetail){
        GoodDetail goodDetail = new GoodDetail();
        goodDetail.setBuyPrice(secondKillCouponDetail.getBuyPrice());
        goodDetail.setGoodsType(Constant.GOODS_TYPE_SECOND_KILL);
        goodDetail.setId(secondKillCouponDetail.getId());
        goodDetail.setOriginalPrice(secondKillCouponDetail.getOriginalPrice());
        goodDetail.setBuyPersonLimit(secondKillCouponDetail.getBuyPersonLimit());
        goodDetail.setGoodsName(secondKillCouponDetail.getOriginalPrice()+"元代金券");
        goodDetail.setShopId(secondKillCouponDetail.getShopId());
        goodDetail.setHomeImg(secondKillCouponDetail.getHomeImg());
        goodDetail.setDateStart(secondKillCouponDetail.getDateStart());
        goodDetail.setDateEnd(secondKillCouponDetail.getDateEnd());
        goodDetail.setTimeEnd(secondKillCouponDetail.getTimeEnd());
        goodDetail.setTimeStart(secondKillCouponDetail.getTimeStart());
        goodDetail.setHomeImg(secondKillCouponDetail.getHomeImg());
        goodDetail.setOnceCount(secondKillCouponDetail.getOnceCount());
        return  goodDetail;
    }
}
