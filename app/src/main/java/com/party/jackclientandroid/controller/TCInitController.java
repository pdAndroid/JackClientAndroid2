package com.party.jackclientandroid.controller;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.party.jackclientandroid.litvideo.common.utils.TCConstants;
import com.party.jackclientandroid.litvideo.common.utils.TCHttpEngine;
import com.party.jackclientandroid.litvideo.config.TCConfigManager;
import com.party.jackclientandroid.litvideo.login.TCUserMgr;
import com.tencent.ugc.TXUGCBase;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class TCInitController {
    private static String username = "t123456789";
    private static String password = "123456789";
    private static String ugcKey = "09bb91939d9ef9669f7ff16a850c92e5";
    private static String ugcLicenceUrl = "http://download-1252463788.cossh.myqcloud.com/xiaoshipin/licence_xsp/TXUgcSDK.licence";

    public static void initTCSdk(Application application) {
        TCConfigManager.init(application);
        initSDK(application);
        // 短视频licence设置
        TXUGCBase.getInstance().setLicence(application, ugcLicenceUrl, ugcKey);
        // 上报启动次数
        TCUserMgr.getInstance().uploadLogs(TCConstants.ELK_ACTION_START_UP, TCUserMgr.getInstance().getUserId(), 0, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });

        final TCUserMgr tcLoginMgr = TCUserMgr.getInstance();
        tcLoginMgr.login(username, password, new TCUserMgr.Callback() {
            @Override
            public void onSuccess(JSONObject data) {
                Log.d("TCUserMgr", "=========login success");
            }

            @Override
            public void onFailure(int code, final String msg) {
                Log.d("TCUserMgr", "=========login failure");
            }
        });
    }

    /**
     * 初始化SDK，包括Bugly，LiteAVSDK等
     */
    private static void initSDK(Application application) {
        TCUserMgr.getInstance().initContext(application);
        TCHttpEngine.getInstance().initContext(application);
        application.registerActivityLifecycleCallbacks(new MyActivityLifecycleCallbacks(application));
    }

    private static class MyActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
        private int foregroundActivities;
        private boolean isChangingConfiguration;
        private long time;

        public MyActivityLifecycleCallbacks(Application application) {

        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {
            foregroundActivities++;
            if (foregroundActivities == 1 && !isChangingConfiguration) {
                // 应用进入前台
                time = System.currentTimeMillis();
            }
            isChangingConfiguration = false;
        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {
            foregroundActivities--;
            if (foregroundActivities == 0) {
                // 应用切入后台
                long bgTime = System.currentTimeMillis();
                long diff = (bgTime - time) / 1000;
                uploadStayTime(diff);
            }
            isChangingConfiguration = activity.isChangingConfigurations();
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    }

    private static void uploadStayTime(long diff) {
        TCUserMgr.getInstance().uploadLogs(TCConstants.ELK_ACTION_STAY_TIME, TCUserMgr.getInstance().getUserId(), diff, "", new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

            }
        });
    }
}
