package com.party.jackclientandroid.controller;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;

import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.ShopService;
import com.party.jackclientandroid.api.VideoService;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.PlayerInfoBean;
import com.party.jackclientandroid.bean.RedPacketBean;
import com.party.jackclientandroid.ui_home.fragment.ShareFragment;
import com.party.jackclientandroid.view.dialog.RedPacketDialogCopy;

import io.reactivex.functions.Consumer;

public class TCVodPlayerController {
    ShareFragment playerFragment;
    VideoService videoService;
    ShopService shopService;
    MApplication mApplication;
    private boolean showRedPacketDialog;
    RedPacketDialogCopy redPacketDialog;

    public interface TCVodPlayerListener {
        /**
         * 关注成功
         *
         * @param position
         */
        void followSuccess(int position, boolean flag);

        /**
         * 关注失败
         *
         * @param position
         */
        void followFailed(int position, boolean flag);

        /**
         * 点赞成功
         *
         * @param position
         */
        void favoriteSuccess(int position, boolean flag);

        /**
         * 点赞失败
         *
         * @param position
         */
        void favoriteFailed(int position, boolean flag);
    }

    public TCVodPlayerController(ShareFragment playerFragment) {
        this.playerFragment = playerFragment;
        mApplication = (MApplication) playerFragment.getActivity().getApplication();
        videoService = new VideoService((BaseActivity) playerFragment.getActivity());
        shopService = new ShopService((BaseActivity) playerFragment.getActivity());
    }

    public boolean isShowRedPacketDialog() {
        return showRedPacketDialog;
    }

    /**
     * 隐藏红包弹窗
     */
    public void hideRedPacketDialog() {
        redPacketDialog.dismiss();
    }

    /**
     * 当一个视频播放之后发红包
     */
    public void handleVideoPlayEnd(PlayerInfoBean playerInfoBean, int position) {
        playerFragment.pausePlayVideo();
        RedPacketBean redPacketBean = new RedPacketBean();
        redPacketBean.setPosition(position);
        redPacketBean.setPacketId(playerInfoBean.getVideoId());
        redPacketBean.setFrontCover(playerInfoBean.getHomeImg());
        redPacketBean.setShopName(playerInfoBean.getShopName());
        redPacketBean.setCanOperate(true);
        showRedPacketDialog = true;
        redPacketDialog = DialogController.showRedPacketDialog(playerFragment.getActivity(), redPacketBean, new RedPacketDialogCopy.OkListener() {
            @Override
            public void grabMoney(RedPacketDialogCopy dialog, RedPacketBean redPacketBean) {
                dialog.startGrabAnim();
                mockGrabMoney(dialog, redPacketBean.getPosition());
            }
        });
    }

    private void mockGrabMoney(final RedPacketDialogCopy dialog, int position) {
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInter) {
                showRedPacketDialog = false;
                playerFragment.restartPlay();
            }
        });
        playerFragment.grabMoney(dialog, position);
    }



    /**
     * 取消关注
     */
    public void cancelAttention(final View view, final int position, String shopId, final TCVodPlayerListener listener) {
        view.setClickable(false);
        playerFragment.addDisposableIoMain(shopService.cancelAttention(shopId), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                view.setClickable(true);
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                if (listener != null) {
                    listener.followSuccess(position, false);
                }
                view.setClickable(true);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                view.setClickable(true);
            }
        });
    }

    /**
     * 关注
     */
    public void addAttention(final View view, final int position, String shopId, final TCVodPlayerListener listener) {
        view.setClickable(false);
        playerFragment.addDisposableIoMain(shopService.addAttention(shopId), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                view.setClickable(true);
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                if (listener != null) {
                    listener.followSuccess(position, true);
                }
                view.setClickable(true);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                view.setClickable(true);
            }
        });
    }

    /**
     * 取消点赞
     */
    public void cancelFavorite(final View view, final int position, String videoId, final TCVodPlayerListener listener) {
        view.setClickable(false);
        playerFragment.addDisposableIoMain(videoService.cancelNice(videoId), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                view.setClickable(true);
                if (listener != null) {
                    listener.favoriteFailed(position, false);
                }
            }

            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                listener.favoriteSuccess(position, false);
                view.setClickable(true);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                view.setClickable(true);
                if (listener != null) {
                    listener.favoriteFailed(position, false);
                }
            }
        });
    }

    /**
     * 点赞
     */
    public void addFavorite(final View view, final int position, String videoId, final TCVodPlayerListener listener) {
        view.setClickable(false);
        playerFragment.addDisposableIoMain(videoService.videoNice(videoId), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                view.setClickable(true);
                if (listener != null) {
                    listener.favoriteFailed(position, true);
                }
            }

            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                listener.favoriteSuccess(position, true);
                view.setClickable(true);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                view.setClickable(true);
                if (listener != null) {
                    listener.favoriteFailed(position, true);
                }
            }
        });
    }
}
