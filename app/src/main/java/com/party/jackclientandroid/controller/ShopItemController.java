package com.party.jackclientandroid.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.bean.Coupons;
import com.party.jackclientandroid.bean.ShopItemMessage;
import com.party.jackclientandroid.event.AMapBeanEvent;
import com.party.jackclientandroid.ui_home.activity.GroupBuyCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ReplaceMoneyCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.SecondKillCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.SpellingGroupCouponDetailActivity;
import com.party.jackclientandroid.utils.DistanceUtil;
import com.party.jackclientandroid.utils.Utils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by 派对 on 2018/11/17.
 */

public class ShopItemController {
    BaseActivity mActivity;
    Resources resources;
    AMapBeanEvent aMapBeanEvent;
    MyCommonAdapter<ShopItemMessage> commonAdapter;
    RecyclerView mRecyclerView;
    SmartRefreshLayout mRefreshLayout;

    public ShopItemController(BaseActivity activity, RecyclerView recyclerView, SmartRefreshLayout refreshLayout) {
        mActivity = activity;
        resources = mActivity.getResources();
        aMapBeanEvent = mActivity.getmApplication().getMap();
        mRecyclerView = recyclerView;
        mRefreshLayout = refreshLayout;
    }

    private NearbyController controller;


    public MyCommonAdapter<ShopItemMessage> initAdapter(List<ShopItemMessage> shopItemMessage) {
        controller = new NearbyController(mActivity);
        commonAdapter = new MyCommonAdapter<ShopItemMessage>(mActivity, R.layout.item_home_recommend_shop, shopItemMessage) {
            @Override
            protected void convert(ViewHolder viewHolder, final ShopItemMessage recommendShopMessage, final int position) {
                viewHolder.setText(R.id.shop_name_tv, recommendShopMessage.getShopName());
                viewHolder.setText(R.id.average_consume_tv, "粉丝" + recommendShopMessage.getConcernCount());
                viewHolder.setText(R.id.shop_street_tv, recommendShopMessage.getSymbolBuild());
                double distance = DistanceUtil.getDistance(aMapBeanEvent.getLongitude(),
                        aMapBeanEvent.getLatitude(), recommendShopMessage.getLongitude(), recommendShopMessage.getLatitude());
                viewHolder.setText(R.id.distance_tv, DistanceUtil.handleDistance(distance));
                viewHolder.setText(R.id.food_type_tv, recommendShopMessage.getTab());
                Glide.with(mActivity).load(recommendShopMessage.getHomeImg()).into((ImageView) viewHolder.getView(R.id.shop_image_iv));

                //评分
                showAverageScore(viewHolder, recommendShopMessage);
                //显示 折扣 和首单返利
                handleDiscount(viewHolder, recommendShopMessage);

            }


        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
        return commonAdapter;
    }

    private void handleDiscount(ViewHolder viewHolder, ShopItemMessage shopItemMessage) {
        TextView text = viewHolder.getView(R.id.first_dis_tv);

        if (shopItemMessage.getDiscount() == null || Double.parseDouble(shopItemMessage.getDiscount()) == 1) {
            text.setVisibility(View.GONE);
        } else {
            text.setVisibility(View.VISIBLE);
            text.setText("首单" + Utils.changeStr10(shopItemMessage.getDiscount()) + "折");
        }

        TextView reText = viewHolder.getView(R.id.rebate_dis_tv);
        TextView guaranteeText = viewHolder.getView(R.id.rebate_guarantee_tv);
        if (shopItemMessage.getRebateDiscount() == null || Float.parseFloat(shopItemMessage.getRebateDiscount()) == 0) {
            reText.setVisibility(View.GONE);
            guaranteeText.setVisibility(View.GONE);
        } else {
            reText.setVisibility(View.VISIBLE);
            guaranteeText.setVisibility(View.VISIBLE);
            reText.setText("消费返利" + Utils.changeStr100(shopItemMessage.getRebateDiscount()) + "%");
        }
    }

    private void addTextView(TextView view, String textContent) {
        if (textContent == null) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            view.setText(textContent);
        }
    }


    /**
     * 显示星星
     *
     * @param viewHolder
     * @param recommendShopMessage
     */
    private void showAverageScore(ViewHolder viewHolder, ShopItemMessage recommendShopMessage) {
        long averageScore = Math.round(recommendShopMessage.getAvgScore());
        LinearLayout linearLayout = viewHolder.getView(R.id.star_ll);
        linearLayout.removeAllViews();
        if (averageScore % 2 == 0) {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    linearLayout.addView(controller.getStarIv(R.mipmap.star_all_selected, linearLayout));
                } else {
                    linearLayout.addView(controller.getStarIv(R.mipmap.star_no_selected, linearLayout));
                }
            }
        } else {
            for (int i = 1; i <= 5; i++) {
                if (i <= averageScore / 2) {
                    linearLayout.addView(controller.getStarIv(R.mipmap.star_all_selected, linearLayout));
                } else if (i == averageScore / 2 + 1) {
                    linearLayout.addView(controller.getStarIv(R.mipmap.star_half_selected, linearLayout));
                } else {
                    linearLayout.addView(controller.getStarIv(R.mipmap.star_no_selected, linearLayout));
                }
            }
        }
    }

//    private View createLine() {
//        View line = new View(mActivity);
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
//        layoutParams.setMargins(0, DensityUtil.dp2px(5), 0, 0);
//        line.setBackgroundColor(resources.getColor(R.color.color_e6e6e6));
//        line.setLayoutParams(layoutParams);
//        return line;
//    }


}
