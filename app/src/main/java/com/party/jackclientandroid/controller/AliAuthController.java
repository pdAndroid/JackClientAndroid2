package com.party.jackclientandroid.controller;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.AuthTask;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MainActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BindingInfo;
import com.party.jackclientandroid.ui_mine.my_wallet.SetPayPasswordActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.OnClick;

/**
 * Created by 派对 on 2018/12/17.
 */

public class AliAuthController {

    Handler handler;
    UserService mUserService;
    BaseActivity mActivity;

    public interface AuthInterface {
        void success(String auth_code);
    }

    public interface BindInterface {
        void success(BindingInfo bindingInfo);
    }

    public AliAuthController(BaseActivity mActivity) {
        this.mActivity = mActivity;
    }

    public void auth(AuthInterface authInterface) {
        mUserService = new UserService(mActivity);
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                Map<String, String> result = (Map<String, String>) msg.obj;
                String resultStr = result.get("result");
                if (!resultStr.contains("&")) return;
                String[] split = resultStr.split("&");
                Map<String, String> mapData = new HashMap<>();
                for (int i = 0; i < split.length; i++) {
                    String[] split1 = split[i].split("=");
                    mapData.put(split1[0], split1[1]);
                }
                if (!"200".equals(mapData.get("result_code"))) {
                    mActivity.showToast("授权失败");
                    return;
                }
                String auth_code = mapData.get("auth_code");
                authInterface.success(auth_code);
            }
        };
        getAliAuthInfoStr();
    }

    @OnClick(R.id.binding_btn)
    public void getAliAuthInfoStr() {
        mActivity.addDisposableIoMain(mUserService.getAliAuthInfoStr(), new DefaultConsumer<String>(mActivity.getmApplication()) {
            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                String infoStr = baseBean.getData();
                authTask(infoStr);
            }

            @Override
            public void operateError(String message) {
                super.operateError(message);
            }
        });
    }

    public void authTask(String infoStr) {
        new Thread(() -> {
            AuthTask authTask = new AuthTask(mActivity);
            Map<String, String> result = authTask.authV2(infoStr, true);
            Message msg = new Message();
            msg.obj = result;
            handler.sendMessage(msg);
        }).start();
    }

    /**
     * 使用授权code 去获取 用户信息
     *
     * @param auth_code
     */
    public void bindingZfbUser(String auth_code, BindInterface bindInterface) {
        mActivity.addDisposableIoMain(mUserService.checkZMScoreAndBindingAli(auth_code), new DefaultConsumer<BindingInfo>(mActivity.getmApplication()) {
            @Override
            public void operateSuccess(BaseResult<BindingInfo> baseBean) {
                bindInterface.success(baseBean.getData());
            }

            @Override
            public void operateError(String message) {
                DialogController.showConfirmDialog(mActivity, "温馨提示", message);
                super.operateError(message);

            }
        }, (Throwable throwable) -> {
            mActivity.showToast(throwable.getMessage());
        });
    }

}
