package com.party.jackclientandroid.controller;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.HomeService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.EatWhatForEveryday;
import com.party.jackclientandroid.bean.HomeBanner;
import com.party.jackclientandroid.bean.ShopClassification;
import com.party.jackclientandroid.imageloader.GlideImageLoader;
import com.party.jackclientandroid.ui_home.activity.BlackCardActivity;
import com.party.jackclientandroid.ui_home.activity.NearbyActivity;
import com.party.jackclientandroid.ui_home.fragment.HomeFragment;
import com.youth.banner.Banner;
import com.youth.banner.listener.OnBannerListener;

import java.util.ArrayList;
import java.util.List;

public class HomeFragmentController extends BaseController implements View.OnClickListener {
    HomeFragment mFragment;
    HomeService homeService;
    View headerView;
    LinearLayout categoryLayout;
    Banner banner;
    List<LinearLayout> categoryView;
    List<ImageView> imageViews;


    List<ShopClassification> dataList;
    private List<String> paths;
    private RequestOptions requestOptionsNoAim;

    public HomeFragmentController(HomeFragment homeFragment, HomeService homeService) {
        super((BaseActivity) homeFragment.getActivity());
        mFragment = homeFragment;
        this.homeService = homeService;
        requestOptionsNoAim = new RequestOptions();
        requestOptionsNoAim
                .placeholder(R.color.color_b2b2b2)
                .error(R.color.color_8a8a8a)
                .fitCenter()
                .skipMemoryCache(false)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    public View getHeaderView(ViewGroup viewGroup) {
        headerView = mInflater.inflate(R.layout.layout_home_header_view, viewGroup, false);

        banner = headerView.findViewById(R.id.banner);
        banner.setImageLoader(new GlideImageLoader());
        categoryView = new ArrayList<>();
        categoryView.add(headerView.findViewById(R.id.ll_0));
        categoryView.add(headerView.findViewById(R.id.ll_1));
        categoryView.add(headerView.findViewById(R.id.ll_2));
        categoryView.add(headerView.findViewById(R.id.ll_3));
        categoryView.add(headerView.findViewById(R.id.ll_4));
        categoryView.add(headerView.findViewById(R.id.ll_5));
        categoryView.add(headerView.findViewById(R.id.ll_6));
        categoryView.add(headerView.findViewById(R.id.ll_7));
        categoryView.add(headerView.findViewById(R.id.ll_8));
        categoryView.add(headerView.findViewById(R.id.ll_9));

        imageViews = new ArrayList<>();
        imageViews.add(headerView.findViewById(R.id.iv1));
        imageViews.add(headerView.findViewById(R.id.iv2));
        imageViews.add(headerView.findViewById(R.id.iv3));
        imageViews.add(headerView.findViewById(R.id.iv4));

        mFragment.addDisposableIoMain(homeService.getShopClassificationList(), new DefaultConsumer<List<ShopClassification>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ShopClassification>> baseBean) {
                dataList = baseBean.getData();
                mApplication.setShopClassificationList(baseBean.getData());
                for (int i = 0; i < dataList.size(); i++) {
                    LinearLayout linearLayout = categoryView.get(i);
                    ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                    TextView textView = (TextView) linearLayout.getChildAt(1);
                    mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, dataList.get(i).getImgUrl(), imageView);
                    textView.setText(dataList.get(i).getName());
                    linearLayout.setOnClickListener(HomeFragmentController.this);
                    linearLayout.setTag(i);
                }
            }
        });
        mFragment.addDisposableIoMain(homeService.getBannerImages(), new DefaultConsumer<List<HomeBanner>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<HomeBanner>> baseBean) {
                paths = new ArrayList<>();
                for (HomeBanner homeBanner : baseBean.getData()) {
                    paths.add(homeBanner.getImgUrl());
                }
                banner.setImages(paths);
                banner.setOnBannerListener(new OnBannerListener() {
                    @Override
                    public void OnBannerClick(int position) {

                    }
                });
                banner.start();

            }
        });

        mFragment.addDisposableIoMain(homeService.eatWhatForEveryday(), new DefaultConsumer<List<EatWhatForEveryday>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<EatWhatForEveryday>> baseBean) {
                List<EatWhatForEveryday> dataList = baseBean.getData();
                for (int i = 0; i < dataList.size(); i++) {
                    Glide.with(mFragment).load(dataList.get(i).getImgUrl()).into(imageViews.get(i));
                    imageViews.get(i).setOnClickListener((View v) -> {
                        Intent intent = new Intent(mFragment.getActivity(), BlackCardActivity.class);
                        intent.putExtra("categoryId", "");
                        intent.putExtra("conditionType", "2");
                        mFragment.startActivity(intent);
                    });
                }
            }
        });
        return headerView;
    }


    public void startAutoPlay() {
        banner.startAutoPlay();
    }

    public void stopAutoPlay() {
        banner.stopAutoPlay();
    }

    public void releaseBanner() {
        banner.releaseBanner();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(mFragment.getActivity(), NearbyActivity.class);
        int id = dataList.get((int) view.getTag()).getId();
        intent.putExtra("categoryId", id + "");
        intent.putExtra("conditionType", "1");
        mFragment.startActivity(intent);
    }
}