package com.party.jackclientandroid.controller;

import android.graphics.Color;
import android.support.annotation.IdRes;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.CashBean;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.zyyoona7.popup.EasyPopup;

/**
 * Created by 南宫灬绝痕 on 2018/12/22.
 */

public class CashController extends BaseController {
    private BaseActivity mActivity;
    private EasyPopup popup;
    private TextView wxName, aliName;
    private TextView payBtn;
    private RadioButton wxCheck, aliCheck;
    private View btn;

    public CashController(BaseActivity activity, View btn) {
        super(activity);
        mActivity = activity;
        this.btn = btn;
        initView();
    }

    private void initView() {
        View layout = mActivity.getLayoutInflater().inflate(R.layout.choose_cash_type_popup, null);

        wxName = layout.findViewById(R.id.wx_account_name);
        aliName = layout.findViewById(R.id.ali_account_name);
        payBtn = layout.findViewById(R.id.pay_btn);
        wxCheck = layout.findViewById(R.id.wx_check);
        aliCheck = layout.findViewById(R.id.ali_check);
        View dismiss_iv = layout.findViewById(R.id.dismiss_iv);

        dismiss_iv.setOnClickListener((View v) -> {
            popup.dismiss();
        });

        View wxLl = layout.findViewById(R.id.wx_ll);
        View aliLl = layout.findViewById(R.id.ali_ll);

        wxLl.setOnClickListener((View v) -> {
            wxCheck.setChecked(true);
        });

        aliLl.setOnClickListener((View v) -> {
            aliCheck.setChecked(true);
        });
        wxCheck.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            if (isChecked) aliCheck.setChecked(false);
        });
        aliCheck.setOnCheckedChangeListener((CompoundButton buttonView, boolean isChecked) -> {
            if (isChecked) wxCheck.setChecked(false);
        });

        layout.setOnKeyListener((View v, int keyCode, KeyEvent event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                if (popup != null) {
                    popup.dismiss();
                    popup = null;
                }
                return true;
            }
            return false;
        });


        popup = EasyPopup.create(mActivity)
                .setContentView(layout)
                .setAnimationStyle(R.style.Popupwindow)
                .setBackgroundDimEnable(true)
                .setDimValue(0.4f)
                .setDimColor(Color.GRAY)
                .setFocusAndOutsideEnable(true)
                .setWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply();
    }


    public void show(CashBean bean, View.OnClickListener listener) {
        if (bean.getWxAccount() != null) {
            wxName.setText("(" + bean.getWxAccount().getUserName() + ")");
        }
        if (bean.getAliAccount() != null) {
            aliName.setText("(" + bean.getAliAccount().getUserName() + ")");
        } else {

        }

        payBtn.setOnClickListener((View v) -> {
            popup.dismiss();
            if (wxCheck.isChecked()) {
                v.setTag("wx");
            }
            if (aliCheck.isChecked()) {
                v.setTag("ali");
            }
            listener.onClick(v);
        });
        popup.showAtLocation(btn, Gravity.BOTTOM, 0, 0);
    }

}
