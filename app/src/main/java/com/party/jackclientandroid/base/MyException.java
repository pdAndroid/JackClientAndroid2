package com.party.jackclientandroid.base;

/**
 * Created by 派对 on 2018/11/13.
 */

public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }
}
