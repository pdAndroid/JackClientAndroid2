package com.party.jackclientandroid.base;

import android.text.Editable;
import android.text.TextWatcher;

import com.party.jackclientandroid.utils.Utils;

import java.math.BigDecimal;

/**
 * Created by 派对 on 2018/11/8.
 */

public abstract class BaseTextWatch implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public abstract void afterTextChanged(Editable s);

    /**
     * 检查输入的数据是否合格
     *
     * @param s
     * @return
     */
    public boolean checkNumber(Editable s) {
        String number = s.toString();
        try {
            if (number.length() > 0) {
                Float.parseFloat(number);
            }
        } catch (Exception e) {
            s.delete(s.length() - 1, s.length());
            return false;
        }
        if (number.startsWith("00")) {
            s.delete(0, 1);
            return false;
        }

        if (Utils.checkScale2(number)) {
            s.delete(s.length() - 1, s.length());
            return false;
        }
        return true;
    }
}
