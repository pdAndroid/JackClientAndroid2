package com.party.jackclientandroid.base;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.party.jackclientandroid.BaseFragment;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.controller.FragmentUserVisibleController;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.okhttp.RxSchedulers;
import com.party.jackclientandroid.utils.ToastUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/**
 * Created by Tangxb on 2016/12/13.
 */

public abstract class BaseFragmentDrawer extends BaseFragment {

}
