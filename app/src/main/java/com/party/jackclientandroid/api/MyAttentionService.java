package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.MyAttention;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/4.
 */

public class MyAttentionService extends BaseService {
    public MyAttentionService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<MyAttention>>> getUserAttentionShopList(int pageNumber, int pageSize) {
        Map<String, String> map = getTokenMap();
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", pageSize + "");
        Observable<BaseResult<List<MyAttention>>> observable =
                getObservable().getUserAttentionShopList(map);
        return observable;
    }
}
