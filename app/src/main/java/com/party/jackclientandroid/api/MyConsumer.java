package com.party.jackclientandroid.api;

import com.party.jackclientandroid.bean.BaseResult;

import io.reactivex.functions.Consumer;


/**
 * 默认的请求返回处理<br>
 * Created by Taxngb on 2017/12/22.
 */

public abstract class MyConsumer<T> implements Consumer<BaseResult<T>> {

    private final int successCode = 200;
    private final int tokenErrorCode = 402;

    @Override
    public void accept(BaseResult<T> baseBean) throws Exception {

    }
}
