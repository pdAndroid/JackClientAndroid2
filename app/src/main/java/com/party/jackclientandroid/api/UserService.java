package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.AppInfo;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BindingInfo;
import com.party.jackclientandroid.bean.CashBean;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.ConsumeRecord;
import com.party.jackclientandroid.bean.EatWhatForEveryday;
import com.party.jackclientandroid.bean.RankingBean;
import com.party.jackclientandroid.bean.RecommendDetailListBean;
import com.party.jackclientandroid.bean.RegisterInfo;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.bean.VerCode;
import com.party.jackclientandroid.bean.VerifyUser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/8/24.
 */

public class UserService extends BaseService {

    public int second = 60;

    public UserService(BaseActivity baseActivity) {
        super(baseActivity);

    }

    public void getUserInfo(Consumer<BaseResult<User>> consumer) {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<User>> observable = getObservable().getUserInfo(tokenMap);
        baseActivity.addDisposableIoMain(observable, consumer);
    }

    public Observable<BaseResult<String>> alterPayPassword(String payPassword, String verKey) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("payPassword", payPassword);
        tokenMap.put("verKey", verKey);
        Observable<BaseResult<String>> observable = getObservable().alterPayPassword(tokenMap);
        return observable;
    }

    public Observable<BaseResult<VerCode>> getVerCode(String phone) {
        Observable<BaseResult<VerCode>> observable = getObservable().getVerCode(phone);
        return observable;
    }

    public Observable<BaseResult<VerCode>> getRegisterVerCode(String phone) {
        Observable<BaseResult<VerCode>> observable = getObservable().getRegisterVerCode(phone);
        return observable;
    }


    public Observable<BaseResult<Object>> getUpdatePasswordVerCode(String phone) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("phone", phone);
        Observable<BaseResult<Object>> observable = getObservable().getUpdatePasswordVerCode(tokenMap);
        return observable;
    }


    public Observable<BaseResult<VerifyUser>> authentication(String idCard, String name) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("idCard", idCard);
        tokenMap.put("name", name);
        Observable<BaseResult<VerifyUser>> authentication = getObservable().authentication(tokenMap);
        return authentication;
    }

    public Observable<BaseResult<VerifyUser>> humanAuthentication(String idCard, String name, String idCardImg) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("idCard", idCard);
        tokenMap.put("name", name);
        tokenMap.put("idImg", idCardImg);
        Observable<BaseResult<VerifyUser>> authentication = getObservable().humanAuthentication(tokenMap);
        return authentication;
    }


    //用户钱包
    public Observable<BaseResult<CommonResult>> getUserWallet() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<CommonResult>> observable = getObservable().getUserWallet(tokenMap);
        return observable;
    }


    public Observable<BaseResult<CashBean>> getPayBankInfo() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<CashBean>> observable = getObservable().getPayBankInfo(tokenMap);
        return observable;
    }

    //用户交易记录
    public Observable<BaseResult<List<ConsumeRecord>>> getUserConsumeRecord(int pageNumber, int pageSize) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("pageNumber", pageNumber + "");
        tokenMap.put("pageSize", pageSize + "");
        Observable<BaseResult<List<ConsumeRecord>>> observable = getObservable().getUserConsumeRecordList(tokenMap);
        return observable;
    }

    public HashMap<String, Object> getMap() {
        return new HashMap<>();
    }

    //交易记录中的总支出总收入
    public Observable<BaseResult<List<CommonResult>>> getUserTotalIncomeAndPayList() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<List<CommonResult>>> observable = getObservable().getUserTotalIncomeAndPayList(tokenMap);
        return observable;
    }

    //推荐有礼中的三个总收益
    public Observable<BaseResult<CommonResult>> getSumUserIncome() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<CommonResult>> observable = getObservable().getSumUserIncome(tokenMap);
        return observable;
    }

    //推荐有礼中的三个详情
    public Observable<BaseResult<List<RecommendDetailListBean>>> getListUserTradeRecordByState(int state, int pageNum, int pageSize) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("state", state + "");
        tokenMap.put("pageNumber", pageNum + "");
        tokenMap.put("pageSize", pageSize + "");
        Observable<BaseResult<List<RecommendDetailListBean>>> observable = getObservable().getListUserTradeRecordByState(tokenMap);
        return observable;
    }

    public Observable<BaseResult<User>> loginUser(String phone, String verCode, String adCode) {
        Map<String, String> map = new HashMap<>();
        map.put("verCode", verCode);
        map.put("phone", phone);
        map.put("cityId", adCode);
        map.put("loginType", "app");
        map.put("latitude", mApplication.getMap().getLatitude() + "");
        map.put("longitude", mApplication.getMap().getLongitude() + "");
        Observable<BaseResult<User>> observable = getObservable().login(map);
        return observable;
    }


    public Observable<BaseResult<User>> registerUser(String registerKey, String phone, String verCode, String inviteCode, String adCode) {
        Map<String, String> map = new HashMap<>();
        map.put("verCode", verCode);
        map.put("phone", phone);
        map.put("phoneParent", inviteCode);
        map.put("cityId", adCode);
        map.put("registerKey", registerKey);
        map.put("latitude", mApplication.getMap().getLatitude() + "");
        map.put("longitude", mApplication.getMap().getLongitude() + "");
        Observable<BaseResult<User>> observable = getObservable().register(map);
        return observable;
    }


    public Observable<BaseResult<CommonResult>> getUserCode() {
        Map<String, String> map = getTokenMap();
        Observable<BaseResult<CommonResult>> observable = getObservable().getUserCode(map);
        return observable;
    }


    public Observable<BaseResult<VerifyUser>> verifVerCode(String verCode, String phone, String idCard) {
        Map<String, String> map = getTokenMap();
        map.put("verCode", verCode);
        map.put("phone", phone);
        map.put("idcard", idCard);
        Observable<BaseResult<VerifyUser>> observable = getObservable().verifVerCode(map);
        return observable;
    }


    public Observable<BaseResult<CashBean.Account>> addAccount(String password, String cardType, String accountToken) {
        Map<String, String> map = getTokenMap();
        map.put("password", password);
        map.put("cardType", cardType);
        map.put("accessToken", accountToken);
        Observable<BaseResult<CashBean.Account>> observable = getObservable().addAccount(map);
        return observable;
    }

    public Observable<BaseResult<String>> addPayBankRecord(String password, String cardType, String money) {
        Map<String, String> map = getTokenMap();
        map.put("password", password);
        map.put("cardType", cardType);
        map.put("money", money);
        Observable<BaseResult<String>> observable = getObservable().addPayBankRecord(map);
        return observable;
    }

    public Observable<BaseResult<RegisterInfo>> checkWxIsRegister(String code) {
        HashMap<String, String> map = new HashMap<>();
        map.put("wxCode", code);
        Observable<BaseResult<RegisterInfo>> observable = getObservable().checkWxIsRegister(map);
        return observable;
    }

    public Observable<BaseResult<Object>> bindingWx(String bindingKey) {
        HashMap<String, String> map = getTokenMap();
        map.put("bindingKey", bindingKey);
        Observable<BaseResult<Object>> observable = getObservable().userBindingWx(map);
        return observable;
    }

    public Observable<BaseResult<BindingInfo>> getBindingUserInfo(String code) {
        HashMap<String, String> map = getTokenMap();
        map.put("wxCode", code);
        Observable<BaseResult<BindingInfo>> observable = getObservable().getBindingUserInfo(map);
        return observable;
    }

    public Observable<BaseResult<User>> wxLogin(String code) {
        HashMap<String, String> map = new HashMap<>();
        map.put("wxCode", code);
        getLocalMap(map);
        Observable<BaseResult<User>> observable = getObservable().wxLogin(map);
        return observable;
    }


    public Observable<BaseResult<AppInfo>> getAppId() {
        Observable<BaseResult<AppInfo>> observable = getObservable().getAppId();
        return observable;
    }

    //用户钱包
    public Observable<BaseResult<String>> getAliAuthInfo() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<String>> observable = getObservable().getAliAuthInfo(tokenMap);
        return observable;
    }

    /**
     * 获取阿里认证信息
     *
     * @return
     */
    public Observable<BaseResult<String>> getAliAuthInfoStr() {
        Observable<BaseResult<String>> observable = getObservable().getAliAuthInfo();
        return observable;
    }


    public Observable<BaseResult<BindingInfo>> checkZMScoreAndBindingAli(String auth_code) {
        HashMap<String, String> map = getTokenMap();
        map.put("accessToken", auth_code);
        Observable<BaseResult<BindingInfo>> observable = getObservable().checkZMScoreAndBindingAli(map);
        return observable;
    }

    public Observable<BaseResult<String>> initPayPassword(String password) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("payPassword", password);
        Observable<BaseResult<String>> observable = getObservable().initPayPassword(tokenMap);
        return observable;
    }

    //获取前十排名
    public Observable<BaseResult<List<RankingBean>>> getHighestReward() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<List<RankingBean>>> observable = getObservable().getHighestReward(tokenMap);
        return observable;
    }


}
