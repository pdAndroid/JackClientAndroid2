package com.party.jackclientandroid.api;

import com.party.jackclientandroid.bean.AppUpdateBean;
import com.party.jackclientandroid.bean.BaseResult;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by Taxngb on 2017/12/22.
 */

public interface AppUpdateRxAPI {

    /**
     * 检查app版本信息
     *
     * @return
     */
    @GET("http://www.paiduikeji.com/apkupdate/checkVersion")
    Observable<BaseResult<AppUpdateBean>> getAppUpdate(@QueryMap Map<String, String> data);
}
