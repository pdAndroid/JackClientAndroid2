package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.PayResult;
import com.party.jackclientandroid.bean.WxPaymentBean;

import java.util.Map;

import io.reactivex.Observable;

public class PayService extends BaseService {
    public PayService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 支付宝支付
     *
     * @param orderId
     * @return
     */
    public Observable<BaseResult<String>> aliPayOrder(String orderId, String ip) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderId", orderId + "");
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<String>> observable = getObservable().aliPayOrder(tokenMap);
        return observable;
    }

    /**
     * 微信支付
     *
     * @param orderId
     * @param ip
     * @return
     */
    public Observable<BaseResult<WxPaymentBean>> wxPayOrder(String orderId, String ip) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderId", orderId + "");
        tokenMap.put("ip", ip);
        tokenMap.put("desc", "testWxPayment");
        Observable<BaseResult<WxPaymentBean>> observable = getObservable().wxPayOrder(tokenMap);
        return observable;
    }

    public Observable<BaseResult<PayResult>> doMyWalletPayOrder(String orderId, String payPwd) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderId", orderId);
        tokenMap.put("pwd",payPwd);
        Observable<BaseResult<PayResult>> observable = getObservable().doMyWalletPayOrder(tokenMap);
        return observable;
    }
}
