package com.party.jackclientandroid.api;

import com.party.jackclientandroid.bean.AppInfo;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BindingInfo;
import com.party.jackclientandroid.bean.CashBean;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.ConsumeRecord;
import com.party.jackclientandroid.bean.DeliciousFood;
import com.party.jackclientandroid.bean.DetailsDiscountPaymentBean;
import com.party.jackclientandroid.bean.DiscountPayBillBean;
import com.party.jackclientandroid.bean.EatWhatForEveryday;
import com.party.jackclientandroid.bean.FilterCondition;
import com.party.jackclientandroid.bean.GrabRedPacketBean;
import com.party.jackclientandroid.bean.GroupBuyCoupon;
import com.party.jackclientandroid.bean.GroupBuyCouponDetail;
import com.party.jackclientandroid.bean.GroupBuyCouponIncludeGoods;
import com.party.jackclientandroid.bean.HomeBanner;
import com.party.jackclientandroid.bean.Notice;
import com.party.jackclientandroid.bean.MyAttention;
import com.party.jackclientandroid.bean.MyCouponPackage;
import com.party.jackclientandroid.bean.MyOfferRebatesBean;
import com.party.jackclientandroid.bean.NeighbourSpellingGroupBean;
import com.party.jackclientandroid.bean.OSSConfig;
import com.party.jackclientandroid.bean.Order;
import com.party.jackclientandroid.bean.OrderCondition;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.OrderStatusBean;
import com.party.jackclientandroid.bean.PayResult;
import com.party.jackclientandroid.bean.PayType;
import com.party.jackclientandroid.bean.PlayerInfoBean;
import com.party.jackclientandroid.bean.RankingBean;
import com.party.jackclientandroid.bean.RecommendDetailListBean;
import com.party.jackclientandroid.bean.RegisterInfo;
import com.party.jackclientandroid.bean.BlackCardItem;
import com.party.jackclientandroid.bean.ShopItemMessage;
import com.party.jackclientandroid.bean.RecommendVegetable;
import com.party.jackclientandroid.bean.ReplaceMoneyCoupon;
import com.party.jackclientandroid.bean.ReplaceMoneyCouponDetail;
import com.party.jackclientandroid.bean.SearchMenuBean;
import com.party.jackclientandroid.bean.SearchPoint;
import com.party.jackclientandroid.bean.SecondKillCouponBean;
import com.party.jackclientandroid.bean.SecondKillCouponDetail;
import com.party.jackclientandroid.bean.ShopClassification;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.bean.SpellingCoupon;
import com.party.jackclientandroid.bean.SpellingGroupCouponBean;
import com.party.jackclientandroid.bean.SpellingGroupCouponDetail;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.bean.UserCommentAndReply;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.bean.VerCode;
import com.party.jackclientandroid.bean.VerifyUser;
import com.party.jackclientandroid.bean.VideoCommentBean;
import com.party.jackclientandroid.bean.VideoDetail;
import com.party.jackclientandroid.bean.VipBean;
import com.party.jackclientandroid.bean.WxPaymentBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by 派对 on 2018/7/23.
 */

public interface BaseApi {

    @POST("v5/user/login")
    Observable<BaseResult<User>> login(@QueryMap Map<String, String> data);


    @POST("v5/user/register")
    Observable<BaseResult<User>> register(@QueryMap Map<String, String> data);

    //用户钱包
    @POST("v2/userWallet/getWallet")
    Observable<BaseResult<CommonResult>> getUserWallet(@QueryMap Map<String, String> data);


    @GET("v2/user/getVerCode")
    Observable<BaseResult<VerCode>> getVerCode(@Query("phone") String phone);

    @GET("v4/user/getRegisterVerCode")
    Observable<BaseResult<VerCode>> getRegisterVerCode(@Query("phone") String phone);


    @GET("userConcern/getUserConcernList")
    Observable<BaseResult<Object>> getFans(@Header("sign") String sign, @QueryMap Map<String, String> data);

    @GET("shop/listShopByCategoryId")
    Observable<BaseResult<List<DeliciousFood>>> findByClassify(@QueryMap Map<String, String> data);

    //获得店铺详情
    @GET("shop/getShop")
    Observable<BaseResult<ShopDetail>> getShopDetail(@QueryMap Map<String, String> data);

    @GET("shopCategory/getShopCategoryList")
    Observable<BaseResult<List<ShopClassification>>> getShopClassificationList();


    @POST("condition/getFilterCondition")
    Observable<BaseResult<List<FilterCondition>>> getFilterCondition(@QueryMap Map<String, String> data);


    @POST("condition/getOrderByCondition")
    Observable<BaseResult<List<OrderCondition>>> getOrderByCondition(@QueryMap Map<String, String> data);


    @GET("banner/getHomeBannerList")
    Observable<BaseResult<List<HomeBanner>>> getHomeBannerList();

    //商铺详情-商铺中的代金卷
    @GET("coupon/listCouponByShopId")
    Observable<BaseResult<List<ReplaceMoneyCoupon>>> getReplaceMoneyCouponList(@QueryMap Map<String, String> data);

    //商铺中的套餐（团餐）
    @GET("groupBuy/listGroupBuyByShopId")
    Observable<BaseResult<List<GroupBuyCoupon>>> getGroupBuyCouponList(@QueryMap Map<String, String> data);

    //获取代金券详情
    @GET("coupon/getCoupon")
    Observable<BaseResult<ReplaceMoneyCouponDetail>> getReplaceMoneyCouponDetail(@QueryMap Map<String, String> data);

    //获取团购券券详情
    @GET("groupBuy/getGroupBuy")
    Observable<BaseResult<GroupBuyCouponDetail>> getGroupBuyCouponDetail(@QueryMap Map<String, String> data);

    //获取首页推荐商铺列表
    @GET("shop/listShop")
    Observable<BaseResult<List<ShopItemMessage>>> getRecommendShopList(@QueryMap Map<String, String> data);

    //添加一个订单
    @FormUrlEncoded
    @POST("userOrder/addOrder")
    Observable<BaseResult<CommonResult>> addOrder(@FieldMap Map<String, String> data);

    //添加一个订单
    @POST("user/updatePayPassword")
    Observable<BaseResult<String>> alterPayPassword(@QueryMap Map<String, String> data);


    //添加一个订单
    @POST("user/initPayPassword")
    Observable<BaseResult<String>> initPayPassword(@QueryMap Map<String, String> data);

    //获取前十排名
    @POST("v5/user/getHighestReward")
    Observable<BaseResult<List<RankingBean>>> getHighestReward(@QueryMap Map<String, String> data);

    //余额支付
    @POST("pay/walletPayment")
    Observable<BaseResult<String>> payByWallet(@QueryMap Map<String, String> data);

    //获取支付方式
    @GET("pay/payType")
    Observable<BaseResult<List<PayType>>> getPayType();

    //每天吃什么
    @GET("banner/getSuperDiscountList")
    Observable<BaseResult<List<EatWhatForEveryday>>> getEatWhatForEveryday();

    //搜索框提示
    @GET("shop/getSearchKey")
    Observable<BaseResult<List<SearchPoint>>> getSearchPointList(@QueryMap Map<String, String> data);

    //搜索
    @GET("shop/searchShop")
    Observable<BaseResult<List<ShopItemMessage>>> searchShop(@QueryMap Map<String, String> data);

    //商铺中的推荐菜
    @GET("item/listHotGoods")
    Observable<BaseResult<List<RecommendVegetable>>> getRecommendVegetableList(@QueryMap Map<String, String> data);


    //获取店铺关注状态
    @GET("userConcern/checkConcern")
    Observable<BaseResult<CommonResult>> getAttentionStaus(@QueryMap Map<String, String> data);

    //取消关注
    @POST("userConcern/cancelConcern")
    Observable<BaseResult<CommonResult>> cancelAttention(@QueryMap Map<String, String> data);

    //关注
    @POST("userConcern/concern")
    Observable<BaseResult<CommonResult>> addAttention(@QueryMap Map<String, String> data);

    @GET("item/listItemByGroupBuyId")
    Observable<BaseResult<List<GroupBuyCouponIncludeGoods>>> getGroupBuyCouponIncludeGoods(@QueryMap Map<String, String> data);

    //获取用户订单列表
    @GET("userOrder/listUserOrder")
    Observable<BaseResult<List<Order>>> getUserOrderList(@QueryMap Map<String, String> data);

    //获取订单详情
    @GET("userOrder/getUserOrder")
    Observable<BaseResult<OrderDetail>> getUserOrderDetail(@QueryMap Map<String, String> data);

    //获得订单详情中的每个卷码和商品信息
    @GET("userOrder/listUserOrderDetail")
    Observable<BaseResult<List<UserOrderExtraDetail>>> getUserExtraOrderDetailList(@QueryMap Map<String, String> data);

    //我的券包
    @GET("user/myCoupons")
    Observable<BaseResult<List<MyCouponPackage>>> getMyCoupons(@QueryMap Map<String, String> data);


    //用户关注的商铺列表
    @GET("userConcern/listConcern")
    Observable<BaseResult<List<MyAttention>>> getUserAttentionShopList(@QueryMap Map<String, String> data);


    //获取提现信息
    @GET("payBank/getPayBankInfo")
    Observable<BaseResult<CashBean>> getPayBankInfo(@QueryMap Map<String, String> data);


    //交易记录
    @GET("userTradeRecord/listUserTradeRecord")
    Observable<BaseResult<List<ConsumeRecord>>> getUserConsumeRecordList(@QueryMap Map<String, String> data);

    //交易记录 中的总支出总收入
    @GET("userTradeRecord/countUserTradeRecord")
    Observable<BaseResult<List<CommonResult>>> getUserTotalIncomeAndPayList(@QueryMap Map<String, String> data);

    //获取店铺评论
    @GET("shopComment/listCommentByShopId")
    Observable<BaseResult<List<UserCommentAndReply>>> getUserCommentList(@QueryMap Map<String, String> data);

    //获取店铺回复
    @GET("shopComment/getCommentReply")
    Observable<BaseResult<List<UserCommentAndReply>>> getUserReplyList(@QueryMap Map<String, String> data);

    //全部会员信息
    @GET("v2/userInventPrizeRecord/listUserLevel")
    Observable<BaseResult<VipBean>> getVipList(@QueryMap Map<String, String> data);

    //获取下级列表
    @GET("userInventPrizeRecord/listSubUser")
    Observable<BaseResult<List<CommonResult>>> getSubUserList(@QueryMap Map<String, String> data);

    //获取下级总人数
    @GET("userInventPrizeRecord/countSubUser")
    Observable<BaseResult<CommonResult>> getCountSubUser(@QueryMap Map<String, String> data);

    //推荐有礼中的三个总收益
    @GET("userTradeRecord/sumUserIncome")
    Observable<BaseResult<CommonResult>> getSumUserIncome(@QueryMap Map<String, String> data);

    //推荐有礼中的三个详情
    @GET("userTradeRecord/listUserTradeRecordByState")
    Observable<BaseResult<List<RecommendDetailListBean>>> getListUserTradeRecordByState(@QueryMap Map<String, String> data);

    //优惠买单中的代金卷
    @GET("coupon/getDiscountPayBill")
    Observable<BaseResult<DiscountPayBillBean>> getDiscountPayBill(@QueryMap Map<String, String> data);


    //优惠买单
    @FormUrlEncoded
    @POST("userOrder/addDiscountOrder")
    Observable<BaseResult<CommonResult>> addDiscountOrder(@FieldMap Map<String, String> data);

    /**
     * 申请退款
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/refund")
    Observable<BaseResult<CommonResult>> applyRefund(@FieldMap Map<String, String> data);

    /**
     * 添加评论
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("shopComment/addCommentReply")
    Observable<BaseResult<CommonResult>> addComment(@FieldMap Map<String, String> data);

    /**
     * 获取订单全部状态
     *
     * @return
     */
    @GET("userOrder/getOrderState")
    Observable<BaseResult<List<OrderStatusBean>>> getOrderState(@QueryMap Map<String, String> data);

    /**
     * 微信支付
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/generateAppWxPayOrder")
    Observable<BaseResult<WxPaymentBean>> wxPayOrder(@FieldMap Map<String, String> data);

    /**
     * 余额支付(用我的钱包支付)
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/walletPayment")
    Observable<BaseResult<PayResult>> doMyWalletPayOrder(@FieldMap Map<String, String> data);

    /**
     * 支付宝支付
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/generateAppAliPayOrder")
    Observable<BaseResult<String>> aliPayOrder(@FieldMap Map<String, String> data);

    /**
     * 获得视频列表
     *
     * @param data
     * @return
     */
    @GET("video/listVideo")
    Observable<BaseResult<List<PlayerInfoBean>>> listVideo(@QueryMap Map<String, String> data);

    /**
     * 获得视频列表
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("video/getVideoDetail")
    Observable<BaseResult<VideoDetail>> getVideoDetail(@FieldMap Map<String, String> data);

    /**
     * 抢红包
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("video/GrabRedEnvelope")
    Observable<BaseResult<GrabRedPacketBean>> grabRedEnvelope(@FieldMap Map<String, String> data);

    //获取OSS配置
    @GET("ossConfig/getOssConfig")
    Observable<BaseResult<OSSConfig>> getOssConfig(@Header("sign") String sign, @QueryMap Map<String, String> signData);

    /**
     * 获得邀请二维码
     *
     * @param data
     * @return
     */
    @GET("user/getQRCode")
    Observable<BaseResult<CommonResult>> getQRCode(@QueryMap Map<String, String> data);

    /**
     * 获得视频评论
     *
     * @param data
     * @return
     */
    @GET("videoComment/listVideoComment")
    Observable<BaseResult<List<VideoCommentBean>>> listVideoComment(@QueryMap Map<String, String> data);

    /**
     * 获得二维码邀请参数
     *
     * @param data
     * @return
     */
    @GET("user/userCode")
    Observable<BaseResult<CommonResult>> getUserCode(@QueryMap Map<String, String> data);

    /**
     * 添加视频评论
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("videoComment/addVideoComment")
    Observable<BaseResult<GrabRedPacketBean>> addVideoComment(@FieldMap Map<String, String> data);

    /**
     * <<<<<<< HEAD
     * 支付宝支付
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("pay/generateAppAliPayOrder")
    Observable<BaseResult<String>> payOrder(@FieldMap Map<String, String> data);

    /**
     * 获取全部秒杀券
     *
     * @param data
     * @return
     */
    @GET("secondKill/listSecondKill")
    Observable<BaseResult<List<SecondKillCouponBean>>> getAllSecondKillCouponList(@QueryMap Map<String, String> data);

    /**
     * 获取全部拼团券
     *
     * @param data
     * @return
     */
    @GET("spellingGroup/listSpellingGroup")
    Observable<BaseResult<List<SpellingGroupCouponBean>>> getAllSpellingGroupCouponList(@QueryMap Map<String, String> data);

    /**
     * 查看用户的所有商铺的优惠金列表
     *
     * @param data
     * @return
     */
    @GET("userRebateMoney/listUserRebateMoney")
    Observable<BaseResult<List<MyOfferRebatesBean>>> listUserRebateMoney(@QueryMap Map<String, String> data);

    /**
     * 获得用户在这个商铺中优惠金收支记录
     *
     * @param data
     * @return
     */
    @GET("userRebateMoney/listUserRebateRecord")
    Observable<BaseResult<List<DetailsDiscountPaymentBean>>> listUserRebateRecord(@QueryMap Map<String, String> data);

    /**
     * 获取店铺拼团券列表
     * 获取拼团券列表
     *
     * @param data
     * @return
     */
    @GET("spellingGroup/listSpellingGroupByShopId")
    Observable<BaseResult<List<SpellingGroupCouponBean>>> getSpellingGroupCouponList(@QueryMap Map<String, String> data);


    /**
     * 获取拼团券详情
     *
     * @param data
     * @return
     */
    @GET("spellingGroup/getSpellingGroup")
    Observable<BaseResult<SpellingGroupCouponDetail>> getSpellingGroupCouponDetail(@QueryMap Map<String, String> data);


    @GET("v2/user/getUserInfo")
    Observable<BaseResult<User>> getUserInfo(@QueryMap Map<String, String> tokenMap);

    /**
     * 获取店铺秒杀券列表
     *
     * @param data
     * @return
     */
    @GET("secondKill/listSecondKillByShopId")
    Observable<BaseResult<List<SecondKillCouponBean>>> getShopSecondKillCouponList(@QueryMap Map<String, String> data);

    /**
     * 获取秒杀券详情
     *
     * @param data
     * @return
     */
    @GET("secondKill/getSecondKill")
    Observable<BaseResult<SecondKillCouponDetail>> getSecondKillCouponDetail(@QueryMap Map<String, String> data);

    /**
     * 点赞
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("videoNice/nice")
    Observable<BaseResult<String>> videoNice(@FieldMap Map<String, String> data);

    /**
     * 取消点赞
     *
     * @param data
     * @return
     */
    @FormUrlEncoded
    @POST("videoNice/cancelNice")
    Observable<BaseResult<String>> cancelNice(@FieldMap Map<String, String> data);

    @POST("searchMenu/searchMenu")
    Observable<BaseResult<List<SearchMenuBean>>> getSearchMenu(@QueryMap Map<String, String> data);

    @GET("shop/searchNearbyDiscount")
    Observable<BaseResult<List<NeighbourSpellingGroupBean>>> searchNearbyDiscount(@QueryMap Map<String, String> data);

    //获得公告
    @GET("user/getNotice")
    Observable<BaseResult<Notice>> getNotice(@QueryMap Map<String, String> data);

    @FormUrlEncoded
    @POST("user/authentication")
    Observable<BaseResult<VerifyUser>> authentication(@FieldMap Map<String, String> data);

    @FormUrlEncoded
    @POST("user/addAuditAuthentication")
    Observable<BaseResult<VerifyUser>> humanAuthentication(@FieldMap Map<String, String> data);


    @GET("user/getUpdatePasswordVerCode")
    Observable<BaseResult<Object>> getUpdatePasswordVerCode(@QueryMap Map<String, String> phone);

    @FormUrlEncoded
    @POST("user/verifVerCode")
    Observable<BaseResult<VerifyUser>> verifVerCode(@FieldMap Map<String, String> map);

    /**
     * 根据多个条件查询到商铺
     */
    @POST("shop/getShopByMultiCondition")
    Observable<BaseResult<List<ShopItemMessage>>> getShopByMultiCondition(@QueryMap Map<String, String> data);

    /**
     * 根据多个条件查询到商铺
     */
    @FormUrlEncoded
    @POST("v2/shop/getCouponByMultiCondition")
    Observable<BaseResult<List<SpellingCoupon>>> getSpellingCoupons(@FieldMap Map<String, String> data);

    /**
     * 根据多个条件查询到商铺
     */
    @FormUrlEncoded
    @POST("v2/shop/getCouponByMultiCondition")
    Observable<BaseResult<List<BlackCardItem>>> getCouponsBackCards(@FieldMap Map<String, String> data);


    @FormUrlEncoded
    @POST("account/addAccount")
    Observable<BaseResult<CashBean.Account>> addAccount(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("payBank/addPayBankRecord")
    Observable<BaseResult<String>> addPayBankRecord(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("v4/user/checkWxIsRegister")
    Observable<BaseResult<RegisterInfo>> checkWxIsRegister(@FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST("user/userBindingWx")
    Observable<BaseResult<Object>> userBindingWx(@FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST("v5/user/wxLogin")
    Observable<BaseResult<User>> wxLogin(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("v4/user/getWxBindingUserInfo")
    Observable<BaseResult<BindingInfo>> getBindingUserInfo(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("user/checkUserCouponIsSuccess")
    Observable<BaseResult<String>> checkUserCouponIsSuccess(@FieldMap Map<String, String> map);

    @POST("v4/user/getAppId")
    Observable<BaseResult<AppInfo>> getAppId();

    @FormUrlEncoded
    @POST("user/getShopIdByCode")
    Observable<BaseResult<String>> getShopIdByCode(@FieldMap Map<String, String> map);

    @GET("aliAuth/getAliAuthInfo")
    Observable<BaseResult<String>> getAliAuthInfo();

    @FormUrlEncoded
    @POST("aliAuth/checkZMScoreAndBindingAli")
    Observable<BaseResult<BindingInfo>> checkZMScoreAndBindingAli(@FieldMap Map<String, String> map);

    //获取提现信息
    @POST("payBank/getAliAuthInfo")
    Observable<BaseResult<String>> getAliAuthInfo(@FieldMap Map<String, String> data);

}
