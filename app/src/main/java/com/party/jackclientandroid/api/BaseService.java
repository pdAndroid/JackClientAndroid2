package com.party.jackclientandroid.api;

import android.text.TextUtils;
import android.view.View;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.okhttp.RetrofitRxClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by 派对 on 2018/7/23.
 */

public class BaseService {
    BaseActivity baseActivity;
    MApplication mApplication;

    public BaseApi getObservable() {
        return RetrofitRxClient.INSTANCE
                .getRetrofit()
                .create(BaseApi.class);
    }


    public BaseService(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        this.mApplication = (MApplication) baseActivity.getApplication();
    }


    /**
     * 基本参数的容器
     *
     * @return
     */
    public HashMap<String, Object> getMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        return hashMap;
    }

    public Map<String, String> getLocalMap() {
        return getLocalMap(null);
    }

    public Map<String, String> getLocalMap(Map map) {
        if (map == null) map = new HashMap<>();
        map.put("lon", mApplication.getMap().getLongitude() + "");
        map.put("lat", mApplication.getMap().getLatitude() + "");
        return map;
    }


    /**
     * 基本参数的容器
     *
     * @return
     */
    public HashMap<String, String> getTokenMap() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("token", TextUtils.isEmpty(mApplication.getToken()) ? "" : mApplication.getToken());
        return hashMap;
    }

    public void ok() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();
                HttpUrl url = originalHttpUrl.newBuilder().addQueryParameter("apikey", "you value").build();
                // Request customization: add  request headers
                Request.Builder requestBuilder = original.newBuilder().url(url);
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

    }
}
