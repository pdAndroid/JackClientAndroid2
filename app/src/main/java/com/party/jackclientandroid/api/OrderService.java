package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.Order;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.OrderStatusBean;
import com.party.jackclientandroid.bean.PayType;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/8/16.
 */

public class OrderService extends BaseService {
    public OrderService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<Order>>> getUserOrderList(int orderState, Integer page) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("type", orderState + "");
        tokenMap.put("pageNumber", page + "");
        tokenMap.put("pageSize", Constant.PAGE_SIZE + "");
        Observable<BaseResult<List<Order>>> observable =
                getObservable().getUserOrderList(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<PayType>>> getPayType() {
        Observable<BaseResult<List<PayType>>> observable =
                getObservable().getPayType();
        return observable;
    }

    public Observable<BaseResult<OrderDetail>> getUserOrderDetail(String orderId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderId", orderId);
        Observable<BaseResult<OrderDetail>> observable =
                getObservable().getUserOrderDetail(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<UserOrderExtraDetail>>> getUserExOrderDetail(String orderId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderId", orderId);
        Observable<BaseResult<List<UserOrderExtraDetail>>> observable =
                getObservable().getUserExtraOrderDetailList(tokenMap);
        return observable;
    }

    /**
     * 优惠买单
     *
     * @param tokenMap
     * @return
     */
    public Observable<BaseResult<CommonResult>> addDiscountOrder(Map<String, String> tokenMap) {
        Observable<BaseResult<CommonResult>> observable = getObservable().addDiscountOrder(tokenMap);
        return observable;
    }

    /**
     * 获取订单全部状态
     *
     * @return
     */
    public Observable<BaseResult<List<OrderStatusBean>>> getOrderState() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<List<OrderStatusBean>>> observable = getObservable().getOrderState(tokenMap);
        return observable;
    }

    public Observable<BaseResult<CommonResult>> applyRefund(String ip, String orderIdArr) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("ip", ip);
        tokenMap.put("orderIds", orderIdArr);
        Observable<BaseResult<CommonResult>> observable = getObservable().applyRefund(tokenMap);
        return observable;
    }

    public Observable<BaseResult<String>> checkUserCouponIsSuccess(String orderCode) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("orderCode", orderCode);
        Observable<BaseResult<String>> observable = getObservable().checkUserCouponIsSuccess(tokenMap);
        return observable;
    }
}
