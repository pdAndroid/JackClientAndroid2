package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.GrabRedPacketBean;
import com.party.jackclientandroid.bean.PlayerInfoBean;
import com.party.jackclientandroid.bean.VideoDetail;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class VideoService extends BaseService {
    public VideoService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 获得视频列表
     *
     * @return
     */
    public Observable<BaseResult<List<PlayerInfoBean>>> listVideo(double longitude, double latitude, int type) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("longitude", longitude + "");
        tokenMap.put("latitude", latitude + "");
        tokenMap.put("type", type + "");
        Observable<BaseResult<List<PlayerInfoBean>>> observable = getObservable().listVideo(tokenMap);
        return observable;
    }

    public Observable<BaseResult<VideoDetail>> getVideoDetail(String shopId, String videoId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("videoId", videoId);
        tokenMap.put("shopId", shopId);
        Observable<BaseResult<VideoDetail>> observable = getObservable().getVideoDetail(tokenMap);
        return observable;
    }


    /**
     * 抢红包
     *
     * @return
     */
    public Observable<BaseResult<GrabRedPacketBean>> grabRedEnvelope(String videoId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("videoId", videoId);
        Observable<BaseResult<GrabRedPacketBean>> observable = getObservable().grabRedEnvelope(tokenMap);
        return observable;
    }

    /**
     * 点赞
     *
     * @return
     */
    public Observable<BaseResult<String>> videoNice(String videoId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("videoId", videoId);
        Observable<BaseResult<String>> observable = getObservable().videoNice(tokenMap);
        return observable;
    }

    /**
     * 取消点赞
     *
     * @return
     */
    public Observable<BaseResult<String>> cancelNice(String videoId) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("videoId", videoId);
        Observable<BaseResult<String>> observable = getObservable().cancelNice(tokenMap);
        return observable;
    }
}
