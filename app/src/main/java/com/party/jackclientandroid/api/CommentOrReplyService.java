package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;

import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/9/28.
 */

public class CommentOrReplyService extends BaseService{

    public CommentOrReplyService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 优惠买单
     * @param tokenMap
     * @return
     */
    public Observable<BaseResult<CommonResult>> addComment(Map<String, String> tokenMap) {
        Observable<BaseResult<CommonResult>> observable = getObservable().addComment(tokenMap);
        return observable;
    }
}
