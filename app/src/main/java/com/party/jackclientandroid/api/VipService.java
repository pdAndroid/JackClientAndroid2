package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.VipBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/8/26.
 */

public class VipService extends BaseService {
    public VipService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<VipBean>> getVipData() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<VipBean>> observable = getObservable().getVipList(tokenMap);
        return observable;
    }

    //获得下级列表
    public Observable<BaseResult<List<CommonResult>>> getSubUserList(int type, int pageNumber, int pageSize) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("pageNumber", pageNumber + "");
        tokenMap.put("pageSize", pageSize + "");
        tokenMap.put("type", type + "");
        Observable<BaseResult<List<CommonResult>>> observable =
                getObservable().getSubUserList(tokenMap);
        return observable;
    }

    //获得下级总人数
    public Observable<BaseResult<CommonResult>> getSubUserCount(int type) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("type", type + "");
        Observable<BaseResult<CommonResult>> observable = getObservable().getCountSubUser(tokenMap);
        return observable;
    }

    /**
     * 获得邀请二维码
     *
     * @return
     */
    public Observable<BaseResult<CommonResult>> getQRCode() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<CommonResult>> observable =
                getObservable().getQRCode(tokenMap);
        return observable;
    }
}
