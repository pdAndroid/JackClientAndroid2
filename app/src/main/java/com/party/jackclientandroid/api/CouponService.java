package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.DetailsDiscountPaymentBean;
import com.party.jackclientandroid.bean.GroupBuyCouponDetail;
import com.party.jackclientandroid.bean.GroupBuyCouponIncludeGoods;
import com.party.jackclientandroid.bean.MyCouponPackage;
import com.party.jackclientandroid.bean.MyOfferRebatesBean;
import com.party.jackclientandroid.bean.ReplaceMoneyCouponDetail;
import com.party.jackclientandroid.bean.SecondKillCouponBean;
import com.party.jackclientandroid.bean.SecondKillCouponDetail;
import com.party.jackclientandroid.bean.SpellingGroupCouponBean;
import com.party.jackclientandroid.bean.SpellingGroupCouponDetail;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/8/18.
 */

public class CouponService extends BaseService {

    public CouponService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<ReplaceMoneyCouponDetail>> getReplaceMoneyCouponDetail(Long replaceMoneyCouponId) {
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("couponId", replaceMoneyCouponId + "");
        Observable<BaseResult<ReplaceMoneyCouponDetail>> observable =
                getObservable().getReplaceMoneyCouponDetail(tokenMap);
        return observable;
    }

    public Observable<BaseResult<GroupBuyCouponDetail>> getGroupBuyCouponDetail(Long groupBuyCouponId) {
        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("groupBuyId", groupBuyCouponId + "");
        Observable<BaseResult<GroupBuyCouponDetail>> observable =
                getObservable().getGroupBuyCouponDetail(tokenMap);
        return observable;
    }

    //提交订单
    public Observable<BaseResult<CommonResult>> submitOrder(Map dataMap) {
        HashMap<String, String> tokenMap = getTokenMap();
        tokenMap.putAll(dataMap);
        Observable<BaseResult<CommonResult>> observable = getObservable().addOrder(tokenMap);
        return observable;
    }

    public Observable<BaseResult<String>> payByWallet(String orderId, String payPwd, String tradeType) {

        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("order_id", orderId);
        tokenMap.put("payPwd", payPwd);
        tokenMap.put("trade_type", tradeType);
        Observable<BaseResult<String>> observable =
                getObservable().payByWallet(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<GroupBuyCouponIncludeGoods>>> getGroupBuyCouponIncludeGoods(Long groupBuyCouponId) {
        Map<String, String> map = new HashMap<>();
        map.put("groupBuyId", groupBuyCouponId + "");
        Observable<BaseResult<List<GroupBuyCouponIncludeGoods>>> observable =
                getObservable().getGroupBuyCouponIncludeGoods(map);
        return observable;
    }

    /*//获取用户代金券信息
    public Observable<BaseResult<List<ReplaceMoneyCoupon>>> getUserReplaceMoneyCouponList(int page){
        Map<String,String> map = getTokenMap();
        map.put("pageNumber", page+"");
        map.put("pageSize",Constant.PAGE_SIZE+"");
        Observable<BaseResult<List<ReplaceMoneyCoupon>>> observable =
                getObservable().getUserReplaceMoneyCouponList(map);
        return observable;
    }

    //获取用户团购券信息
    public Observable<BaseResult<List<GroupBuyCoupon>>> getUserGroupBuyCouponList(int page){
        Map<String,String> map = getTokenMap();
        map.put("pageNumber", page+"");
        map.put("pageSize",Constant.PAGE_SIZE+"");
        Observable<BaseResult<List<GroupBuyCoupon>>> observable =
                getObservable().getUserGroupBuyCouponList(map);
        return observable;
    }*/

    /**
     * 我的券包
     *
     * @param page
     * @return
     */
    public Observable<BaseResult<List<MyCouponPackage>>> getMyCoupons(int page, int size) {
        Map<String, String> map = getTokenMap();
        map.put("pageNumber", page + "");
        map.put("pageSize", size + "");
        Observable<BaseResult<List<MyCouponPackage>>> observable =
                getObservable().getMyCoupons(map);
        return observable;
    }


    /**
     * 获取店铺拼团券列表
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Observable<BaseResult<List<SpellingGroupCouponBean>>> getSpellingGroupCouponList(long shopId, int pageNumber, int pageSize) {
        Map<String, String> map = getTokenMap();
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", Constant.PAGE_SIZE + "");
        map.put("shopId", shopId + "");
        Observable<BaseResult<List<SpellingGroupCouponBean>>> observable =
                getObservable().getSpellingGroupCouponList(map);
        return observable;
    }

    /**
     * 获取拼团券详情
     *
     * @param spellingGroupId
     * @return
     */
    public Observable<BaseResult<SpellingGroupCouponDetail>> getSpellingGroupCouponDetail(long spellingGroupId) {
        Map<String, String> map = new HashMap<>();
        map.put("spellingGroupId", spellingGroupId + "");
        Observable<BaseResult<SpellingGroupCouponDetail>> observable =
                getObservable().getSpellingGroupCouponDetail(map);
        return observable;
    }

    /**
     * 获取店铺秒杀券列表
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Observable<BaseResult<List<SecondKillCouponBean>>> getShopSecondKillCouponList(long shopId, int pageNumber, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", Constant.PAGE_SIZE + "");
        Observable<BaseResult<List<SecondKillCouponBean>>> observable =
                getObservable().getShopSecondKillCouponList(map);
        return observable;
    }


    /**
     * 获取秒杀券详情
     *
     * @param secondKillCouponId
     * @return
     */
    public Observable<BaseResult<SecondKillCouponDetail>> getSecondKillCouponDetail(long secondKillCouponId) {
        Map<String, String> map = new HashMap<>();
        map.put("secondKillId", secondKillCouponId + "");
        Observable<BaseResult<SecondKillCouponDetail>> observable =
                getObservable().getSecondKillCouponDetail(map);
        return observable;
    }

    /**
     * 获取全部秒杀券列表
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Observable<BaseResult<List<SecondKillCouponBean>>> getAllSecondKillCouponList(int pageNumber, int pageSize, int categoryId) {
        Map<String, String> map = getTokenMap();
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", Constant.PAGE_SIZE + "");
        map.put("categoryId", categoryId + "");
        Observable<BaseResult<List<SecondKillCouponBean>>> observable =
                getObservable().getAllSecondKillCouponList(map);
        return observable;
    }

    /**
     * 获取全部拼团券列表
     *
     * @param pageNumber
     * @param pageSize
     * @return
     */
    public Observable<BaseResult<List<SpellingGroupCouponBean>>> getAllSpellingGroupCouponList(int pageNumber, int pageSize, int categoryId) {
        Map<String, String> map = getTokenMap();
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", Constant.PAGE_SIZE + "");
        map.put("categoryId", categoryId + "");
        Observable<BaseResult<List<SpellingGroupCouponBean>>> observable =
                getObservable().getAllSpellingGroupCouponList(map);
        return observable;
    }

    /**
     * 查看用户的所有商铺的优惠金列表
     */
    public Observable<BaseResult<List<MyOfferRebatesBean>>> listUserRebateMoney() {
        Map<String, String> map = getTokenMap();
        Observable<BaseResult<List<MyOfferRebatesBean>>> observable = getObservable().listUserRebateMoney(map);
        return observable;
    }

    /**
     * 获得用户在这个商铺中优惠金收支记录
     */
    public Observable<BaseResult<List<DetailsDiscountPaymentBean>>> listUserRebateRecord(String shopId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        Observable<BaseResult<List<DetailsDiscountPaymentBean>>> observable = getObservable().listUserRebateRecord(map);
        return observable;
    }
}
