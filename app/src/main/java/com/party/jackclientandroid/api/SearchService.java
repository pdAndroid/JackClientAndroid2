package com.party.jackclientandroid.api;

import android.text.TextUtils;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BlackCardItem;
import com.party.jackclientandroid.bean.FilterCondition;
import com.party.jackclientandroid.bean.NeighbourSpellingGroupBean;
import com.party.jackclientandroid.bean.OrderCondition;
import com.party.jackclientandroid.bean.ShopItemMessage;
import com.party.jackclientandroid.bean.SearchPoint;
import com.party.jackclientandroid.bean.SpellingCoupon;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/8/25.
 */

public class SearchService extends BaseService {
    public SearchService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<SearchPoint>>> getSearchPointList(String name) {
        Map<String, String> tokenMap = getTokenMap();
        tokenMap.put("categoryId", name);
        tokenMap.put("searchKey", name);
        tokenMap.put("name", name);
        tokenMap.put("name", name);
        tokenMap.put("name", name);
        Observable<BaseResult<List<SearchPoint>>> observable = getObservable().getSearchPointList(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<ShopItemMessage>>> searchShop(Map<String, String> data) {
        Observable<BaseResult<List<ShopItemMessage>>> observable = getObservable().searchShop(data);
        return observable;
    }

    public Observable<BaseResult<List<OrderCondition>>> getOrderByCondition(String parentId, String conditionType) {
        HashMap<String, String> tokenMap = getTokenMap();
        tokenMap.put("parentId", parentId);
        tokenMap.put("conditionType", TextUtils.isEmpty(conditionType) ? "" : conditionType);
        Observable<BaseResult<List<OrderCondition>>> observable = getObservable().getOrderByCondition(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<FilterCondition>>> getFilterCondition() {
        HashMap<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<List<FilterCondition>>> observable = getObservable().getFilterCondition(tokenMap);
        return observable;
    }


    public Observable<BaseResult<List<ShopItemMessage>>> getShopByMultiCondition(int pageNumber, int pageSize, double lon
            , double lat, String areaId, String searchKey, String categoryId, String data) {
        HashMap<String, String> tokenMap = getTokenMap();
        tokenMap.put("pageNumber", pageNumber + "");
        tokenMap.put("pageSize", pageSize + "");
        tokenMap.put("lon", lon + "");
        tokenMap.put("lat", lat + "");
        tokenMap.put("areaId", areaId);
        tokenMap.put("searchKey", TextUtils.isEmpty(searchKey) ? "" : searchKey);
        tokenMap.put("categoryId", TextUtils.isEmpty(categoryId) ? "" : categoryId);
        tokenMap.put("data", TextUtils.isEmpty(data) ? "[]" : data);
        Observable<BaseResult<List<ShopItemMessage>>> observable = getObservable().getShopByMultiCondition(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<BlackCardItem>>> getCouponsBackCards(int pageNumber, int pageSize, double lon
            , double lat, String areaId, String searchKey, String categoryId, String conditionType, String data) {
        HashMap<String, String> tokenMap = getTokenMap();
        tokenMap.put("pageNumber", pageNumber + "");
        tokenMap.put("pageSize", pageSize + "");
        tokenMap.put("lon", lon + "");
        tokenMap.put("lat", lat + "");
        tokenMap.put("areaId", areaId);
        tokenMap.put("searchKey", TextUtils.isEmpty(searchKey) ? "" : searchKey);
        tokenMap.put("categoryId", TextUtils.isEmpty(categoryId) ? "" : categoryId);
        tokenMap.put("conditionType", TextUtils.isEmpty(conditionType) ? "" : conditionType);
        tokenMap.put("data", TextUtils.isEmpty(data) ? "[]" : data);
        Observable<BaseResult<List<BlackCardItem>>> observable = getObservable().getCouponsBackCards(tokenMap);
        return observable;
    }

    public Observable<BaseResult<List<SpellingCoupon>>> getSpellingCoupons(int pageNumber, int pageSize, double lon
            , double lat, String areaId, String searchKey, String categoryId, String conditionType, String data) {
        HashMap<String, String> tokenMap = getTokenMap();
        tokenMap.put("pageNumber", pageNumber + "");
        tokenMap.put("pageSize", pageSize + "");
        tokenMap.put("lon", lon + "");
        tokenMap.put("lat", lat + "");
        tokenMap.put("areaId", areaId);
        tokenMap.put("searchKey", TextUtils.isEmpty(searchKey) ? "" : searchKey);
        tokenMap.put("categoryId", TextUtils.isEmpty(categoryId) ? "" : categoryId);
        tokenMap.put("conditionType", TextUtils.isEmpty(conditionType) ? "" : conditionType);
        tokenMap.put("data", TextUtils.isEmpty(data) ? "[]" : data);
        Observable<BaseResult<List<SpellingCoupon>>> observable = getObservable().getSpellingCoupons(tokenMap);
        return observable;
    }
}
