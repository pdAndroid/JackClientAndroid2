package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.Notice;
import com.party.jackclientandroid.bean.NeighbourSpellingGroupBean;
import com.party.jackclientandroid.bean.DeliciousFood;
import com.party.jackclientandroid.bean.EatWhatForEveryday;
import com.party.jackclientandroid.bean.HomeBanner;
import com.party.jackclientandroid.bean.ShopItemMessage;
import com.party.jackclientandroid.bean.SearchMenuBean;
import com.party.jackclientandroid.bean.ShopClassification;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by Administrator on 2018/8/9.
 */

public class HomeService extends BaseService {
    public HomeService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<List<ShopClassification>>> getShopClassificationList() {
        Observable<BaseResult<List<ShopClassification>>> observable = getObservable().getShopClassificationList();
        return observable;
    }

    public Observable<BaseResult<List<HomeBanner>>> getBannerImages() {
        Observable<BaseResult<List<HomeBanner>>> observable = getObservable().getHomeBannerList();
        return observable;
    }

    public Observable<BaseResult<List<HomeBanner>>> getShop() {
        Observable<BaseResult<List<HomeBanner>>> observable = getObservable().getHomeBannerList();
        return observable;
    }

    public Observable<BaseResult<List<ShopItemMessage>>> getRecommendShopList(int page, int pageSize) {
        Map<String, String> map = getLocalMap();
        map.put("pageNumber", page + "");
        map.put("pageSize", "" + pageSize);
        Observable<BaseResult<List<ShopItemMessage>>> observable = getObservable().getRecommendShopList(map);
        return observable;
    }

    public Observable<BaseResult<List<EatWhatForEveryday>>> eatWhatForEveryday() {
        Observable<BaseResult<List<EatWhatForEveryday>>> observable = getObservable().getEatWhatForEveryday();
        return observable;
    }

    /**
     * 根据店铺类型查询店铺列表
     *
     * @param page
     * @param pageSize
     * @return
     */
    public Observable<BaseResult<List<DeliciousFood>>> getShopListByCategory(int shopCategory, int page, int pageSize) {
        Map<String, String> map = new HashMap<>();
        map.put("shopState", "1");
        map.put("pageNumber", page + "");
        map.put("pageSize", pageSize + "");
        map.put("categoryId", shopCategory + "");
        Observable<BaseResult<List<DeliciousFood>>> observable =
                getObservable().findByClassify(map);
        return observable;
    }


    /**
     * 更具水牌 s 参数来换去商家id
     *
     * @param code
     */
    public Observable<BaseResult<String>> getShopIdByCode(String code) {
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        Observable<BaseResult<String>> observable = getObservable().getShopIdByCode(map);
        return observable;
    }


    public Observable<BaseResult<List<SearchMenuBean>>> getSearchMenu(int menuParentId) {
        Map<String, String> map = new HashMap<>();
        map.put("menuParentId", menuParentId + "");
        Observable<BaseResult<List<SearchMenuBean>>> observable = getObservable().getSearchMenu(map);
        return observable;
    }

    public Observable<BaseResult<List<NeighbourSpellingGroupBean>>> searchNearbyDiscount(Map<String, String> map) {
        Observable<BaseResult<List<NeighbourSpellingGroupBean>>> observable =
                getObservable().searchNearbyDiscount(map);
        return observable;
    }

    public Observable<BaseResult<Notice>> getNotice() {
        Map<String, String> tokenMap = getTokenMap();
        Observable<BaseResult<Notice>> observable = getObservable().getNotice(tokenMap);
        return observable;
    }


}
