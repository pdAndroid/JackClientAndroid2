package com.party.jackclientandroid.api;

import android.widget.Toast;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.DiscountPayBillBean;
import com.party.jackclientandroid.bean.GroupBuyCoupon;
import com.party.jackclientandroid.bean.RecommendVegetable;
import com.party.jackclientandroid.bean.ReplaceMoneyCoupon;
import com.party.jackclientandroid.bean.ShopDetail;
import com.party.jackclientandroid.bean.UserCommentAndReply;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by Administrator on 2018/7/28.
 */

public class ShopService extends BaseService {

    public ShopService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    public Observable<BaseResult<ShopDetail>> getShopDetail(String shopId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId);
        Observable<BaseResult<ShopDetail>> observable = getObservable().getShopDetail(map);
        return observable;
    }

    /**
     * 优惠买单中的团购卷
     */
    public Observable<BaseResult<DiscountPayBillBean>> getDiscountPayBill(String shopId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId);
        Observable<BaseResult<DiscountPayBillBean>> observable = getObservable().getDiscountPayBill(map);
        return observable;
    }

    public Observable<BaseResult<List<ReplaceMoneyCoupon>>> getReplaceMoneyCouponList(Long shopId, Integer pageNumber, Integer pageSize) {
        Toast.makeText(mApplication, "fkdjfk", Toast.LENGTH_SHORT).show();
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", pageSize + "");
        Observable<BaseResult<List<ReplaceMoneyCoupon>>> observable =
                getObservable().getReplaceMoneyCouponList(map);
        return observable;
    }

    public Observable<BaseResult<List<GroupBuyCoupon>>> getGroupBuyCouponList(Long shopId, Integer pageNumber, Integer pageSize) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", pageSize + "");
        Observable<BaseResult<List<GroupBuyCoupon>>> observable =
                getObservable().getGroupBuyCouponList(map);
        return observable;
    }

    public Observable<BaseResult<List<RecommendVegetable>>> getRecommendVegetableList(Long shopId, String pageNumber, String pageSize, Consumer consumer) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        map.put("pageNumber", pageNumber);
        map.put("pageSize", pageSize);
        Observable<BaseResult<List<RecommendVegetable>>> observable =
                getObservable().getRecommendVegetableList(map);
        return observable;
    }


    public Observable<BaseResult<CommonResult>> cancelAttention(String shopId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId);
        Observable<BaseResult<CommonResult>> observable =
                getObservable().cancelAttention(map);
        return observable;
    }

    public Observable<BaseResult<CommonResult>> addAttention(String shopId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId);
        map.put("source", "1");
        Observable<BaseResult<CommonResult>> observable =
                getObservable().addAttention(map);
        return observable;
    }

    //获取店铺评论
    public Observable<BaseResult<List<UserCommentAndReply>>> getUserCommentList(Long shopId, int pageNumber, int pageSize) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", pageSize + "");
        Observable<BaseResult<List<UserCommentAndReply>>> observable =
                getObservable().getUserCommentList(map);
        return observable;
    }

    //获取店铺回复
    public Observable<BaseResult<List<UserCommentAndReply>>> getUserReplyList(Long shopId, int pageNumber, int pageSize, Long orderId) {
        Map<String, String> map = getTokenMap();
        map.put("shopId", shopId + "");
        map.put("pageNumber", pageNumber + "");
        map.put("pageSize", pageSize + "");
        map.put("orderId", orderId + "");
        Observable<BaseResult<List<UserCommentAndReply>>> observable =
                getObservable().getUserCommentList(map);
        return observable;
    }


}
