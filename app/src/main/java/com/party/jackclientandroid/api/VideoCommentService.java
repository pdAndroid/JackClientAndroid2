package com.party.jackclientandroid.api;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.GrabRedPacketBean;
import com.party.jackclientandroid.bean.VideoCommentBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class VideoCommentService extends BaseService {
    public VideoCommentService(BaseActivity baseActivity) {
        super(baseActivity);
    }

    /**
     * 获得视频评论
     *
     * @param videoId
     * @return
     */
    public Observable<BaseResult<List<VideoCommentBean>>> listVideoComment(String videoId) {
        Map<String, String> map = getTokenMap();
        map.put("videoId", videoId);
        Observable<BaseResult<List<VideoCommentBean>>> observable = getObservable().listVideoComment(map);
        return observable;
    }

    /**
     * 添加视频评论
     *
     * @param videoId
     * @param content
     * @return
     */
    public Observable<BaseResult<GrabRedPacketBean>> addVideoComment(String videoId, String content) {
        Map<String, String> map = getTokenMap();
        map.put("videoId", videoId);
        map.put("content", content);
        Observable<BaseResult<GrabRedPacketBean>> observable = getObservable().addVideoComment(map);
        return observable;
    }
}
