package com.party.jackclientandroid.imageloader;

import android.app.Activity;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackclientandroid.R;

import java.io.File;

/**
 * Created by Tangxb on 2017/2/14.
 */

public class GlideLoaderInterImpl implements ImageLoaderInter {
    private int placeholderResId;
    private int errorResId;
    private RequestOptions requestOptions;
    private RequestOptions requestOptionsNoAim;

    public GlideLoaderInterImpl() {
//        placeholderResId = R.mipmap.ic_launcher;
        // 这里不要使用svg,加载卡得一逼
        placeholderResId = R.drawable.ic_image;
        errorResId = R.drawable.ic_img_load_fail;
        requestOptions = new RequestOptions();
        requestOptionsNoAim = new RequestOptions();
        requestOptions
                .placeholder(R.color.color_b2b2b2)
                .error(R.color.color_8a8a8a)
                .centerCrop()
                .skipMemoryCache(false)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
        requestOptionsNoAim
                .placeholder(placeholderResId)
                .error(errorResId)
                .skipMemoryCache(false)
                .centerCrop()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }


    /**
     * 加载普通的图片
     *
     * @param activity
     * @param imageResId
     * @param imageView
     */
    @Override
    public void loadCommonImgByUrl(Activity activity, int imageResId, ImageView imageView) {
        GlideApp.with(activity)
                .load(imageResId)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 加载普通的图片
     *
     * @param activity
     * @param imageUrl
     * @param imageView
     */
    @Override
    public void loadCommonImgByUrl(Activity activity, String imageUrl, ImageView imageView) {
        GlideApp.with(activity)
                .load(imageUrl)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 加载普通的图片
     *
     * @param activity
     * @param imageUrl
     * @param imageView
     */
    @Override
    public void loadCommonImgByUri(Activity activity, File imageUrl, ImageView imageView) {
        GlideApp.with(activity)
                .load(Uri.fromFile(imageUrl))
                .apply(requestOptions)
                .into(imageView);
    }
    /**
     * 加载普通的图片
     *
     * @param fragment
     * @param imageResId
     * @param imageView
     */
    @Override
    public void loadCommonImgByUrl(Fragment fragment, int imageResId, ImageView imageView) {
        GlideApp.with(fragment)
                .load(imageResId)
                .apply(requestOptions)
                .into(imageView);
    }
    /**
     * 加载普通的图片
     *
     * @param fragment
     * @param imageUrl
     * @param imageView
     */
    @Override
    public void loadCommonImgByUrl(Fragment fragment, String imageUrl, ImageView imageView) {
        GlideApp.with(fragment).load(imageUrl)
                .apply(requestOptions)
                .into(imageView);
    }

    /**
     * 加载圆形或者是圆角图片
     *
     * @param activity
     * @param imageUrl
     * @param imageView
     */
    @Override
    public void loadCircleOrReboundImgByUrl(Activity activity, String imageUrl, ImageView imageView) {
        GlideApp.with(activity).load(imageUrl)
                .apply(requestOptionsNoAim)
                .into(imageView);
    }

    /**
     * 加载圆形或者是圆角图片
     *
     * @param fragment
     * @param imageUrl
     * @param imageView
     */
    @Override
    public void loadCircleOrReboundImgByUrl(Fragment fragment, String imageUrl, ImageView imageView) {
        GlideApp.with(fragment).load(imageUrl)
                .apply(requestOptionsNoAim)
                .into(imageView);
    }

    @Override
    public void resumeRequests(Activity activity) {
        if (GlideApp.with(activity).isPaused()) {
            GlideApp.with(activity).resumeRequests();
        }
    }

    @Override
    public void resumeRequests(Fragment fragment) {
        if (GlideApp.with(fragment).isPaused()) {
            GlideApp.with(fragment).resumeRequests();
        }
    }

    @Override
    public void pauseRequests(Activity activity) {
        if (!GlideApp.with(activity).isPaused()) {
            GlideApp.with(activity).pauseRequests();
        }
    }

    @Override
    public void pauseRequests(Fragment fragment) {
        if (!GlideApp.with(fragment).isPaused()) {
            GlideApp.with(fragment).pauseRequests();
        }
    }
}
