package com.party.jackclientandroid.utils;

import android.view.View;

public class MyNullException extends Exception {

    private View view;

    public MyNullException(View view,String message) {
        super(message);
        this.view = view;
    }

    public View getView() {
        return view;
    }
}
