package com.party.jackclientandroid.utils;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ScrollView;
import android.widget.TextView;

import com.party.jackclientandroid.R;

import java.math.BigDecimal;

/**
 * Created by 派对 on 2018/11/3.
 */

public class CheckUtils {


    public static void checkData(String s, View view, String errorMessage) throws MyNullException {
        if (TextUtils.isEmpty(s)) {
            throw new MyNullException(view, errorMessage);
        }
    }

    public static void checkData(boolean isOk, View view, String errorMessage) throws MyNullException {
        if (isOk) {
            throw new MyNullException(view, errorMessage);
        }
    }

    public static String checkData(TextView view, String errorMessage) throws MyNullException {
        String value = view.getText().toString();
        if (TextUtils.isEmpty(view.getText().toString())) {
            throw new MyNullException(view, errorMessage);
        }
        return value;
    }

    public static void checkMoney(TextView view, String errorMessage, int scale) throws MyNullException {
        String money = view.getText().toString();
        try {
            BigDecimal decimal = new BigDecimal(money);
            System.out.println(decimal);
            BigDecimal setScale = decimal.setScale(scale, BigDecimal.ROUND_DOWN);
            view.setText(setScale.toString());
        } catch (Exception e) {
            throw new MyNullException(view, errorMessage);
        }
    }

    /**
     * 货币比较
     *
     * @param one          原价
     * @param two          购买价
     * @param shake        提交位置
     * @param errorMessage 错误信息
     * @throws MyNullException 如果 购买价 高于 原价 就报错。
     */
    public static void checkMoney(TextView one, TextView two, View shake, String errorMessage) throws MyNullException {
        BigDecimal oneTxt = new BigDecimal(one.getText().toString());
        BigDecimal twoTxt = new BigDecimal(two.getText().toString());
        if (oneTxt.compareTo(twoTxt) <= 0) {
            throw new MyNullException(shake, errorMessage);
        }
    }

    public static String checkIDCard(TextView view, String errorMessage) throws MyNullException {
        String value = view.getText().toString();
        if (!IDCardValidateUtil.validate_effective(value)) {
            throw new MyNullException(view, errorMessage);
        }
        return value;
    }

    public static String checkPhone(TextView view, String errorMessage) throws MyNullException {
        String value = view.getText().toString();
        if (!PhoneUtil.isMobileNO(value)) {
            throw new MyNullException(view, errorMessage);
        }
        return value;
    }

    /**
     * @param toView
     * @param topView    界面定模图片，用于匹配界面
     * @param scrollView
     */
    public static void scrollTo(Context context, View toView, View topView, ScrollView scrollView) {
        int[] y = new int[2];
        topView.getLocationInWindow(y);
        int[] y1 = new int[2];
        toView.getLocationInWindow(y1); //获取在当前窗口内的绝对坐标
        scrollView.scrollTo(0, y1[1] - y[1]);
        shake(context, toView);
    }

    public static void shake(Context context, View v) {
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);//加载动画资源文件
        v.startAnimation(shake);
    }


}
