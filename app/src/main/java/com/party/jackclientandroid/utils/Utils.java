/**
 *
 */
package com.party.jackclientandroid.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.amap.api.location.AMapLocation;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.method.KeyListener;
import android.text.method.NumberKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 辅助工具类
 *
 * @author hongming.wang
 * @创建时间： 2015年11月24日 上午11:46:50
 * @项目名称： AMapLocationDemo2.x
 * @文件名称: Utils.java
 * @类型名称: Utils
 */
public class Utils {
    /**
     * 开始定位
     */
    public final static int MSG_LOCATION_START = 0;
    /**
     * 定位完成
     */
    public final static int MSG_LOCATION_FINISH = 1;
    /**
     * 停止定位
     */
    public final static int MSG_LOCATION_STOP = 2;

    public final static String KEY_URL = "URL";
    public final static String URL_H5LOCATION = "file:///android_asset/location.html";

    public static String changeStr10(String discount) {
        BigDecimal multiply = new BigDecimal(discount + "").multiply(new BigDecimal("10"));
        BigDecimal bigDecimal = multiply.setScale(3, RoundingMode.HALF_DOWN);
        return bigDecimal.stripTrailingZeros().toPlainString();
    }

    public static String changeStr100(String discount) {
        BigDecimal multiply = new BigDecimal(discount + "").multiply(new BigDecimal("100"));
        BigDecimal bigDecimal = multiply.setScale(3, RoundingMode.HALF_DOWN);
        return bigDecimal.stripTrailingZeros().toPlainString();
    }


    /**
     * 根据定位结果返回定位信息的字符串
     *
     * @return
     */
    public synchronized static String getLocationStr(AMapLocation location) {
        if (null == location) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        //errCode等于0代表定位成功，其他的为定位失败，具体的可以参照官网定位错误码说明
        if (location.getErrorCode() == 0) {
            sb.append("定位成功" + "\n");
            sb.append("定位类型: " + location.getLocationType() + "\n");
            sb.append("经    度    : " + location.getLongitude() + "\n");
            sb.append("纬    度    : " + location.getLatitude() + "\n");
            sb.append("精    度    : " + location.getAccuracy() + "米" + "\n");
            sb.append("提供者    : " + location.getProvider() + "\n");

            sb.append("速    度    : " + location.getSpeed() + "米/秒" + "\n");
            sb.append("角    度    : " + location.getBearing() + "\n");
            // 获取当前提供定位服务的卫星个数
            sb.append("星    数    : " + location.getSatellites() + "\n");
            sb.append("国    家    : " + location.getCountry() + "\n");
            sb.append("省            : " + location.getProvince() + "\n");
            sb.append("市            : " + location.getCity() + "\n");
            sb.append("城市编码 : " + location.getCityCode() + "\n");
            sb.append("区            : " + location.getDistrict() + "\n");
            sb.append("区域 码   : " + location.getAdCode() + "\n");
            sb.append("地    址    : " + location.getAddress() + "\n");
            sb.append("兴趣点    : " + location.getPoiName() + "\n");
            //定位完成的时间
            sb.append("定位时间: " + formatUTC(location.getTime(), "yyyy-MM-dd HH:mm:ss") + "\n");
        } else {
            //定位失败
            sb.append("定位失败" + "\n");
            sb.append("错误码:" + location.getErrorCode() + "\n");
            sb.append("错误信息:" + location.getErrorInfo() + "\n");
            sb.append("错误描述:" + location.getLocationDetail() + "\n");
        }
        //定位之后的回调时间
        sb.append("回调时间: " + formatUTC(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss") + "\n");
        return sb.toString();
    }

    private static SimpleDateFormat sdf = null;

    public synchronized static String formatUTC(long l, String strPattern) {
        if (TextUtils.isEmpty(strPattern)) {
            strPattern = "yyyy-MM-dd HH:mm:ss";
        }
        if (sdf == null) {
            try {
                sdf = new SimpleDateFormat(strPattern, Locale.CHINA);
            } catch (Throwable e) {
            }
        } else {
            sdf.applyPattern(strPattern);
        }
        return sdf == null ? "NULL" : sdf.format(l);
    }


    public static void setIDCardKeyListener(final EditText idCardView) {
        idCardView.setKeyListener(ID_LISTENER);
    }

    private static KeyListener ID_LISTENER = new NumberKeyListener() {
        /**
         * @return ：返回哪些希望可以被输入的字符,默认不允许输入
         */
        @Override
        protected char[] getAcceptedChars() {
            char[] chars = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'x', 'X'};
            return chars;
        }

        /**
         * 0：无键盘,键盘弹不出来
         * 1：英文键盘
         * 2：模拟键盘
         * 3：数字键盘
         * @return
         */
        @Override
        public int getInputType() {
            return 3;
        }
    };


    public static String clearZero(Object originalPrice) {
        return clearZero(originalPrice + "");
    }

    public static String clearZero(String originalPrice) {
        if (!originalPrice.contains(".")) return originalPrice;

        String[] split = originalPrice.split("\\.");
        int i = Integer.parseInt(split[1]);
        String result = split[0];
        if (i > 0) result = result + "." + split[1];

        return result;
    }

    /**
     * 显示软键盘
     *
     * @param context
     * @param editText
     */
    public void showSoftInput(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            editText.requestFocus();
            imm.showSoftInput(editText, 0);
        }
    }

    protected void hideSoftKeyboard(Context context, EditText editText) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public static String stringToMoney(String money) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        dfs.setGroupingSeparator(',');
        dfs.setMonetaryDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("###,###.##", dfs);
        try {
            money = df.parse(money).toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return money;
    }

    public static String checkNull(String avatar) {
        if (avatar == null) avatar = "";
        return avatar;
    }

    /**
     * 保留两位小数点
     *
     * @param number
     */
    /**
     * 检查数据是否多以两位小数。
     *
     * @param number
     * @return true 为多余两位 ，false 为正常 两位以内
     */
    public static boolean checkScale2(String number) {
        int i = number.indexOf('.');
        return (i > 0 && number.length() - i > 3);
    }
}
