package com.party.jackclientandroid.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Administrator on 2018/9/27.
 */

public class TimeUtil {

    public static long getCurrTimeStamp() {
        return System.currentTimeMillis();
    }

    public static String timeStamp2Date(String seconds, String format) {
        if (seconds == null || seconds.isEmpty() || seconds.equals("null")) {
            return "";
        }
        if (format == null || format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(new Date(Long.valueOf(seconds)));
    }

    public static String timeStamp2Time(long seconds) {
        String result = "";
        long hour = seconds / 3600;
        long time = (seconds % 3600) / 60;
        if (hour < 10) result += "0";
        result += hour;
        result += ":";
        if (time < 10) result += "0";
        result += time;
        return result;
    }

    /*
*计算time2减去time1的差值 差值只设置 几天 几个小时 或 几分钟
* 根据差值返回多长之间前或多长时间后
* */
    public static String getDistanceTime(long currTimeStamp, long endTimeStamp) {
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        long diff = 0;

        if (endTimeStamp > currTimeStamp) {
            diff = endTimeStamp - currTimeStamp;
            day = diff / (24 * 60 * 60 * 1000);
            if (day >= 1) {
                return "还剩" + (day + 1) + "天";
            } else {
                return "不到一天";
            }
        } else {
            return "已结束";
        }
    }

    public static String getDistanceTime2(long currTimeStamp, long endTimeStamp) {
        long diff = 0;
        long day = 0;

        if (endTimeStamp > currTimeStamp) {
            diff = endTimeStamp - currTimeStamp;
            day = diff / (24 * 60 * 60 * 1000);
            day++;
            return day + "";
        } else {
            return "已结束";
        }
    }

    public static String getRemainTime(long timeStamp) {
        long diff = 0;
        long day = 0;
        day = timeStamp / (24 * 60 * 60 * 1000);
        day++;
        return day + "";
    }

    public static String stampToDateString(long timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(timeStamp);
    }

    public static Date timeStampToDate2(long timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String d = format.format(timeStamp);
        Date date = null;
        try {
            date = format.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static boolean isTimeValid(long startDateTS, long endDataTS, long startTimeSumSecond, long endTimeSumSecond) {
        Calendar calendar = Calendar.getInstance();
        Date currDate = new Date();
        int currHourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int currSumSeconds = currHourOfDay * 3600 + minutes * 60;
        Date startDate = timeStampToDate2(startDateTS);
        Date endDate = timeStampToDate2(endDataTS);
        if (startTimeSumSecond > endTimeSumSecond) {//消费时间在第二天结束
            if (endDataTS - getCurrTimeStamp() > 24 * 3600 * 1000) {//当前日期在结束日期的倒数第二天
                if (currSumSeconds > endTimeSumSecond && currSumSeconds < startTimeSumSecond) {//当前总秒数在消费时间之外，说明无效
                    return false;
                } else {
                    return true;
                }
            } else if (endDataTS - getCurrTimeStamp() < 24 * 3600 * 1000) {//当前日期在结束日期

            }
        } else {
            if (currDate.compareTo(startDate) >= 0 && currDate.compareTo(endDate) <= 0) {//当前日期在开始日期和结束日期之间
                if (currSumSeconds >= startTimeSumSecond && currSumSeconds <= endTimeSumSecond) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return false;
    }

    public static String timeAnalyze(int seconds) {
        int hour = seconds / 3600;
        int minute = (seconds % 3600) / 60;
        StringBuffer hourTime = new StringBuffer();
        if (hour < 10) hourTime.append("0");
        hourTime.append(hour).append(":");
        if (minute < 10) hourTime.append("0");
        hourTime.append(minute);

        return hourTime.toString();
    }
}
