package com.party.jackclientandroid.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * Created by tangxuebing on 2018/5/14.
 */
public class MPackageUtils {

    private static Integer versionCode;
    private static String versionName;

    /**
     * 这个方法必须要在下面的方法时候用后才可以调用
     * @return
     */
    public static Integer getVersionCode() {
        return versionCode;
    }

    /**
     * 这个方法必须要在下面的方法时候用后才可以调用
     * @return
     */
    public static String getVersionName() {
        return versionName;
    }

    /**
     * get App versionCode
     *
     * @param context
     * @return
     */
    public static int getVersionCode(Context context) {
        if (versionCode != null) return versionCode;
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    /**
     * get App versionName
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        if (versionName != null) return versionName;
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packageInfo;
        try {
            packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }
}
