package com.party.jackclientandroid.utils;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.Toast;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.view.dialog.AlertProgressDialog;

/**
 * Created by Taxngb on 2017/5/2.
 */

public class ToastUtils {
    public static void t(final Application application, final String text) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showInMainLooper(application, text);
        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    showInMainLooper(application, text);
                }
            });
        }
    }

    private static void showInMainLooper(final Application application, final String text) {
        Toast.makeText(application, text, Toast.LENGTH_SHORT).show();
    }

}
