package com.party.jackclientandroid.pay.alipay;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import com.alipay.sdk.app.AuthTask;
import com.alipay.sdk.app.PayTask;

import java.util.Map;

/**
 * 支付宝支付
 * Created by tsy on 16/6/1.
 */
public class AlipayAuth {
    private String mParams;
    private AuthTask mAuthTask;
    private AlipayResultCallBack mCallback;

    public static final int ERROR_RESULT = 1;   //支付结果解析错误
    public static final int ERROR_PAY = 2;  //支付失败
    public static final int ERROR_NETWORK = 3;  //网络连接错误

    public interface AlipayResultCallBack {
        void onSuccess(); //授权成功

        void onError(int error_code);   //授权失败

        void onCancel();    //授权取消
    }

    public AlipayAuth(Context context, String params, AlipayResultCallBack callback) {
        mParams = params;
        mCallback = callback;
        mAuthTask = new AuthTask((Activity) context);
    }

    //支付
    public void doAuth() {
        final Handler handler = new Handler();
        new Thread(() -> {
            final Map<String, String> pay_result = mAuthTask.authV2(mParams, true);
            handler.post(() -> {
                if (mCallback == null) {
                    return;
                }

                if (pay_result == null) {
                    mCallback.onError(ERROR_RESULT);
                    return;
                }

                String resultStatus = pay_result.get("resultStatus");
                if (TextUtils.equals(resultStatus, "9000")) {    //支付成功
                    mCallback.onSuccess();
                } else if (TextUtils.equals(resultStatus, "6001")) {        //支付取消
                    mCallback.onCancel();
                } else if (TextUtils.equals(resultStatus, "6002")) {     //网络连接出错
                    mCallback.onError(ERROR_NETWORK);
                } else if (TextUtils.equals(resultStatus, "4000")) {        //支付错误
                    mCallback.onError(ERROR_PAY);
                }
            });
        }).start();
    }
}
