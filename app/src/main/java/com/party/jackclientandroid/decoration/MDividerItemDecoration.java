package com.party.jackclientandroid.decoration;

/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * limitations under the License.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.party.jackclientandroid.utils.DensityUtils;


/**
 * This class is from the v7 samples of the Android SDK. It's not by me!
 * <p/>
 * See the license above for details.
 */
public class MDividerItemDecoration extends RecyclerView.ItemDecoration {

    private static final int[] ATTRS = new int[]{
            android.R.attr.listDivider
    };

    public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;

    public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;

    private Drawable mDivider;
    /**
     * 第一item顶部也显示
     */
    private boolean mIsShowFirstTopDivider = true;
    private boolean mIsShowBottomDivider;
    private int mOrientation;
    private int mDividerHeight;
    private int mDividerWidth;

    public MDividerItemDecoration(Context context, int orientation) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
    }

    public void setDrawable(Drawable drawable) {
        mDivider = drawable;
    }

    public void setIsShowFirstTopDivider(boolean needFirstTopDivider) {
        this.mIsShowFirstTopDivider = needFirstTopDivider;
    }

    public void setIsShowBottomDivider(boolean mIsShowBottomDivider) {
        this.mIsShowBottomDivider = mIsShowBottomDivider;
    }

    public MDividerItemDecoration(Context context, int orientation, float dpH) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
        mDividerHeight = DensityUtils.dip2px(context, dpH);
    }

    public MDividerItemDecoration(Context context, int orientation, float dpW, float dpH) {
        final TypedArray a = context.obtainStyledAttributes(ATTRS);
        mDivider = a.getDrawable(0);
        a.recycle();
        setOrientation(orientation);
        mDividerWidth = DensityUtils.dip2px(context, dpW);
        mDividerHeight = DensityUtils.dip2px(context, dpH);
    }

    public void setOrientation(int orientation) {
        if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
            throw new IllegalArgumentException("invalid orientation");
        }
        mOrientation = orientation;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            drawVertical(c, parent);
        } else {
            drawHorizontal(c, parent);
        }
    }

    public void drawVertical(Canvas c, RecyclerView parent) {
        final int left = parent.getPaddingLeft();
        final int right = parent.getWidth() - parent.getPaddingRight();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            int position = parent.getChildAdapterPosition(child);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int top = child.getBottom() + params.bottomMargin;
            final int bottom = top + mDivider.getIntrinsicHeight();
            if (mDividerHeight > 0) {
                mDivider.setBounds(left, top, right, top + mDividerHeight);
            } else {
                mDivider.setBounds(left, top, right, bottom);
            }
            mDivider.draw(c);
            if (mIsShowFirstTopDivider && position == 0) {
                c.save();
                mDivider.setBounds(left, 0, right, mDivider.getIntrinsicHeight());
                mDivider.draw(c);
                c.restore();
            }
        }
    }

    public void drawHorizontal(Canvas c, RecyclerView parent) {
        final int top = parent.getPaddingTop();
        final int bottom = parent.getHeight() - parent.getPaddingBottom();

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                    .getLayoutParams();
            final int left = child.getRight() + params.rightMargin;
            final int right = left + mDivider.getIntrinsicHeight();
            if (mDividerHeight > 0) {
                mDivider.setBounds(left, top, left + mDividerHeight, bottom);
            } else {
                mDivider.setBounds(left, top, right, bottom);
            }
            mDivider.draw(c);
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mOrientation == VERTICAL_LIST) {
            if (mDividerHeight > 0) {
                outRect.set(0, 0, 0, mDividerHeight);
            } else {
                int lastPosition = state.getItemCount() - 1;
                int position = parent.getChildAdapterPosition(view);
                if (mIsShowBottomDivider || position < lastPosition) {
                    outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
                } else {
                    outRect.set(0, 0, 0, 0);
                }
                if (mIsShowFirstTopDivider && position == 0) {
                    outRect.set(0, mDivider.getIntrinsicHeight(), 0, mDivider.getIntrinsicHeight());
                }
            }
        } else {
            if (mDividerWidth > 0) {
                outRect.set(0, 0, mDividerWidth, 0);
            } else {
                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
            }
        }
    }
}