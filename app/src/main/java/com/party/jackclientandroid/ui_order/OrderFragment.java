package com.party.jackclientandroid.ui_order;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.TabContentPagerAdapter;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.BaseFragment;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderStatusBean;
import com.party.jackclientandroid.event.ReLoginSuccessEvent;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 首页-订单
 */
public class OrderFragment extends BaseFragment {
    @BindView(R.id.ll_container)
    ViewGroup ll_container;
    @BindView(R.id.view_pager3)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tab_layout;

    OrderService orderService;
    TabContentPagerAdapter contentAdapter;
    private List<OrderStatusBean> orderStatusBeanList;
    private List<String> titleList = new ArrayList<>();
    private List<Fragment> fragmentList = new ArrayList<>();

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_order;
    }



    @Override
    protected void initData() {
        orderService = new OrderService((BaseActivity) mActivity);
        tab_layout.setTabMode(TabLayout.MODE_FIXED);
        tab_layout.setTabGravity(TabLayout.GRAVITY_FILL);
        getNeedData();
    }

    private void getNeedData() {
        addDisposableIoMain(orderService.getOrderState(), new DefaultConsumer<List<OrderStatusBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<OrderStatusBean>> baseBean) {
                orderStatusBeanList = baseBean.getData();
                titleList.clear();
                fragmentList.clear();
                for (OrderStatusBean orderStatusBean : orderStatusBeanList) {
                    titleList.add(orderStatusBean.getName());
                    fragmentList.add(OrderStateFragment.newInstance(orderStatusBean.getId()));
                }
                tab_layout.setTabMode(TabLayout.MODE_FIXED);
                tab_layout.setTabGravity(TabLayout.GRAVITY_FILL);
                contentAdapter = new TabContentPagerAdapter(getFragmentManager(), fragmentList, titleList);
                viewPager.setAdapter(contentAdapter);
                tab_layout.setupWithViewPager(viewPager);
            }
        });
    }

    @Subscribe
    public void onEventFromLogin(ReLoginSuccessEvent reLoginSuccessEvent) {
        getNeedData();
    }
}
