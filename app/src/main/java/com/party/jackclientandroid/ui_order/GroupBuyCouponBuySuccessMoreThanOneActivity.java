package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class GroupBuyCouponBuySuccessMoreThanOneActivity extends BaseActivityTitle {

    @BindView(R.id.goods_name_tv)
    TextView goods_name_tv;
    @BindView(R.id.buy_time_tv)
    TextView buy_time_tv;
    @BindView(R.id.user_phone_tv)
    TextView user_phone_tv;
    @BindView(R.id.num_tv)
    TextView num_tv;

    private AlertDialog dialog;
    private View contentView;

    private String order_id;
    private OrderDetail orderDetail;
    private OrderService orderService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_buy_coupon_buy_success_more_than_one);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        order_id = intent.getStringExtra("order_id");
    }

    protected void initData() {
        setMiddleText("购买成功");
        orderService = new OrderService(this);
    }

    public void loadData() {
        addDisposableIoMain(orderService.getUserOrderDetail(order_id), new DefaultConsumer<OrderDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OrderDetail> result) {
                orderDetail = result.getData();
                user_phone_tv.setText(orderDetail.getUserPhone());
                num_tv.setText(orderDetail.getNum() + "张");
                addDisposableIoMain(orderService.getUserExOrderDetail(order_id), new DefaultConsumer<List<UserOrderExtraDetail>>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<List<UserOrderExtraDetail>> result) {
                        goods_name_tv.setText(result.getData().get(0).getItemName());
                    }
                });
                helper.showContent();
            }
        });
    }

    public void showInputNumDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mActivity);
        contentView = LayoutInflater.from(mActivity).inflate(R.layout.layout_edit, null);
        TextView cancel_tv = contentView.findViewById(R.id.cancel_tv);
        TextView ok_tv = contentView.findViewById(R.id.ok_tv);
        EditText photoNameEt = contentView.findViewById(R.id.name_et);
        photoNameEt.setHint("请输入名称");
        cancel_tv.setOnClickListener(onClickListener);
        ok_tv.setOnClickListener(onClickListener);
        alertDialog.setView(contentView);
        dialog = alertDialog.create();
        dialog.show();
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cancel_tv:
                    //showToast("cancel");
                    dialog.dismiss();
                    break;
                case R.id.ok_tv:
                    String photoName = ((EditText) contentView.findViewById(R.id.name_et)).getText().toString();
                    if (TextUtils.isEmpty(photoName)) {
                        showToast("名称不能为空");
                        return;
                    }
                    //showToast("num:"+photoName);
                    dialog.dismiss();
                    break;
            }
        }
    };

    @OnClick({R.id.return_shop_btn, R.id.now_use_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.return_shop_btn:
                startActivity(ShopDetailActivity.class,"shopId",orderDetail.getShopId()+"");
                finish();
                break;
            case R.id.see_order_btn:
                startActivity(OrderDetailWaitUseActivity.class,"order_id",order_id);
                break;
        }
    }
}
