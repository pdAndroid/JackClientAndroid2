package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;

import butterknife.BindView;

/**
 * 退款
 */
public class RefundProcessActivity extends BaseActivityTitle {

    @BindView(R.id.whole_layout)
    LinearLayout whole_layout;

    private OrderService orderService;
    private OrderDetail orderDetail;
    private String order_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_process);
        receivePassDataIfNeed(getIntent());
        initData();
    }


    protected void initData() {
        setMiddleText("退款进度");
        helper = new LoadViewHelper(whole_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        helper.showLoading();
        orderService = new OrderService(this);
    }

    protected void receivePassDataIfNeed(Intent intent) {
        order_id = intent.getStringExtra("order_id");
    }

    public void loadData() {
        addDisposableIoMain(orderService.getUserOrderDetail(order_id), new DefaultConsumer<OrderDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OrderDetail> result) {
                orderDetail = result.getData();
                helper.showContent();
            }
        });
    }
}
