package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;

public class WatchCouponQRCodeActivity extends BaseActivityTitle {

    @BindView(R.id.qr_code_iv)
    ImageView qr_code_iv;

    private long orderId;
    private int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_coupon_qrcode);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        orderId = intent.getLongExtra("order_id", -1L);
        num = intent.getIntExtra("num", 0);
    }

    protected void initData() {
        setMiddleText("二维码");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("num", num);
            jsonObject.put("orderId", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String content = jsonObject.toString();
        Bitmap mBitmap = CodeUtils.createImage(content, 400, 400, null);
        qr_code_iv.setImageBitmap(mBitmap);
    }
}
