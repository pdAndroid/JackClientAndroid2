package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.party.jackclientandroid.ui_home.activity.GroupBuyCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ReplaceMoneyCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.SecondKillCouponDetailActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.ui_home.activity.SpellingGroupCouponDetailActivity;
import com.party.jackclientandroid.utils.TimeUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 支付-购买成功
 */
public class ReplaceMoneyCouponBuySuccessActivity extends BaseActivityTitle {

    @BindView(R.id.goods_name_tv)
    TextView goods_name_tv;
    @BindView(R.id.buy_time_tv)
    TextView buy_time_tv;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.total_price)
    TextView total_price;

    private OrderService orderService;
    private String order_id;
    private OrderDetail orderDetail;
    private UserOrderExtraDetail userOrderExtraDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replace_money_coupon_buy_success);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        order_id = intent.getStringExtra("order_id");
    }


    protected void initData() {
        setMiddleText("购买成功");
        orderService = new OrderService(this);
        loadData();
    }

    public void loadData() {
        addDisposableIoMain(orderService.getUserOrderDetail(order_id), new DefaultConsumer<OrderDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OrderDetail> result) {
                orderDetail = result.getData();
                num_tv.setText(orderDetail.getNum() + "张");
                total_price.setText("¥" + orderDetail.getFinalPrice());
                addDisposableIoMain(orderService.getUserExOrderDetail(order_id), new DefaultConsumer<List<UserOrderExtraDetail>>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<List<UserOrderExtraDetail>> result) {
                        userOrderExtraDetail = result.getData().get(0);
                        goods_name_tv.setText(userOrderExtraDetail.getItemName());
                        buy_time_tv.setText(TimeUtil.stampToDateString(userOrderExtraDetail.getCreate()));
//                        goods_name_tv.setText(userOrderExtraDetail.g());
//                        buy_time_tv.setText();
//                        shop_name_tv.setText();
                        //  helper.showContent();
                    }
                });

            }
        });
    }

    @OnClick({R.id.return_shop_btn, R.id.now_use_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.return_shop_btn:
                mApplication.finishActivity(ReplaceMoneyCouponDetailActivity.class);
                mApplication.finishActivity(SpellingGroupCouponDetailActivity.class);
                mApplication.finishActivity(SecondKillCouponDetailActivity.class);
                mApplication.finishActivity(GroupBuyCouponDetailActivity.class);
                finish();
                break;
            case R.id.now_use_btn:
                Intent intent = new Intent(mActivity, DiscountPayBillActivity.class);
                intent.putExtra("shopId", orderDetail.getShopId() + "");// 序列化
                intent.putExtra("couponId", userOrderExtraDetail.getItemId() + "");// 序列化
                startActivity(intent);
                mApplication.finishActivity(ShopDetailActivity.class);
                mApplication.finishActivity(ReplaceMoneyCouponDetailActivity.class);
                mApplication.finishActivity(SpellingGroupCouponDetailActivity.class);
                mApplication.finishActivity(SecondKillCouponDetailActivity.class);
                mApplication.finishActivity(GroupBuyCouponDetailActivity.class);
                finish();
                break;
        }

    }
}
