package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.ui_home.activity.AddCommentActivity;

import butterknife.OnClick;

public class OrderDetailGoCommentActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_go_comment);
    }

    @OnClick({R.id.comment_btn})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.comment_btn:
                startActivity(new Intent(mActivity, AddCommentActivity.class));
                break;
        }
    }
}
