package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.PayResult;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.controller.PayController;
import com.party.jackclientandroid.event.PayResultEvent;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.utils.PhoneUtil;
import com.party.jackclientandroid.utils.TimeUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 订单详情-待付款
 */
public class OrderDetailRePayActivity extends BaseActivityTitle {
    @BindView(R.id.whole_layout)
    ViewGroup whole_layout;
    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.shop_address_tv)
    TextView shop_address_tv;
    @BindView(R.id.add_order_time_tv)
    TextView add_order_time_tv;
    @BindView(R.id.user_phone_tv)
    TextView user_phone_tv;
    @BindView(R.id.num_tv)
    TextView num_tv;
    @BindView(R.id.single_price_tv)
    TextView single_price_tv;
    @BindView(R.id.real_price_tv)
    TextView real_price_tv;
    @BindView(R.id.rl_merchant)
    RelativeLayout rl_merchant;

    String order_id;
    PayController payController;
    private OrderService orderService;
    private OrderDetail orderDetail;
    //    private RelativeLayout rl_merchant;
    private int goodType;
    private int num;//购买商品的数量
    private UserService userService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail_repay);
        EventBus.getDefault().register(this);
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected void initData() {
//        rl_merchant.findViewById(R.id.rl_merchant);
        order_id = getIntent().getStringExtra("order_id");
        setMiddleText("订单详情");
        userService = new UserService(this);
        orderService = new OrderService(this);

        payController = new PayController(this, num_tv);
        helper = new LoadViewHelper(whole_layout);
        helper.showLoading();
        loadData();
    }

    public void loadData() {
        addDisposableIoMain(orderService.getUserOrderDetail(order_id), new DefaultConsumer<OrderDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OrderDetail> result) {
                orderDetail = result.getData();
                shop_name_tv.setText(orderDetail.getShopName());
                shop_address_tv.setText(orderDetail.getShopAddress());
                add_order_time_tv.setText(TimeUtil.timeStamp2Date(orderDetail.getCreate() + "", "yyyy-MM-dd HH:mm"));
                user_phone_tv.setText(orderDetail.getUserPhone());
                num_tv.setText(orderDetail.getNum() + "张");
                num = orderDetail.getNum();
                //single_price_tv.setText();
                real_price_tv.setText("¥" + orderDetail.getFinalPrice());
                addDisposableIoMain(orderService.getUserExOrderDetail(order_id), new DefaultConsumer<List<UserOrderExtraDetail>>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<List<UserOrderExtraDetail>> result) {
                        goodType = result.getData().get(0).getType();
                        helper.showContent();
                    }
                });
            }
        });
    }

    @OnClick({R.id.pay_btn, R.id.call_phone_iv, R.id.rl_merchant})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.pay_btn:
                if (orderDetail == null) {
                    showToast("订单数据为空");
                } else {
                    toPay(orderDetail);
                }
                break;
            case R.id.call_phone_iv:
                PhoneUtil.callPhone(mActivity, orderDetail.getShopName(),
                        "电话号码：" + orderDetail.getShopPhone(),
                        orderDetail.getShopPhone());
                break;
            case R.id.rl_merchant:
                Intent intent = new Intent(this, ShopDetailActivity.class);
                intent.putExtra("shopId", orderDetail.getShopId());
                startActivity(intent);
                break;
        }
    }


    public void toPay(OrderDetail orderDetail) {

        payController.showPayMethod(orderDetail.getFinalPrice() + "", "" + order_id, (View vv) -> {
            showToast("订单不存在");
        });

        payController.setInputPassFinish(new DiscountPayBillActivity.PayCompleteListener() {
            @Override
            public void payOk(BaseResult<PayResult> result) {
                String message = "消费: " + orderDetail.getFinalPrice() + "元  ";
                DialogController.showMustConfirmDialog(mActivity, "支付成功", message, (View v) -> {
                    finish();
                });
            }

            @Override
            public void payFail(String errorMessage) {
                DialogController.showConfirmDialog(mActivity, "支付失败", errorMessage, (View v) -> {

                });
            }
        });

    }


    @Subscribe
    public void onPayResultEvent(PayResultEvent event) {
        if (event.getResultType() == 1) {
            switch (goodType) {

                case Constant.GOODS_TYPE_SECOND_KILL:
                case Constant.GOODS_TYPE_REPLACE_MONEY:
                    Intent intent = new Intent(this, ReplaceMoneyCouponBuySuccessActivity.class);
                    intent.putExtra("order_id", order_id);
                    startActivity(intent);
                    finish();
                    break;
                case Constant.GOODS_TYPE_SPELLING_GROUP:
                case Constant.GOODS_TYPE_GROUP_BUY:
                    if (num > 1) {
                        Intent intent1 = new Intent(this, GroupBuyCouponBuySuccessMoreThanOneActivity.class);
                        intent1.putExtra("order_id", order_id);
                        startActivity(intent1);
                    } else {
                        Intent intent1 = new Intent(this, GroupBuyCouponBuySuccessActivity.class);
                        intent1.putExtra("order_id", order_id);
                        startActivity(intent1);
                    }
                    finish();
                    break;
            }
        }
    }
}
