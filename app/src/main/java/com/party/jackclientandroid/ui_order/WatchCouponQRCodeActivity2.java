package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.uuzuche.lib_zxing.activity.CodeUtils;

import butterknife.BindView;

public class WatchCouponQRCodeActivity2 extends BaseActivityTitle {

    @BindView(R.id.qr_code_iv)
    ImageView qr_code_iv;

    private String orderCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watch_coupon_qrcode2);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        orderCode = intent.getStringExtra("orderCode");
    }

    protected void initData() {
        setMiddleText("二维码");
        /*JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderCode",orderCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        //Bitmap mBitmap = CodeUtils.createImage(jsonObject.toString(), 400, 400, null);
        Bitmap mBitmap = CodeUtils.createImage(orderCode, 400, 400, null);
        qr_code_iv.setImageBitmap(mBitmap);
    }
}
