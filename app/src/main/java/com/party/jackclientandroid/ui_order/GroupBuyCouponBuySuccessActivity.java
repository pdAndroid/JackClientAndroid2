package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.OrderDetail;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.uuzuche.lib_zxing.activity.CodeUtils;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;
import com.zhy.adapter.recyclerview.wrapper.HeaderAndFooterWrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class GroupBuyCouponBuySuccessActivity extends BaseActivityTitle {


    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private String order_id;
    private OrderService orderService;
    private OrderDetail orderDetail;
    private long shopId;
    private List<UserOrderExtraDetail> dataList = new ArrayList<>();
    private CommonAdapter<UserOrderExtraDetail> commonAdapter;
    private HeaderAndFooterWrapper mHeaderAndFooterWrapper;
    private TextView coupon_title_tv;
    private Button return_shop_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_buy_coupon_buy_success);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        order_id = intent.getStringExtra("order_id");
    }

    protected void initData() {
        setMiddleText("购买成功");
        orderService = new OrderService(this);
        initView();
        helper = new LoadViewHelper(refresh_layout);
        helper.showLoading();
        loadData();
    }

    public void initView() {
        refresh_layout.setEnableLoadMore(false);
        refresh_layout.setEnableRefresh(false);
        commonAdapter = new CommonAdapter<UserOrderExtraDetail>(mActivity, R.layout.item_group_buy_coupon_buy_success, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final UserOrderExtraDetail item, int position) {
                Bitmap mBitmap = CodeUtils.createImage(item.getOrderCode(), 400, 400, null);
                viewHolder.setText(R.id.coupon_num_tv, "券码" + (position + 1) + ":");
                viewHolder.setText(R.id.coupon_code_tv, item.getOrderCode());
                ImageView qr_code_iv = viewHolder.getView(R.id.qr_code_iv);
                qr_code_iv.setImageBitmap(mBitmap);
            }
        };
        /*//添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this,R.drawable.layout_divider_item_decoration));
        recycler_view.addItemDecoration(divider);*/
        recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        mHeaderAndFooterWrapper = new HeaderAndFooterWrapper(commonAdapter);
        mHeaderAndFooterWrapper.addHeaderView(getHeadView());
        mHeaderAndFooterWrapper.addFootView(getFootView());
        recycler_view.setAdapter(mHeaderAndFooterWrapper);
    }


    public void loadData() {
        addDisposableIoMain(orderService.getUserOrderDetail(order_id), new DefaultConsumer<OrderDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<OrderDetail> result) {
                orderDetail = result.getData();
                shopId = orderDetail.getShopId();
                addDisposableIoMain(orderService.getUserExOrderDetail(order_id), new DefaultConsumer<List<UserOrderExtraDetail>>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<List<UserOrderExtraDetail>> result) {
                        coupon_title_tv.setText(result.getData().get(0).getItemName());
                        dataList.addAll(result.getData());
                        mHeaderAndFooterWrapper.notifyDataSetChanged();
                        helper.showContent();
                    }
                });
            }
        });
    }

    public View getHeadView() {
        View headerView = getLayoutInflater().inflate(R.layout.layout_group_buy_coupon_buy_success_head_view, recycler_view, false);
        coupon_title_tv = headerView.findViewById(R.id.coupon_title_tv);
        return headerView;
    }

    public View getFootView() {
        View footView = getLayoutInflater().inflate(R.layout.layout_group_buy_coupon_buy_success_foot_view, recycler_view, false);
        return_shop_btn = footView.findViewById(R.id.return_shop_btn);
        return_shop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActivity.startActivity(ShopDetailActivity.class,"shopId", shopId+"");
                finish();
            }
        });
        return footView;
    }
}
