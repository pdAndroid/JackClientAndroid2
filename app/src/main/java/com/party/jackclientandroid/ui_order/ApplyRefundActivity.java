package com.party.jackclientandroid.ui_order;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.OrderService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.UserOrderExtraDetail;
import com.party.jackclientandroid.event.RefundEvent;
import com.party.jackclientandroid.utils.IPUtils;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

public class ApplyRefundActivity extends BaseActivityTitle {
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.refund_money_tv)
    TextView refund_money_tv;

    private OrderService orderService;
    private String ip;
    private String orderId;
    private CommonAdapter<UserOrderExtraDetail> commonAdapter;
    private List<UserOrderExtraDetail> userOrderExtraDetailList = new ArrayList<>();
    private StringBuffer orderIdArr = new StringBuffer();
    private int count = 1;
    private double refundMoney = 0;
    private static final String TAG = "ApplyRefundActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_refund);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void initData() {
        setMiddleText("申请退款");
        orderService = new OrderService(this);
        ip = IPUtils.getIPAddress(this);
        initView();
        loadData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        orderId = intent.getStringExtra("order_id");
    }

    public void loadData() {
        addDisposableIoMain(orderService.getUserExOrderDetail(orderId), new DefaultConsumer<List<UserOrderExtraDetail>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<UserOrderExtraDetail>> result) {
                userOrderExtraDetailList.clear();
                userOrderExtraDetailList.addAll(result.getData());
                commonAdapter.notifyDataSetChanged();
            }
        });
    }

    public void initView() {
        commonAdapter = new CommonAdapter<UserOrderExtraDetail>(mActivity, R.layout.item_apply_refund_order_layout, userOrderExtraDetailList) {
            @Override
            protected void convert(ViewHolder viewHolder, final UserOrderExtraDetail item, int position) {
                CheckBox order_id_cb = viewHolder.getView(R.id.order_id_cb);
                order_id_cb.setText("订单" + (count++) + ":" + item.getId());
                order_id_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            item.setSelected(true);
                        } else {
                            item.setSelected(false);
                        }
                        updateRefundMoney();
                    }
                });
            }
        };

        recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        recycler_view.setAdapter(commonAdapter);
    }

    @OnClick(R.id.apply_refund_btn)
    public void applyRefund(View view) {
        addDisposableIoMain(orderService.applyRefund(ip, getOrderIdArr()), new DefaultConsumer<CommonResult>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                showToast("退款失败");
            }

            @Override
            public void operateSuccess(BaseResult<CommonResult> result) {
                showToast("申请退款成功");
                EventBus.getDefault().postSticky(new RefundEvent(true));
                finish();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                showToast("退款失败");
            }
        });
    }

    public void updateRefundMoney() {
        refundMoney = 0;
        for (UserOrderExtraDetail userOrderExtraDetail : userOrderExtraDetailList) {
            if (userOrderExtraDetail.isSelected()) {
                refundMoney += userOrderExtraDetail.getPrice();
            }
        }
        refund_money_tv.setText(refundMoney + "元");
    }

    public String getOrderIdArr() {
        orderIdArr.delete(0, orderIdArr.length());
        for (UserOrderExtraDetail userOrderExtraDetail : userOrderExtraDetailList) {
            if (userOrderExtraDetail.isSelected()) {
                orderIdArr.append(userOrderExtraDetail.getId() + ",");
            }
        }
        if (TextUtils.isEmpty(orderIdArr.toString())) {
            return "没有";
        } else {
            return orderIdArr.substring(0, orderIdArr.length() - 1);
        }
    }
}
