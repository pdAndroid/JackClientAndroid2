package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.utils.ConstUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;

import butterknife.BindView;

public class ShopBusinessProveActivity extends BaseActivityTitle {

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;

    private CommonAdapter<ImageBean> commonAdapter;
    private ArrayList<ImageBean> dataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_business_prove);
        initView();
    }

    public void initView() {
        refresh_layout.setEnableRefresh(false);
        refresh_layout.setEnableLoadMore(false);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 4);
        commonAdapter = new CommonAdapter<ImageBean>(mActivity, R.layout.shop_business_prove_photo, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final ImageBean bean, final int position) {
                ImageView imageView = viewHolder.getView(R.id.iv_picture);
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, bean.getImgPath(), imageView);
                viewHolder.getConvertView().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mActivity, PictureDetailActivity.class);
                        intent.putExtra("currentItem", position);
                        intent.putExtra("dataList", dataList);
                        intent.putExtra("deleteFlag", false);
                        startActivityForResult(intent, ConstUtils.IMAGE_DETAIL_CODE);
                    }
                });
            }
        };
        recycler_view.setLayoutManager(gridLayoutManager);
        recycler_view.setAdapter(commonAdapter);
    }
}
