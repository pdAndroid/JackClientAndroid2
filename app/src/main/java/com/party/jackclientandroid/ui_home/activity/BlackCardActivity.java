package com.party.jackclientandroid.ui_home.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;

/**
 * 黑卡专区界面<br>
 */
public class BlackCardActivity extends BaseActivity {

    BlackCardFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        fragment = new BlackCardFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragmentByTag = getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName());
        if (fragmentByTag == null) {
            transaction.add(R.id.nearby_fragment, fragment, fragment.getClass().getSimpleName());
        }
        transaction.show(fragment);
        transaction.commit();

    }


    @Override
    public void onBackPressed() {
        if (fragment.isDrawerOpen()) {
            fragment.hideFilterLayout();
            return;
        }
        super.onBackPressed();
    }
}
