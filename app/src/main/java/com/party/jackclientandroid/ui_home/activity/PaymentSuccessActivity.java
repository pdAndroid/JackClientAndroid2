package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 */

public class PaymentSuccessActivity extends BaseActivity {
    @BindView(R.id.iv_payment_success_back)
    ImageView ivPaymentSuccessBack;
    @BindView(R.id.total_money_big_tv)
    TextView total_money_big_tv;
    @BindView(R.id.total_money_tv)
    TextView total_money_tv;
    @BindView(R.id.not_dis_tv)
    TextView not_dis_tv;
    @BindView(R.id.rebate_money_tv)
    TextView rebate_money_tv;
    @BindView(R.id.consume_money_tv)
    TextView consume_money_tv;
    @BindView(R.id.pay_money_tv)
    TextView pay_money_tv;

    private String notDisMoney;
    private String shopName;
    private String totalMoney;
    private String isUseXiapinCoin;
    private String rebateMoney;
    private String useRebateMoney;
    private String payMoney;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_success);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        Intent intent = getIntent();
        shopName = intent.getStringExtra("shopName");
        notDisMoney = intent.getStringExtra("notDisMoney");
        totalMoney = intent.getStringExtra("totalMoney");
        isUseXiapinCoin = intent.getStringExtra("isUseXiapinCoin");
        useRebateMoney = intent.getStringExtra("useRebateMoney");
        rebateMoney = intent.getStringExtra("rebateMoney");
        payMoney = intent.getStringExtra("payMoney");

        total_money_big_tv.setText("¥ " + totalMoney);
        total_money_tv.setText("¥ " + totalMoney);
        not_dis_tv.setText("¥ " + notDisMoney);
        rebate_money_tv.setText("¥ " + useRebateMoney);
        consume_money_tv.setText("¥ " + isUseXiapinCoin);
        pay_money_tv.setText("¥ " + payMoney);


        DialogController.showRebateDialog(mActivity, shopName, Utils.clearZero(rebateMoney), (View v) -> {

        });
    }

    @OnClick({R.id.iv_payment_success_back, R.id.finish_tv})
    public void onViewClicked(View view) {
        finish();
    }
}
