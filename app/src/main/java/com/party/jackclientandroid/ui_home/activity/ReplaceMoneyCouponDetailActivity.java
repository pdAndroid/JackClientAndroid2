package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.ReplaceMoneyCouponDetail;
import com.party.jackclientandroid.controller.ConvertController;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * 代金券详情
 */
public class ReplaceMoneyCouponDetailActivity extends BaseActivityTitle {

    @BindView(R.id.shop_image_iv)
    ImageView shopImageIv;
    @BindView(R.id.describe_tv)
    TextView describeTv;
    @BindView(R.id.shop_name_tv)
    TextView shopNameTv;
    @BindView(R.id.address_tv)
    TextView addressTv;
    @BindView(R.id.original_price_tv)
    TextView originalPriceTv;
    @BindView(R.id.buy_person_limit_tv)
    TextView buy_person_limit_tv;
    @BindView(R.id.replace_money_coupon_title_tv2)
    TextView replaceMoneyCouponTitleTv2;
    @BindView(R.id.replace_money_coupon_title_tv)
    TextView replaceMoneyCouponTitleTv;
    @BindView(R.id.buy_price_tv)
    TextView buyPriceTv;
    @BindView(R.id.original_buy_price_tv)
    TextView original_buy_price_tv;
    @BindView(R.id.whole_layout)
    RelativeLayout whole_layout;
    @BindView(R.id.rule_splicing)
    TextView rule_splicing;
    @BindView(R.id.ll_merchant)
    LinearLayout ll_merchant;

    private Long couponId;
    private CouponService couponService;
    private ReplaceMoneyCouponDetail replaceMoneyCouponDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replace_money_coupon_detail);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void initData() {
        setMiddleText("代金券详情");
//        rl_merchant.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(this, ShopDetailActivity.class);
//                intent.putExtra("shopId", replaceMoneyCouponDetail.getShopId());
//                startActivity(intent);
//            }
//        });
        couponService = new CouponService(this);
        helper = new LoadViewHelper(whole_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                helper.showLoading();
                loadData();
            }
        });
        helper.showLoading();
        loadData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        couponId = intent.getLongExtra("couponId", -1L);
    }

    public void loadData() {
        addDisposableIoMain(couponService.getReplaceMoneyCouponDetail(couponId), new DefaultConsumer<ReplaceMoneyCouponDetail>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError();
            }

            @Override
            public void operateSuccess(BaseResult<ReplaceMoneyCouponDetail> result) {
                if (result.getData() != null) {
                    replaceMoneyCouponDetail = result.getData();
                    Glide.with(ReplaceMoneyCouponDetailActivity.this).load(replaceMoneyCouponDetail.getHomeImg()).into(shopImageIv);
                    replaceMoneyCouponTitleTv.setText(replaceMoneyCouponDetail.getShopName() + "代金券");
                    describeTv.setText(replaceMoneyCouponDetail.getTitle() + "," + CommonUtil.getOnceCount(replaceMoneyCouponDetail.getOnceCount()));
                    replaceMoneyCouponTitleTv2.setText(replaceMoneyCouponDetail.getShopName() + "代金券");
                    //代金券信息固定一张  单价数量
//                    buy_person_limit_tv.setText(replaceMoneyCouponDetail.getBuyPersonLimit() + "张");
                    shopNameTv.setText(replaceMoneyCouponDetail.getShopName());
                    addressTv.setText(replaceMoneyCouponDetail.getShopAddress());
                    originalPriceTv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    originalPriceTv.setText("¥" + replaceMoneyCouponDetail.getOriginalPrice());
                    buyPriceTv.setText("¥" + replaceMoneyCouponDetail.getBuyPrice());
                    original_buy_price_tv.setText("门市价：¥" + replaceMoneyCouponDetail.getOriginalPrice());
                    rule_splicing.setText(Html.fromHtml(replaceMoneyCouponDetail.getRuleSplicing()));

                    helper.showContent();
                } else {
                    helper.showEmpty();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showError();
            }
        });
    }

    @OnClick({R.id.buy_btn, R.id.phone_ll, R.id.shop_image_iv, R.id.ll_merchant})
    public void handleClckSth(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.buy_btn:
                if (!isLogin()) {
                    handleToLogin();
                    return;
                }
                intent = new Intent(ReplaceMoneyCouponDetailActivity.this, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", ConvertController.getGoodsDetailObj(replaceMoneyCouponDetail));
                startActivity(intent);
                break;
            case R.id.phone_ll:
                callPhone(replaceMoneyCouponDetail.getShopPhone());
                break;
            case R.id.shop_image_iv:
                ArrayList<ImageBean> urlList = new ArrayList<>();
                ImageBean imageBean = new ImageBean();
                imageBean.setImgPath(replaceMoneyCouponDetail.getHomeImg());
                urlList.add(imageBean);
                intent = new Intent(mApplication, PictureDetailActivity.class);
                intent.putExtra("currentItem", 0);
                intent.putExtra("dataList", urlList);
                intent.putExtra("deleteFlag", false);
                startActivity(intent);
            case R.id.ll_merchant:
                startActivity(ShopDetailActivity.class,"shopId", replaceMoneyCouponDetail.getShopId()+"");
                break;
        }
    }
}
