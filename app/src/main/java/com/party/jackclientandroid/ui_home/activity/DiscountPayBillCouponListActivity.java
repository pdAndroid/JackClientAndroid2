package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.CouponBean;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.utils.CommonUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;

public class DiscountPayBillCouponListActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private ArrayList<CouponBean> dataList;
    CommonAdapter<CouponBean> commonAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourable_pay_bill_coupon_type_list);
        initData();
    }

    protected void initData() {
        dataList = getIntent().getParcelableArrayListExtra("couponList");
        setMiddleText("优惠券");
        helper = new LoadViewHelper(mRefreshLayout);
        if (dataList.size() == 0) helper.showEmpty("没有可用的卷",null);

        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<CouponBean>(mActivity, R.layout.item_replace_money_coupon_2, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final CouponBean item, int position) {
                viewHolder.setText(R.id.buy_price_tv, "¥" + item.getBuyPrice());
                viewHolder.setText(R.id.coupon_title_tv, item.getOriginalPrice() + "元代金券");
                viewHolder.setText(R.id.desc_tv, CommonUtil.getAvailableDate(item.getAvailableDate()) + "|" + CommonUtil.getGoodsRange(item.getGoodsRange()));
                viewHolder.setText(R.id.coupon_num_tv, "x " + item.getCount());
            }
        };
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                CouponBean CouponBean = dataList.get(position);
                Intent intent = new Intent();
                intent.putExtra("couponId", CouponBean.getId() + "");
                setResult(100, intent);
                finish();
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
    }


    public boolean isOnclick(CouponBean CouponBean) {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
