package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.SecondKillCouponDetail;
import com.party.jackclientandroid.controller.ConvertController;
import com.party.jackclientandroid.photoselector.PictureDetailActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.PhoneUtil;
import com.party.jackclientandroid.utils.TimeUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.iwgang.countdownview.CountdownView;

/**
 * 秒杀券详情
 */
public class SecondKillCouponDetailActivity extends BaseActivity {

    @BindView(R.id.whole_layout)
    RelativeLayout whole_layout;
    @BindView(R.id.remaining_time_cv)
    CountdownView remaining_time_cv;
    @BindView(R.id.coupon_image_iv)
    ImageView coupon_image_iv;
    @BindView(R.id.desc_tv)
    TextView desc_tv;
    @BindView(R.id.remain_count_tv)
    TextView remain_count_tv;
    @BindView(R.id.shop_name_tv)
    TextView shop_name_tv;
    @BindView(R.id.address_tv)
    TextView address_tv;
    @BindView(R.id.coupon_title_tv2)
    TextView coupon_title_tv2;
    @BindView(R.id.original_price_tv)
    TextView original_price_tv;
    @BindView(R.id.buy_person_limit_tv)
    TextView buy_person_limit_tv;
    @BindView(R.id.over_tv)
    TextView over_tv;
    @BindView(R.id.remain_time_ll)
    LinearLayout remain_time_ll;
    @BindView(R.id.buy_price_tv)
    TextView buy_price_tv;
    @BindView(R.id.original_buy_price_tv)
    TextView original_buy_price_tv;
    @BindView(R.id.rule_splicing)
    TextView rule_splicing;

    private long coupon_id;
    private CouponService couponService;
    private SecondKillCouponDetail secondKillCouponDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_kill_coupon_detail);
        receivePassDataIfNeed(getIntent());
        initErrorView(whole_layout);
        initData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        coupon_id = intent.getLongExtra("couponId", -1);
    }

    protected void initData() {
        couponService = new CouponService(this);
        getNeedData();
    }

    public void getNeedData() {
        helper.showLoading();
        addDisposableIoMain(couponService.getSecondKillCouponDetail(coupon_id), new DefaultConsumer<SecondKillCouponDetail>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<SecondKillCouponDetail> result) {
                secondKillCouponDetail = result.getData();
                setRemainingTime();
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, secondKillCouponDetail.getHomeImg(), coupon_image_iv);
                desc_tv.setText(secondKillCouponDetail.getOriginalPrice() + "代金券");
//                desc_tv.setText(secondKillCouponDetail.getOriginalPrice() + "代金券" + secondKillCouponDetail.getBuyPersonLimit() + "张，" + (secondKillCouponDetail.getOnceCount() > 0 ? "可叠加" : "不可叠加"));
                remain_count_tv.setText("剩余：" + secondKillCouponDetail.getStockCount() + "次");
                shop_name_tv.setText(secondKillCouponDetail.getShopName());
                address_tv.setText(secondKillCouponDetail.getShopAddress());
                coupon_title_tv2.setText(secondKillCouponDetail.getShopName() + "代金券");
                buy_person_limit_tv.setText(secondKillCouponDetail.getBuyPersonLimit() + "张");
                original_price_tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                original_price_tv.setText("¥" + secondKillCouponDetail.getOriginalPrice());
                buy_price_tv.setText("¥" + secondKillCouponDetail.getBuyPrice());
                original_buy_price_tv.setText("门市价：¥" + secondKillCouponDetail.getOriginalPrice());
                rule_splicing.setText(Html.fromHtml(secondKillCouponDetail.getRuleSplicing()));
                helper.showContent();
            }
        });

    }

    /**
     * 设置剩余时间（倒计时）
     */
    public void setRemainingTime() {
        long endSaleTime = secondKillCouponDetail.getEndSaleTime();
        long currSystemTime = TimeUtil.getCurrTimeStamp();
        long remainingTime = endSaleTime - currSystemTime;
        if (remainingTime > 0) {
            remain_time_ll.setVisibility(View.VISIBLE);
            over_tv.setVisibility(View.GONE);
            remaining_time_cv.start(remainingTime);
        } else {
            remain_time_ll.setVisibility(View.GONE);
            over_tv.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.buy_btn, R.id.phone_ll, R.id.iv_back})
    public void handleClckSth(View view) {
        switch (view.getId()) {
            case R.id.buy_btn:
                if (!isLogin()) {
                    handleToLogin();
                    return;
                }
                Intent intent = new Intent(mActivity, SubmitOrderActivity.class);
                intent.putExtra("goodDetail_data", ConvertController.getGoodsDetailObj(secondKillCouponDetail));
                startActivity(intent);
                break;
            case R.id.phone_ll:
                PhoneUtil.callPhone(mActivity, secondKillCouponDetail.getShopName(),
                        "电话号码：" + secondKillCouponDetail.getShopPhone(),
                        secondKillCouponDetail.getShopPhone());
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.shop_image_iv:
                ArrayList<ImageBean> urlList = new ArrayList<>();
                ImageBean imageBean = new ImageBean();
                imageBean.setImgPath(secondKillCouponDetail.getHomeImg());
                urlList.add(imageBean);
                Intent intent1 = new Intent(mApplication, PictureDetailActivity.class);
                intent1.putExtra("currentItem", 0);
                intent1.putExtra("dataList", urlList);
                intent1.putExtra("deleteFlag", false);
                startActivity(intent1);
        }
    }

}
