package com.party.jackclientandroid.ui_home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.base.BaseFragmentFilter;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.FilterCondition;
import com.party.jackclientandroid.bean.ShopItemMessage;
import com.party.jackclientandroid.controller.ShopItemController;
import com.party.jackclientandroid.ui_home.activity.SearchActivity;
import com.party.jackclientandroid.ui_home.activity.ShopDetailActivity;
import com.party.jackclientandroid.ui_mine.choose_location.ChooseLocationActivity;
import com.party.jackclientandroid.utils.ConstUtils;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

public class NeighbourFragment extends BaseFragmentFilter {


    ShopItemController shopItemController;
    MyCommonAdapter<ShopItemMessage> commonAdapter;
    List<ShopItemMessage> shopItemMessage = new ArrayList<>();
    int pageSize = 30, pageNumber = 1;

    public static NeighbourFragment newInstance() {
        NeighbourFragment fragment = new NeighbourFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_filter_full;
    }


    @OnClick(R.id.btn_back)
    public void mLeftBtn(View view) {
        getActivity().finish();
    }


    @Override
    protected void initData() {
        super.initData();
        btn_back.setVisibility(View.VISIBLE);
        shopItemController = new ShopItemController((BaseActivity) getActivity(), mRecyclerView, mRefreshLayout);
        commonAdapter = shopItemController.initAdapter(shopItemMessage);
        initFilterLayout();
        getNeedData();
    }

    public void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return true;
            }
        });

        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                pageNumber = 1;
                getNeedData();
            }
        });
    }

    /**
     * item点击事件
     *
     * @param position
     */
    private void handleItemClick(int position) {
        ShopItemMessage recommendShopMessage = shopItemMessage.get(position);
        mActivity.startActivity(ShopDetailActivity.class,"shopId",recommendShopMessage.getId()+"");
    }


    public void getNeedData() {
        super.getNeedData();
        Observable<BaseResult<List<ShopItemMessage>>> shopByMultiCondition = searchService.getShopByMultiCondition(pageNumber, pageSize, longitude, latitude, areaId, needSearchKey, categoryId, jsonDataStr);
        addDisposableIoMain(shopByMultiCondition, new DefaultConsumer<List<ShopItemMessage>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ShopItemMessage>> baseBean) {
                commonAdapter.clear(pageNumber);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(pageNumber, pageSize, baseBean.getData().size(), mRefreshLayout);
                finishData(commonAdapter.getDatas());
            }
        });
    }

    /**
     * 加载更多
     */
    public void loadMoreData() {
        pageNumber++;
        getNeedData();
    }

    @Override
    public boolean onFragmentBackPressed() {
        if (isDrawerOpen()) {
            hideFilterLayout();
            return true;
        }
        return false;
    }
}
