package com.party.jackclientandroid.ui_home.fragment;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.HomeService;
import com.party.jackclientandroid.base.BaseFragmentFilter;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BlackCardItem;
import com.party.jackclientandroid.bean.NeighbourSpellingGroupBean;
import com.party.jackclientandroid.bean.SpellingCoupon;
import com.party.jackclientandroid.ui_home.activity.SpellingGroupCouponDetailActivity;
import com.party.jackclientandroid.utils.DistanceUtil;
import com.party.jackclientandroid.utils.TimeUtil;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;


import java.util.ArrayList;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;

/**
 * 首页-拼团
 */
public class SpellingGroupFragment extends BaseFragmentFilter {

    HomeService homeService;
    List<SpellingCoupon> dataList = new ArrayList<>();
    MyCommonAdapter<SpellingCoupon> commonAdapter;
    int page = 1;
    int pageSize = Constant.PAGE_SIZE;

    public static SpellingGroupFragment newInstance() {
        SpellingGroupFragment fragment = new SpellingGroupFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_filter_main;
    }

    public SpellingCoupon isSpellingOk(ViewHolder holder, SpellingCoupon item) {
        ProgressBar progressBar = holder.getView(R.id.progress_bar);
        if (item.getSaleNumber() < item.getUnionStartNumber()) {
            item.setSpellingType(Constant.SPELLING);
            holder.setBackgroundRes(R.id.now_rob_btn, R.color.spelling);
            holder.setTextColorRes(R.id.buy_prce_tv, R.color.spelling);
            holder.setTextColorRes(R.id.remain_count_tv, R.color.spelling);
            holder.setText(R.id.state_str, "拼团中");
            progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.not_cheng_progress_bar_style));

            item.setSpellingStateString("差" + (item.getUnionStartNumber() - item.getSaleNumber()) + "人成团");
        } else if (item.getSaleNumber() >= item.getUnionStartNumber()) {
            item.setSpellingType(Constant.SPELLING_OK);
            holder.setBackgroundRes(R.id.now_rob_btn, R.color.spelling_ok);
            holder.setTextColorRes(R.id.buy_prce_tv, R.color.spelling_ok);
            holder.setTextColorRes(R.id.remain_count_tv, R.color.spelling_ok);
            holder.setText(R.id.state_str, "已成团");
            progressBar.setProgressDrawable(getResources().getDrawable(R.drawable.not_man_progress_bar_style));

            item.setSpellingStateString("差" + (item.getUnionEndNumber() - item.getSaleNumber()) + "人满团");
        }
        return item;
    }


    @Override
    protected void initData() {
        super.initData();
        if (conditionType == null) {
            conditionType = "3";//拼团卷类型为3
        }
        homeService = new HomeService((BaseActivity) mActivity);
        commonAdapter = new MyCommonAdapter<SpellingCoupon>(mActivity, R.layout.item_spelling_group_coupon2, dataList) {
            @Override
            protected void convert(ViewHolder holder, SpellingCoupon item, int position) {
                item = isSpellingOk(holder, item);
                holder.setText(R.id.shop_name_tv, item.getShopName());
                // 拼团券显示groupBuyImg
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, item.getCouponImg(), (ImageView) holder.getView(R.id.coupon_iv));
                holder.setText(R.id.coupon_name_tv, item.getTitle());
                holder.setText(R.id.buy_prce_tv, "¥" + item.getBuyPrice());
                holder.setText(R.id.remain_count_tv, item.getSpellingStateString());
                TextView original_price_tv = holder.getView(R.id.original_price_tv);
                original_price_tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                holder.setText(R.id.original_price_tv, "¥" + item.getOriginalPrice());
                double distance = DistanceUtil.getDistance(mApplication.getMap().getLongitude(), mApplication.getMap().getLatitude(), item.getLongitude(), item.getLatitude());
                holder.setText(R.id.distance_tv, item.getSymbolBuild() + " " + DistanceUtil.handleDistance(distance));
                holder.setOnClickListener(R.id.now_rob_btn, (View v) -> {
                    handleItemClick(position);
                });
                ProgressBar progressBar = holder.getView(R.id.progress_bar);
                int total = item.getUnionEndNumber(), saleNum = item.getSaleNumber();
                progressBar.setMax(total);
                progressBar.setProgress(saleNum);

                //处理剩余时间
                CountdownView countdownView = holder.getView(R.id.count_down_view);
                LinearLayout remain_time_ll = holder.getView(R.id.remain_time_ll);
                TextView remain_date_tv1 = holder.getView(R.id.remain_date_tv1);
                TextView remain_date_tv2 = holder.getView(R.id.remain_date_tv2);
                TextView remain_date_tv3 = holder.getView(R.id.remain_date_tv3);
                LinearLayout remain_date_ll = holder.getView(R.id.remain_date_ll);
                long currTimeStamp = TimeUtil.getCurrTimeStamp();
                long endTimeStamp = item.getEndSaleTime();
                long remainTimeStamp = endTimeStamp - currTimeStamp;
                if (remainTimeStamp <= 0) {
                    remain_time_ll.setVisibility(View.GONE);
                    remain_date_ll.setVisibility(View.VISIBLE);
                    remain_date_tv1.setText("已");
                    remain_date_tv2.setText("结");
                    remain_date_tv3.setText("束");
                } else if (remainTimeStamp <= 24 * 3600 * 1000) {
                    countdownView.start(remainTimeStamp);
                    remain_time_ll.setVisibility(View.VISIBLE);
                    remain_date_ll.setVisibility(View.GONE);
                } else {
                    remain_time_ll.setVisibility(View.GONE);
                    remain_date_ll.setVisibility(View.VISIBLE);
                    remain_date_tv1.setText("剩");
                    remain_date_tv2.setText(TimeUtil.getRemainTime(remainTimeStamp));
                    remain_date_tv3.setText("天");
                }
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);

        initFilterLayout();
        initListener();
    }

    @Override
    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return true;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getNeedData();
            }
        });
        getNeedData();
    }

    /**
     * item点击事件
     *
     * @param position
     */
    private void handleItemClick(int position) {
        Intent intent = new Intent(mActivity, SpellingGroupCouponDetailActivity.class);
        intent.putExtra("couponId", dataList.get(position).getId());
        startActivity(intent);
    }

    /**
     * 下拉获取数据
     */
    public void getNeedData() {
        super.getNeedData();
        addDisposableIoMain(searchService.getSpellingCoupons(page, pageSize, longitude, latitude, areaId, needSearchKey, categoryId, conditionType, jsonDataStr), new DefaultConsumer<List<SpellingCoupon>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<SpellingCoupon>> baseBean) {
                commonAdapter.clear(page);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(page, pageSize, baseBean.getData().size(), mRefreshLayout);
                finishData(commonAdapter.getDatas());
            }
        });
    }

    /**
     * 加载更多
     */
    private void loadMoreData() {
        page++;
        getNeedData();
    }

}
