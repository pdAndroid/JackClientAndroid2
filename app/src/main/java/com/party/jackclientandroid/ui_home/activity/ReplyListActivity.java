package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.ShopService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.UserCommentAndReply;
import com.party.jackclientandroid.utils.CommonUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;
import com.zhy.adapter.recyclerview.wrapper.HeaderAndFooterWrapper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ReplyListActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private HeaderAndFooterWrapper mHeaderAndFooterWrapper;
    private List<UserCommentAndReply> userCommentAndReplyList = new ArrayList<>();
    private Integer page = 1;
    private Integer size = 0;
    private ShopService shopService;
    private Long shopId, orderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_list);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    protected void initData() {
        setMiddleText("评论详情");
        shopService = new ShopService(this);
        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        CommonAdapter commonAdapter = new CommonAdapter<UserCommentAndReply>(mActivity, R.layout.item_comment_to_shop, userCommentAndReplyList) {
            @Override
            protected void convert(ViewHolder viewHolder, final UserCommentAndReply userCommentAndReply, int position) {
                long averageScore = (userCommentAndReply.getEnviroScore() + userCommentAndReply.getServiceScore() + userCommentAndReply.getTasteScore()) / 3;
                LinearLayout starLl = viewHolder.getView(R.id.star_ll);
                starLl.removeAllViews();
                if (averageScore % 2 == 0) {
                    for (int i = 1; i <= 5; i++) {
                        if (i <= averageScore / 2) {
                            starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                        } else {
                            starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                        }
                    }
                } else {
                    for (int i = 1; i <= 5; i++) {
                        if (i <= averageScore / 2) {
                            starLl.addView(getStarIv(R.mipmap.star_all_selected, starLl));
                        } else if (i == averageScore / 2 + 1) {
                            starLl.addView(getStarIv(R.mipmap.star_half_selected, starLl));
                        } else {
                            starLl.addView(getStarIv(R.mipmap.star_no_selected, starLl));
                        }
                    }
                }
                viewHolder.setText(R.id.nickname_tv, userCommentAndReply.getNickName());
                viewHolder.setText(R.id.comment_time_tv, CommonUtil.formatTime(userCommentAndReply.getCreate()));
                viewHolder.setText(R.id.comment_content_tv, userCommentAndReply.getContent());
                Glide.with(mActivity).load(userCommentAndReply.getIcon()).into((ImageView) viewHolder.getView(R.id.header_image));
            }
        };
        mHeaderAndFooterWrapper = new HeaderAndFooterWrapper(commonAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(mHeaderAndFooterWrapper);
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
        mRefreshLayout.autoRefresh();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        shopId = intent.getLongExtra("shopId", -1L);
        orderId = intent.getLongExtra("orderId", -1L);
    }

    private ImageView getStarIv(int imageId, View view) {
        ImageView starIv = (ImageView) getLayoutInflater().inflate(R.layout.layout_star_iv, (ViewGroup) view.getParent(), false);
        Glide.with(mActivity).load(getResources().getDrawable(imageId)).into(starIv);
        return starIv;
    }

    public void loadData() {
        page = 1;
        addDisposableIoMain(shopService.getUserReplyList(shopId, page, Constant.PAGE_SIZE, orderId), new DefaultConsumer<List<UserCommentAndReply>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<UserCommentAndReply>> result) {
                size = result.getData().size();
                if (size == 0) {

                } else {
                    page++;
                    userCommentAndReplyList.clear();
                    userCommentAndReplyList.addAll(result.getData());
                }
                mHeaderAndFooterWrapper.notifyDataSetChanged();
                mRefreshLayout.finishRefresh();
                mRefreshLayout.setNoMoreData(false);
            }
        });
    }

    private void loadMoreData() {
        addDisposableIoMain(shopService.getUserReplyList(shopId, page, Constant.PAGE_SIZE, orderId), new DefaultConsumer<List<UserCommentAndReply>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<UserCommentAndReply>> result) {
                size = result.getData().size();
                page++;
                userCommentAndReplyList.addAll(result.getData());
                mHeaderAndFooterWrapper.notifyDataSetChanged();
                if (size < Constant.PAGE_SIZE) {
                    mRefreshLayout.finishLoadMoreWithNoMoreData();
                } else {
                    mRefreshLayout.finishLoadMore();
                }
            }
        });
    }
}
