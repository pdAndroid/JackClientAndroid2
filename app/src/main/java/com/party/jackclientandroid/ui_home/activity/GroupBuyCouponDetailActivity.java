package com.party.jackclientandroid.ui_home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.GroupBuyCouponDetail;
import com.party.jackclientandroid.bean.GroupBuyCouponIncludeGoods;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.controller.ConvertController;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_mine.my_wallet.RealNameAuthenticationActivity;
import com.party.jackclientandroid.ui_order.SubmitOrderActivity;
import com.party.jackclientandroid.utils.PhoneUtil;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * 套餐券（团购券）详情
 */

public class GroupBuyCouponDetailActivity extends BaseActivityTitle {
    @BindView(R.id.group_buy_coupon_iv)
    ImageView groupBuyCouponIv;
    @BindView(R.id.desc_tv)
    TextView describeTv;
    @BindView(R.id.shop_name_tv)
    TextView shopNameTv;
    @BindView(R.id.address_tv)
    TextView addressTv;
    @BindView(R.id.group_buy_coupon_title_tv)
    TextView groupBuyCouponTitleTv;
    @BindView(R.id.contain_goods_ll)
    LinearLayout containGoodsLl;
    @BindView(R.id.buy_price_tv)
    TextView buyPriceTv;
    @BindView(R.id.original_buy_price_tv)
    TextView originalPriceTv;
    @BindView(R.id.whole_layout)
    RelativeLayout whole_layout;
    @BindView(R.id.no_goods_ll)
    LinearLayout no_goods_ll;
    @BindView(R.id.buy_introduce_wv)
    TextView buy_introduce_wv;

    private Long couponId;
    private CouponService couponService;
    private GroupBuyCouponDetail groupBuyCouponDetail;
    private List<GroupBuyCouponIncludeGoods> groupBuyCouponIncludeGoodsList;
    private int isAuthentication;
    private int isPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_buy_coupon_detail);
        receivePassDataIfNeed(getIntent());
        initData();
    }

    public void initData() {
        setMiddleText("团购券详情");
        couponService = new CouponService(this);
        helper = new LoadViewHelper(whole_layout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                helper.showLoading();
                loadData();
            }
        });
        User user = mApplication.getUser();
        isAuthentication = user.getIsAuthentication();
        isPassword = user.getIsPassword();
        helper.showLoading();
        loadData();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        couponId = intent.getLongExtra("couponId", -1L);
    }

    public void loadData() {
        addDisposableIoMain(couponService.getGroupBuyCouponDetail(couponId), new DefaultConsumer<GroupBuyCouponDetail>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError();
            }

            @Override
            public void operateSuccess(BaseResult<GroupBuyCouponDetail> result) {
                groupBuyCouponDetail = result.getData();
                Glide.with(GroupBuyCouponDetailActivity.this).load(groupBuyCouponDetail.getGroupBuyImg()).into(groupBuyCouponIv);
                groupBuyCouponTitleTv.setText(groupBuyCouponDetail.getTitle() + groupBuyCouponDetail.getDinersNumberName() + "团购券");
//                describeTv.setText(groupBuyCouponDetail.getOriginalPrice() + "元团购券" + groupBuyCouponDetail.getBuyPersonLimit() + "张，每次可使用" + groupBuyCouponDetail.getOnceCount() + "张");
                describeTv.setText(groupBuyCouponDetail.getTitle() + "");
                shopNameTv.setText(groupBuyCouponDetail.getShopName());
                addressTv.setText(groupBuyCouponDetail.getShopAddress());
                originalPriceTv.setText("¥" + groupBuyCouponDetail.getOriginalPrice());
                buy_introduce_wv.setText(Html.fromHtml(groupBuyCouponDetail.getRuleSplicing()));
                buyPriceTv.setText("¥" + groupBuyCouponDetail.getBuyPrice());
                originalPriceTv.setText("门市价：¥" + groupBuyCouponDetail.getOriginalPrice());

                addDisposableIoMain(couponService.getGroupBuyCouponIncludeGoods(couponId), new DefaultConsumer<List<GroupBuyCouponIncludeGoods>>(mApplication) {
                    @Override
                    public void operateSuccess(BaseResult<List<GroupBuyCouponIncludeGoods>> result) {
                        groupBuyCouponIncludeGoodsList = result.getData();
                        if (groupBuyCouponIncludeGoodsList.size() <= 0) {
                            no_goods_ll.setVisibility(View.VISIBLE);
                            containGoodsLl.setVisibility(View.GONE);
                        } else {
                            no_goods_ll.setVisibility(View.GONE);
                            containGoodsLl.setVisibility(View.VISIBLE);
                            for (GroupBuyCouponIncludeGoods groupBuyCouponIncludeGoods : groupBuyCouponIncludeGoodsList) {
                                containGoodsLl.addView(getGoodsView(groupBuyCouponIncludeGoods));
                            }
                        }

                        helper.showContent();
                    }
                });
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showError();
            }
        });

    }

    public View getGoodsView(GroupBuyCouponIncludeGoods groupBuyCouponIncludeGoods) {
        View view = getLayoutInflater().inflate(R.layout.layout_contain_goods, null, false);
        ((TextView) view.findViewById(R.id.goods_name_tv)).setText(groupBuyCouponIncludeGoods.getTitle());
        ((TextView) view.findViewById(R.id.num_tv)).setText(groupBuyCouponIncludeGoods.getCount() + "份");
        ((TextView) view.findViewById(R.id.price_tv)).setText("¥" + groupBuyCouponIncludeGoods.getPrice());
        return view;
    }

    @OnClick({R.id.buy_btn, R.id.phone_ll})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.buy_btn:
                if (!isLogin()) {
                    handleToLogin();
                    return;
                } else {
                    Intent intent = new Intent(GroupBuyCouponDetailActivity.this, SubmitOrderActivity.class);
                    intent.putExtra("goodDetail_data", ConvertController.getGoodsDetailObj(groupBuyCouponDetail));
                    startActivity(intent);
                }
                break;
//                    DialogController.showConfirmDialog2(this, "您尚未实名认证", "使用其他支付", "去实名认证", new ConfirmDialog.DialogListener() {
//                        @Override
//                        public void cancel(ConfirmDialog dialog) {
//                            Intent intent = new Intent(GroupBuyCouponDetailActivity.this, SubmitOrderActivity.class);
//                            intent.putExtra("goodDetail_data", ConvertController.getGoodsDetailObj(groupBuyCouponDetail));
//                            startActivity(intent);
//                        }
//
//                        @Override
//                        public void ok(ConfirmDialog dialog) {
//                            Intent intent = new Intent(GroupBuyCouponDetailActivity.this, RealNameAuthenticationActivity.class);
//                            startActivity(intent);
//                        }
//                    });
            case R.id.phone_ll:
                PhoneUtil.callPhone(mActivity, groupBuyCouponDetail.getShopName(),
                        "电话号码：" + groupBuyCouponDetail.getShopPhone(),
                        groupBuyCouponDetail.getShopPhone());
                break;
        }
    }
}
