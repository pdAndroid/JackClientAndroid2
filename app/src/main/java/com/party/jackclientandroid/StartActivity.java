package com.party.jackclientandroid;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import com.party.jackclientandroid.controller.AMapController;
import com.party.jackclientandroid.controller.AppUpdateController;
import com.party.jackclientandroid.event.AMapBeanEvent;
import com.party.jackclientandroid.permission.RuntimeRationale;
import com.party.jackclientandroid.utils.ConstUtils;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.Permission;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;

public class StartActivity extends BaseActivity {
    Handler mHandler;
    AppUpdateController appUpdateController;


    /**
     * 是否有读写手机目录的权限
     */
    boolean isHasPer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        initData();
    }

    protected void initData() {
        mHandler = new Handler();
        appUpdateController = new AppUpdateController(this, (boolean showMsg) -> {
            initMap();
        });
        appUpdateController.checkAppUpdate();
    }

    protected void initMap() {
        mApplication.updateMap((View v) -> {
            startActivity(MainActivity.class);
            finish();
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }


    /**
     * 申请读写权限
     */
    public void applyNeedPermissions() {
        final String[] permissions = new String[]{Permission.READ_EXTERNAL_STORAGE, Permission.WRITE_EXTERNAL_STORAGE};
        AndPermission.with(this)
                .runtime()
                .permission(permissions)
                .rationale(new RuntimeRationale())
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        isHasPer = true;
                        checkPermissionsAgain(true, permissions);
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        isHasPer = false;
                        checkPermissionsAgain(false, permissions);
                    }
                }).start();
    }

    public boolean isHasPer() {
        return isHasPer;
    }

    /**
     * 用户点击不在提示,或者在应用管理里面关闭权限之后
     */
    public void checkPermissionsAgain(boolean onGranted, String[] permissions) {
        if (onGranted) {
            if (AndPermission.hasPermissions(mActivity, permissions)) {
                if (appUpdateController != null) {
                    appUpdateController.showApkDownloadDialog();
                }
            } else {
                showSettingDialog(mActivity, permissions);
            }
        } else {
            if (AndPermission.hasAlwaysDeniedPermission(mActivity, permissions)) {
                showSettingDialog(mActivity, permissions);
            }
        }
    }

    /**
     * Display setting dialog.
     */
    public void showSettingDialog(Context context, final String[] permissions) {
        List<String> permissionNames = Permission.transformText(context, permissions);
        String message = context.getString(R.string.message_permission_always_failed, TextUtils.join("\n", permissionNames));

        new AlertDialog.Builder(mActivity)
                .setCancelable(false)
                .setTitle(R.string.title_dialog)
                .setMessage(message)
                .setPositiveButton(R.string.setting, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        toSystemSetting();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    /**
     * 调整到系统设置界面去申请权限
     */
    public void toSystemSetting() {
        AndPermission.with(mActivity).runtime().setting().start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // 安装未知源请求码
        if (requestCode == ConstUtils.MANAGE_UNKNOWN_APP_SOURCES_CODE && resultCode == RESULT_OK) {
            if (appUpdateController != null) {
                appUpdateController.continueInstallApk();
            }
        }
        if (requestCode == AMapController.ACTION_LOCATION_SOURCE_SETTINGS) {
            initMap();
        }
    }


}
