package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.R;

/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class ConfirmDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView title_tv;
    private TextView message_tv;
    private TextView cancel_tv;
    private TextView ok_tv;
    private DialogListener dialogListener;
    private int position;

    protected ConfirmDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected ConfirmDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_confirm, null);
        setView(mView);
        cancel_tv = mView.findViewById(R.id.cancel_tv);
        ok_tv = mView.findViewById(R.id.ok_tv);
        title_tv = mView.findViewById(R.id.title_tv);
        message_tv = mView.findViewById(R.id.message_tv);
        cancel_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.cancel(ConfirmDialog.this);
            }
        });
        ok_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogListener.ok(ConfirmDialog.this);
            }
        });
    }

    public void setMessage(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        message_tv.setText(message);
    }

    public void setTitle(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        title_tv.setText(message);
    }

    public void setPosition(int position) {
        this.position = position;
    }


    public interface DialogListener {
        void cancel(ConfirmDialog dialog);
        void ok(ConfirmDialog dialog);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private DialogListener dialogListener;
        private CharSequence message;
        private CharSequence title;
        private String cancelBtnText;
        private String okBtnText;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(CharSequence title) {
            this.title = title;
            return this;
        }

        public Builder setCancelBtnText(String text) {
            this.cancelBtnText = text;
            return this;
        }

        public Builder setOkBtnText(String text) {
            this.okBtnText = text;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setDialogListener(DialogListener dialogListener) {
            this.dialogListener = dialogListener;
            return this;
        }


        public ConfirmDialog create() {
            final ConfirmDialog dialog = new ConfirmDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setTitle(title);
            dialog.setMessage(message);
            dialog.setCancelBtnText(cancelBtnText);
            dialog.setOkBtnText(okBtnText);
            dialog.setDialogListener(dialogListener);
            return dialog;
        }
    }

    public DialogListener getDialogListener() {
        return dialogListener;
    }

    public void setDialogListener(DialogListener dialogListener) {
        this.dialogListener = dialogListener;
    }

    public void setCancelBtnText(String text){
        if (!TextUtils.isEmpty(text)){
            cancel_tv.setText(text);
        }
    }
    public void setOkBtnText(String text){
        if (!TextUtils.isEmpty(text)){
            ok_tv.setText(text);
        }
    }

    public void setCancelTextColor(int resId){
        cancel_tv.setTextColor(resId);
    }
    public void setOkTextColor(int resId){
        ok_tv.setTextColor(resId);
    }
}
