package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.utils.CommonUtil;

import butterknife.BindView;

/**
 * Created by 南宫灬绝痕 on 2019/1/12.
 */

public class MonitoringDialog extends AlertDialog implements View.OnClickListener {
    @BindView(R.id.tv_mo_remarks)
    TextView tvMoRemarks;
    @BindView(R.id.tv_mo_time)
    TextView tvMoTime;
    @BindView(R.id.btn_mo_wrong)
    Button btnMoWrong;

    private Context mContext;
    private View mView;
    private String remarks;
    private long unsealingTime;

    protected MonitoringDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected MonitoringDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        initView(context);
    }


    protected MonitoringDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    private void setContent(String mRemarks, long mUnsealingTime) {
        remarks = mRemarks;
        unsealingTime = mUnsealingTime;
        tvMoRemarks.setText(remarks);
        tvMoTime.setText("封停时间至：" + CommonUtil.formatTime(unsealingTime));
    }

    private void initView(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_monitoring, null);
        tvMoRemarks = mView.findViewById(R.id.tv_mo_remarks);
        tvMoTime = mView.findViewById(R.id.tv_mo_time);
        btnMoWrong = mView.findViewById(R.id.btn_mo_wrong);
        setView(mView);
        btnMoWrong.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_mo_wrong:
                dismiss();
                break;
        }
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private String remarks;
        private long unsealingTime;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public MonitoringDialog.Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public MonitoringDialog.Builder setContent(String remarks, long unsealingTime) {
            this.remarks = remarks;
            this.unsealingTime = unsealingTime;
            return this;
        }

        public MonitoringDialog create() {
            final MonitoringDialog dialog = new MonitoringDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setContent(remarks, unsealingTime);
            return dialog;
        }
    }
}
