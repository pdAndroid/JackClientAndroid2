package com.party.jackclientandroid.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.party.jackclientandroid.utils.DensityUtils;

public class RedPacketView extends View {
    int mWidth;
    int mHeight;
    RectF mRectF;
    int mRadius;
    Paint mPaint1;
    Paint mPaint2;

    public RedPacketView(Context context) {
        this(context, null);
    }

    public RedPacketView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RedPacketView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mRadius = DensityUtils.dip2px(context, 20);
        mPaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint1.setColor(Color.RED);
        mPaint2 = new Paint(mPaint1);
        mPaint2.setColor(Color.DKGRAY);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        mRectF = new RectF(0, 0, mWidth, mHeight);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawRoundRect(mRectF, mRadius, mRadius, mPaint1);
    }
}
