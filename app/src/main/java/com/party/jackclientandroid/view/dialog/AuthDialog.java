package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.party.jackclientandroid.R;

public class AuthDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private View mView;
    private View.OnClickListener aliAuthListener;
    private View ok_btn, delete_dialog;


    protected AuthDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected AuthDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.auth_dialog, null);
        setView(mView);

        ok_btn = mView.findViewById(R.id.ok_btn);
        delete_dialog = mView.findViewById(R.id.delete_dialog);


        ok_btn.setOnClickListener(this);
        delete_dialog.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        v.setTag(this);
        switch (v.getId()) {
            case R.id.delete_dialog:
                this.dismiss();
                break;
            case R.id.ok_btn:
                this.dismiss();
                aliAuthListener.onClick(v);
                break;
        }
    }

    public void setAliAuthListener(View.OnClickListener listener) {
        aliAuthListener = listener;
    }

    public static class Builder {
        private Context mContext;
        private View.OnClickListener aliAuth, idAuth;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setAliAuth(View.OnClickListener aliAuth) {
            this.aliAuth = aliAuth;
            return this;
        }

        public Builder setIdAuth(View.OnClickListener idAuth) {
            this.idAuth = idAuth;
            return this;
        }

        public AuthDialog create() {
            final AuthDialog dialog = new AuthDialog(mContext);
            dialog.setAliAuthListener(aliAuth);
            return dialog;
        }
    }
}
