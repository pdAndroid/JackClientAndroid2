package com.party.jackclientandroid.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.party.jackclientandroid.R;


/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class DeletePictureDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView mTitleTv;
    private TextView mContentTv;
    private TextView mCancelTv;
    private TextView mOkTv;
    private OkListener mOkListener;
    private int position;

    protected DeletePictureDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected DeletePictureDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.layout_delete_picture_dialog, null);
        setView(mView);
        mCancelTv = mView.findViewById(R.id.tv_cancel);
        mOkTv = mView.findViewById(R.id.tv_ok);
        mTitleTv = mView.findViewById(R.id.tv_title);
        mContentTv = mView.findViewById(R.id.tv_address);
        mCancelTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mOkTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOkListener != null) {
                    mOkListener.deletePicture(position);
                }
            }
        });
    }

    public void setMessage(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        mContentTv.setText(message);
    }

    public void setTitle(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        mTitleTv.setText(message);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setOkListener(OkListener okListener) {
        this.mOkListener = okListener;
    }

    public interface OkListener {
        void deletePicture(int position);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private int position;
        private OkListener mOkListener;
        private CharSequence message;
        private CharSequence normalMessage;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(CharSequence message) {
            this.normalMessage = message;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setOkListener(OkListener okListener) {
            this.mOkListener = okListener;
            return this;
        }

        public Builder setPosition(int position) {
            this.position = position;
            return this;
        }

        public DeletePictureDialog create() {
            final DeletePictureDialog dialog = new DeletePictureDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setPosition(position);
            dialog.setTitle(normalMessage);
            dialog.setMessage(message);
            dialog.setOkListener(mOkListener);
            return dialog;
        }
    }
}
