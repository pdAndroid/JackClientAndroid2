package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.ui_home.activity.PaymentSuccessActivity;

public class RebatePackageDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private View mView;
    private TextView okBtn;
    private TextView rebate_money_tv, shop_name_tv;
    private String rebatemoney, shopname;

    protected RebatePackageDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    private void setContent(String shopName, String rebateMoney) {
        rebate_money_tv.setText(rebateMoney + "元 返利金");
        shop_name_tv.setText(shopName);
        rebatemoney = rebateMoney;
        shopname = shopName;
    }


    protected RebatePackageDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    View.OnClickListener listener;

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.layout_rebate_package_dialog, null);
        setView(mView);

        okBtn = mView.findViewById(R.id.ok_btn);

        rebate_money_tv = mView.findViewById(R.id.rebate_money_tv);
        shop_name_tv = mView.findViewById(R.id.shop_name_tv);
        okBtn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok_btn:
                if (listener != null) listener.onClick(v);
                dismiss();
                Intent intent = new Intent(mContext, PaymentSuccessActivity.class);
                intent.putExtra("rebateMoney", rebatemoney);
                intent.putExtra("shopName", shopname);
                mContext.startActivity(intent);
                break;
        }
    }

    public static class Builder {
        private Context mContext;
        private String rebateMoney, shopName;
        private View.OnClickListener listener;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setContent(String shopName, String rebateMoney) {
            this.rebateMoney = rebateMoney;
            this.shopName = shopName;
            return this;
        }

        public Builder setOnClickListener(View.OnClickListener listener) {
            this.listener = listener;
            return this;
        }

        public RebatePackageDialog create() {
            final RebatePackageDialog dialog = new RebatePackageDialog(mContext);
            dialog.setContent(shopName, rebateMoney);
            dialog.setOnClickListener(listener);
            return dialog;
        }
    }


}
