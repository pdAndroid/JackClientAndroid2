package com.party.jackclientandroid.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;

import java.util.List;


public class IndicatorBar extends LinearLayout implements OnPageChangeListener {

	private int mVisibleCount = DEFAULT_VISIBLE_COUNT;
	private static final int DEFAULT_VISIBLE_COUNT = 4;
	private static final float DEFAULT_TEXT_SIZE=16;
	private static final int DEFAULT_INDICATOR_COLOR = 0xaaff0000;
	private static final int DEFAULT_TEXT_COLOR = 0xaaffffff;
	private static final int DEFAULT_SELECT_TEXT_COLOR = 0xffffffff;
	private static final int DEAFAULT_BACKGROUND_COLOR = 0xffff00;
	private ViewPager mViewPager;
	private Paint mPaint;
	private float mIndiHeight = 10;
	private int mIndicatorColor;
	private int mTextColor;
	private int mBackgroundColor;
	private float mTextSize;
	private int mTextSelectColor;
	private int mInitPosition = 0;

	public IndicatorBar(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	public IndicatorBar(Context context) {
		this(context, null);
	}
	public IndicatorBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		TypedArray ta = context.obtainStyledAttributes(attrs,
				R.styleable.IndicatorBar);
		mVisibleCount = ta.getInt(R.styleable.IndicatorBar_visible_count,
				DEFAULT_VISIBLE_COUNT);
		mBackgroundColor = ta.getColor(R.styleable.IndicatorBar_background_color,DEAFAULT_BACKGROUND_COLOR);
		mIndicatorColor = ta.getColor(R.styleable.IndicatorBar_indicator_color,
				DEFAULT_INDICATOR_COLOR);
		mTextColor = ta.getColor(R.styleable.IndicatorBar_text_color,
				DEFAULT_INDICATOR_COLOR);
		mTextSelectColor = ta.getColor(
				R.styleable.IndicatorBar_text_select_color,
				DEFAULT_SELECT_TEXT_COLOR);
		mTextSize=ta.getDimension(R.styleable.IndicatorBar_text_size,DEFAULT_TEXT_SIZE);
		mIndiHeight = ta.getDimension(R.styleable.IndicatorBar_indicator_height, mIndiHeight);
		ta.recycle();
		init();
	}
	private void init() {
		mPaint = new Paint();
		this.setBackgroundColor(mBackgroundColor);
		this.setOrientation(LinearLayout.HORIZONTAL);
	}
	public void setViewPager(ViewPager viewPager) {
		this.mViewPager = viewPager;
		mViewPager.addOnPageChangeListener(this);
		changeSelectTextColor(0);
	}
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.save();
		canvas.translate(0, getHeight());
		mPaint.setColor(mIndicatorColor);
		canvas.drawRect(mInitPosition, -mIndiHeight, mInitPosition + getScreenWidth() / mVisibleCount, 0, mPaint);
		canvas.restore();
	}
	public void setTitles(List<String> titles) {
		if (titles != null && titles.size() > 0) {
			for (int i = 0; i < titles.size(); i++) {
				TextView tv = new TextView(getContext());
				tv.setTextColor(DEFAULT_TEXT_COLOR);
				tv.setTextSize(mTextSize);
				tv.setText(titles.get(i));
				tv.setGravity(Gravity.CENTER);
				LayoutParams lp = new LayoutParams(getScreenWidth() / mVisibleCount, LayoutParams.MATCH_PARENT);
				tv.setLayoutParams(lp);
				final int j = i;
				tv.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						mViewPager.setCurrentItem(j);
					}
				});
				this.addView(tv);
			}
		}
	}
	public int getScreenWidth() {
		DisplayMetrics dm = new DisplayMetrics();
		((Activity) getContext()).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		return dm.widthPixels;
	}
	@Override
	public void onPageScrolled(int position, float positionOffset,
			int positionOffsetPixels) {
		mInitPosition = (int) ((getScreenWidth() / mVisibleCount) * (position + positionOffset));
		invalidate();
		if (position >= mVisibleCount - 2 && positionOffset > 0
				&& position < (getChildCount() - 2)) {
			scrollTo(
					(int) ((position - mVisibleCount + 2 + positionOffset) * (getScreenWidth() / mVisibleCount)),
					0);
		}
	}
	@Override
	public void onPageSelected(int position) {
		changeSelectTextColor(position);
	}
	@Override
	public void onPageScrollStateChanged(int state) {

	}
	private void changeSelectTextColor(int position) {
		for (int i = 0; i < getChildCount(); i++) {
			TextView tv = (TextView) getChildAt(i);
			int color = mTextColor;
			if (i == position) {
				color = mTextSelectColor;
			}
			tv.setTextColor(color);
		}
	}
}
