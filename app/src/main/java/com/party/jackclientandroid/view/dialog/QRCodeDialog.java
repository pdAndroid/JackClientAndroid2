package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.R;

/**
 * <p>
 * new DeletePictureDialog.Builder(mActivity)
 * .show();
 * </p>
 * <p>
 * 使用系统兼容性的material dialog 显示
 * </p>
 * Created by Taxngb on 2018/1/3.
 */

public class QRCodeDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private ImageView qr_code_iv;

    protected QRCodeDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected QRCodeDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_q_r_code, null);
        qr_code_iv = (ImageView)mView.findViewById(R.id.qr_code_iv);
        setView(mView);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private Bitmap bitmap;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setBitmap(Bitmap bitmap1) {
            bitmap = bitmap1;
            return this;
        }

        public QRCodeDialog create() {
            final QRCodeDialog dialog = new QRCodeDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setBitmap(bitmap);
            return dialog;
        }
    }
    public void setBitmap(Bitmap bitmap){
        qr_code_iv.setImageBitmap(bitmap);
    }
}
