package com.party.jackclientandroid.view;
import android.view.View;
import android.view.ViewGroup;

import com.party.jackclientandroid.R;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.zyyoona7.popup.BasePopup;

/**
 * Created by Administrator on 2018/8/15.
 */

public class ShareBottomPopup extends BasePopup<ShareBottomPopup> {

    public static ShareBottomPopup create(){
        return new ShareBottomPopup();
    }

    @Override
    protected void initAttributes() {
        setContentView(R.layout.layout_share_comment_bottom);
        setHeight(DensityUtil.dp2px(200));
        setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        setFocusAndOutsideEnable(true);
    }

    @Override
    protected void initViews(View view, ShareBottomPopup shareBottomPopup) {

    }
}
