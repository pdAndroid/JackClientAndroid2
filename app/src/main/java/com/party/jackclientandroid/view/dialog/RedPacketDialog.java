package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.RedPacketBean;

public class RedPacketDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private View mView;
    private FrameLayout closeRedBag, openRedBag;
    private View openRedBtn, toUseBtn;
    private View.OnClickListener openListener, toUserListener;
    private FrameAnimation frameAnimation;

    protected RedPacketDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected RedPacketDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.layout_red_packet_dialog, null);
        setView(mView);

        openRedBag = mView.findViewById(R.id.open_redbag);
        closeRedBag = mView.findViewById(R.id.close_redbag);

        openRedBtn = mView.findViewById(R.id.red_bag_iv);
        toUseBtn = mView.findViewById(R.id.to_user_btn);

        openRedBtn.setOnClickListener(this);
        toUseBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.red_bag_iv:
                closeRedBag.setVisibility(View.GONE);
                openRedBag.setVisibility(View.VISIBLE);
                break;
            case R.id.to_user_btn:
                v.setTag(this);
                toUserListener.onClick(v);
                break;
        }
    }


    private int[] getRes(Context context) {
        TypedArray typedArray = context.getResources().obtainTypedArray(R.array.bg_grab_redpack_imgs);
        int len = typedArray.length();
        int[] resId = new int[len];
        for (int i = 0; i < len; i++) {
            resId[i] = typedArray.getResourceId(i, -1);
        }
        typedArray.recycle();
        return resId;
    }


    public void setRedBagOpenListener(View.OnClickListener listener) {
        openListener = listener;
    }

    public void setRedBagToUserListener(View.OnClickListener listener) {
        toUserListener = listener;
    }

    public static class Builder {
        private Context mContext;
        private View.OnClickListener openListener, toUserListener;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setOpenListener(View.OnClickListener openListener) {
            this.openListener = openListener;
            return this;
        }

        public Builder setToUserListener(View.OnClickListener toUserListener) {
            this.toUserListener = toUserListener;
            return this;
        }

        public RedPacketDialog create() {
            final RedPacketDialog dialog = new RedPacketDialog(mContext);
            dialog.setRedBagOpenListener(openListener);
            dialog.setRedBagToUserListener(toUserListener);
            return dialog;
        }
    }
}
