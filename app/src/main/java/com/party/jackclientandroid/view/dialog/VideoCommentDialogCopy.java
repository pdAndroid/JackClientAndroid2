package com.party.jackclientandroid.view.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.bean.VideoCommentBean;
import com.party.jackclientandroid.controller.AddVideoCommentListener;
import com.party.jackclientandroid.utils.DateUtils;
import com.party.jackclientandroid.utils.ScreenUtils;
import com.party.jackclientandroid.utils.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

public class VideoCommentDialogCopy extends BottomSheetDialog {
    private VideoCommentDialogCopy sheetDialog;
    private CommonAdapter<VideoCommentBean> commonAdapter;
    private List<VideoCommentBean> dataList = new ArrayList<>();
    private EditText commentEt;

    public VideoCommentDialogCopy(@NonNull Context context) {
//        super(context, R.style.CommentDialogStyle);
//        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super(context);
        sheetDialog = this;
    }

    public void showCommentList(final Activity activity, List<VideoCommentBean> dataList, final AddVideoCommentListener listener) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        commonAdapter = new CommonAdapter<VideoCommentBean>(activity, R.layout.item_comment_list, this.dataList) {
            @Override
            protected void convert(ViewHolder holder, VideoCommentBean item, int position) {
                ImageView imageView = holder.getView(R.id.civ);
                ((MApplication) activity.getApplication()).getImageLoaderFactory().loadCommonImgByUrl(activity, item.getCommentIcon(), imageView);
                holder.setText(R.id.tv_name, TextUtils.isEmpty(item.getCommentNickname()) ? "用户昵称不见了" : item.getCommentNickname());
                holder.setText(R.id.tv_time, DateUtils.formatDate2(item.getCreate()));
                holder.setText(R.id.tv_content, TextUtils.isEmpty(item.getContent()) ? "用户评论走丢了" : item.getContent());
            }
        };
        View rootView = activity.getLayoutInflater().inflate(R.layout.layout_comment_list_dialog, null, false);
        setContentView(rootView);
        commentEt = rootView.findViewById(R.id.et_comment);
        TextView addTv = rootView.findViewById(R.id.tv_add);
        addTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = commentEt.getText().toString().trim();
                if (TextUtils.isEmpty(str)) {
                    ToastUtils.t(activity.getApplication(), "请输入评论");
                    return;
                }
                if (listener != null) {
                    listener.addComment(str, sheetDialog);
                }
            }
        });
        SmartRefreshLayout refreshLayout = rootView.findViewById(R.id.refresh_layout);
        refreshLayout.setEnableRefresh(false);
        RecyclerView recyclerView = refreshLayout.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setAdapter(commonAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(activity, LinearLayoutManager.VERTICAL));
        refreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishLoadMore();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                refreshLayout.finishRefresh();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int screenHeight = ScreenUtils.getScreenHeight(getContext());
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.gravity = Gravity.BOTTOM;
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = screenHeight / 2;
        getWindow().setAttributes(lp);
    }

    public void showCommentList(VideoCommentBean vcb) {
        commentEt.setText("");
        dataList.add(0, vcb);
        commonAdapter.notifyDataSetChanged();
    }
}
