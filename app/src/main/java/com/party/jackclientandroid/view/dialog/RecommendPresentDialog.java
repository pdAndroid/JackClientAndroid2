package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.RecommendDetailListBean;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_mine.RecommendPresentActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.ConstUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/10/19.
 */

public class RecommendPresentDialog extends AlertDialog {

    private Context mContext;
    private View mView;
    private RecyclerView recycler_view;
    private SmartRefreshLayout refresh_layout;
    private CommonAdapter<RecommendDetailListBean> commonAdapter;
    private List<RecommendDetailListBean> dataList = new ArrayList<>();
    private int pageNum = 1;
    private int pageSize = ConstUtils.PAGE_SIZE;
    private UserService userService;
    private int recommend_state;//（4 合伙人红利 9 邀请收益 10充值奖励）
    private TextView showError;

    protected RecommendPresentDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected RecommendPresentDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_recommend_present, null);
        View viewById = mView.findViewById(R.id.cancel_btn);
        viewById.setOnClickListener((View v) -> {
            dismiss();
        });
        userService = new UserService(((RecommendPresentActivity) mContext));
        initView();
        setView(mView);
        showError = mView.findViewById(R.id.show_error);
    }

    public void initView() {
        recycler_view = mView.findViewById(R.id.recycler_view);
        refresh_layout = mView.findViewById(R.id.refresh_layout);
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<RecommendDetailListBean>(mContext, R.layout.item_recommend_detail, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final RecommendDetailListBean item, int position) {
                viewHolder.setText(R.id.name_et, item.getRemarks());
                viewHolder.setText(R.id.money_tv, "+" + item.getValue());
                viewHolder.setText(R.id.time_tv, CommonUtil.formatTime(item.getCreate()));
                viewHolder.setText(R.id.type_tv, item.getTradeRecordStateName());
            }
        };
        recycler_view.setLayoutManager(new LinearLayoutManager(mContext));
        recycler_view.setAdapter(commonAdapter);
        refresh_layout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                pageNum++;
                loadData();
                refresh_layout.finishLoadMore();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                pageNum = 1;
                loadData();
                refresh_layout.finishRefresh();
            }
        });
    }


    public void loadData() {
        ((RecommendPresentActivity) mContext).addDisposableIoMain(userService.getListUserTradeRecordByState(recommend_state, pageNum, pageSize), new DefaultConsumer<List<RecommendDetailListBean>>(((RecommendPresentActivity) mContext).getmApplication()) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                showError.setText("出错啦");
                showError.setVisibility(View.VISIBLE);
            }

            @Override
            public void operateSuccess(BaseResult<List<RecommendDetailListBean>> result) {
                if (result.getData().size() < ConstUtils.PAGE_SIZE) {
                    refresh_layout.setEnableLoadMore(false);
                }
                if (result.getData().isEmpty()) {
                    showError.setText("暂无数据");
                    showError.setVisibility(View.VISIBLE);
                } else {
                    showError.setVisibility(View.GONE);
                }
                if (pageNum == 1) dataList.clear();
                dataList.addAll(result.getData());
                commonAdapter.notifyDataSetChanged();
            }
        });
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private int recommendState;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public RecommendPresentDialog.Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public RecommendPresentDialog.Builder setRecommendState(int recommend_state) {
            recommendState = recommend_state;
            return this;
        }

        public RecommendPresentDialog create() {
            final RecommendPresentDialog dialog = new RecommendPresentDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setRecommend_state(recommendState);
            dialog.loadData();
            return dialog;
        }
    }

    public int getRecommend_state() {
        return recommend_state;
    }

    public void setRecommend_state(int recommend_state) {
        this.recommend_state = recommend_state;
    }
}
