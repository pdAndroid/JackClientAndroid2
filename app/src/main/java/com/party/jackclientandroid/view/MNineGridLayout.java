package com.party.jackclientandroid.view;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.party.jackclientandroid.event.OnClickImageEvent;
import com.scwang.smartrefresh.layout.util.DensityUtil;
import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.ImageBean;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by tangxuebing on 2018/5/15.
 */

public class MNineGridLayout extends NineGridLayout {

    public MNineGridLayout(Context context) {
        super(context);
    }

    public MNineGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected boolean displayOneImage(RatioImageView imageView, String url, int parentWidth) {
        return true;
    }


    @Override
    protected void displayImage(RatioImageView imageView, String url) {
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        MApplication mApplication = (MApplication) mContext.getApplicationContext();
        Activity activity = (Activity) mContext;
        //添加主题色边框
        imageView.setPadding(DensityUtil.dp2px(1), DensityUtil.dp2px(1), DensityUtil.dp2px(1), DensityUtil.dp2px(1));
        if (TextUtils.isEmpty(url)) {
//            imageView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.color_f2f2f2));
            imageView.setBackgroundResource(R.drawable.shape_of_view_stroke2_white);
            mApplication.getImageLoaderFactory().loadCommonImgByUrl(activity, R.mipmap.bg_plus_gray, imageView);
        } else {
//            imageView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
            imageView.setBackgroundResource(R.drawable.shape_of_view_stroke2_transparent);
            mApplication.getImageLoaderFactory().loadCommonImgByUrl(activity, url, imageView);
        }
    }

    OnClickImageEvent mListener;

    public void setOnClickItem(OnClickImageEvent listener) {
        mListener = listener;
    }

    @Override
    protected void onClickImage(int position, String url, ArrayList<ImageBean> urlList) {
        mListener.onClick(isAdded, itemPosition, position, url, urlList);
    }
}
