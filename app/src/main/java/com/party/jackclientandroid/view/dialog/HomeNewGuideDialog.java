package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;

import com.party.jackclientandroid.R;

public class HomeNewGuideDialog extends AlertDialog implements View.OnClickListener {
    private Context mContext;
    private View mView;
    private View.OnClickListener aliAuthListener;
    private View closeBtn, aliAuthBtn;


    protected HomeNewGuideDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected HomeNewGuideDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.home_new_guide_dialog, null);
        setView(mView);

        closeBtn = mView.findViewById(R.id.close_home_new);
        aliAuthBtn = mView.findViewById(R.id.ali_auth);


        closeBtn.setOnClickListener(this);
        aliAuthBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        v.setTag(this);
        switch (v.getId()) {
            case R.id.close_home_new:
                this.dismiss();
                break;
            case R.id.ali_auth:
                aliAuthListener.onClick(v);
                break;
        }
    }

    public void setAliAuthListener(View.OnClickListener listener) {
        aliAuthListener = listener;
    }

    public static class Builder {
        private Context mContext;
        private View.OnClickListener aliAuth, idAuth;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setAliAuth(View.OnClickListener aliAuth) {
            this.aliAuth = aliAuth;
            return this;
        }

        public Builder setIdAuth(View.OnClickListener idAuth) {
            this.idAuth = idAuth;
            return this;
        }

        public HomeNewGuideDialog create() {
            final HomeNewGuideDialog dialog = new HomeNewGuideDialog(mContext);
            dialog.setAliAuthListener(aliAuth);
            return dialog;
        }
    }
}
