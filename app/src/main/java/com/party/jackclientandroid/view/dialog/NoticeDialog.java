package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.Notice;

import butterknife.BindView;

/**
 * Created by 南宫灬绝痕 on 2019/1/9.
 */

public class NoticeDialog extends AlertDialog implements View.OnClickListener {
    TextView ivHaClose;
    TextView notice_tv;
    ImageView ivHaImg;
    private BaseActivity mContext;
    private View mView;
    private Notice notice;

    protected NoticeDialog(@NonNull BaseActivity context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected NoticeDialog(@NonNull BaseActivity context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    View.OnClickListener listener;

    public void setOnClickListener(View.OnClickListener listener) {
        this.listener = listener;
    }

    private void init(final BaseActivity context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_homepage_announcement, null);

        ivHaClose = mView.findViewById(R.id.iv_ha_close);
        ivHaImg = mView.findViewById(R.id.iv_ha_img);
        notice_tv = mView.findViewById(R.id.notice_tv);
        setView(mView);
        ivHaClose.setOnClickListener(this);
        ivHaImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_ha_close:
                dismiss();
                break;
            case R.id.iv_ha_img:
                dismiss();
                break;
        }
    }

    public ImageView getImg() {
        return ivHaImg;
    }

    public void setNotice(Notice notice) {

        mContext.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(mContext, notice.getImg(), ivHaImg);
        notice_tv.setText(notice.getContent());
        this.notice = notice;
    }

    public static class Builder {
        private BaseActivity mContext;
        private boolean mCancelable;
        private Notice notice;
        private View.OnClickListener listener;

        public Builder(@NonNull BaseActivity context) {
            mContext = context;
        }

        public NoticeDialog.Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public NoticeDialog.Builder setOnClickListener(View.OnClickListener listener) {
            this.listener = listener;
            return this;
        }

        public NoticeDialog create() {
            final NoticeDialog dialog = new NoticeDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setOnClickListener(listener);
            dialog.setNotice(notice);
            return dialog;
        }

        public NoticeDialog.Builder setNotice(Notice notice) {
            this.notice = notice;
            return this;
        }
    }

}
