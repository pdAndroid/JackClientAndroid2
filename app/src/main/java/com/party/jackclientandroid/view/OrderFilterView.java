package com.party.jackclientandroid.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.OrderCondition;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 派对 on 2018/12/2.
 */

public class OrderFilterView extends LinearLayout {

    private TextView textView;
    private TextView flagImg;
    private OrderCondition data;
    private int chooseValueIndex;

    public OrderFilterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        View inflate = LayoutInflater.from(context).inflate(R.layout.order_filter_layout, null);
        textView = inflate.findViewById(R.id.tab_text_1);
        flagImg = inflate.findViewById(R.id.flag_1);
        addView(inflate);
    }

    public TextView getTextView() {
        return textView;
    }

    public TextView getFlagImg() {
        return flagImg;
    }

    public void setData(OrderCondition data) {
        this.data = data;
        textView.setText(data.getTitle());
        flagImg.setVisibility(data.getValue().contains(",") ? View.VISIBLE : View.INVISIBLE);
    }

    public void clean() {
        chooseValueIndex = 0;
        textView.setTextColor(Color.BLACK);
        flagImg.setBackgroundResource(R.mipmap.order_def);
    }

    public String select() {
        boolean contains = data.getValue().contains(",");
        if (contains) {
            if (chooseValueIndex == 0) {
                flagImg.setBackgroundResource(R.mipmap.order_down);
            } else {
                flagImg.setBackgroundResource(R.mipmap.order_up);
            }
        }
        textView.setTextColor(getResources().getColor(R.color.colorMain));
        return null;

    }

    public JSONObject getSearchKey() {
        String searchValue = null;
        boolean contains = data.getValue().contains(",");

        if (contains) {
            String[] split = data.getValue().split(",");
            if (split.length == 2) {
                if (chooseValueIndex == 0) {
                    chooseValueIndex = 1;
                    searchValue = split[0];
                } else {
                    chooseValueIndex = 0;
                    searchValue = split[1];
                }
            }
        } else {
            chooseValueIndex = 0;
            searchValue = data.getValue();
        }

        JSONObject leftJsonObject = new JSONObject();
        try {
            leftJsonObject.put("key", data.getKey());
            leftJsonObject.put("type", data.getType());
            leftJsonObject.put("value", searchValue);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return leftJsonObject;
    }
}
