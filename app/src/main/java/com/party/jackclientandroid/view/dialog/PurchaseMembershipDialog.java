package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.ui_mine.PurchaseMembershipActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * Created by 南宫灬绝痕 on 2019/1/10.
 */

public class PurchaseMembershipDialog extends AlertDialog implements View.OnClickListener {
    @BindView(R.id.et_pm_num)
    EditText etPmNum;
    @BindView(R.id.tv_pm_cancel)
    TextView tvPmCancel;
    @BindView(R.id.tv_pm_confirm)
    TextView tvPmConfirm;

    private Context mContext;
    private View mView;
    private String phoneNumber;
    private MApplication mApplication;
    private SettingListener mSListener = null;

    protected PurchaseMembershipDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
    }

    protected PurchaseMembershipDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    public interface SettingListener {
        public void onSetting(String parentPhone);
    }

    public void setOnSettingListener(SettingListener listener) {
        mSListener = listener;
    }


    private void init(final Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_purchase_membership, null);
        tvPmCancel = mView.findViewById(R.id.tv_pm_cancel);
        tvPmConfirm = mView.findViewById(R.id.tv_pm_confirm);

        setView(mView);
        tvPmCancel.setOnClickListener(this);
        tvPmConfirm.setOnClickListener(this);
        etPmNum = mView.findViewById(R.id.et_pm_num);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_pm_cancel:
                dismiss();
                break;
            case R.id.tv_pm_confirm:
                Toast.makeText(mContext, "修改成功", Toast.LENGTH_SHORT).show();
                phoneNumber = etPmNum.getText().toString();
                if (mSListener != null) {
                    mSListener.onSetting(phoneNumber);
                }
                dismiss();
                break;
        }
    }


    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private SettingListener listener;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setListener(SettingListener listener) {
            this.listener = listener;
            return this;
        }

        public PurchaseMembershipDialog create() {
            final PurchaseMembershipDialog dialog = new PurchaseMembershipDialog(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setOnSettingListener(listener);
            return dialog;
        }
    }
}
