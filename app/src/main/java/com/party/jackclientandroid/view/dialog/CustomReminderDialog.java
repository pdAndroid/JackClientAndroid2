package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.R;

/**
 * Created by fank on 2018/7/26.
 * 提示公共弹框
 */
public class CustomReminderDialog extends AlertDialog {
    private Context mContext;
    private View mView;
    private TextView titleTV;
    private TextView contentTV;
    private TextView mCancelTV;
    private TextView mOkTV;
    private CustomReminderDialog.OkListener mOkListener;

    protected CustomReminderDialog(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle);
    }

    protected CustomReminderDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_of_custom_reminder, null);
        setView(mView);
        titleTV = mView.findViewById(R.id.tv_custom_reminder_title);
        contentTV = mView.findViewById(R.id.tv_custom_reminder_content);
        mCancelTV = mView.findViewById(R.id.tv_custom_reminder_cancel);
        mOkTV = mView.findViewById(R.id.tv_custom_reminder_ok);

        mCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mOkTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (mOkListener != null) {
                    mOkListener.ok();
                }
            }
        });
    }

    public void setTitle(String mTitle) {
        if (titleTV != null && !TextUtils.isEmpty(mTitle)) {
            titleTV.setText(mTitle);
        }
    }

    public void setContent(String mContent) {
        if (contentTV != null && !TextUtils.isEmpty(mContent)) {
            contentTV.setText(mContent);
        }
    }

    public void setOkListener(OkListener okListener) {
        this.mOkListener = okListener;
    }

    public interface OkListener {
        void ok();
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private OkListener mOkListener;
        private String mTitle, mContent;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public CustomReminderDialog.Builder setTitle(String flag) {
            mTitle = flag;
            return this;
        }

        public CustomReminderDialog.Builder setContent(String flag) {
            mContent = flag;
            return this;
        }

        public CustomReminderDialog.Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public CustomReminderDialog.Builder setOkListener(CustomReminderDialog.OkListener okListener) {
            this.mOkListener = okListener;
            return this;
        }

        public CustomReminderDialog create() {
            final CustomReminderDialog dialog = new CustomReminderDialog(mContext);
            dialog.setTitle(mTitle);
            dialog.setContent(mContent);
            dialog.setCancelable(mCancelable);
            dialog.setOkListener(mOkListener);
            return dialog;
        }
    }
}
