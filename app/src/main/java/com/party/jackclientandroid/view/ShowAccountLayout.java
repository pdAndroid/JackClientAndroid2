package com.party.jackclientandroid.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.zyyoona7.popup.EasyPopup;

/**
 * Created by Administrator on 2018/8/24.
 */

public class ShowAccountLayout extends LinearLayout {

    View view;
    ImageView imageView;
    TextView changeAccount;
    View commitBtn;
    TextView textView;

    public ShowAccountLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public static EasyPopup showPop(Activity activity, View view) {
        ShowAccountLayout passwordView = new ShowAccountLayout(activity);
        EasyPopup apply = EasyPopup.create(activity)
                .setContentView(passwordView)
                .setAnimationStyle(R.style.Popupwindow)
                .setBackgroundDimEnable(true)
                .setDimValue(0.4f)
                .setDimColor(Color.GRAY)
                .setFocusAndOutsideEnable(true)
                .setWidth(ViewGroup.LayoutParams.MATCH_PARENT)
                .setHeight(ViewGroup.LayoutParams.WRAP_CONTENT)
                .apply();
        apply.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        return apply;
    }

    public ShowAccountLayout(Context context) {
        super(context);
        view = View.inflate(context, R.layout.show_account_pop, null);
        changeAccount = view.findViewById(R.id.change_account);
        imageView = view.findViewById(R.id.account_icon);
        textView = view.findViewById(R.id.account_name);
        commitBtn = view.findViewById(R.id.commit_btn);
        addView(view);
    }

    public void setOnClickCommit(View.OnClickListener listener) {
        commitBtn.setOnClickListener(listener);
    }

    public void setOnClickChangeAccount(View.OnClickListener listener) {
        changeAccount.setOnClickListener(listener);
    }



    public void setAccountImage(BaseActivity activity,String accountUrl) {
        activity.getmApplication().getImageLoaderFactory().loadCommonImgByUrl(activity, accountUrl, imageView);
    }

    public void setAccountName(String accountName) {
        textView.setText(accountName);
    }
}
