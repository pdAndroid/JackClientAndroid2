package com.party.jackclientandroid.view.dialog;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.MApplication;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.bean.RedPacketBean;
import com.party.jackclientandroid.utils.MContextUtils;

public class RedPacketDialogCopy extends AlertDialog {
    private Context mContext;
    private View mView;
    private ImageView mCiv;
    private TextView mNameTv;
    private TextView tv_money_tips;
    private TextView mTipsTv;
    private ImageView mOkTv;
    private OkListener mOkListener;
    private RedPacketBean redPacketBean;
    private ImageView packageImage;
    private Context context;
    private FrameAnimation frameAnimation;

    protected RedPacketDialogCopy(@NonNull Context context) {
        this(context, R.style.MyAlertDialogStyle2);
        this.context = context;
    }

    protected RedPacketDialogCopy(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mView = LayoutInflater.from(mContext).inflate(R.layout.layout_red_packet_dialog_copy, null);
        setView(mView);
        packageImage = mView.findViewById(R.id.fl_redpack);
        mOkTv = mView.findViewById(R.id.iv_grab);
        mCiv = mView.findViewById(R.id.civ);
        mNameTv = mView.findViewById(R.id.tv_money);
        tv_money_tips = mView.findViewById(R.id.tv_money_tips);
        mTipsTv = mView.findViewById(R.id.tv_tips);
        mOkTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOkListener != null) {
                    mOkListener.grabMoney(RedPacketDialogCopy.this, redPacketBean);
                }
            }
        });
        packageImage.setImageResource(R.mipmap.bg_redpack);
        mNameTv.setVisibility(View.INVISIBLE);
        tv_money_tips.setVisibility(View.INVISIBLE);
        mTipsTv.setVisibility(View.INVISIBLE);
    }

    /**
     * 获取需要播放的动画资源
     */
    private int[] getRes(Context context) {
        TypedArray typedArray = context.getResources().obtainTypedArray(R.array.bg_grab_redpack_imgs);
        int len = typedArray.length();
        int[] resId = new int[len];
        for (int i = 0; i < len; i++) {
            resId[i] = typedArray.getResourceId(i, -1);
        }
        typedArray.recycle();
        return resId;
    }

    public void setGrabEnable(boolean enable) {
        mOkTv.setEnabled(enable);
    }

    public void setCover(String message) {
        ((MApplication) MContextUtils.getApplication(getContext())).getImageLoaderFactory()
                .loadCommonImgByUrl(MContextUtils.scanForActivity(getContext()), message, mCiv);
    }

    public void setTitle(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        mNameTv.setText(message);
    }

    public void setTitleSize(int sp) {
        mNameTv.setTextSize(sp);
    }

    public void setMoneyTips(boolean flag) {
        tv_money_tips.setVisibility(flag ? View.VISIBLE : View.INVISIBLE);
    }

    public void setTips(boolean flag) {
        mTipsTv.setVisibility(flag ? View.VISIBLE : View.INVISIBLE);
    }

    public void setMessage(@Nullable CharSequence message) {
        if (TextUtils.isEmpty(message)) return;
        mTipsTv.setText(message);
    }


    public void setRedPacketBean(RedPacketBean redPacketBean) {
        this.redPacketBean = redPacketBean;
    }

    public void setOkListener(OkListener okListener) {
        this.mOkListener = okListener;
    }

    /**
     * https://blog.csdn.net/black_bread/article/details/62895570
     * 以Y轴为轴心旋转
     */
    public void startGrabAnim() {
        mOkTv.setEnabled(false);
        // 每50ms一帧 循环播放动画
        frameAnimation = new FrameAnimation(mOkTv, getRes(context), 50, true);
//        float centerX = mOkTv.getWidth() / 2.0f;
//        float centerY = mOkTv.getHeight() / 2.0f;
//        float centerZ = 0f;
//        Rotate3dAnimation rotate3dAnimationY = new Rotate3dAnimation(0f, 359f, centerX, centerY, centerZ, Rotate3dAnimation.ROTATE_Y_AXIS, false);
//        rotate3dAnimationY.setDuration(250);
//        rotate3dAnimationY.setInterpolator(new LinearInterpolator());
//        rotate3dAnimationY.setRepeatCount(-1);
//        mOkTv.startAnimation(rotate3dAnimationY);
    }

    public void restartGrabAnim() {
        if (frameAnimation != null) {
            frameAnimation.release();
        }
        mOkTv.setEnabled(true);
        mOkTv.setVisibility(View.VISIBLE);
        mNameTv.setVisibility(View.INVISIBLE);
        tv_money_tips.setVisibility(View.INVISIBLE);
        mTipsTv.setVisibility(View.INVISIBLE);
    }

    /**
     * 停止动画
     *
     * @param enable
     */
    public void stopGrabAnim(boolean enable) {
        if (frameAnimation != null) {
            frameAnimation.release();
        }
//        clearAnimation();
        mOkTv.setEnabled(enable);
        packageImage.setImageResource(R.mipmap.bg_redpack_open);
        mOkTv.setVisibility(View.INVISIBLE);
        mNameTv.setVisibility(View.VISIBLE);
    }

    /**
     * 清除动画
     */
    public void clearAnimation() {
        mOkTv.clearAnimation();
    }

    public interface OkListener {
        void grabMoney(RedPacketDialogCopy dialog, RedPacketBean redPacketBean);
    }

    public static class Builder {
        private Context mContext;
        private boolean mCancelable;
        private RedPacketBean redPacketBean;
        private OkListener mOkListener;
        private CharSequence message;
        private CharSequence normalMessage;

        public Builder(@NonNull Context context) {
            mContext = context;
        }

        public Builder setTitle(CharSequence message) {
            this.normalMessage = message;
            return this;
        }

        public Builder setMessage(CharSequence message) {
            this.message = message;
            return this;
        }

        public Builder setCancelable(boolean flag) {
            mCancelable = flag;
            return this;
        }

        public Builder setOkListener(OkListener okListener) {
            this.mOkListener = okListener;
            return this;
        }

        public Builder setRedPacketBean(RedPacketBean redPacketBean) {
            this.redPacketBean = redPacketBean;
            return this;
        }

        public RedPacketDialogCopy create() {
            final RedPacketDialogCopy dialog = new RedPacketDialogCopy(mContext);
            dialog.setCancelable(mCancelable);
            dialog.setCanceledOnTouchOutside(mCancelable);
            dialog.setRedPacketBean(redPacketBean);
//            dialog.setTitle(redPacketBean.getShopName());
//            dialog.setCover(redPacketBean.getFrontCover());
            dialog.setGrabEnable(redPacketBean.isCanOperate());
            dialog.setOkListener(mOkListener);
            return dialog;
        }
    }
}
