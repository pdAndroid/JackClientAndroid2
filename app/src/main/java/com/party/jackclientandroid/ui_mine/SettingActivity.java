package com.party.jackclientandroid.ui_mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.party.jackclientandroid.LoginActivity;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingActivity extends BaseActivityTitle {

    @BindView(R.id.account_secure_rl)
    RelativeLayout accountSecureRl;
    @BindView(R.id.common_rl)
    RelativeLayout commonRl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initData();
    }

    protected void initData() {
        setMiddleText("设置");
    }

    @OnClick({R.id.account_secure_rl, R.id.common_rl, R.id.exit_tv})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.account_secure_rl:
                if (!CommonUtil.isFastClick()) {
                    startActivity(new Intent(mActivity, MyMessageActivity.class));
                }
                //showToast("正在开发中。。。");
                break;
            case R.id.common_rl:
                //startActivity(new Intent(mActivity, CommonUseActivity.class));
                showToast("正在开发中。。。");
                break;
            case R.id.exit_tv:
                if (!CommonUtil.isFastClick()) {
                    DialogController.showConfirmDialog(mActivity, "您确定要退出登录？", new ConfirmDialog.DialogListener() {
                        @Override
                        public void cancel(ConfirmDialog dialog) {
                            dialog.dismiss();
                        }

                        @Override
                        public void ok(ConfirmDialog dialog) {
                            logout();
                            dialog.dismiss();
                        }
                    });
                }
                break;
        }
    }

    private void logout() {
        // 删除sp里面数据
        mApplication.cleanUser();
        // finish其他Activity
        mApplication.finishOtherActivity(this);
        // 跳转到登录界面
        startActivity(new Intent(mActivity, LoginActivity.class));
        finish();
    }
}
