package com.party.jackclientandroid.ui_mine.choose_location;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocationClient;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.UiSettings;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.MarkerOptions;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.controller.AMapController;
import com.scwang.smartrefresh.layout.util.DensityUtil;

public class DisplayPositionActivity extends BaseActivity {

    private MapView mMapView;
    private RelativeLayout whole_layout;
    private ImageView return_iv;
    private AMap mAMap;
    private TextView shop_name_tv,shop_address_tv;
    private UiSettings mUiSettings;
    private float zoom = 14;//地图缩放级别
    private Button go_there_btn;
    private AMapController aMapController;
    private AMapLocationClient locationClient = null;
    private double latitude,longitude;
    private String shopName;
    private String shopAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_position);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//禁止横竖屏转换
        Intent intent = getIntent();
        latitude = intent.getDoubleExtra("latitude",-1);
        longitude = intent.getDoubleExtra("longitude",-1);
        shopAddress = intent.getStringExtra("shop_address");
        shopName = intent.getStringExtra("shop_name");
        initView();
        initDatas(savedInstanceState);
        moveMapCamera(latitude, longitude);
        LatLng latLng = new LatLng(latitude,longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        Bitmap bitmap = ((BitmapDrawable)getResources().getDrawable(R.mipmap.display_position_icon)).getBitmap();
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(setImgSize(bitmap,DensityUtil.dp2px(40),DensityUtil.dp2px(40))));
        markerOptions.position(latLng);
        mAMap.addMarker(markerOptions);
    }

    public Bitmap setImgSize(Bitmap bm, int newWidth ,int newHeight){
        // 获得图片的宽高.
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例.
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数.
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        // 得到新的图片.
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newbm;
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        if (null != locationClient) {
            locationClient.onDestroy();
        }
    }

    private void initView() {
        return_iv = findViewById(R.id.return_iv);
        mMapView =  findViewById(R.id.map);
        go_there_btn = findViewById(R.id.go_there_btn);
        whole_layout = findViewById(R.id.whole_layout);
        shop_name_tv = findViewById(R.id.shop_name_tv);
        shop_address_tv = findViewById(R.id.shop_address_tv);
        shop_name_tv.setText(shopName);
        shop_address_tv.setText(shopAddress);
        aMapController = new AMapController(this);
        aMapController.setHelpView(mMapView);
        aMapController.setWholeLayout(whole_layout);
        aMapController.setCurrLatitude(latitude);
        aMapController.setCurrLongitude(longitude);
        go_there_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aMapController.showNavigationChoosePopupWindow();
            }
        });
        return_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initDatas(Bundle savedInstanceState) {
        mMapView.onCreate(savedInstanceState);// 此方法必须重写
        mAMap = mMapView.getMap();
        mUiSettings = mAMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//是否显示地图中放大缩小按钮
        mUiSettings.setMyLocationButtonEnabled(false); // 是否显示默认的定位按钮
        mUiSettings.setScaleControlsEnabled(true);//是否显示缩放级别
        mAMap.setMyLocationEnabled(false);// 是否可触发定位并显示定位层
    }

    /**
     * 把地图画面移动到定位地点(使用moveCamera方法没有动画效果)
     *
     * @param latitude
     * @param longitude
     */
    private void moveMapCamera(double latitude, double longitude) {
        if (null != mAMap) {
            mAMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), zoom));
        }
    }

}
