package com.party.jackclientandroid.ui_mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.BaseFragment;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.event.MainActivityCompleteEvent;
import com.party.jackclientandroid.ui_mine.my_wallet.MyWalletActivity;
import com.party.jackclientandroid.ui_mine.partner_center.VipCenterActivity;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 首页-我的
 */
public class MineFragment extends BaseFragment {
    @BindView(R.id.ll_container)
    ViewGroup ll_container;
    @BindView(R.id.attention_rl)
    RelativeLayout attentionRl;
    @BindView(R.id.wallet_rl)
    RelativeLayout walletRl;
    @BindView(R.id.user_head_icon_civ)
    CircleImageView userHeadIcon;
    @BindView(R.id.setting_iv)
    ImageView settingIv;
    @BindView(R.id.user_name_tv)
    TextView user_name_tv;
    @BindView(R.id.level_icon)
    ImageView level_icon;

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    UserService userService;
    @BindView(R.id.my_consume_rebates)
    LinearLayout myConsumeRebates;
    Unbinder unbinder;

    public static MineFragment newInstance() {
        MineFragment fragment = new MineFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onMyReStart() {
        super.onMyReStart();
        initData();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_mine;
    }

    private long refresh_time;

    @Override
    protected void initData() {
        User user = mApplication.getUser();
        userService = new UserService((BaseActivity) getActivity());
        refresh_layout.setOnRefreshListener((@NonNull RefreshLayout refreshLayout) -> {
            if (System.currentTimeMillis() - refresh_time < 3000) {//最快3秒刷新一次
                showToast("已经是最新数据了~！");
                refresh_layout.finishRefresh();
                return;
            }
            userService.getUserInfo((BaseResult<User> result) -> {
                mApplication.setUser(result.getData());
                refresh_layout.finishRefresh();
                refresh_time = System.currentTimeMillis();
                showToast("刷新成功");
                initData();
            });
        });

        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mFragment, user.getIcon(), userHeadIcon);
        user_name_tv.setText(user.getNickname());
        mApplication.post(new MainActivityCompleteEvent());
        level_icon.setImageResource(user.getRoleId() == 1 ? R.mipmap.level_1 : user.getRoleId() == 2 ? R.mipmap.level_2 : R.mipmap.level_3);
    }

    @OnClick({R.id.my_consume_rebates, R.id.setting_iv, R.id.my_coupon, R.id.my_helper, R.id.my_joint, R.id.my_recommend
            , R.id.my_vip, R.id.wallet_rl, R.id.attention_rl, R.id.user_head_icon_civ})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.my_helper:
                startActivity(HelpCenterActivity.class);
                break;
            case R.id.my_joint:
//                mActivity.startActivity(new Intent(mActivity, ShopEnterActivity.class));
                showToast("请使用商家端app进行加盟合作");
                break;
            case R.id.wallet_rl:
                startActivity(MyWalletActivity.class);
                break;
            case R.id.my_consume_rebates:
                startActivity(MyConsumerRebatesActivity.class);
                break;
            case R.id.my_coupon:
                startActivity(MyCouponPackageActivity.class);
                break;
            case R.id.setting_iv:
                startActivity(SettingActivity.class);
                break;
            case R.id.my_vip:
                startActivity(VipCenterActivity.class);
                break;
            case R.id.my_recommend:
                startActivity(RecommendPresentActivity.class);
                break;
            case R.id.attention_rl:
                startActivity(AttentionListActivity.class);
                break;
            case R.id.iv_notice:
                startActivity(NoticeActivity.class);
                break;
            case R.id.user_head_icon_civ:
                startActivity(MyMessageActivity.class);
                break;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        unbinder = ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
