package com.party.jackclientandroid.ui_mine.partner_center;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.TabContentPagerAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

public class RecommendDetailActivity extends BaseActivityTitle {

    @BindView(R.id.tab_layout)
    TabLayout tab_layout;
    @BindView(R.id.id_viewpager)
    ViewPager viewPager;

    private List<Fragment> fragmentList = new ArrayList<>();
    private Integer ONE_LEVEL = 1, TWO_LEVEL = 2;
    private List<String> titleList;
    private TabContentPagerAdapter contentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommend_detail);
        initData();
    }


    protected void initData() {
        setMiddleText("推荐详情");
        titleList = Arrays.asList("一级合伙人", "二级合伙人");
        viewPager.setOffscreenPageLimit(2);
        fragmentList.add(PartnerLevelFragment.newInstance(ONE_LEVEL));
        fragmentList.add(PartnerLevelFragment.newInstance(TWO_LEVEL));
        contentAdapter = new TabContentPagerAdapter(getSupportFragmentManager(), fragmentList, titleList);
        viewPager.setAdapter(contentAdapter);
        tab_layout.setTabMode(TabLayout.MODE_FIXED);
        tab_layout.setTabTextColors(ContextCompat.getColor(mActivity, R.color.black), ContextCompat.getColor(mActivity, R.color.yellow));
        tab_layout.setSelectedTabIndicatorColor(ContextCompat.getColor(mActivity, R.color.yellow));
        //ViewCompat.setElevation(tab_layout, 10);
        tab_layout.setupWithViewPager(viewPager);
    }
}
