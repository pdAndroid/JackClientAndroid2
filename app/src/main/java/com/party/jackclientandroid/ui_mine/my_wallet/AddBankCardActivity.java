package com.party.jackclientandroid.ui_mine.my_wallet;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.utils.CheckBank;

import butterknife.BindView;

/**
 * 添加银行卡 账户  暂时没有使用，保留
 */
public class AddBankCardActivity extends BaseActivity {
    @BindView(R.id.bank_card_num_et)
    EditText bankNoEt;
    @BindView(R.id.bank_name_et)
    TextView bankNameEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_card);
        setTitle("添加账户");
        initData();
    }


    private void initData() {

        bankNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 6)
                    bankNameEt.setText(CheckBank.getname(s.toString()));
            }
        });
    }


}
