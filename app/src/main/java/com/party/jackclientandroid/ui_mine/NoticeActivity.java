package com.party.jackclientandroid.ui_mine;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.GroupBuyCoupon;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.ConstUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.MultiItemTypeAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NoticeActivity extends BaseActivityTitle {
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    CommonAdapter<GroupBuyCoupon> commonAdapter;
    List<GroupBuyCoupon> dataList = new ArrayList<>();
    int page = 1;
    int pageSize = ConstUtils.PAGE_SIZE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice);
        initData();
        initListener();
    }

    protected void initData() {
        setMiddleText("通知");

        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<GroupBuyCoupon>(mActivity, R.layout.item_my_coupon_package, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, GroupBuyCoupon item, int position) {
                viewHolder.setText(R.id.buy_price_tv, "¥" + item.getBuyPrice());
                TextView original_price_tv = viewHolder.getView(R.id.original_price_tv);
                original_price_tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                viewHolder.setText(R.id.original_price_tv, item.getOriginalPrice() + "");
                viewHolder.setText(R.id.shop_name_tv, item.getShopName());
                // viewHolder.setText(R.id.dinner_num_person_tv, item.getDinersNumberName());
                viewHolder.setText(R.id.desc_tv, CommonUtil.getAvailableDate(item.getAvailableDate()));
                ImageView shopImageIv = viewHolder.getView(R.id.coupon_image_iv);
                Glide.with(mActivity).load(item.getGroupBuyImg()).into(shopImageIv);
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
    }

    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getNeedData();
            }
        });
        getNeedData();
    }

    /**
     * item点击事件
     *
     * @param position
     */
    private void handleItemClick(int position) {
//        Intent intent = new Intent(mActivity, OrderDetailRePayActivity.class);
//        intent.putExtra("order_id", groupBuyCouponList.get(position).getId());
//        startActivity(intent);
    }

    /**
     * 下拉获取数据
     */
    public void getNeedData() {
        page = 1;
        mRefreshLayout.finishRefresh();
//        addDisposableIoMain(couponService.getUserGroupBuyCouponList(page), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {
//            @Override
//            public void operateError(String message) {
//                super.operateError(message);
//                helper.showError("获取数据异常", "");
//            }
//
//            @Override
//            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
//                dataList.clear();
//                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
//                    dataList.addAll(baseBean.getData());
//                }
//                commonAdapter.notifyDataSetChanged();
//                mRefreshLayout.finishRefresh();
//                if (dataList.size() > 0) {
//                    helper.showContent();
//                } else {
//                    helper.showEmpty("暂无团购券", "");
//                }
//            }
//        }, new Consumer<Throwable>() {
//            @Override
//            public void accept(Throwable throwable) throws Exception {
//                helper.showEmpty();
//            }
//        });
    }

    /**
     * 加载更多
     */
    private void loadMoreData() {
        page++;
        mRefreshLayout.finishLoadMore();
//        addDisposableIoMain(couponService.getUserGroupBuyCouponList(page), new DefaultConsumer<List<GroupBuyCoupon>>(mApplication) {
//            @Override
//            public void operateSuccess(BaseResult<List<GroupBuyCoupon>> baseBean) {
//                List<GroupBuyCoupon> data = baseBean.getData();
//                boolean noMoreData = false;
//                if (data != null && data.size() > 0) {
//                    dataList.addAll(data);
//                    if (data.size() <= ConstUtils.PAGE_SIZE) {
//                        noMoreData = true;
//                    }
//                } else {
//                    noMoreData = true;
//                }
//                commonAdapter.notifyDataSetChanged();
//                // 没有更多数据了
//                if (noMoreData) {
//                    mRefreshLayout.finishLoadMoreWithNoMoreData();
//                } else {
//                    mRefreshLayout.finishLoadMore();
//                }
//            }
//        });
    }
}
