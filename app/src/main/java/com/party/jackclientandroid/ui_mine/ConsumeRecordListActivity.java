package com.party.jackclientandroid.ui_mine;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.MyAttentionService;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.ConsumeRecord;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;
import com.zhy.adapter.recyclerview.wrapper.HeaderAndFooterWrapper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ConsumeRecordListActivity extends BaseActivityTitle {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    HeaderAndFooterWrapper mHeaderAndFooterWrapper;
    List<ConsumeRecord> dataList = new ArrayList<>();
    CommonAdapter<ConsumeRecord> commonAdapter;
    int page = 1;
    int size;
    private UserService userService;
    MyAttentionService myAttentionService;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consume_record_list);
        initData();
    }

    protected void initData() {
        setMiddleText("交易记录");
        userService = new UserService(this);
        myAttentionService = new MyAttentionService(this);
        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<ConsumeRecord>(mActivity, R.layout.item_consume_record_item, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, final ConsumeRecord item, int position) {
                viewHolder.setText(R.id.consume_record_name_tv, item.getRemarks());
                viewHolder.setText(R.id.time_tv, df.format(item.getCreate()));
                if (item.getTradeRecordType() == 1) {
                    viewHolder.setText(R.id.money_tv, "-" + item.getValue());
                } else {
                    viewHolder.setText(R.id.money_tv, "+" + item.getValue());
                }
                viewHolder.setText(R.id.status_tv, item.getTradeRecordStateName());
            }
        };
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
        mHeaderAndFooterWrapper = new HeaderAndFooterWrapper(commonAdapter);
        mHeaderAndFooterWrapper.addHeaderView(getHeadView());
        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        mRecyclerView.addItemDecoration(divider);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(mHeaderAndFooterWrapper);
        helper = new LoadViewHelper(mRefreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        helper.showLoading();
        loadData();
    }

    public void loadData() {
        addDisposableIoMain(userService.getUserConsumeRecord(page, Constant.PAGE_SIZE), new DefaultConsumer<List<ConsumeRecord>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ConsumeRecord>> result) {
                page = 1;
                size = result.getData().size();
                if (size == 0) {
                    helper.showEmpty();
                } else {
                    page++;
                    dataList.clear();
                    dataList.addAll(result.getData());
                    mHeaderAndFooterWrapper.notifyDataSetChanged();
                    helper.showContent();
                }
            }
        });
    }

    private void loadMoreData() {
        addDisposableIoMain(userService.getUserConsumeRecord(page, Constant.PAGE_SIZE), new DefaultConsumer<List<ConsumeRecord>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<ConsumeRecord>> result) {
                size = result.getData().size();
                page++;
                dataList.addAll(result.getData());
                mHeaderAndFooterWrapper.notifyDataSetChanged();
                if (size < Constant.PAGE_SIZE) {
                    mRefreshLayout.finishLoadMoreWithNoMoreData();
                } else {
                    mRefreshLayout.finishLoadMore();
                }
            }
        });
    }

    public View getHeadView() {
        View view = getLayoutInflater().inflate(R.layout.layout_consume_record_head_view, null, false);
        final TextView totalPayTv = (TextView) view.findViewById(R.id.total_pay_tv);
        final TextView totalIncomeTv = (TextView) view.findViewById(R.id.total_income_tv);
        addDisposableIoMain(userService.getUserTotalIncomeAndPayList(), new DefaultConsumer<List<CommonResult>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<CommonResult>> result) {
                List<CommonResult> commonResultList = result.getData();
                for (CommonResult commonResult : commonResultList) {
                    if (commonResult.getTradeRecordType() == 1) {
                        totalPayTv.setText("¥" + commonResult.getSum() + "元");
                    } else if (commonResult.getTradeRecordType() == 0) {
                        totalIncomeTv.setText("¥" + commonResult.getSum() + "元");
                    }
                }
            }
        });
        return view;
    }
}
