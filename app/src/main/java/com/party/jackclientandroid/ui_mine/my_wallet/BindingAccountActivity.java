package com.party.jackclientandroid.ui_mine.my_wallet;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.base.MyException;
import com.party.jackclientandroid.bean.AppInfo;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.BindingInfo;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.event.AuthResultEvent;
import com.party.jackclientandroid.wxapi.WXPayUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * 微信绑定
 */

public class BindingAccountActivity extends BaseActivityTitle {

    @BindView(R.id.binding_btn)
    Button binding_btn;

    @BindView(R.id.wx_icon)
    CircleImageView wx_icon;

    @BindView(R.id.wx_name)
    TextView wx_name;

    UserService userService;

    BindingInfo mBindingInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_wx);
        EventBus.getDefault().register(this);
        initData();
    }

    protected void initData() {
        userService = new UserService(this);
        setMiddleText("微信绑定");
        getAppId();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 获取一键登录信息。
     */
    private void getAppId() {
        showLoadingDialog();
        addDisposableIoMain(userService.getAppId(), new DefaultConsumer<AppInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<AppInfo> baseBean) {
                AppInfo mAppInfo = baseBean.getData();
                try {
                    WXPayUtils build = new WXPayUtils.WXPayBuilder().build();
                    build.getUserCode(mActivity, mAppInfo.getWxAppId());
                } catch (MyException e) {
                    showToast(e.getMessage());
                }
            }
        });
    }


    /**
     * 微信授权成功,回调
     *
     * @param authResultEvent
     */
    @Subscribe(priority = 100)
    public void authFinish(AuthResultEvent authResultEvent) {
        EventBus.getDefault().cancelEventDelivery(authResultEvent);

        addDisposableIoMain(userService.getBindingUserInfo(authResultEvent.getCode()), new DefaultConsumer<BindingInfo>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<BindingInfo> baseBean) {
                hideAlertDialog();
                mBindingInfo = baseBean.getData();
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, baseBean.getData().getAvatar(), wx_icon);
                wx_name.setText(baseBean.getData().getNickname());
            }
        }, (Throwable throwable) -> {
            showToast(throwable.getMessage());
        });
    }

    @OnClick(R.id.binding_btn)
    public void bindingBtn() {
        showLoadingDialog();
        addDisposableIoMain(userService.bindingWx(mBindingInfo.getBindingKey()), new DefaultConsumer<Object>(mApplication) {

            @Override
            public void operateError(String message) {
                super.operateError(message);
                DialogController.showMustConfirmDialog(mActivity, "提示", message, (View v) -> {
                });
            }

            @Override
            public void operateSuccess(BaseResult<Object> baseBean) {
                hideAlertDialog();
                User user = mApplication.getUser();
                user.setIsBindingWx(1);
                mApplication.setUser(user);
                DialogController.showMustConfirmDialog(mActivity, "提示", baseBean.getMessage(), (View v) -> {
                    finish();
                });
            }
        });
    }


}
