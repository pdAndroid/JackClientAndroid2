package com.party.jackclientandroid.ui_mine;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.LoginActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.ui_mine.my_wallet.SetPayPasswordActivity;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyMessageActivity extends BaseActivityTitle {
    @BindView(R.id.set_pay_password_rl)
    RelativeLayout setPayPasswordRl;
    @BindView(R.id.portrait_rl)
    RelativeLayout protraitRl;
    @BindView(R.id.user_name_rl)
    RelativeLayout userNameRl;
    @BindView(R.id.phone_rl)
    RelativeLayout phoneRl;
    @BindView(R.id.user_name_tv)
    TextView user_name_tv;
    @BindView(R.id.phone_tv)
    TextView phone_tv;
    @BindView(R.id.header_image)
    CircleImageView header_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_message);
        initData();
    }

    protected void initData() {
        setMiddleText("个人信息");
        user_name_tv.setText(TextUtils.isEmpty(mApplication.getUser().getNickname()) ? "" : mApplication.getUser().getNickname());
        phone_tv.setText(TextUtils.isEmpty(mApplication.getUser().getPhone()) ? "" : mApplication.getUser().getPhone());
        if (TextUtils.isEmpty(mApplication.getUser().getIcon())) {
            mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, R.mipmap.logo, header_image);
        } else {
            mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, mApplication.getUser().getIcon(), header_image);
        }
    }

    @OnClick({R.id.set_pay_password_rl, R.id.portrait_rl, R.id.user_name_rl, R.id.phone_rl, R.id.exit_tv})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.set_pay_password_rl:
                startActivity(new Intent(this, SetPayPasswordActivity.class));
                //startActivity(new Intent(this,FirstInputPayPasswordActivity.class));
                break;
            case R.id.exit_tv:
                if (!CommonUtil.isFastClick()) {
                    DialogController.showConfirmDialog(mActivity, "您确定要退出登录？", new ConfirmDialog.DialogListener() {
                        @Override
                        public void cancel(ConfirmDialog dialog) {
                            dialog.dismiss();
                        }

                        @Override
                        public void ok(ConfirmDialog dialog) {
                            logout();
                            dialog.dismiss();
                        }
                    });
                }
                break;
        }
    }

    private void logout() {
        // 删除sp里面数据
        mApplication.cleanUser();
        // finish其他Activity
        mApplication.finishOtherActivity(this);
        // 跳转到登录界面
        startActivity(new Intent(mActivity, LoginActivity.class));
        finish();
    }
}
