package com.party.jackclientandroid.ui_mine.my_wallet;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;

/**
 * Created by 南宫灬绝痕 on 2018/12/25.
 * 常见问题
 */

public class FrequentlyAskedQuestionActivity extends BaseActivityTitle {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frequently_asked_question);
        setMiddleText("常见问题");
    }
}
