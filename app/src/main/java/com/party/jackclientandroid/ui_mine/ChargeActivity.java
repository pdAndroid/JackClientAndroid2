package com.party.jackclientandroid.ui_mine;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;

import butterknife.BindView;
import butterknife.OnClick;

public class ChargeActivity extends BaseActivityTitle {
    @BindView(R.id.et_money)
    EditText et_money;
    @BindView(R.id.rg)
    RadioGroup rg;
    int payType = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge);
        initData();
        initListener();
    }

    protected void initData() {
        setMiddleText("充值");
    }

    protected void initListener() {
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_alipay:
                        payType = 0;
                        break;
                    case R.id.rb_wxpay:
                        payType = 1;
                        break;
                }
            }
        });
    }

    @OnClick(R.id.btn_ok)
    public void clickOkBtn() {
        //showToast(et_money.getText().toString().trim());
    }
}
