package com.party.jackclientandroid.ui_mine;

import android.os.Bundle;
import android.view.View;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.controller.DialogController;

import butterknife.OnClick;

public class FeedbackActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initData();
    }

    protected void initData() {
        setMiddleText("反馈");
    }

    @OnClick(R.id.btn_commit)
    public void clickCommit() {

        DialogController.showMustConfirmDialog(this, "反馈成功", "您的反馈是对我们最大的帮助", (View v) -> {
            finish();
        });
    }
}
