package com.party.jackclientandroid.ui_mine.my_wallet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.view.dialog.ConfirmDialog;

import butterknife.BindView;
import butterknife.OnClick;

public class PaySettingActivity extends BaseActivityTitle {

    @BindView(R.id.real_text)
    TextView realText;
    @BindView(R.id.set_pass_text)
    TextView setPassText;
    @BindView(R.id.binding_wx)
    TextView binding_wx;
    @BindView(R.id.binding_zfb)
    TextView binding_zfb;
    @BindView(R.id.warn)
    TextView warnTx;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_setting);
        initData();
    }

    protected void initData() {
        setMiddleText("支付设置");
        user = mApplication.getUser();
        realText.setText(user.getAuthText());
        setPassText.setText(user.getIsPassword() == 0 ? "去设置" : "修改");
        binding_wx.setText(user.getIsBindingWx() == 0 ? "去绑定" : "修改");
        warnTx.setVisibility(user.isIDCardAuth() ? View.VISIBLE : View.INVISIBLE);
        binding_zfb.setText(user.getIsBindingAli() == 0 ? "去绑定" : "修改");
    }

    @OnClick({R.id.real_name_authentication_ll, R.id.pay_password_ll, R.id.common_problem_ll, R.id.binding_wx_ll, R.id.binding_zfb_ll})
    public void handleClickSth(View view) {
        switch (view.getId()) {
            case R.id.real_name_authentication_ll:
                if (user.isIDCardAuth()) {
                    startActivity(RealNameAuthenticationActivity.class);
                } else {
                    showToast(user.getAuthText());
                }
                break;
            case R.id.pay_password_ll:
                if (user.hasNeedAuth()) {
                    DialogController.showConfirmDialog2(mActivity, "你还需要身份认证哦！", "支付宝", "去认证", new ConfirmDialog.DialogListener() {
                        @Override
                        public void cancel(ConfirmDialog dialog) {
                            startActivity(BindingAccountZfbActivity.class);
                        }

                        @Override
                        public void ok(ConfirmDialog dialog) {
                            startActivity(RealNameAuthenticationActivity.class);
                        }
                    });
                } else {
                    startActivity(UpdatePayPasswordActivity.class);
                }
                break;
            case R.id.common_problem_ll:
                startActivity(new Intent(mActivity, CommonProblemActivity.class));
                break;
            case R.id.binding_wx_ll:
                startActivity(new Intent(mActivity, BindingAccountActivity.class));
                break;
            case R.id.binding_zfb_ll:
                startActivity(new Intent(mActivity, BindingAccountZfbActivity.class));
                break;
        }
    }
}
