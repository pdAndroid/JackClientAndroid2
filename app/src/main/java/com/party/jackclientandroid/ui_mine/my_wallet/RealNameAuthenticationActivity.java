package com.party.jackclientandroid.ui_mine.my_wallet;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.bean.VerifyUser;
import com.party.jackclientandroid.controller.ChoosePictureController;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.utils.CheckUtils;
import com.party.jackclientandroid.utils.MyNullException;
import com.party.jackclientandroid.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;

/**
 * 实名认证
 */
public class RealNameAuthenticationActivity extends BaseActivityTitle {

    @BindView(R.id.name_et)
    EditText name_et;
    @BindView(R.id.id_card_num_et)
    EditText id_card_num_et;

    private UserService userService;
    protected ChoosePictureController choosePictureController;

    private boolean isOk = false;//是否回调成功

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_name_authentication);
        initData();
    }

    @OnClick(R.id.human_auth)
    public void humanAuth() {
        startHumanAuth();
    }

    public void startHumanAuth() {
        String id_card = id_card_num_et.getText().toString();
        String name = name_et.getText().toString();
        Intent intent = new Intent();
        intent.putExtra("idCard", id_card);
        intent.putExtra("name", name);
        startActivity(HumanAuthenticationActivity.class, intent);
    }

    protected void initData() {
        Utils.setIDCardKeyListener(id_card_num_et);
        setMiddleText("实名认证");
        userService = new UserService(this);
    }

    @OnClick(R.id.auth_btn)
    public void handleClickSth(View view) {
        verifyData();
    }

    public void auth(String idCard, String name) {
        if (isOk) {
            isOk = false;
            showToast("请稍后再试");
            return;
        }
        showLoadingDialog();
        addDisposableIoMain(userService.authentication(idCard, name), new DefaultConsumer<VerifyUser>(mApplication) {
            @Override
            public void operateError(BaseResult<VerifyUser> result) {
                hideAlertDialog();
                DialogController.showConfirmDialog(mActivity, "认证提示", result.getMessage(), (View v) -> {
                    startHumanAuth();
                });
            }

            @Override
            public void operateSuccess(BaseResult<VerifyUser> result) {
                isOk = true;
                User user = mApplication.getUser();
                user.setIsAuthentication(1);
                mApplication.setUser(user);
                Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
                intent.putExtra("verKey", result.getData().getVerKey());
                startActivity(intent);
                finish();
            }
        }, (Throwable throwable) -> {
            isOk = true;
        });
    }

    public void verifyData() {
        try {
            String idCard = CheckUtils.checkIDCard(id_card_num_et, "身份证号有误");
            String name = CheckUtils.checkData(name_et, "姓名不能为空");
            auth(idCard, name);
        } catch (MyNullException e) {
            showToast(e.getMessage());
            CheckUtils.shake(mActivity, e.getView());
        }

    }
}
