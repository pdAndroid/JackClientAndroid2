package com.party.jackclientandroid.ui_mine;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.utils.PhoneUtil;

import butterknife.OnClick;

public class HelpCenterActivity extends BaseActivityTitle {
    private String phone = "02882669949";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_center);
        initData();
    }

    protected void initData() {
        setMiddleText("帮助中心");
    }

    @OnClick({R.id.suggestion_rl, R.id.client_service_rl})
    public void onClickSth(View view) {
        switch (view.getId()) {
            case R.id.suggestion_rl:
                Intent intent = new Intent(mActivity, FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.client_service_rl:
                PhoneUtil.callPhone(mActivity, "联系我们",
                        "您的反馈是对我们最大的帮助，官方电话服务时间 9:00~6:00",
                        phone);
                break;
        }
    }
}
