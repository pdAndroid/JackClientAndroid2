package com.party.jackclientandroid.ui_mine.choose_location;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.amap.api.location.AMapLocation;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.help.Inputtips;
import com.amap.api.services.help.InputtipsQuery;
import com.amap.api.services.help.Tip;
import com.amap.api.services.poisearch.PoiSearch;
import com.google.gson.Gson;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.ui_mine.choose_location.adapter.SearchResultAdapter;
import com.party.jackclientandroid.ui_mine.choose_location.utils.DatasKey;
import com.party.jackclientandroid.ui_mine.choose_location.utils.OnItemClickLisenter;
import com.party.jackclientandroid.ui_mine.choose_location.utils.SPUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * https://github.com/xiaofuchen/WeixinLocation
 * https://github.com/leifu1107/WeChatLoction/blob/master/app/src/main/java/leifu/wechatloction/SearchResultActivity.java
 */
public class SearchResultActivity extends AppCompatActivity {

    private ImageView mIvBack;
    private EditText mEtSearch;
    private RecyclerView mRecyclerView;
    private List<Tip> mList;
    private SearchResultAdapter mSearchAddressAdapter;

    private PoiSearch mPoiSearch;
    private View.OnClickListener mOnClickListener;
    private OnItemClickLisenter mOnItemClickLisenter;

    private Gson gson;
    public AMapLocation location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//禁止横竖屏转换
        initView();
        initDatas();
        initListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null == location) {
            String s = SPUtils.getString(this, DatasKey.LOCATION_INFO);//获取保存的定位信息
            if (!TextUtils.isEmpty(s)) {
                try {
                    location = gson.fromJson(s, AMapLocation.class);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (null != mPoiSearch) {
            mPoiSearch = null;
        }
    }

    private void initView() {
        mIvBack = (ImageView) findViewById(R.id.iv_back);
        mEtSearch = (EditText) findViewById(R.id.et_search);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);
    }

    private void initDatas() {
        mList = new ArrayList<>();
        mSearchAddressAdapter = new SearchResultAdapter(this, mList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mSearchAddressAdapter);

        gson = new Gson();
    }

    private void initListener() {

        mOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()) {
                    case R.id.iv_back:
                        finish();
                        break;
                }
            }
        };
        mIvBack.setOnClickListener(mOnClickListener);

        mEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (null != editable) {
                    if (0 == editable.length()) {//没有输入则清空搜索记录
                        mList.clear();
                        mSearchAddressAdapter.setList(mList, "");
                    } else {
                        if (null != location) {
                            doSearchQuery(editable.toString(), location.getCity(), new LatLonPoint(location.getLatitude(), location.getLongitude()));
                        } else {
                            doSearchQuery(editable.toString(), "", null);
                        }
                    }
                }
            }
        });

        mOnItemClickLisenter = new OnItemClickLisenter() {
            @Override
            public void onItemClick(int position) {
//                PoiItem poiItem = mList.get(position);
//                if (null != poiItem) {//获取信息并回传上一页面
//                    Intent intent = new Intent();
//                    intent.putExtra(DatasKey.SEARCH_INFO, poiItem);
//                    setResult(RESULT_OK, intent);
//                    finish();
//                }
            }
        };
        mSearchAddressAdapter.setOnItemClickLisenter(mOnItemClickLisenter);
    }

    /**
     * 开始进行poi搜索
     */
    protected void doSearchQuery(String keyWord, String city, LatLonPoint lpTemp) {
        InputtipsQuery inputquery = new InputtipsQuery(keyWord, "");
        inputquery.setCityLimit(true);//限制在当前城市
        Inputtips inputTips = new Inputtips(SearchResultActivity.this, inputquery);
        inputTips.requestInputtipsAsyn();
        inputTips.setInputtipsListener(new Inputtips.InputtipsListener() {
            @Override
            public void onGetInputtips(List<Tip> list, int i) {
                if (null != mList) {
                    mList.clear();
                }
                mList.addAll(list);// 取得第一页的poiitem数据，页数从数字0开始
                if (null != mSearchAddressAdapter) {
                    mSearchAddressAdapter.setList(mList, mEtSearch.getText().toString().trim());
                    mRecyclerView.smoothScrollToPosition(0);
                }
            }
        });
    }

}
