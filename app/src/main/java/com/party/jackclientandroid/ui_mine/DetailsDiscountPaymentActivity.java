package com.party.jackclientandroid.ui_mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.DetailsDiscountPaymentBean;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 南宫灬绝痕 on 2018/12/19.
 * 我的-优惠返利-优惠金明细
 */

public class DetailsDiscountPaymentActivity extends BaseActivityTitle {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;
    private CouponService couponService;
    private List<DetailsDiscountPaymentBean> data;
    private MyCommonAdapter<DetailsDiscountPaymentBean> commonAdapter;
    private int page;
    private String shopId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_discount_payment);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        setMiddleText("返利金明细");
        page = 1;
        Intent intent = getIntent();
        shopId = intent.getStringExtra("shopId");
        data = new ArrayList<>();
        couponService = new CouponService(this);
        refreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new MyCommonAdapter<DetailsDiscountPaymentBean>(mActivity, R.layout.item_details_discount_payment, data) {
            @Override
            protected void convert(ViewHolder holder, DetailsDiscountPaymentBean detailsDiscountPaymentBean, int position) {
                detailsDiscountPaymentBean.getTradeType();
                if (detailsDiscountPaymentBean.getTradeType().equals("0")) {
                    holder.setTextColor(R.id.tv_details_discount_payment_value, getResources().getColor(R.color.red));
                    holder.setText(R.id.tv_details_discount_payment_value, "+" + detailsDiscountPaymentBean.getValue() + "");
                }
                holder.setText(R.id.tv_details_discount_payment_tradename, detailsDiscountPaymentBean.getTradeName());
                holder.setText(R.id.tv_details_discount_payment_create, detailsDiscountPaymentBean.getCreate() + "");
                holder.setText(R.id.tv_details_discount_payment_value, "-" + detailsDiscountPaymentBean.getValue() + "");
            }
        };


        refreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });
        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recyclerView.addItemDecoration(divider);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(commonAdapter);

        helper = new LoadViewHelper(refreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        helper.showLoading();
    }


    private void requestData() {
        addDisposableIoMain(couponService.listUserRebateRecord(shopId), new DefaultConsumer<List<DetailsDiscountPaymentBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<DetailsDiscountPaymentBean>> baseBean) {
                commonAdapter.clear(page);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(page, page, baseBean.getData().size(), refreshLayout);
                helper.check(commonAdapter.getDatas());
            }
        });
    }

    public void loadData() {
        page = 1;
        requestData();
    }

    public void loadMoreData() {
        page++;
        requestData();
    }
}
