package com.party.jackclientandroid.ui_mine;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.HomeService;
import com.party.jackclientandroid.api.VipService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.bean.PayResult;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.bean.VipBean;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.controller.PayController;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.party.jackclientandroid.view.dialog.PurchaseMembershipDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by 南宫灬绝痕 on 2019/1/9.
 * 合伙人中心-购买会员
 */

public class PurchaseMembershipActivity extends BaseActivity {


    @BindView(R.id.iv_pm_return)
    ImageView ivPmReturn;
    @BindView(R.id.tv_pm_parent_phone)
    TextView tvPmParentPhone;
    @BindView(R.id.btn_pm_modification)
    Button btnPmModification;
    @BindView(R.id.wv_purchase_membership)
    WebView wvPurchaseMembership;
    @BindView(R.id.tv_pm_current_price)
    TextView tvPmCurrentPrice;
    @BindView(R.id.tv_pm_original_price)
    TextView tvPmOriginalPrice;
    @BindView(R.id.btn_pm_start)
    Button btnPmStart;
    @BindView(R.id.iv_pm_card)
    ImageView ivPmCard;
    @BindView(R.id.tv_pm_div)
    TextView tvPmDiv;

    private String id;
    private String name;
    private String currentPrice;
    private String originalPrice;
    private String parentPhone;
    private String setBuyPrice;
    private String orderId;
    private String role;
    private int currChooseRolePosition;
    private int roleId;

    private PayController payController;
    private CouponService couponService;
    private Context context;
    private VipService vipService;
    private List<VipBean.ResponsesBean> data;
    private PurchaseMembershipDialog dialog = null;
    private static final String TAG = "PartnerCenterActivity";

    private String level_1 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/bronzeVip.png";
    private String level_2 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/notGold.png";
    private String level_3 = "https://pdkj.oss-cn-beijing.aliyuncs.com/background-img/notDiamonde.png";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_membership);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        Intent intent = getIntent();
        context = getApplicationContext();
        data = new ArrayList<>();
        vipService = new VipService(this);
        couponService = new CouponService(this);
        payController = new PayController(this, btnPmStart);
        id = intent.getStringExtra("id");
        name = intent.getStringExtra("name");
        currentPrice = intent.getStringExtra("currentPrice");
        originalPrice = intent.getStringExtra("originalPrice");
        parentPhone = intent.getStringExtra("parentPhone");
        setBuyPrice = intent.getStringExtra("setBuyPrice");
        role = intent.getStringExtra("role");
        currChooseRolePosition = intent.getIntExtra("currChooseRolePosition", 0);
        tvPmParentPhone.setText(parentPhone);
        tvPmDiv.setText(Html.fromHtml(role));
        tvPmCurrentPrice.setText("¥ " + currentPrice);
        tvPmOriginalPrice.setText("¥ " + originalPrice);
        tvPmOriginalPrice.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线

        switch (currChooseRolePosition) {
            case 0: //青铜会员
                Glide.with(context).load(level_1).into(ivPmCard);
                break;
            case 1://黄金会员
                Glide.with(context).load(level_2).into(ivPmCard);
                break;
            case 2://钻石会员
                Glide.with(context).load(level_3).into(ivPmCard);
                break;
        }
    }


    private void requestData() {
        addDisposableIoMain(vipService.getVipData(), new DefaultConsumer<VipBean>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<VipBean> baseBean) {
                data.clear();
                parentPhone = baseBean.getData().getParentPhone();
                data.addAll(baseBean.getData().getResponses());
//                checkBtn(currChooseRolePosition, data);

            }
        });
    }

    public void submitOrder() {
        payController.showPayMethod(setBuyPrice + "", orderId, (View v) -> {
            roleId = data.get(currChooseRolePosition).getId();
            Map<String, String> map = new HashMap();
            map.put("finalPrice", setBuyPrice + "");
            map.put("totalPrice", originalPrice + "");
            map.put("shopId", "666666");
            map.put("quantity", "1");// 购买数量为 1
            map.put("itemId", roleId + "");//角色id
            map.put("type", "4");//购买会员是 类型 4
            map.put("price", setBuyPrice + "");//单价
            map.put("parentPhone", parentPhone + "");//邀请人电话
            addDisposableIoMain(couponService.submitOrder(map), new DefaultConsumer<CommonResult>(mApplication) {
                @Override
                public void operateSuccess(BaseResult<CommonResult> result) {
                    orderId = result.getData().getId() + "";
                    payController.pay(orderId);
                }
            });
        });
        payController.setInputPassFinish(new DiscountPayBillActivity.PayCompleteListener() {
            @Override
            public void payOk(BaseResult<PayResult> result) {
                changeVipStat();
            }

            @Override
            public void payFail(String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    public void changeVipStat() {
        User user = mApplication.getUser();
        user.setRoleId(roleId);
        mApplication.setUser(user);
        String roleText = roleId == 2 ? "黄金会员" : "钻石会员";
//        myPagerAdapter.notifyDataSetChanged();
        DialogController.showMustConfirmDialog(mActivity, roleText, "恭喜你成为" + roleText + "!", (View v) -> {
            finish();
        });
    }

    @OnClick({R.id.iv_pm_return, R.id.btn_pm_modification, R.id.btn_pm_start})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_pm_return:
                finish();
                break;
            case R.id.btn_pm_modification:
                DialogController.PurchaseMembershipDialog(this, (String data) -> {
                    tvPmParentPhone.setText(data);
                    parentPhone = data;
                });
                break;
            case R.id.btn_pm_start:
                submitOrder();
                break;
        }
    }
}
