package com.party.jackclientandroid.ui_mine;

import android.os.Bundle;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;

public class CommonUseActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_use);
        initData();
    }

    protected void initData() {
        setMiddleText("通用");
    }
}
