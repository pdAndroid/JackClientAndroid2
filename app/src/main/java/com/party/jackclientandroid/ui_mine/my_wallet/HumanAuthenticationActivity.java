package com.party.jackclientandroid.ui_mine.my_wallet;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.ImageBean;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.bean.VerifyUser;
import com.party.jackclientandroid.controller.AliOssService;
import com.party.jackclientandroid.controller.AliyunOSSController;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.photoselector.entity.Image;
import com.party.jackclientandroid.utils.CheckUtils;
import com.party.jackclientandroid.utils.CommonUtil;
import com.party.jackclientandroid.utils.MyNullException;
import com.party.jackclientandroid.utils.Utils;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import io.reactivex.Observable;

/**
 * 实名认证-人工实名认证
 */
public class HumanAuthenticationActivity extends BaseActivityTitle {

    @BindView(R.id.id_card_front_iv)
    ImageView id_card_front_iv;
    @BindView(R.id.id_card_behind_iv)
    ImageView id_card_behind_iv;

    @BindView(R.id.name_et)
    EditText name_et;
    @BindView(R.id.id_card_num_et)
    EditText id_card_num_et;

    private UserService userService;
    private AliOssService mOssService;
    private boolean isOk = true;//是否回调成功
    private ImageView mImageView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_human_authentication);
        initData();
    }


    protected void initData() {
        String idCard = getIntent().getStringExtra("idCard");
        String name = getIntent().getStringExtra("name");
        id_card_num_et.setText(idCard);
        name_et.setText(name);

        Utils.setIDCardKeyListener(id_card_num_et);
        setMiddleText("人工实名认证");
        userService = new UserService(this);
        mOssService = new AliOssService(this);
    }

    @OnClick({R.id.id_card_front_iv, R.id.id_card_behind_iv})
    public void onClickImage(View view) {
        mImageView = (ImageView) view;
        if (choosePictureController != null) {
            choosePictureController.clearBeforeChoosePictures();
            choosePictureController.handleToChooseCropPictures();
        }
    }

    /**
     * 接收选择照片之后的结果
     *
     * @param list
     */
    @Override
    public void updateChoosePictures(List<ImageBean> list) {
        String imgPath = list.get(0).getImgPath();
        mOssService.putImagePath(mImageView.getId(), imgPath);
        mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, imgPath, mImageView);
    }


    /**
     * 长按删除掉之前选中的图片
     *
     * @param view
     * @return
     */
    @OnLongClick({R.id.id_card_behind_iv, R.id.id_card_front_iv})
    public boolean deleteChoosePictures(final View view) {
        DialogController.showConfirmDialog(mActivity, "删除图片", (View v) -> {
            ImageView vv = (ImageView) view;
            vv.setImageBitmap(null);
        });
        return true;
    }


    /**
     * 上传图片操作
     */
    private void doUploadPictures() {
        showLoadingDialog("正在处理...");
        mOssService.uploadImageAll(new AliOssService.UploadListener() {
            @Override
            public void handler(String message) {
                submitData();
            }
        });
    }

    public void submitData() {
        String idCard = id_card_num_et.getText().toString();
        String name = name_et.getText().toString();
        String idImg = mOssService.getOssUrl(R.id.id_card_front_iv) + "," + mOssService.getOssUrl(R.id.id_card_behind_iv);
        addDisposableIoMain(userService.humanAuthentication(idCard, name, idImg), new DefaultConsumer<VerifyUser>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<VerifyUser> result) {
                isOk = true;
                User user = mApplication.getUser();
                user.setIsAuthentication(2);
                mApplication.setUser(user);

                Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
                intent.putExtra("verKey", result.getData().getVerKey());
                startActivity(intent);
                finish();
            }
        }, (Throwable throwable) -> {
            isOk = true;
        });
    }


    @OnClick(R.id.auth_btn)
    public void handleClickSth(View view) {
        verifyData();
    }

    public void verifyData() {
        try {
//            CheckUtils.checkIDCard(id_card_num_et, "身份证号有误");
            CheckUtils.checkData(name_et, "姓名不能为空");
            CheckUtils.checkData(name_et, "姓名不能为空");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.id_card_front_iv), findViewById(R.id.plus_3), "请上传身份证正面");
            CheckUtils.checkData(mOssService.getLocalPath(R.id.id_card_behind_iv), findViewById(R.id.plus_4), "请上传身份证背面");
            doUploadPictures();
        } catch (MyNullException e) {
            showToast(e.getMessage());
            CheckUtils.shake(mActivity, e.getView());
        }
    }
}
