package com.party.jackclientandroid.ui_mine.my_wallet;

import android.os.Bundle;

import com.party.jackclientandroid.R;
import com.party.jackclientandroid.BaseActivityTitle;

public class CommonProblemActivity extends BaseActivityTitle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_problem);
        initData();
    }


    protected void initData() {
        setMiddleText("常见问题");
    }
}
