package com.party.jackclientandroid.ui_mine.my_wallet;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivity;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.controller.DialogController;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_mine.TradeListActivity;
import com.party.jackclientandroid.utils.Utils;
import com.party.jackclientandroid.view.dialog.MonitoringDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 我的钱包
 */
public class MyWalletActivity extends BaseActivity {
    @BindView(R.id.ll_container)
    ViewGroup ll_container;
    @BindView(R.id.tv_right)
    TextView tv_right;
    UserService userService;
    @BindView(R.id.wallet_question_ll)
    LinearLayout walletQuestionLl;
    @BindView(R.id.money_tv)
    TextView money_tv;
    @BindView(R.id.wait_money)
    TextView wait_money;
    @BindView(R.id.consumer_money)
    TextView consumer_money;
    @BindView(R.id.btn_mw_monitoring)
    Button btnMwMonitoring;

    private String money;
    private long unsealingTime;
    private String remarks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        ButterKnife.bind(this);
        setTitle("我的钱包");
        tv_right.setOnClickListener((View v) -> {
            startActivity(PaySettingActivity.class);
        });
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getNeedData();
    }

    protected void initData() {
        userService = new UserService(this);
        helper = new LoadViewHelper(ll_container);

        checkAuth();
    }

    public void getNeedData() {
        helper.showLoading();
        addDisposableIoMain(userService.getUserWallet(), new DefaultConsumer<CommonResult>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<CommonResult> baseBean) {
                money = Utils.clearZero(baseBean.getData().getMoney());
                String waitMoney = Utils.clearZero(baseBean.getData().getWaitMoney());
                String consumerMoney = Utils.clearZero(baseBean.getData().getXiapinCoin());
                unsealingTime = baseBean.getData().getUnsealingTime();
                remarks = baseBean.getData().getRemarks() + "";
                if (unsealingTime != 0) {
                    btnMwMonitoring.setVisibility(View.VISIBLE);
                }
                money_tv.setText(money);
                wait_money.setText(waitMoney);
                consumer_money.setText(consumerMoney);
                helper.showContent();
            }
        });
    }

    @OnClick({R.id.account_ll, R.id.wallet_cash_ll, R.id.wallet_detail_ll, R.id.iv_back, R.id.wallet_question_ll, R.id.btn_mw_monitoring})
    public void handleClickSth(View view) {
        if (dupCommit()) return;
        switch (view.getId()) {
            case R.id.wallet_cash_ll:
                if (checkAuth()) {
                    startActivity(CashActivity.class, "money", money);
                }
                break;
            case R.id.wallet_detail_ll:
                startActivity(TradeListActivity.class);
                break;
            case R.id.iv_back:
                finish();
                break;
            case R.id.account_ll:
                startActivity(MyAccountActivity.class);
                break;
            case R.id.wallet_question_ll:
                startActivity(FrequentlyAskedQuestionActivity.class);
                break;
            case R.id.btn_mw_monitoring:
                DialogController.showMonitoringDialog(this, remarks, unsealingTime);
                break;
        }
    }

    public boolean checkAuth() {
        if (!mApplication.getUser().isIDCardAuth() || !mApplication.getUser().isBandingAli()) {
            if (mApplication.getUser().getIsPassword() == 0) {
                DialogController.showConfirmDialog(mActivity, "密码设置", "请你设置初始密码", (View v) -> {
                    startActivity(SetPayPasswordActivity.class);
                });
                return false;
            } else {
                return true;
            }
        } else {
            DialogController.showAuthTip(mActivity, (View vv) -> {
                mActivity.startActivity(BindingAccountZfbActivity.class);
            });
            return false;
        }
    }


}
