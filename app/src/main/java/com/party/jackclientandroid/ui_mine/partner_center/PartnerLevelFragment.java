package com.party.jackclientandroid.ui_mine.partner_center;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.BaseFragment;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.VipService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.CommonResult;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.utils.ConstUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.zhy.adapter.recyclerview.CommonAdapter;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import io.reactivex.functions.Consumer;

/**
 * 合伙人列表
 */
public class PartnerLevelFragment extends BaseFragment {

    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refresh_layout;
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    private Integer page = 1;
    private VipService vipService;
    private List<CommonResult> dataList = new ArrayList<>();
    private int partnerLevel;
    private static final String PARTNER_LEVEL = "partnerLevel";
    private MyCommonAdapter<CommonResult> commonAdapter;

    public static BaseFragment newInstance(Integer level) {
        BaseFragment fragment = new PartnerLevelFragment();
        Bundle args = new Bundle();
        args.putInt(PARTNER_LEVEL, level);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_partner_level;
    }

    @Override
    protected void receiveBundleFromActivity(Bundle arg) {
        super.receiveBundleFromActivity(arg);
        partnerLevel = arg.getInt(PARTNER_LEVEL);
    }

    @Override
    protected void initData() {
        super.initData();
        vipService = new VipService((BaseActivityTitle) mActivity);
        helper = new LoadViewHelper(refresh_layout);
        helper.showLoading();
        loadData();
    }


    public void initView() {
        refresh_layout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new MyCommonAdapter<CommonResult>(mActivity, R.layout.item_partner_level, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, CommonResult item, int position) {
                viewHolder.setText(R.id.vip_text, item.getPhone());
                viewHolder.setText(R.id.vip_level, item.getRoleText());
            }
        };
        recycler_view.setLayoutManager(new LinearLayoutManager(mActivity));
        recycler_view.setAdapter(commonAdapter);
    }

    public void loadData() {
        page = 1;
        getData();
    }

    public void loadMoreData() {
        page++;
        getData();
    }

    public void getData() {
        addDisposableIoMain(vipService.getSubUserList(partnerLevel, page, Constant.PAGE_SIZE), new DefaultConsumer<List<CommonResult>>(mApplication) {

            @Override
            public void operateSuccess(BaseResult<List<CommonResult>> baseBean) {
                commonAdapter.clear(page);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(page, Constant.PAGE_SIZE, baseBean.getData().size(), refresh_layout);
                helper.check(commonAdapter.getDatas());
            }
        });
    }


}
