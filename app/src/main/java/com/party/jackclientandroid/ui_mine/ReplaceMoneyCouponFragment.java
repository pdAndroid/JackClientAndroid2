package com.party.jackclientandroid.ui_mine;


import android.support.v4.app.Fragment;

import com.party.jackclientandroid.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReplaceMoneyCouponFragment extends BaseFragment {
    @Override
    protected int getLayoutResId() {
        return 0;
    }
/*
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;

    private CouponService couponService;
    private CommonAdapter<ReplaceMoneyCoupon> commonAdapter;
    private List<ReplaceMoneyCoupon> dataList = new ArrayList<>();
    private int page = 1;
    private int pageSize = ConstUtils.PAGE_SIZE;

    public static ReplaceMoneyCouponFragment newInstance() {
        ReplaceMoneyCouponFragment fragment = new ReplaceMoneyCouponFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_replace_money_coupon;
    }

    @Override
    protected void initData() {
        helper = new LoadViewHelper(mRefreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                getNeedData();
            }
        });
        helper.showLoading();
        couponService = new CouponService((BaseActivity) mActivity);
        mRefreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new CommonAdapter<ReplaceMoneyCoupon>(mActivity, R.layout.item_my_coupon_package, dataList) {
            @Override
            protected void convert(ViewHolder viewHolder, ReplaceMoneyCoupon item, int position) {
                viewHolder.setText(R.id.original_price_tv, item.getOriginalPrice() + "元代金券");
                viewHolder.setText(R.id.buy_price_tv, "¥" + item.getBuyPrice());
                viewHolder.setText(R.id.desc_tv, CommonUtil.getAvailableDate(item.getAvailableDate()) + "|" + item.getGoodsRange());
                viewHolder.setText(R.id.shop_name_tv, item.getShopName());
            }
        };
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        mRecyclerView.setAdapter(commonAdapter);
    }

    @Override
    protected void initListener() {
        commonAdapter.setOnItemClickListener(new MultiItemTypeAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, RecyclerView.ViewHolder holder, int position) {
                handleItemClick(position);
            }

            @Override
            public boolean onItemLongClick(View view, RecyclerView.ViewHolder holder, int position) {
                return false;
            }
        });
        mRefreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getNeedData();
            }
        });
        getNeedData();
    }

    *//**
     * item点击事件
     *
     * @param position
     *//*
    private void handleItemClick(int position) {
//        Intent intent = new Intent(mActivity, OrderDetailRePayActivity.class);
//        intent.putExtra("order_id", replaceMoneyCouponList.get(position).getId());
//        startActivity(intent);
    }

    *//**
     * 下拉获取数据
     *//*
    public void getNeedData() {
        page = 1;
        addDisposableIoMain(couponService.getUserReplaceMoneyCouponList(page), new DefaultConsumer<List<ReplaceMoneyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                helper.showError("获取数据异常", "");
            }

            @Override
            public void operateSuccess(BaseResult<List<ReplaceMoneyCoupon>> baseBean) {
                dataList.clear();
                if (baseBean.getData() != null && baseBean.getData().size() > 0) {
                    dataList.addAll(baseBean.getData());
                }
                commonAdapter.notifyDataSetChanged();
                mRefreshLayout.finishRefresh();
                if (dataList.size() > 0) {
                    helper.showContent();
                } else {
                    helper.showEmpty("暂无代金券", "");
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                helper.showEmpty();
            }
        });
    }

    *//**
     * 加载更多
     *//*
    private void loadMoreData() {
        page++;
        addDisposableIoMain(couponService.getUserReplaceMoneyCouponList(page), new DefaultConsumer<List<ReplaceMoneyCoupon>>(mApplication) {
            @Override
            public void operateError(String message) {
                super.operateError(message);
                mRefreshLayout.finishLoadMore();
            }

            @Override
            public void operateSuccess(BaseResult<List<ReplaceMoneyCoupon>> baseBean) {
                List<ReplaceMoneyCoupon> data = baseBean.getData();
                boolean noMoreData = false;
                if (data != null && data.size() > 0) {
                    dataList.addAll(data);
                    if (data.size() <= ConstUtils.PAGE_SIZE) {
                        noMoreData = true;
                    }
                } else {
                    noMoreData = true;
                }
                commonAdapter.notifyDataSetChanged();
                // 没有更多数据了
                if (noMoreData) {
                    mRefreshLayout.finishLoadMoreWithNoMoreData();
                } else {
                    mRefreshLayout.finishLoadMore();
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                mRefreshLayout.finishLoadMore();
            }
        });
    }*/
}
