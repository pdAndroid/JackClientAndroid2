package com.party.jackclientandroid.ui_mine;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.adapter.MyCommonAdapter;
import com.party.jackclientandroid.api.CouponService;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.MyOfferRebatesBean;
import com.party.jackclientandroid.loadviewhelper.help.OnLoadViewListener;
import com.party.jackclientandroid.loadviewhelper.load.LoadViewHelper;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivity;
import com.party.jackclientandroid.ui_home.activity.DiscountPayBillActivityBackUp;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener;
import com.zhy.adapter.recyclerview.base.ViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by 南宫灬绝痕 on 2018/12/18.
 * 我的-优惠返利
 */

public class MyConsumerRebatesActivity extends BaseActivityTitle {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SmartRefreshLayout refreshLayout;

    private CouponService couponService;
    private List<MyOfferRebatesBean> data;
    private MyCommonAdapter<MyOfferRebatesBean> commonAdapter;
    private int page;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_offer_rebates);
        ButterKnife.bind(this);
        initView();
        requestData();
    }

    private void initView() {
        setMiddleText("返利金");
        page = 1;
        context = getApplicationContext();
        data = new ArrayList<>();
        couponService = new CouponService(this);
        refreshLayout.setEnableFooterFollowWhenLoadFinished(true);
        commonAdapter = new MyCommonAdapter<MyOfferRebatesBean>(mActivity, R.layout.item_my_offer_rebates, data) {
            @Override
            protected void convert(ViewHolder holder, MyOfferRebatesBean myOfferRebatesBean, int position) {
                mApplication.getImageLoaderFactory().loadCommonImgByUrl(mActivity, myOfferRebatesBean.getHomeImg(), (ImageView) holder.getView(R.id.iv_my_offer_rebates_homeimg));
                holder.setText(R.id.tv_my_offer_rebates_shopname, myOfferRebatesBean.getShopName());
                holder.setText(R.id.tv_my_offer_rebates_shopaddress, myOfferRebatesBean.getShopAddress());
                holder.setText(R.id.tv_my_offer_rebates_money, "￥" + myOfferRebatesBean.getMoney());
                holder.setOnClickListener(R.id.ll_my_offer_rebates, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, DetailsDiscountPaymentActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("shopId", data.get(position).getShopId());
                        context.startActivity(intent);
                    }
                });
                holder.setOnClickListener(R.id.btn_my_offer_rebates_use, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mActivity.startActivity(DiscountPayBillActivity.class, "shopId", data.get(position).getShopId());
                    }
                });
            }
        };


        refreshLayout.setOnRefreshLoadMoreListener(new OnRefreshLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                loadMoreData();
            }

            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                loadData();
            }
        });

        //添加自定义分割线
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.layout_divider_item_decoration));
        recyclerView.addItemDecoration(divider);
        recyclerView.setLayoutManager(new LinearLayoutManager(mActivity));
        recyclerView.setAdapter(commonAdapter);

        helper = new LoadViewHelper(refreshLayout);
        helper.setListener(new OnLoadViewListener() {
            @Override
            public void onRetryClick() {
                loadData();
            }
        });
        helper.showLoading();
    }

    private void requestData() {
        addDisposableIoMain(couponService.listUserRebateMoney(), new DefaultConsumer<List<MyOfferRebatesBean>>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<List<MyOfferRebatesBean>> baseBean) {
                commonAdapter.clear(page);
                commonAdapter.addData(baseBean.getData());
                commonAdapter.finishLoading(page, page, baseBean.getData().size(), refreshLayout);
                helper.check(commonAdapter.getDatas());
            }
        });
    }

    public void loadData() {
        page = 1;
        requestData();
    }

    public void loadMoreData() {
        page++;
        requestData();
    }
}
