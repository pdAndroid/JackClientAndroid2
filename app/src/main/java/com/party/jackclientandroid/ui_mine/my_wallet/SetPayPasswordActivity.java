package com.party.jackclientandroid.ui_mine.my_wallet;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.party.jackclientandroid.BaseActivityTitle;
import com.party.jackclientandroid.R;
import com.party.jackclientandroid.api.DefaultConsumer;
import com.party.jackclientandroid.api.UserService;
import com.party.jackclientandroid.bean.BaseResult;
import com.party.jackclientandroid.bean.User;
import com.party.jackclientandroid.controller.DialogController;

import butterknife.OnClick;

public class SetPayPasswordActivity extends BaseActivityTitle {

    TextView[] tvList;
    int currentIndex = -1;
    private String verKey;
    private String firstPass;
    private UserService userService;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_paypassword);
        receivePassDataIfNeed(getIntent());
        initData();
        initListener();
    }

    protected void receivePassDataIfNeed(Intent intent) {
        verKey = intent.getStringExtra("verKey");
        firstPass = intent.getStringExtra("firstPass");
    }

    protected void initData() {
        userService = new UserService(this);
        setMiddleText("设置支付密码");

        tvList = new TextView[6];
        tvList[0] = findViewById(R.id.pay_box1);
        tvList[1] = findViewById(R.id.pay_box2);
        tvList[2] = findViewById(R.id.pay_box3);
        tvList[3] = findViewById(R.id.pay_box4);
        tvList[4] = findViewById(R.id.pay_box5);
        tvList[5] = findViewById(R.id.pay_box6);
    }

    protected void initListener() {
        tvList[5].addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 1) {
                    String strPassword = "";
                    for (int i = 0; i < 6; i++) {
                        strPassword += tvList[i].getText().toString().trim();
                    }
                    inputComplete(strPassword);
                }
            }
        });
    }

    @OnClick({R.id.pay_keyboard_one, R.id.pay_keyboard_two, R.id.pay_keyboard_three
            , R.id.pay_keyboard_four, R.id.pay_keyboard_five, R.id.pay_keyboard_sex
            , R.id.pay_keyboard_seven, R.id.pay_keyboard_eight, R.id.pay_keyboard_nine
            , R.id.pay_keyboard_del, R.id.pay_keyboard_zero})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.pay_keyboard_one:
                getPass("1");
                break;
            case R.id.pay_keyboard_two:
                getPass("2");
                break;
            case R.id.pay_keyboard_three:
                getPass("3");
                break;
            case R.id.pay_keyboard_four:
                getPass("4");
                break;
            case R.id.pay_keyboard_five:
                getPass("5");
                break;
            case R.id.pay_keyboard_sex:
                getPass("6");
                break;
            case R.id.pay_keyboard_seven:
                getPass("7");
                break;
            case R.id.pay_keyboard_eight:
                getPass("8");
                break;
            case R.id.pay_keyboard_nine:
                getPass("9");
                break;
            case R.id.pay_keyboard_zero:
                getPass("0");
                break;
            case R.id.pay_keyboard_del:
                if (currentIndex - 1 >= -1) {
                    tvList[currentIndex--].setText("");
                }
                break;
        }
    }

    public void getPass(String str) {
        if (currentIndex >= -1 && currentIndex < 5) {
            tvList[++currentIndex].setText(str);
        }
    }


    /**
     * 输入密码完成
     *
     * @param password
     */
    public void inputComplete(String password) {
        //如果 第二次输入密码为空 说明是第一次输入密码。
        if (firstPass == null) {
            Intent intent = new Intent(mActivity, SetPayPasswordActivity.class);
            intent.putExtra("firstPass", password);
            intent.putExtra("verKey", verKey);
            startActivity(intent);
            finish();

            //说明是 第二次输入密码
        } else if (password.equals(firstPass)) {
            if (mApplication.getUser().getIsPassword() == 1) {
                showLoadingDialog("支付密码修改中...");
                updatePassword(password);
            } else {
                showLoadingDialog("支付密码设置中...");
                initPayPassword(password);
            }
        } else {
            showToast("2次输入密码不一致");
            currentIndex = -1;
            for (int i = 0; i < tvList.length; i++) {
                tvList[i].setText("");
            }
        }
    }

    public void initPayPassword(String password) {
        addDisposableIoMain(userService.initPayPassword(password), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                User user = mApplication.getUser();
                user.setIsPassword(1);
                mApplication.setUser(user);
                DialogController.showMustConfirmDialog(mActivity, "温馨提示", "恭喜你密码设置成功", (View v) -> {
                    finish();
                });
            }
        });
    }

    public void updatePassword(String password) {
        addDisposableIoMain(userService.alterPayPassword(password, verKey), new DefaultConsumer<String>(mApplication) {
            @Override
            public void operateSuccess(BaseResult<String> baseBean) {
                User user = mApplication.getUser();
                user.setIsPassword(1);
                mApplication.setUser(user);
                DialogController.showMustConfirmDialog(mActivity, "温馨提示", "恭喜你密码修改成功", (View v) -> {
                    finish();
                });
            }
        });
    }


}
