package com.party.jackclientandroid;

/**
 * <a href="https://blog.csdn.net/jdsjlzx/article/details/51578231">Android Retrofit2&OkHttp3添加统一的请求头Header</a><br>
 * <a href="https://www.cnblogs.com/galibujianbusana/p/7891807.html">Retrofit进行post提交json数据</a><br>
 */
public class Constant {
    public static String BASE_URL = "https://www.paiduikeji.com/jack_shop/";
    //public static String BASE_URL = "http://47.99.140.130/jack_shop/";
    //public static String BASE_URL = "http://test.paiduikeji.com:80/jack_shop/";

    public static final boolean ECRYPT = false;

    public static final String UID_KEY = "uid";
    public static final String MID_KEY = "mid";
    public static final String CONTENT_TYPE_KEY = "Content-Type";
    public static final String CONTENT_TYPE = "application/json; charset=utf-8";
    public static final String ACCEPT_KEY = "Accept";
    public static final String ACCEPT = "application/json";

    public static final int SPELLING = 1, SPELLING_OK = 2;

    /**
     * 图片上传地址
     */
    public static final String IMAGE_UPLOAD_URL = BASE_URL + "ImageUpload/upload";
    /**
     * 签名图片上传地址
     */
    public static final String SIGN_UPLOAD_URL = BASE_URL + "ImageUpload/uploadSign";

    /**
     * 每页请求多少数据
     */
    public static final Integer PAGE_SIZE = 30;
    public static final Integer PAGE_SIZE_MAX = 100000;
    public static final Integer DELICIOUS_FOOD_ID = 2;
    public static final Integer ENTERTAINMENT_ID = 1;
    public static final Integer HOTEL_ID = 4;
    public static final Integer HAIRDRESSING_ID = 3;
    public static final Integer SWIM_ID = 5;


    //订单状态（0、全部 1、待付款2、待使用3、待评价4、已完成5、已取消6、退款中 7,退款完成 8拼团中 9拼团成功 10 拼团失败）
    public final static int ORDER_STATE_ALL = 0;//全部
    public final static int ORDER_STATE_WAIT_PAY = 1;//待付款
    public final static int ORDER_STATE_WAIT_USE = 2;//待使用
    public final static int ORDER_STATE_WAIT_COMMENT = 3;//待评价
    public final static int ORDER_STATE_FINISHED = 4;//已完成
    public final static int ORDER_STATE_CANCELED = 5;//已取消
    public final static int ORDER_STATE_REFUNDING = 6;//退款中
    public final static int ORDER_STATE_REFUNDED = 7;//退款完成
    public final static int ORDER_STATE_SPELLING_GROUP_ING = 8;//拼团中
    public final static int ORDER_STATE_SPELLING_GROUP_SUCCESS = 9;//拼团成功
    public final static int ORDER_STATE_SPELLING_GROUP_FAILED = 10;//拼团失败

    //提交订单需要传的商品类型（1代金卷 2为套餐 3为菜品 4提升会员 5拼团 6秒杀 7其它）
    public final static int GOODS_TYPE_REPLACE_MONEY = 1;
    public final static int GOODS_TYPE_GROUP_BUY = 2;
    public final static int GOODS_TYPE_GOODS = 3;
    public final static int GOODS_TYPE_VIP = 4;
    public final static int GOODS_TYPE_SPELLING_GROUP = 5;
    public final static int GOODS_TYPE_SECOND_KILL = 6;
    public final static int GOODS_TYPE_OTHER = 7;

    public final static int FROM_SHOP_DETAIL = 1;
    public final static int FROM_MY_COUPON_PACKAGE = 2;
    public final static int FROM_ORDER_DETAIL = 3;

}
