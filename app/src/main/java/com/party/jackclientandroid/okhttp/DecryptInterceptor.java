package com.party.jackclientandroid.okhttp;

import com.party.jackclientandroid.Constant;
import com.party.jackclientandroid.utils.AesCBC;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * <a href="https://blog.csdn.net/knight1996/article/details/77995547">OKHTTP3的简单使用</a><br>
 * 解密拦截器<br>
 */
public class DecryptInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(chain.request());
        try {
            String originalStr = response.body().string();
            // 解密
            String decryptData = AesCBC.decrypt(originalStr);
            // contentType 是服务器返回的数据类型MediaType
            MediaType contentType = MediaType.parse(Constant.CONTENT_TYPE);
            ResponseBody body = ResponseBody.create(contentType, decryptData);
            return response.newBuilder().body(body).build();
        } catch (Exception e) {
        }
        return response;
    }
}
