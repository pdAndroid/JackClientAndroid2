package com.party.jackclientandroid.okhttp;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 头部拦截器<br>
 */
public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder builder = chain.request()
                .newBuilder();
//                .addHeader(Constant.CONTENT_TYPE_KEY, Constant.CONTENT_TYPE)
//                .addHeader(Constant.ACCEPT_KEY, Constant.ACCEPT);
//        if (!TextUtils.isEmpty(ConstUtils.UID)) {
//            builder.addHeader(Constant.UID_KEY, ConstUtils.UID);
//        }
//        if (!TextUtils.isEmpty(ConstUtils.MID)) {
//            builder.addHeader(Constant.MID_KEY, ConstUtils.MID);
//        }
        return chain.proceed(builder.build());
    }
}
