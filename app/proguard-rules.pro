# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}

# for DexGuard only
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

# permission
-dontwarn com.yanzhenjie.permission.**

# retrofit2
-keepattributes Signature
# Retain service method parameters.
-keepclassmembers,allowshrinking,allowobfuscation interface * {
    @retrofit2.http.* <methods>;
}
# Ignore annotation used for build tooling.
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
#抛出异常时保留代码行号
-keepattributes SourceFile,LineNumberTable

# eventbus used start
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
# eventbus used end

# umeng used start
-keepclassmembers class * {
   public <init> (org.json.JSONObject);
}
-keep public class [com.party.jackclientandroid].R$*{
public static final int *;
}
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
# umeng used end

# Gson used start
-keepattributes Signature
-keep class sun.misc.Unsafe { *; }
# Gson used end

# retrofit2 start
# Platform calls Class.forName on types which do not exist on Android to determine platform.
-dontnote retrofit2.Platform
# Platform used when running on RoboVM on iOS. Will not be used at runtime.
-dontnote retrofit2.Platform$IOS$MainThreadExecutor
# Platform used when running on Java 8 VMs. Will not be used at runtime.
-dontwarn retrofit2.Platform$Java8
# Retain generic type information for use by reflection by converters and adapters.
-keepattributes Signature
# Retain declared checked exceptions for use by a Proxy instance.
-keepattributes Exceptions
# retrofit2 end

# RxJava RxAndroid start
-dontwarn sun.misc.**
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
    long producerIndex;
    long consumerIndex;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}
-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}
# RxJava RxAndroid end

# ButterKnife start
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}
# ButterKnife end

#greenDAO start
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**
#greenDAO end

#okhttp
-dontwarn okhttp3.**
-keep class okhttp3.**{*;}

#okio
-dontwarn okio.**
-keep class okio.**{*;}

# bugly
-dontwarn com.tencent.bugly.**
-keep public class com.tencent.bugly.**{*;}

# 3D 地图 V5.0.0之后：
-keep class com.amap.api.maps.**{*;}
-keep class com.autonavi.**{*;}
-keep class com.amap.api.trace.**{*;}
# 定位  http://lbs.amap.com/api/android-location-sdk/guide/create-project/dev-attention
-keep class com.amap.api.location.**{*;}
-keep class com.amap.api.fence.**{*;}
-keep class com.autonavi.aps.amapapi.model.**{*;}
# 搜索
-keep class com.amap.api.services.**{*;}
# 2D地图
-keep class com.amap.api.maps2d.**{*;}
-keep class com.amap.api.mapcore2d.**{*;}
# 导航
-keep class com.amap.api.navi.**{*;}
-keep class com.autonavi.**{*;}

# banner
-keep class com.youth.banner.** {
    *;
 }

 # 暂时保留 bean 文件夹
 -keep class com.party.jackclientandroid.bean.** {
     *;
  }

  # citypicker
  -keep class com.lljjcoder.**{
     *;
  }

  # tencent
  -keep class com.tencent.mm.sdk.modelmsg.WXMediaMessage {*;}
  -keep class com.tencent.mm.sdk.modelmsg.** implements com.tencent.mm.sdk.modelmsg.WXMediaMessage$IMediaObject {*;}
  -keep class com.tencent.mm.sdk.** {
     *;
  }
  -keep class com.tencent.mm.opensdk.** {
     *;
  }
  -keep class com.tencent.wxop.** {
     *;
  }
  -keep class com.tencent.mm.sdk.** {
     *;
  }
  -keep class com.tencent.** {*;}
  -dontwarn com.tencent.**
  -keep class com.tencent.open.TDialog$*
  -keep class com.tencent.open.TDialog$* {*;}
  -keep class com.tencent.open.PKDialog
  -keep class com.tencent.open.PKDialog {*;}
  -keep class com.tencent.open.PKDialog$*
  -keep class com.tencent.open.PKDialog$* {*;}
  -keep class com.umeng.socialize.impl.ImageImpl {*;}
  -keep class com.sina.** {*;}
  -dontwarn com.sina.**
  -keepnames class * implements android.os.Parcelable {
      public static final ** CREATOR;
  }

  # Alipay
#  -dontshrink
  -dontpreverify
#  -dontoptimize
#  -dontusemixedcaseclassnames

  -flattenpackagehierarchy
  -allowaccessmodification
#  -printmapping map.txt

  -optimizationpasses 7
  -verbose
#  -keepattributes Exceptions,InnerClasses
  -dontskipnonpubliclibraryclasses
  -dontskipnonpubliclibraryclassmembers
  -ignorewarnings
  -keep class com.alipay.android.app.IAlixPay{*;}
  -keep class com.alipay.android.app.IAlixPay$Stub{*;}
  -keep class com.alipay.android.app.IRemoteServiceCallback{*;}
  -keep class com.alipay.android.app.IRemoteServiceCallback$Stub{*;}
  -keep class com.alipay.sdk.app.PayTask{ public *;}
  -keep class com.alipay.sdk.app.AuthTask{ public *;}
  -keep class com.alipay.sdk.app.H5PayCallback {
      <fields>;
      <methods>;
  }
  -keep class com.alipay.android.phone.mrpc.core.** { *; }
  -keep class com.alipay.apmobilesecuritysdk.** { *; }
  -keep class com.alipay.mobile.framework.service.annotation.** { *; }
  -keep class com.alipay.mobilesecuritysdk.face.** { *; }
  -keep class com.alipay.tscenter.biz.rpc.** { *; }
  -keep class org.json.alipay.** { *; }
  -keep class com.alipay.tscenter.** { *; }
  -keep class com.ta.utdid2.** { *;}
  -keep class com.ut.device.** { *;}

  # alphatabs
  -keep class com.yinglan.alphatabs.**{
     *;
  }

# 混淆相关资料
# https://www.jianshu.com/p/b471db6a01af
# https://blog.csdn.net/shensky711/article/details/52770993
# https://blog.csdn.net/weixue9/article/details/72472571